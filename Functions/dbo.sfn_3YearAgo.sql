SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[sfn_3YearAgo] 
(
	@EndDate smalldatetime
)
RETURNS smalldatetime
AS
BEGIN

	SET @EndDate = DATEADD(day, 1, DATEADD(year, -3, @EndDate))

	RETURN @EndDate

END
GO

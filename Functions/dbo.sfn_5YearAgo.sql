SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[sfn_5YearAgo] 
(
	@EndDate smalldatetime
)
RETURNS smalldatetime
AS
BEGIN

	SET @EndDate = DATEADD(day, 1, DATEADD(year, -5, @EndDate))

	RETURN @EndDate

END
GO

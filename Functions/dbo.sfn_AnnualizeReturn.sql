SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE FUNCTION [dbo].[sfn_AnnualizeReturn]
(
	@TWR numeric(18,6),
	@BeginDate smalldatetime,
	@EndDate smalldatetime
)
RETURNS numeric(18,6)
AS
BEGIN

	DECLARE @NumOfDays NUMERIC(18,6)
	DECLARE @DaysInYear NUMERIC(18,6) = 365

	SELECT @NumOfDays = ABS(DATEDIFF(d, @BeginDate, @EndDate))

	IF @NumOfDays > @DaysInYear AND @TWR <> 0
	BEGIN
		SELECT @TWR = ((POWER((1 + ABS(@TWR)),(1/(@NumOfDays/@DaysInYear)))) - 1) * (CASE WHEN @TWR < 0 THEN -1 ELSE 1 END)
	END

	-- Return the result of the function
	RETURN @TWR

END

GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[sfn_LevelName]
(
	@Level char(1),
	@LevelID bigint
)
RETURNS varchar(100)
AS
BEGIN

	DECLARE @LevelName varchar(100)

	IF @Level = 'F'
	BEGIN
		SELECT @LevelName = Family.FamilyName
		FROM Family
		WHERE FamilyID = @LevelID
	END
	ELSE IF @Level = 'E'
	BEGIN
		SELECT @LevelName = Entity.EntityName
		FROM Entity
		WHERE EntityID = @LevelID
	END
	ELSE IF @Level = 'A'
	BEGIN
		SELECT @LevelName = Account.AccountName
		FROM Account
		WHERE AccountID = @LevelID
	END
	
	-- Return the result of the function
	RETURN @LevelName

END

GO

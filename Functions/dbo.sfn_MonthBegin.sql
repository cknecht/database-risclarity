SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[sfn_MonthBegin] 
(
	@EndDate smalldatetime
)
RETURNS smalldatetime
AS
BEGIN
	DECLARE @Month int
	DECLARE @Year int

	SET @Month = MONTH(@EndDate)
	SET @Year = YEAR(@EndDate)

	SET @EndDate = CONVERT(smalldatetime, CONVERT(varchar(2), @Month) + '/01/' + CONVERT(varchar(4), @Year))

	RETURN @EndDate

END
GO

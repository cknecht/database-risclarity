SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[sfn_MonthEnd] 
(
	@EndDate smalldatetime
)
RETURNS smalldatetime
AS
BEGIN

	SET @EndDate = DATEADD(day, -1, DATEADD(Month, 1, [dbo].[sfn_MonthBegin](@EndDate)))
	
	RETURN @EndDate

END

GO

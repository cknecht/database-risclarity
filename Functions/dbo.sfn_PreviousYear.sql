SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[sfn_PreviousYear] 
(
	@EndDate SMALLDATETIME,
	@NumYearsBack SMALLINT
)
RETURNS smalldatetime
AS
BEGIN

	SET @EndDate = DATEADD(day, 1, DATEADD(year, ABS(@NumYearsBack) * -1, @EndDate))

	RETURN @EndDate

END

GO

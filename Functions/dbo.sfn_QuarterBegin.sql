SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[sfn_QuarterBegin] 
(
	@EndDate smalldatetime
)
RETURNS smalldatetime
AS
BEGIN
	DECLARE @Month int
	DECLARE @Year int

	SET @Month = MONTH(@EndDate)

	SELECT @Month = CASE @Month
					WHEN 2 THEN 1
					WHEN 3 THEN 1
					WHEN 5 THEN 4
					WHEN 6 THEN 4
					WHEN 8 THEN 7
					WHEN 9 THEN 7
					WHEN 11 THEN 10
					WHEN 12 THEN 10
					ELSE @Month
					END 
 

	SET @Year = YEAR(@EndDate)

	SET @EndDate = CONVERT(smalldatetime, CONVERT(varchar(2), @Month) + '/01/' + CONVERT(varchar(4), @Year))

	RETURN @EndDate

END
GO

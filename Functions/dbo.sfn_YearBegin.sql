SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[sfn_YearBegin] 
(
	@EndDate smalldatetime
)
RETURNS smalldatetime
AS
BEGIN
	DECLARE @Month int
	DECLARE @Year int

	SELECT @Month = 1
 
	SET @Year = YEAR(@EndDate)

	SET @EndDate = CONVERT(smalldatetime, CONVERT(varchar(2), @Month) + '/01/' + CONVERT(varchar(4), @Year), 101)

	RETURN @EndDate

END
GO

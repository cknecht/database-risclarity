SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[tfn_AccountList_by_Level] (@Level varchar(10), @LevelID BIGINT, @AsOfDate smalldatetime, @IncludeLinkedAccounts BIT)
RETURNS @AccountList TABLE
	(
	AccountID	bigint,
	AccountName varchar(max),
	EntityID bigint,
	FamilyID bigint,
	AccountNumber VARCHAR(4000),
	AccountDescription VARCHAR(MAX),
	PercentOwnership NUMERIC(18,6),
	ExcludeFromPerformance BIT,
	InceptionDate SMALLDATETIME,
	TaxLotReliefMethodID tinyint
	)
AS
BEGIN 

	IF @Level = 'F'
	BEGIN
		INSERT INTO @AccountList
			(
			AccountID,
			AccountName,
			EntityID,
			FamilyID,
			AccountNumber,
			AccountDescription,
			PercentOwnership,
			ExcludeFromPerformance,
			InceptionDate,
			TaxLotReliefMethodID
			)
		SELECT Account.AccountID,
			Account.AccountName,
			Entity.EntityID,
			FamilyEntity.FamilyID,
			Account.AccountNumber,
			Account.AccountDescription,
			100 AS PercentOwnership,
			Account.ExcludeFromPerformance,
			Account.InceptionDate,
			Account.TaxLotReliefMethodID
		FROM Account
			JOIN EntityAccount ON Account.AccountID = EntityAccount.AccountID
			JOIN Entity ON EntityAccount.EntityID = Entity.EntityID
			JOIN FamilyEntity ON EntityAccount.EntityID = FamilyEntity.EntityID
		WHERE FamilyEntity.FamilyID = @LevelID
			AND Entity.Inactive = 0
			AND Account.Inactive = 0
	END
	ELSE IF @Level = 'P'
	BEGIN
		INSERT INTO @AccountList
			(
			AccountID,
			AccountName,
			EntityID,
			FamilyID,
			AccountNumber,
			AccountDescription,
			PercentOwnership,
			ExcludeFromPerformance,
			InceptionDate,
			TaxLotReliefMethodID
			)
		SELECT Account.AccountID,
			Account.AccountName,
			Entity.EntityID,
			'' AS FamilyID, --FamilyEntity.FamilyID,
			Account.AccountNumber,
			Account.AccountDescription,
			PortfolioEntity.PercentOwnership AS PercentOwnership,
			Account.ExcludeFromPerformance,
			Account.InceptionDate,
			Account.TaxLotReliefMethodID
		FROM Account
			JOIN EntityAccount ON Account.AccountID = EntityAccount.AccountID
			JOIN Entity ON EntityAccount.EntityID = Entity.EntityID
			JOIN [dbo].[tfn_PortfolioEntity](@AsOfDate) AS PortfolioEntity ON EntityAccount.EntityID = PortfolioEntity.EntityID
		WHERE PortfolioEntity.PortfolioID = @LevelID
			AND Entity.Inactive = 0
			AND Account.Inactive = 0
		UNION
		SELECT Account.AccountID,
			Account.AccountName,
			EntityAccount.EntityID,
			FamilyEntity.FamilyID,
			Account.AccountNumber,
			Account.AccountDescription,
			SUM(EntityOwner.PercentOwnership) AS PercentOwnership,
			Account.ExcludeFromPerformance,
			Account.InceptionDate,
			Account.TaxLotReliefMethodID
		FROM [dbo].[tfn_EntityOwnership] (@AsOfDate) AS EntityOwner
			JOIN EntityAccount ON EntityAccount.EntityID = EntityOwner.EntitySubID
			JOIN Account ON Account.AccountID = EntityAccount.AccountID
			JOIN FamilyEntity ON EntityAccount.EntityID = FamilyEntity.EntityID
			JOIN [dbo].[tfn_PortfolioEntity](@AsOfDate) AS PortfolioEntity ON EntityOwner.EntityOwnerID = PortfolioEntity.EntityID
		WHERE PortfolioEntity.PortfolioID = @LevelID
			AND Account.Inactive = 0
			AND @IncludeLinkedAccounts = 1
		GROUP BY Account.AccountID,
			Account.AccountName,
			EntityAccount.EntityID,
			FamilyEntity.FamilyID,
			Account.AccountNumber,
			Account.AccountDescription,
			Account.ExcludeFromPerformance,
			Account.InceptionDate,
			Account.TaxLotReliefMethodID
	END
	ELSE IF @Level = 'E'
	BEGIN
		INSERT INTO @AccountList
			(
			AccountID,
			AccountName,
			EntityID,
			FamilyID,
			AccountNumber,
			AccountDescription,
			PercentOwnership,
			ExcludeFromPerformance,
			InceptionDate,
			TaxLotReliefMethodID
			)
		SELECT Account.AccountID,
			Account.AccountName,
			EntityAccount.EntityID,
			FamilyEntity.FamilyID,
			Account.AccountNumber,
			Account.AccountDescription,
			100 AS PercentOwnership,
			Account.ExcludeFromPerformance,
			Account.InceptionDate,
			Account.TaxLotReliefMethodID
		FROM Account
			JOIN EntityAccount ON Account.AccountID = EntityAccount.AccountID
			JOIN FamilyEntity ON EntityAccount.EntityID = FamilyEntity.EntityID
		WHERE EntityAccount.EntityID = @LevelID
			AND Account.Inactive = 0
		UNION
		SELECT Account.AccountID,
			Account.AccountName,
			EntityAccount.EntityID,
			FamilyEntity.FamilyID,
			Account.AccountNumber,
			Account.AccountDescription,
			Entity.PercentOwnership AS PercentOwnership,
			Account.ExcludeFromPerformance,
			Account.InceptionDate,
			Account.TaxLotReliefMethodID
		FROM dbo.tfn_EntityEntityOwnership (@LevelID, @AsOfDate) AS Entity
			JOIN EntityAccount ON EntityAccount.EntityID = Entity.EntitySubID
			JOIN Account ON Account.AccountID = EntityAccount.AccountID
			JOIN FamilyEntity ON EntityAccount.EntityID = FamilyEntity.EntityID
		WHERE Account.Inactive = 0
			AND @IncludeLinkedAccounts = 1
	END
	ELSE IF @Level = 'A'
	BEGIN
		INSERT INTO @AccountList
			(
			AccountID,
			AccountName,
			EntityID,
			FamilyID,
			AccountNumber,
			AccountDescription,
			PercentOwnership,
			ExcludeFromPerformance,
			InceptionDate,
			TaxLotReliefMethodID
			)
		SELECT Account.AccountID,
			Account.AccountName,
			EntityAccount.EntityID,
			FamilyEntity.FamilyID,
			Account.AccountNumber,
			Account.AccountDescription,
			100 AS PercentOwnership,
			Account.ExcludeFromPerformance,
			Account.InceptionDate,
			Account.TaxLotReliefMethodID
		FROM Account
			JOIN EntityAccount ON Account.AccountID = EntityAccount.AccountID
			JOIN FamilyEntity ON EntityAccount.EntityID = FamilyEntity.EntityID
		WHERE Account.AccountID = @LevelID
			AND Account.Inactive = 0
	END

	RETURN;
END;


GO

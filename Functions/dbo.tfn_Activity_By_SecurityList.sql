SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[tfn_Activity_By_SecurityList] (
	@SecurityList SecurityList Readonly
	)

RETURNS @Activity TABLE (
	AsOfDate smalldatetime,
	CashFlow numeric(18,6),
	Addition numeric(18,6),
	Withdrawal numeric(18,6),
	Fee numeric(18,6),
	Interest numeric(18,6),
	Dividend numeric(18,6),
	Income numeric(18,6),
	TradeDate smalldatetime,
	TransCodeName varchar(100),
	EntityID bigint,
	AccountID bigint,
	AccountSecurityID bigint,
	SecurityID bigint
	)

AS
BEGIN
	INSERT INTO @Activity (
		AccountSecurityID,
		AsOfDate,
		Income,
		CashFlow,
		Addition,
		Withdrawal,
		Interest,
		Dividend,
		Fee
		)
	SELECT Trans.AccountSecurityID,
		Trans.TradeDate,
		SUM(Trans.Amount_Base * Trans.CalcIncome) as Income,
		SUM(Trans.Amount_Base * Trans.CalcCashFlow) as CashFlow,
		SUM(Trans.Amount_Base * IIF(Trans.CalcIncome = 0, IIF(Trans.CalcCashFlow < 0, 1, 0), 0)) as Additions,
		SUM(trans.Amount_Base * IIF(Trans.CalcIncome = 0, IIF(Trans.CalcCashFlow > 0, 1, 0), 0)) as Withdrawals,
		SUM(Trans.Amount_Base * Trans.CalcInterest) as Interest,
		SUM(Trans.Amount_Base * Trans.CalcDividend) as Dividend,
		SUM(Trans.Amount_Base * Trans.CalcFee) as Fee
	FROM dbo.[tfn_Trans_By_SecurityList] (
				@SecurityList
				) as Trans
	GROUP BY Trans.AccountSecurityID,
		Trans.TradeDate


	RETURN;
END;


GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE FUNCTION [dbo].[tfn_AssetClass_By_Security] 
(	
	@AsOfDate datetime
)
RETURNS TABLE 
AS
RETURN 
(
	SELECT Security.SecurityID,
		Security.SecurityName,
		AssetClass.AssetClassID,
		AssetClass.AssetClassName,
		AssetClass.SortOrder as AssetClassSortOrder,
		AssetClass.AssetClassColor,
		Security.SourceName as SecuritySourceName,
		Security.DataSourceID as SecurityDataSourceID,
		AssetClass.SourceName as AssetClassSourceName,
		SecurityAssetClass.AsOfDate as AsOfDate
	FROM SecurityAssetClass
		 JOIN (SELECT SecurityAssetClass.SecurityID, 
			SecurityAssetClass.AssetClassID, 
			SecurityAssetClass.AsOfDate
				FROM SecurityAssetClass 
					JOIN (SELECT SecurityAssetClass.SecurityID, 
						MAX(AsOfDate) as MaxDate
					FROM SecurityAssetClass
					WHERE SecurityAssetClass.AsOfDate <= @AsOfDate
					GROUP BY SecurityAssetClass.SecurityID) as MaxAsOfDate ON SecurityAssetClass.SecurityID = MaxAsOfDate.SecurityID
						AND SecurityAssetClass.AsOfDate = MaxAsOfDate.MaxDate
					) AS AsOfDate ON SecurityAssetClass.SecurityID = AsOfDate.SecurityID 
						AND SecurityAssetClass.AssetClassID = AsOfDate.AssetClassID
		JOIN AssetClass ON SecurityAssetClass.AssetClassID = AssetClass.AssetClassID
		JOIN Security ON SecurityAssetClass.SecurityID = Security.SecurityID
	WHERE SecurityAssetClass.AsOfDate = AsOfDate.AsOfDate
	UNION
	SELECT Security.SecurityID,
		Security.SecurityName,
		0 as AssetClassID,
		'Unknown' as AssetClassName,
		0 as AssetClassSortOrder,
		'Black' as AssetClassColor,
		Security.SourceName as SecuritySourceName,
		Security.DataSourceID as SecurityDataSourceID,
		'Unknown' as AssetClassSourceName,
		NULL as AsOfDate
	FROM Security 
	LEFT JOIN SecurityAssetClass ON SecurityAssetClass.SecurityID = Security.SecurityID
	WHERE ISNULL(SecurityAssetClass.AssetClassID, -1) = -1
)

GO

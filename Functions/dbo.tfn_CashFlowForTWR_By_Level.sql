SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[tfn_CashFlowForTWR_By_Level] (@SecurityList SecurityList Readonly,
	@BeginDate smalldatetime,
	@EndDate smalldatetime)

RETURNS @CashFlows TABLE (
	CashFlows numeric(18,6),
	TradeDate smalldatetime,
	TransCodeName varchar(100),
	EntityID bigint,
	AccountID bigint,
	AccountSecurityID bigint,
	SecurityID bigint
	)

AS
BEGIN
	INSERT INTO @CashFlows (
		CashFlows,
		TradeDate,
		TransCodeName,
		EntityID,
		AccountID,
		AccountSecurityID,
		SecurityID
		)
	SELECT (ABS(Trans.Amount_Base) * Trans.CalcCashFlow) as CashFlows, 
		Trans.TradeDate, 
		Trans.TransCodeName, 
		Trans.EntityID, 
		Trans.AccountID, 
		Trans.AccountSecurityID, 
		Trans.SecurityID
	FROM dbo.[tfn_Transactions_By_Level] (
				@SecurityList,
				@BeginDate,
				@EndDate
				) as Trans
	WHERE Trans.CalcCashFlow <> 0
		AND Trans.TradeDate >= @BeginDate
		AND Trans.TradeDate <= @EndDate
	ORDER BY TradeDate


	RETURN;
END;


GO

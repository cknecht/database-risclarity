SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[tfn_CashFlow_By_SecurityList_By_DateList] (
	@SecurityList SecurityList Readonly,
	@DateList DateList Readonly
	)

RETURNS @CashFlows TABLE (
	AsOfDate smalldatetime,
	CashFlows numeric(18,6),
	TradeDate smalldatetime,
	TransCodeName varchar(100),
	EntityID bigint,
	AccountID bigint,
	AccountSecurityID bigint,
	SecurityID bigint
	)

AS
BEGIN
	INSERT INTO @CashFlows (
		AsOfDate,
		CashFlows
		)
	SELECT DateList.AsOfDate,
		SUM(Trans.Amount_Base * Trans.CalcCashFlow) as CashFlows
	FROM dbo.[tfn_Transactions_By_Level_By_DateList] (
				@SecurityList,
				@DateList
				) as Trans
		LEFT JOIN @DateList as DateList ON Trans.TradeDate = DateList.AsOfDate
	WHERE Trans.CalcCashFlow <> 0
	GROUP BY DateList.AsOfDate


	RETURN;
END;


GO

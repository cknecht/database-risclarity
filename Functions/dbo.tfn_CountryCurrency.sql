SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[tfn_CountryCurrency] (@Date SmallDateTime)
RETURNS @CountryCurrency TABLE (
   CountryID	smallint      NOT NULL,
   CurrencyID	smallint     NOT NULL,
   CountryName varchar(255),
   CountryCode2 varchar(2),
   CountryCode3 varchar(3),
   CurrencyName varchar(255),
   CurrencyCode varchar(3),
   EffectiveDate smalldatetime,
   ExpirationDate smalldatetime
) 
AS
BEGIN
	INSERT INTO @CountryCurrency (
		CountryID,
		CurrencyID,
		CountryName ,
		CountryCode2,
		CountryCode3,
		CurrencyName,
		CurrencyCode,
		EffectiveDate,
		ExpirationDate
	   )
	SELECT Country.CountryID, 
			Currency.CurrencyID,
			Country.CountryName,
			Country.CountryCode2,
			Country.CountryCode3,
			Currency.CurrencyName,
			Currency.CurrencyCode,
			Currency.EffectiveDate,
			Currency.ExpirationDate
		FROM   CountryCurrency
			JOIN Currency ON CountryCurrency.CurrencyID = Currency.CurrencyID
			JOIN Country ON CountryCurrency.CountryID = Country.CountryID
		WHERE  ISNULL(CountryCurrency.EffectiveDate, @Date) <= @Date
			AND ISNULL(CountryCurrency.ExpirationDate, DATEADD(day, 1, @Date)) > @Date
  
	RETURN;
END;

GO

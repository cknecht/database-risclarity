SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[tfn_DateList] (@BeginDate smalldatetime, @EndDate smalldatetime)
RETURNS @DateList TABLE
	(
	AsOfDate smalldatetime
	)
	-- This will list all the days between and including the begin and end dates
AS
BEGIN 

	WHILE @BeginDate <= @EndDate
	BEGIN
		INSERT INTO @DateList (AsOfDate) VALUES (@BeginDate);
		SET @BeginDate = DATEADD(day, 1, @BeginDate)
	END 


	RETURN;
END;


GO

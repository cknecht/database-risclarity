SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Function [dbo].[tfn_DisplayAccounts_By_Entity]
	(
	@EntityID bigint,
	@AsOfDate smalldatetime
	)

RETURNS TABLE 
AS
RETURN (
		SELECT Account.AccountID,
			Account.AccountName,
			Account.AccountNumber,
			Account.DefaultCashSecurityID,
			Currency.CurrencyName as BaseCurrency,
			Count(Holdings.HoldingsID) as NumberOfHoldings,
			Count(Trans.TransID) as NumberOfTransactions
		FROM Account
			JOIN EntityAccount ON Account.AccountID = EntityAccount.AccountID
			JOIN Currency ON Account.BaseCurrencyID =  Currency.CurrencyID
			JOIN AccountSecurity ON Account.AccountID = AccountSecurity.AccountID
			LEFT JOIN Holdings ON AccountSecurity.AccountSecurityID = Holdings.AccountSecurityID
				AND Holdings.AsOfDate = @AsOfDate
			LEFT JOIN Trans ON AccountSecurity.AccountSecurityID = Trans.AccountSecurityID
		WHERE EntityAccount.EntityID = @EntityID
		GROUP BY Account.AccountID,
			Account.AccountName,
			Account.AccountNumber,
			Account.DefaultCashSecurityID,
			Currency.CurrencyName
	);

GO

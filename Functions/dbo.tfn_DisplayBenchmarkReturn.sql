SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE  function   [dbo].[tfn_DisplayBenchmarkReturn]
	(@BenchmarkID bigint = 1,
	@BeginDate	smalldatetime,
	@EndDate SMALLDATETIME)

RETURNS @Benchmark TABLE
	(
	BenchmarkID	bigint,
	BenchmarkName varchar(max),
	BeginDate smalldatetime,
	EndDate smalldatetime,
	BenchmarkReturn numeric(18,6)
	)
AS
BEGIN 

INSERT INTO @Benchmark (
	BenchmarkID,
	BenchmarkName,
	BeginDate,
	EndDate,
	BenchmarkReturn
	)
SELECT @BenchmarkID,
	Benchmark.BenchmarkName,
	@BeginDate AS BeginDate,
	@EndDate AS EndDate,
	BenchmarkReturn.BenchmarkReturn
FROM Benchmark
	JOIN BenchmarkReturn ON Benchmark.BenchmarkID = BenchmarkReturn.BenchmarkID
WHERE Benchmark.BenchmarkID = @BenchmarkID
	AND BeginDate = @BeginDate
	AND EndDate = @EndDate

	RETURN;
END;
GO

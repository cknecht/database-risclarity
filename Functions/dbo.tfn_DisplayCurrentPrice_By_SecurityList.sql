SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCtion [dbo].[tfn_DisplayCurrentPrice_By_SecurityList] (
	@SecurityList securitylist readonly,
	@AsOfDate smalldatetime
	)

RETURNS TABLE 
AS
RETURN (
	SELECT Price.SecurityID,
		Price.AsOfDate,
		Price.Price * ISNULL(AccountSecurity.PriceFactor, 1) AS Price,  
		Price.Price_CurrencyID,
		ISNULL(AccountSecurity.PriceFactor, 1) AS PriceFactor,
		'Price' AS SourceTable
	FROM @SecurityList as AccountSecurity
		JOIN (
		SELECT Price.SecurityID,
					MAX(Price.AsOfDate) as AsOfDate 
				FROM @SecurityList as AccountSecurity
					JOIN Price as Price ON AccountSecurity.SecurityID = Price.SecurityID
				WHERE FLOOR(CAST(Price.AsOfDate as float)) <= @AsOfDate
				GROUP BY Price.SecurityID
		
			) AS AsOfDate ON AsOfDate.SecurityID = AccountSecurity.SecurityID
		JOIN Price as Price ON AsOfDate.SecurityID = Price.SecurityID
			AND Price.AsOfDate = AsOfDate.AsOfDate
	GROUP BY Price.SecurityID,
		Price.AsOfDate,
		Price.Price,  
		Price.Price_CurrencyID,
		AccountSecurity.PriceFactor
	UNION
	-- ADD Prices from Transactions where prices dont exist in the Price Table
	SELECT Trans.SecurityID,
		Trans.TradeDate AS AsOfDate,
		MAX(COALESCE(Trans.UnitPrice_Base, (Trans.Amount_Base / Trans.Quantity))) AS Price,
		Trans.CurrencyID_Base as Price_CurrencyID,
		1 AS PriceFactor,
		'Trans' AS SourceTable
	FROM (SELECT DISTINCT AccountSecurityID FROM @SecurityList) AS SecurityList 
		JOIN dbo.vTrans AS Trans ON Trans.AccountSecurityID = SecurityList.AccountSecurityID
		JOIN (SELECT Trans.SecurityID,
					MAX(Trans.TradeDate) as AsOfDate 
				FROM @SecurityList as AccountSecurity
					JOIN vTrans as Trans ON AccountSecurity.AccountSecurityID = Trans.AccountSecurityID
				WHERE FLOOR(CAST(Trans.TradeDate as float)) <= @AsOfDate
				GROUP BY Trans.SecurityID
			) AS TransAsOfDate ON TransAsOfDate.SecurityID = Trans.SecurityID
		LEFT JOIN (SELECT AccountSecurity.SecurityID,
				COUNT(Price.Price) AS CountPrices
			FROM @SecurityList as AccountSecurity
				JOIN Price as Price ON AccountSecurity.SecurityID = Price.SecurityID
			WHERE FLOOR(CAST(Price.AsOfDate as float)) <= @AsOfDate
			GROUP BY AccountSecurity.SecurityID
			) AS CountPrices ON CountPrices.SecurityID = Trans.SecurityID -- There are no prices in the price table before the @AsOfPrice
	WHERE Trans.TradeDate = TransAsOfDate.AsOfDate
		--AND (Trans.CalcQuantity <> 0 OR Trans.CalcQuantityOfOne <> 0)
		AND ISNULL(CountPrices.CountPrices, 0) = 0
	GROUP BY Trans.SecurityName,
		Trans.SecurityID,
		Trans.TradeDate,
		--Trans.UnitPrice_Base, 
		--Trans.Amount_Base, 
		--Trans.Quantity,
		Trans.CurrencyID_Base,
		CountPrices.CountPrices
	)

GO

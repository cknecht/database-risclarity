SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCtion [dbo].[tfn_DisplayCurrentPrice_By_SecurityList_By_DateList] (
	@SecurityList securitylist readonly,
	@DateList datelist readonly
	)

RETURNS @Prices TABLE (
	AsOfDate smalldatetime NOT NULL,
	SecurityID bigint NOT NULL,
	PriceAsOfDate smalldatetime NOT NULL,
	Price numeric(18,6) NOT NULL,
	Price_CurrencyID int NOT NULL,
	PriceFactor NUMERIC(18,6)
	)

AS
BEGIN
	--DECLARE @BeginDate smalldatetime,
	--	@EndDate smalldatetime
	
	--SELECT @BeginDate = DATEADD(day, -1, MIN(BeginDate)),
	--	@EndDate = MAX(EndDate)
	--FROM @PeriodList

	INSERT INTO @Prices (
		AsOfDate,
		SecurityID,
		PriceAsOfDate,
		Price,
		Price_CurrencyID,
		PriceFactor
		)
SELECT AsOfDate.AsOfDate,
		Price.SecurityID,
		Price.AsOfDate as PriceAsOfDate,
		Price.Price * ISNULL(Security.PriceFactor, 1) AS Price,
		Price.Price_CurrencyID,
		ISNULL(Security.PriceFactor, 1) AS PriceFactor
	FROM  (
			SELECT DateList.AsOfDate as AsOfDate,
					Price.SecurityID,
					MAX(Price.AsOfDate) as PriceAsOfDate 
				FROM @SecurityList as AccountSecurity
					CROSS JOIN @DateList as Datelist
					JOIN Price as Price ON AccountSecurity.SecurityID = Price.SecurityID
				WHERE FLOOR(CAST(Price.AsOfDate as float)) <= DateList.AsOfDate
				GROUP BY Price.SecurityID,
					DateList.AsOfDate
			) 
			AS AsOfDate 
			JOIN Price ON Price.SecurityID = AsOfDate.SecurityID 
						AND Price.AsOfDate = AsOfDate.PriceAsOfDate
			JOIN Security ON Price.SecurityID = Security.SecurityID
			JOIN AccountSecurity ON Security.SecurityID = AccountSecurity.SecurityID
			JOIN Account ON AccountSecurity.AccountID = Account.AccountID 
				AND DATEADD(d, -1, ISNULL(Account.InceptionDate, '1/2/1900')) <= AsOfDate.AsOfDate
	RETURN;
END;


GO

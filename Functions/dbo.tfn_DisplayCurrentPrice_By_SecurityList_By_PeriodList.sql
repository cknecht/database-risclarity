SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCtion [dbo].[tfn_DisplayCurrentPrice_By_SecurityList_By_PeriodList] (
	@SecurityList securitylist readonly,
	@PeriodList periodlist readonly
	)

RETURNS @Prices TABLE (
	AsOfDate smalldatetime NOT NULL,
	SecurityID bigint NOT NULL,
	PriceAsOfDate smalldatetime NOT NULL,
	Price numeric(18,6) NOT NULL,
	Price_CurrencyID int NOT NULL,
	PriceFactor NUMERIC(18,6),
	SourceTable VARCHAR(100),
	ContractSize NUMERIC(18,6)
	)

AS
BEGIN
	DECLARE @BeginDate smalldatetime,
		@EndDate smalldatetime
	
	SELECT @BeginDate = DATEADD(day, -1, MIN(BeginDate)),
		@EndDate = MAX(EndDate)
	FROM @PeriodList

	DECLARE @DateList DateList

	INSERT INTO @DateList (AsOfDate)
	SELECT AsOfDate
	FROM [tfn_DateList] (@BeginDate, @EndDate)

	--CREATE A TEMP TABLE of All PRICES we may need
	DECLARE @PriceTable TABLE (
		SecurityID BIGINT NOT NULL,
		AsOfDate SMALLDATETIME NOT NULL,
		Price NUMERIC(18,6) NOT NULL,
		Price_CurrencyID INT NOT NULL,
		SourceTable VARCHAR(100),
		ContractSize NUMERIC(18,6)
		)

	INSERT INTO @PriceTable (
		SecurityID,
		AsOfDate,
		--PriceAsOfDate,
		Price,
		Price_CurrencyID,
		SourceTable,
		ContractSize
		)
	SELECT Price.SecurityID,
		Price.AsOfDate,
		--Price.AsOfDate,
		Price.Price,-- * ISNULL(AccountSecurity.PriceFactor, 1) AS Price,  
		Price.Price_CurrencyID,
		'Price',
		AccountSecurity.ContractSize
	FROM @SecurityList as AccountSecurity
		JOIN Price as Price ON AccountSecurity.SecurityID = Price.SecurityID
	GROUP BY Price.SecurityID,
		Price.AsOfDate,
		Price.Price,  
		Price.Price_CurrencyID,
		AccountSecurity.ContractSize
	UNION
	 -- ADD Prices from Transactions where prices dont exist in the Price Table
	SELECT 
		Trans.SecurityID,
		Trans.TradeDate AS AsOfDate,
		--Trans.TradeDate,
		MAX(COALESCE(Trans.UnitPrice_Base, (Trans.Amount_Base / Trans.Quantity))) / ISNULL(Security.PriceFactor, 1) AS Price,
		Trans.CurrencyID_Base as Price_CurrencyID,
		'Trans',
		Security.ContractSize
	FROM (SELECT DISTINCT AccountSecurityID, SecurityID FROM @SecurityList) AS SecurityList 
		JOIN dbo.vTrans AS Trans ON Trans.AccountSecurityID = SecurityList.AccountSecurityID
		LEFT JOIN Price as Price ON SecurityList.SecurityID = Price.SecurityID
			AND Price.AsOfDate = Trans.TradeDate
		LEFT JOIN (SELECT MIN(AsOFDate) AS AsOfDate, SecurityID FROM Price GROUP BY SecurityID) AS LastPrice ON SecurityList.SecurityID = LastPrice.SecurityID	
		JOIN Security ON LastPrice.SecurityID = Security.SecurityID
	WHERE (Trans.CalcQuantity <> 0 OR Trans.CalcQuantityOfOne <> 0)
		AND Price.PriceID IS NULL
		AND Trans.TradeDate < ISNULL(LastPrice.AsOfDate, CONVERT(SMALLDATETIME, CURRENT_TIMESTAMP))
	GROUP BY Trans.SecurityName,
		Trans.SecurityID,
		Trans.TradeDate,
		Trans.CurrencyID_Base,
		Security.PriceFactor,
		Security.ContractSize
	

	INSERT INTO @Prices (
		AsOfDate,
		SecurityID,
		PriceAsOfDate,
		Price,
		Price_CurrencyID,
		PriceFactor,
		SourceTable,
		ContractSize
		)
	SELECT AsOfDate.AsOfDate,
		Price.SecurityID,
		Price.AsOfDate as PriceAsOfDate,
		Price.Price * ISNULL(SecurityList.PriceFactor, 1) AS Price,
		Price.Price_CurrencyID,
		ISNULL(SecurityList.PriceFactor, 1) AS PriceFactor,
		Price.SourceTable,
		Price.ContractSize
	FROM (SELECT DISTINCT SecurityID, AccountID, PriceFactor FROM @SecurityList) AS SecurityList
		JOIN (
			SELECT DateList.AsOfDate as AsOfDate,
					Price.SecurityID,
					MAX(Price.AsOfDate) as PriceAsOfDate 
				FROM (SELECT DISTINCT SecurityID FROM @SecurityList) as AccountSecurity
					JOIN @PriceTable as Price ON AccountSecurity.SecurityID = Price.SecurityID
					JOIN (-- Make sure we have a beginning price
							SELECT @BeginDate as AsOfDate
							UNION 
							--Add all the EndDates from @PeriodList
							SELECT EndDate as AsOfDate
							FROM @PeriodList
							UNION 
							--Add all the BeginDates from @PeriodList
							SELECT BeginDate as AsOfDate
							FROM @PeriodList
							UNION 
							-- Add in any day we have a price entered
							SELECT Price.AsOfDate as AsOfDate
							FROM (SELECT DISTINCT SecurityID FROM @SecurityList) as AccountSecurity
								JOIN @PriceTable as Price ON AccountSecurity.SecurityID = Price.SecurityID
							WHERE Price.AsOfDate > @BeginDate
								AND Price.AsOfDate <= @EndDate
							UNION 
							--Add in any day we have a cash flow
							SELECT TradeDate as AsOfDate
							FROM [tfn_Transactions_By_Level_By_DateList] (
								@SecurityList,
								@DateList
								) as Price
							WHERE CalcCashFlow <> 0
								OR CalcIncome <> 0
								OR CalcFee <> 0
							--GROUP BY Price.TradeDate
							) as Datelist ON FLOOR(CAST(Price.AsOfDate as float)) <= DateList.AsOfDate --This is important so as not to get prices greater than the asofdate
				GROUP BY Price.SecurityID,
					DateList.AsOfDate
			) AS AsOfDate ON AsOfDate.SecurityID = SecurityList.SecurityID
			JOIN Account ON SecurityList.AccountID = Account.AccountID 
				AND DATEADD(d, -1, ISNULL(Account.InceptionDate, '1/2/1900')) <= AsOfDate.AsOfDate
			JOIN @PriceTable AS Price ON Price.SecurityID = SecurityList.SecurityID 
				AND Price.AsOfDate = AsOfDate.PriceAsOfDate
	GROUP BY Price.ContractSize,
		AsOfDate.AsOfDate,
		Price.SecurityID,
		Price.AsOfDate,
		Price.Price,
		Price.Price_CurrencyID,
		SecurityList.PriceFactor,
		Price.SourceTable

	

	RETURN;
END;

GO

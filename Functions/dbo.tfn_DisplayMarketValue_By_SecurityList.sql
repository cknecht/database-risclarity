SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCtion [dbo].[tfn_DisplayMarketValue_By_SecurityList] (
	@SecurityList Securitylist readonly,
	@AsOfDate smalldatetime
	)

RETURNS TABLE 
AS
RETURN 
(
	SELECT AccountSecurity.AccountSecurityID, 
		Trans.EntityID,
		Trans.EntityName,
		Trans.AccountID,
		Trans.AccountName,
		Trans.SecurityID,
		Trans.SecurityName, 
		CASE WHEN ISNULL(Trans.QuantityofOne, 0) = 1
			THEN ((1 * SUM(Trans.CalcQuantityOfOne)) + 1) * (ISNULL(AccountSecurity.PercentOwnership, 100) * .01)
			ELSE SUM(Trans.Quantity)
			END as Quantity,
		Price.Price / ISNULL(Price.PriceFactor, 1) as Price_Base,
		Price.Price /  ISNULL(Price.PriceFactor, 1) as Price_Local,
		Price.AsOfDate AS PriceAsOfDate,
		CASE WHEN ISNULL(Trans.QuantityofOne, 0) = 1
			THEN ((1 * SUM(Trans.CalcQuantityOfOne)) + 1) * (ISNULL(AccountSecurity.PercentOwnership, 100) * .01)
			ELSE SUM(Trans.Quantity * ISNULL(AccountSecurity.ContractSize, 1))
			END * (Price.Price) as MarketValue_Base,
		CASE WHEN ISNULL(Trans.QuantityofOne, 0) = 1
			THEN ((1 * SUM(Trans.CalcQuantityOfOne)) + 1) * (ISNULL(AccountSecurity.PercentOwnership, 100) * .01)
			ELSE SUM(Trans.Quantity * ISNULL(AccountSecurity.ContractSize, 1))
			END * (Price.Price) as MarketValue_Local,
		1 as ExchangeRate
	FROM @SecurityList as AccountSecurity
		JOIN tfn_DisplayCurrentPrice_By_SecurityList (
				@SecurityList,
				@AsOfDate
				) as Price ON AccountSecurity.SecurityID = Price.SecurityID
		JOIN [dbo].[tfn_Trans_by_SecurityList] (
				@SecurityList
				) as Trans ON Trans.AccountSecurityID = AccountSecurity.AccountSecurityID
						--AND Trans.TradeDate <= Price.AsOfDate
	WHERE Trans.TradeDate <= @AsOfDate
	GROUP BY AccountSecurity.AccountSecurityID,
		Trans.EntityID,
		Trans.EntityName,
		Trans.AccountID,
		Trans.AccountName,
		Trans.SecurityID,
		Trans.SecurityName,
		Trans.SecurityName,
		Trans.QuantityOfOne,
		Price.Price,
		Price.AsOfDate,
		Price.PriceFactor,
		AccountSecurity.PercentOwnership,
		AccountSecurity.ContractSize
)



GO

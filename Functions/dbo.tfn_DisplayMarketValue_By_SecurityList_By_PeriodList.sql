SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCtion [dbo].[tfn_DisplayMarketValue_By_SecurityList_By_PeriodList] (
	@SecurityList securitylist readonly,
	@PeriodList periodlist readonly
	)

RETURNS @MarketValue TABLE (
	AsOfDate smalldatetime,
	AccountSecurityID bigint,
	EntityID bigint,
	EntityName varchar(1000),
	AccountID bigint,
	AccountName varchar(1000),
	SecurityID bigint,
	SecurityName varchar(1000),
	Quantity numeric(18,6),
	Price_Base numeric(18,6),
	Price_Local numeric(18,6),
	MarketValue_Base numeric(18,6),
	MarketValue_Local numeric(18,6),
	ExchangeRate numeric(18,6)
	)

AS

BEGIN

	DECLARE @DateList DateList
	DECLARE @BeginDate smalldatetime
	DECLARE @EndDate smalldatetime

	SELECT @BeginDate = DATEADD(day, -1, MIN(BeginDate)),
		@EndDate = MAX(EndDate)
	FROM @PeriodList

	INSERT INTO @DateList (AsOfDate)
	SELECT AsOfDate
	FROM [tfn_DateList] (@BeginDate, @EndDate)

	INSERT INTO @MarketValue (
		AsOfDate,
		AccountSecurityID,
		EntityID,
		EntityName,
		AccountID,
		AccountName,
		SecurityID,
		SecurityName,
		Quantity,
		Price_Base,
		Price_Local,
		MarketValue_Base,
		MarketValue_Local,
		ExchangeRate
		)
	SELECT Price.PriceAsOfDate,
		Trans.AccountSecurityID, 
		Trans.EntityID,
		Trans.EntityName,
		Trans.AccountID,
		Trans.AccountName,
		Trans.SecurityID,
		Trans.SecurityName, 
		CASE WHEN ISNULL(Trans.QuantityofOne, 1) = 1
			THEN ((1 * SUM(Trans.CalcQuantityOfOne)) + 1) * (ISNULL(Trans.PercentOwnership, 100) * .01)
			ELSE SUM(Trans.Quantity * Price.ContractSize * Trans.CalcQuantity)
			END as Quantity,
		Price.Price as Price_Base,
		Price.Price as Price_Local,
		CASE WHEN ISNULL(Trans.QuantityofOne, 1) = 1
			THEN ((1 * SUM(Trans.CalcQuantityOfOne)) + 1) * (ISNULL(Trans.PercentOwnership, 100) * .01)
			ELSE SUM(Trans.Quantity * Price.ContractSize * Trans.CalcQuantity)
			END * Price.Price as MarketValue_Base,
		CASE WHEN ISNULL(Trans.QuantityofOne, 1) = 1
			THEN ((1 * SUM(Trans.CalcQuantityOfOne)) + 1) * (ISNULL(Trans.PercentOwnership, 100) * .01)
			ELSE SUM(Trans.Quantity * Trans.CalcQuantity)
			END * Price.Price as MarketValue_Local,
		1 as ExchangeRate
	FROM tfn_DisplayCurrentPrice_By_SecurityList_By_PeriodList (
				@SecurityList,
				@PeriodList
				) as Price 
		JOIN @DateList as DateList ON Price.PriceAsOfDate = DateList.AsOfDate
		JOIN [dbo].[tfn_Trans_by_SecurityList] (
				@SecurityList
				) as Trans ON Trans.SecurityID = Price.SecurityID
						AND Trans.TradeDate <= Price.PriceAsOfDate
	GROUP BY Price.PriceAsOfDate,
		Price.ContractSize,
		Trans.AccountSecurityID,
		Trans.EntityID,
		Trans.EntityName,
		Trans.AccountID,
		Trans.AccountName,
		Trans.SecurityID,
		Trans.SecurityName,
		Trans.SecurityName,
		Trans.QuantityOfOne,
		Price.Price,
		Trans.PercentOwnership

	RETURN;
END

GO

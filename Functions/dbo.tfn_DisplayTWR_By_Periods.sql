SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[tfn_DisplayTWR_By_Periods] (
	@SecurityList SecurityList Readonly,
	@PeriodList PeriodList Readonly
	)

RETURNS @TWR TABLE (
	TWR_ID bigint identity(1,1),
	PeriodBeginDate smalldatetime,
	PeriodEndDate smalldatetime,
	TWRBeginDate smalldatetime,
	TWREndDate smalldatetime,
	BeginMarketValue numeric(18,6),
	EndMarketValue numeric(18,6),
	CashFlow numeric(18,6),
	Income numeric(18,6),
	AccruedIncome numeric(18,6),
	Fees numeric(18,6),
	PeriodTWR numeric(18,6),
	PeriodTWR_Net_Of_Fees numeric(18,6),
	PeriodTWR_Plus_One numeric(18,6),
	PeriodTWR_Net_Of_Fees_Plus_One numeric(18,6),
	TWR numeric(18,6),
	AnnualizedTWR numeric(18,6),
	TWR_Net_Of_Fees numeric(18,6),
	AnnualizedTWR_Net_Of_Fees numeric(18,6)
	)

AS
BEGIN
	DECLARE @TWR_Detail TABLE (
		TWR_ID bigint identity(1,1),
		BeginDate smalldatetime,
		EndDate smalldatetime,
		BeginMarketValue numeric(18,6),
		EndMarketValue numeric(18,6),
		CashFlow numeric(18,6),
		Income numeric(18,6),
		AccruedIncome numeric(18,6),
		Fees numeric(18,6),
		TWR numeric(18,6),
		TWR_Net_Of_Fees numeric(18,6),
		TWR_plus_one numeric(18,6),
		TWR_plus_one_Net_Of_Fees numeric(18,6),
		TotalTWR numeric(18,6),
		AnnualizedTotalTWR numeric(18,6),
		TotalTWR_Net_Of_Fees numeric(18,6),
		AnnualizedTotalTWR_Net_Of_Fees numeric(18,6)
		)

	INSERT INTO @TWR_Detail (
		BeginDate,
		EndDate,
		BeginMarketValue,
		EndMarketValue,
		CashFlow,
		Income,
		AccruedIncome,
		Fees
		)
	SELECT BeginDate,
		EndDate,
		BeginMarketValue,
		EndMarketValue,
		CashFlow,
		Income,
		AccruedIncome,
		Fees
	FROM [tfn_TWR] (
			@SecurityList,
			@PeriodList
			) as TWR

	--Populate the period returns
	UPDATE @TWR_Detail
	SET TWR = (((EndMarketValue + ISNULL(CashFlow, 0)) - BeginMarketValue) / BeginMarketValue),
		TWR_plus_one = (((EndMarketValue + ISNULL(CashFlow, 0)) - BeginMarketValue) / BeginMarketValue) + 1,
		TWR_Net_Of_Fees = (((EndMarketValue + ISNULL(CashFlow, 0) + ISNULL(Fees, 0)) - BeginMarketValue) / BeginMarketValue),
		TWR_plus_one_Net_Of_Fees = (((EndMarketValue + ISNULL(CashFlow, 0) + ISNULL(Fees, 0)) - BeginMarketValue) / BeginMarketValue) + 1

	--Declare variables
	DECLARE @TotalTWR numeric(18,6)
	DECLARE @TotalTWR_Net_Of_Fees numeric(18,6)

	INSERT INTO @TWR (
		PeriodBeginDate, 
		PeriodEndDate,
		TWRBeginDate,
		TWREndDate,
		BeginMarketValue,
		EndMarketVAlue,
		CashFlow,
		Income,
		AccruedIncome,
		Fees,
		PeriodTWR,
		PeriodTWR_Net_Of_Fees,
		PeriodTWR_Plus_One,
		PeriodTWR_Net_Of_Fees_Plus_One
		)
	SELECT PeriodList.BeginDate, 
		PeriodList.EndDate,
		TWRDetail.BeginDate,
		TWRDetail.EndDate,
		TWRDetail.BeginMarketValue,
		TWRDetail.EndMarketVAlue,
		TWRDetail.CashFlow,
		TWRDetail.Income,
		TWRDetail.AccruedIncome,
		TWRDetail.Fees,
		TWRDetail.TWR,
		TWRDetail.TWR_Net_Of_Fees,
		TWRDetail.TWR_plus_one,
		TWRDetail.TWR_plus_one_Net_Of_Fees
	FROM @PeriodList as PeriodList
		JOIN @TWR_Detail as TWRDetail ON PeriodList.EndDate >= TWRDetail.EndDate
			AND PeriodList.BeginDate <= TWRDetail.BeginDate
	GROUP BY PeriodList.BeginDate, 
		PeriodList.EndDate,
		TWRDetail.BeginDate,
		TWRDetail.EndDate,
		TWRDetail.BeginMarketValue,
		TWRDetail.EndMarketVAlue,
		TWRDetail.CashFlow,
		TWRDetail.Income,
		TWRDetail.AccruedIncome,
		TWRDetail.Fees,
		TWRDetail.TWR,
		TWRDetail.TWR_Net_Of_Fees,
		TWRDetail.TWR_plus_one,
		TWRDetail.TWR_plus_one_Net_Of_Fees

	UPDATE @TWR 
	SET TWR = (TotalTWR.TWR - 1)
	FROM @TWR as TWR
		JOIN (SELECT PeriodBeginDate, 
			PeriodEndDate, 
			CASE
			   WHEN MinVal = 0 THEN 0
			   WHEN Neg % 2 = 1 THEN -1 * EXP(ABSMult)
			   ELSE EXP(ABSMult)
			END as TWR
		FROM  (
		SELECT PeriodBeginDate,
			PeriodEndDate,
		   --log of +ve row values
		   SUM(LOG(ABS(NULLIF(PeriodTWR_plus_one, 0)))) AS ABSMult,
		   --count of -ve values. Even = +ve result.
		   SUM(SIGN(CASE WHEN PeriodTWR_plus_one < 0 THEN 1 ELSE 0 END)) AS Neg,
		   --anything * zero = zero
		   MIN(ABS(PeriodTWR_plus_one)) AS MinVal
		FROM @TWR
		GROUP BY PeriodBeginDate,
			PeriodEndDate
		) as TWR) as TotalTWR ON TWR.PeriodBeginDate = TotalTWR.PeriodBeginDate
								AND TWR.PeriodEndDate = TotalTWR.PeriodEndDate

	RETURN;
END;


GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[tfn_EntityEntityOwnership] (@EntityPrimaryID BIGINT, @AsOfDate SMALLDATETIME)

RETURNS TABLE 
AS
RETURN 
(
	WITH REC AS
    (	
    SELECT t.EntityPrimaryID as baseid,
		t.EntityPrimaryID,
		t.EntitySubID, 
		t.AsOfDate,
		CAST(t.PercentOwnership / 100 AS NUMERIC(18,6)) AS PercentOwnership
    FROM dbo.[tfn_tblEntityEntityOwnership](@AsOfDate) as t
    UNION ALL
    SELECT  r.baseid,
		p.EntityPrimaryID,
		p.EntitySubID,
		p.AsOfDate,
		CAST(CAST(r.PercentOwnership / 100 AS NUMERIC(18,6)) * CAST(p.PercentOwnership AS NUMERIC(18,6)) as NUMERIC(18,6)) AS PercentOwnership
    FROM    rec r
		JOIN dbo.[tfn_tblEntityEntityOwnership](@AsOfDate) p ON p.EntityPrimaryID = r.EntitySubID 
	)

SELECT EntitySubID, 
	SUM(PercentOwnership) * 100 AS PercentOwnership
FROM REC
WHERE baseid = @EntityPrimaryID
GROUP BY EntitySubID

)

GO

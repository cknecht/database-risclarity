SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[tfn_EntityOwnership] (@AsOfDate SMALLDATETIME)

RETURNS TABLE 
AS
RETURN 
(
	WITH REC AS
    (	
    SELECT t.EntityPrimaryID as baseid,
		t.EntityPrimaryID,
		t.EntitySubID, 
		t.AsOfDate,
		CAST(t.PercentOwnership / 100 AS NUMERIC(18,6)) AS PercentOwnership
    FROM dbo.[tfn_tblEntityEntityOwnership](@AsOfDate) t
		--JOIN (SELECT EntityPrimaryID, EntitySubID, MAX(AsOfDate) as MaxDate
		--			FROM EntityEntityOwnership
		--			WHERE AsOfDate <= @AsOfDate 
		--				--AND EntityPrimaryID = @EntityPrimaryID
		--			GROUP BY EntityPrimaryID, EntitySubID
		--			) as AsOfDate ON t.EntityPrimaryID = AsOfDate.EntityPrimaryID 
		--			AND t.EntitySubID = AsOfDate.EntitySubID
		--WHERE t.AsOfDate = AsOfDate.MaxDate
		--	AND t.EntityPrimaryID = AsOfDate.EntityPrimaryID
    UNION ALL
    SELECT  r.baseid,
		p.EntityPrimaryID,
		p.EntitySubID,
		p.AsOfDate,
		CAST(CAST(r.PercentOwnership / 100 AS NUMERIC(18,6)) * CAST(p.PercentOwnership AS NUMERIC(18,6)) as NUMERIC(18,6)) AS PercentOwnership
    FROM    rec r
		JOIN dbo.[tfn_tblEntityEntityOwnership](@AsOfDate) p ON p.EntityPrimaryID = r.EntitySubID 

    )

SELECT baseid AS EntityOwnerID,
	EntitySubID, 
	SUM(PercentOwnership) * 100 AS PercentOwnership
FROM REC
--WHERE baseid = @EntityPrimaryID
GROUP BY baseid, 
	EntitySubID

)

GO

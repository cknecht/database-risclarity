SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[tfn_Fees_By_SecurityList_By_DateList] (
	@SecurityList SecurityList Readonly,
	@DateList DateList Readonly
	)

RETURNS @Fees TABLE (
	AsOfDate smalldatetime,
	Fees numeric(18,6),
	TradeDate smalldatetime,
	TransCodeName varchar(100),
	EntityID bigint,
	AccountID bigint,
	AccountSecurityID bigint,
	SecurityID bigint
	)

AS
BEGIN
	INSERT INTO @Fees (
		AsOfDate,
		Fees
		)
	SELECT DateList.AsOfDate,
		SUM(Trans.Amount_Base * Trans.CalcFee) as Income
	FROM dbo.[tfn_Transactions_By_Level_By_DateList] (
				@SecurityList,
				@DateList
				) as Trans
		LEFT JOIN @DateList as DateList ON Trans.TradeDate = DateList.AsOfDate
	WHERE Trans.CalcFee <> 0
	GROUP BY DateList.AsOfDate


	RETURN;
END;


GO

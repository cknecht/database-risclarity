SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[tfn_Holdings_by_Level] (@Level varchar(1), @LevelID bigint, @AsOfDate SMALLDATETIME, @IncludeLinkedAccounts BIT, @ShowQuantityOfZero bit)
RETURNS @Holdings TABLE
	(
	FamilyID	bigint,
	FamilyName	varchar(max),
	EntityID	bigint,
	EntityName	varchar(max),
	AccountID	bigint,
	AccountName varchar(max),
	AccountSecurityID bigint,
	SecurityID	bigint,
	SecurityName	varchar(max),
	Symbol VARCHAR(1000),
	CUSIP VARCHAR(1000),
	SEDOL VARCHAR(1000),
	ISIN VARCHAR(1000),
	Quantity	numeric(18,6),
	DateAcquired	smalldatetime,
	CurrencyCode_Local varchar(3),
	Price_Base	numeric(18,6),
	PriceFactor NUMERIC(18,6),
	PriceAsOfDate SMALLDATETIME,
	MarketValue_Base	numeric(18,6),
	TotalMarketValue_Base	numeric(18,6),
	Cost_Base	numeric(18,6),
	GainLossShort_Base	numeric(18,6),
	GainLossLong_Base	numeric(18,6),
	DateLongTerm	smalldatetime,
	LiquidityPeriodDays	int,
	LiquidityHardLockupDate	smalldatetime,
	LiquiditySoftLockupDate	smalldatetime,
	LiquiditySoftLockupPercent	numeric(18,6),
	LiquidityRedemptionPeriodID int,
	LiquidityRedemptionPeriodText varchar(50),
	LiquidityNoticePeriodDays	int,
	LiquidityGatePercent	numeric(18,6),
	SectorName varchar(255),
	SectorID	bigint,
	SectorSortOrder int,
	SectorColor VARCHAR(100),
	RegionName varchar(255),
	RegionID bigint,
	RegionSortOrder int,
	AssetClassName varchar(255),
	AssetClassID bigint,
	AssetClassSortOrder int,
	AssetClassColor varchar(100),
	PercentOwnership NUMERIC(18,6),
	ContractSize NUMERIC(18,6)
	)
AS
BEGIN 
	DECLARE @SecurityList SecurityList

	INSERT INTO @SecurityList
			(
			FamilyID,
			FamilyName,
			EntityID,
			EntityName,
			AccountID,
			AccountName,
			AccountSecurityID,
			SecurityID,
			SecurityName,
			SecuritySymbol,
			SecurityCUSIP,
			SecuritySEDOL,
			SecurityISIN,
			PriceFactor,
			PercentOwnership,
			ContractSize
			)
	SELECT FamilyID,
			FamilyName,
			EntityID,
			EntityName,
			AccountID,
			AccountName,
			AccountSecurityID,
			SecurityID,
			SecurityName,
			SecuritySymbol,
			SecurityCUSIP,
			SecuritySEDOL,
			SecurityISIN,
			PriceFactor,
			PercentOwnership,
			ContractSize
	FROM [dbo].[tfn_SecurityList_by_Level] (@Level, @LevelID, @AsOfDate, @IncludeLinkedAccounts)

	INSERT INTO @Holdings 
		(
		FamilyID,
		FamilyName,
		EntityID,
		EntityName,
		AccountID,
		AccountName,
		AccountSecurityID,
		SecurityID,
		SecurityName,
		Symbol,
		CUSIP,
		SEDOL,
		ISIN,
		Quantity,
		DateAcquired,
		CurrencyCode_Local,
		Price_Base,
		PriceFactor,
		PriceAsOfDate,
		MarketValue_Base,
		Cost_Base,
		GainLossShort_Base,
		GainLossLong_Base,
		DateLongTerm,
		LiquidityPeriodDays,
		LiquidityHardLockupDate,
		LiquiditySoftLockupDate,
		LiquiditySoftLockupPercent,
		LiquidityRedemptionPeriodID,
		LiquidityRedemptionPeriodText,
		LiquidityNoticePeriodDays,
		LiquidityGatePercent,
		SectorName,
		SectorID,
		SectorColor,
		SectorSortOrder,
		RegionName,
		RegionID,
		RegionSortOrder,
		AssetClassName,
		AssetClassID,
		AssetClassSortOrder,
		AssetClassColor,
		PercentOwnership
		)
	SELECT SecurityList.FamilyID as FamilyID,
		SecurityList.FamilyName as FamilyName,
		SecurityList.EntityID as EntityID,
		SecurityList.EntityName as EntityName,
		SecurityList.AccountID as AccountID,
		SecurityList.AccountName as AccountName,
		SecurityList.AccountSecurityID as AccountSecurityID,
		SecurityList.SecurityID as SecurityID,
		SecurityList.SecurityName as SecurityName,
		SecurityList.SecuritySymbol,
		SecurityList.SecurityCUSIP,
		SecurityList.SecuritySEDOL,
		SecurityList.SecurityISIN,
		Holdings.Quantity as Quantity,
		'' as  DateAcquired,
		Currency.CurrencyCode as CurrencyCode_Local,
		Holdings.Price_Base as Price_Base,
		Security.PriceFactor,
		Holdings.PriceAsOfDate AS PriceAsOfDate,
		Holdings.MarketValue_Base as MarketValue_Base,
		0 as Cost_Base, --Holdings.Cost_Base as Cost_Base,
		--CASE WHEN Holdings.DateLongTerm > @AsOfDate
		--	THEN Holdings.GainLoss_Base 
		--	ELSE 0
		--	END as GainLossShort_Base,
		--CASE WHEN Holdings.DateLongTerm <= @AsOfDate
		--	THEN Holdings.GainLoss_Base 
		--	ELSE 0
		--	END as GainLossLong_Base,
		0 as GainLossShort_Base,
		0 as GainLossLong_Base,
		--Holdings.DateLongTerm as DateLongTerm,
		'' as DateLongTerm,
		Liquidity.LiquidityDays as LiquidityPeriodDays,
		Liquidity.LiquidityHardLockupDate as LiquidityHardLockupDate,
		Liquidity.LiquiditySoftLockupDate as LiquiditySoftLockupDate,
		Liquidity.LiquiditySoftLockupPercent as LiquiditySoftLockupPercent,
		Liquidity.LiquidityRedemptionPeriodID as LiquidityRedemptionPeriodID,
		Liquidity.LiquidityRedemptionPeriodName as LiquidityRedemptionPeriodText,
		Liquidity.LiquidityNoticePeriodDays as LiquidityNoticePeriodDays,
		Liquidity.LiquidityGatePercent as LiquidityGatePercent,
		Sector.SectorName,
		Sector.SectorID,
		Sector.SectorColor,
		Sector.SectorSortOrder,
		Region.RegionName,
		Region.RegionID,
		Region.RegionSortOrder,
		AssetClass.AssetClassName,
		AssetClass.AssetClassID,
		AssetClass.AssetClassSortOrder,
		AssetClass.AssetClassColor,
		SecurityList.PercentOwnership
	FROM @SecurityList as SecurityList 
		LEFT JOIN [dbo].[tfn_DisplayMarketValue_By_SecurityList](@SecurityList, @AsOfDate) as Holdings ON Holdings.AccountSecurityID = SecurityList.AccountSecurityID
		LEFT JOIN Security ON Holdings.SecurityID = Security.SecurityID
		LEFT JOIN Currency as Currency ON Security.LocalCurrencyID = Currency.CurrencyID 
		--LEFT JOIN LiquidityRedemptionPeriod ON AccountSecurity.LiquidityRedemptionPeriodID = LiquidityRedemptionPeriod.LiquidityRedemptionPeriodID
		LEFT JOIN [dbo].[tfn_Liquidity_By_Security](@AsOfDate) as Liquidity ON Holdings.SecurityID = Liquidity.SecurityID
		LEFT JOIN [dbo].[tfn_Sector_By_Security](@AsOfDate) as Sector ON Holdings.SecurityID = Sector.SecurityID
		LEFT JOIN [dbo].[tfn_Region_By_Security](@AsOfDate) as Region ON Holdings.SecurityID = Region.SecurityID
		LEFT JOIN [dbo].[tfn_AssetClass_By_Security](@AsOfDate) as AssetClass ON Holdings.SecurityID = AssetClass.SecurityID
	WHERE CASE @ShowQuantityOfZero 
		WHEN 0
		THEN Holdings.Quantity 
		ELSE 1
		END <> 0

  
	UPDATE @Holdings
	SET TotalMarketValue_Base = (SELECT SUM(MarketValue_Base) FROM @Holdings)


	RETURN;
END;

GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[tfn_Income_By_Level] (@SecurityList SecurityList Readonly,
	@BeginDate smalldatetime,
	@EndDate smalldatetime)

RETURNS @Income TABLE (
	Income numeric(18,6),
	TradeDate smalldatetime,
	TransCodeName varchar(100),
	EntityID bigint,
	AccountID bigint,
	AccountSecurityID bigint,
	SecurityID bigint
	)

AS
BEGIN
	INSERT INTO @Income (
		Income,
		TradeDate,
		TransCodeName,
		EntityID,
		AccountID,
		AccountSecurityID,
		SecurityID
		)
	SELECT (Trans.Amount_Base * Trans.CalcIncome) as Income, 
		Trans.TradeDate, 
		Trans.TransCodeName, 
		Trans.EntityID, 
		Trans.AccountID, 
		Trans.AccountSecurityID, 
		Trans.SecurityID
	FROM dbo.[tfn_Transactions_By_Level] (
				@SecurityList,
				@BeginDate,
				@EndDate
				) as Trans
	WHERE Trans.CalcIncome <> 0
		AND Trans.TradeDate >= @BeginDate
		AND Trans.TradeDate <= @EndDate
	ORDER BY TradeDate


	RETURN;
END;


GO

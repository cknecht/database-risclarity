SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[tfn_Income_By_SecurityList_By_DateList] (
	@SecurityList SecurityList Readonly,
	@DateList DateList Readonly
	)

RETURNS @Income TABLE (
	AsOfDate smalldatetime,
	Income numeric(18,6),
	TradeDate smalldatetime,
	TransCodeName varchar(100),
	EntityID bigint,
	AccountID bigint,
	AccountSecurityID bigint,
	SecurityID bigint
	)

AS
BEGIN
	INSERT INTO @Income (
		AsOfDate,
		Income
		)
	SELECT DateList.AsOfDate,
		SUM(Trans.Amount_Base * Trans.CalcIncome) as Income
	FROM dbo.[tfn_Transactions_By_Level_By_DateList] (
				@SecurityList,
				@DateList
				) as Trans
		LEFT JOIN @DateList as DateList ON Trans.TradeDate = DateList.AsOfDate
	WHERE Trans.Calcincome<> 0
	GROUP BY DateList.AsOfDate


	RETURN;
END;


GO

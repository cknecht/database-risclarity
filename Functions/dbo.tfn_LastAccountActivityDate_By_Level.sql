SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[tfn_LastAccountActivityDate_By_Level] (
	@Level varchar(1), 
	@LevelID bigint,
	@EndDate SMALLDATETIME,
	@SecurityList Securitylist READONLY,
	@IncludeEstimates BIT = 0,
	@IncludeTransactions BIT = 0
	)

RETURNS @LastPriceDate TABLE
	(
	AccountID bigint,
	AsOfDate smalldatetime
	)
AS
BEGIN 
	INSERT INTO @LastPriceDate
	        ( 
				AccountID,
				AsOfDate
	        )
	SELECT AccountID,
		MAX(AsOfDate) AS AsOfDate
	FROM
	(SELECT SecurityList.AccountID,
		MAX(Price.AsOfDate) AS AsOfDate
	FROM Price
		JOIN (SELECT MAX(AsOfDate) AS AsOfDate, 
					SecurityList.SecurityID	
				FROM Price 
					CROSS JOIN @SecurityList AS SecurityList
				WHERE AsOfDate <= @EndDate
					AND SecurityList.SecurityID = Price.SecurityID
					AND Price.Estimate IN (0, @IncludeEstimates)
				GROUP BY SecurityList.SecurityID
				) AS RecentDates ON Price.SecurityID = RecentDates.SecurityID 
					AND Price.AsOfDate = RecentDates.AsOfDate 
		CROSS JOIN @SecurityList AS SecurityList
	WHERE SecurityList.SecurityID = Price.SecurityID
		AND Price.Estimate IN (0, @IncludeEstimates)
	GROUP BY SecurityList.AccountID
	UNION all
	SELECT SecurityList.AccountID,
		MAX(Trans.TradeDate) AS AsOfDate
	FROM Trans
		JOIN (SELECT MAX(TradeDate) AS AsOfDate, 
					SecurityList.AccountSecurityID	
				FROM Trans 
					CROSS JOIN @SecurityList AS SecurityList
				WHERE Trans.TradeDate <= @EndDate
					AND SecurityList.AccountSecurityID = Trans.AccountSecurityID
				GROUP BY SecurityList.AccountSecurityID
				) AS RecentDates ON Trans.AccountSecurityID = RecentDates.AccountSecurityID 
					AND Trans.TradeDate = RecentDates.AsOfDate 
		CROSS JOIN @SecurityList AS SecurityList
	WHERE SecurityList.AccountSecurityID = Trans.AccountSecurityID
		AND @IncludeTransactions = 1
	GROUP BY SecurityList.AccountID
	) AS AsOfDates
	GROUP BY AccountID

	RETURN;
END


GO

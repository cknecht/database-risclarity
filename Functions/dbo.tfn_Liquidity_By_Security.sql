SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[tfn_Liquidity_By_Security] 
(	
	@AsOfDate datetime
)
RETURNS TABLE 
AS
RETURN 
(
	SELECT Security.SecurityID,
		Security.SecurityName,
		SecurityLiquidity.LiquidityDays,
		SecurityLiquidity.LiquidityHardLockupDate,
		SecurityLiquidity.LiquiditySoftLockupDate,
		SecurityLiquidity.LiquiditySoftLockupPercent,
		SecurityLiquidity.LiquidityNoticePeriodDays,
		SecurityLiquidity.LiquidityGatePercent,
		SecurityLiquidity.LiquidityRedemptionPeriodID,
		LiquidityRedemptionPeriod.LiquidityRedemptionPeriodName,
		'Lime' as LiquidityColor,
		Security.SourceName as SecuritySourceName,
		Security.DataSourceID as SecurityDataSourceID,
		SecurityLiquidity.AsOfDate as AsOfDate
	FROM SecurityLiquidity
		LEFT JOIN LiquidityRedemptionPeriod ON ISNULL(SecurityLiquidity.LiquidityRedemptionPeriodID, 0) = LiquidityRedemptionPeriod.LiquidityRedemptionPeriodID
		JOIN (SELECT SecurityID, LiquidityDays, MAX(AsOfDate) as MaxDate
			FROM SecurityLiquidity
			WHERE AsOfDate <= @AsOfDate
			GROUP BY SecurityID, LiquidityDays) as AsOfDate ON SecurityLiquidity.SecurityID = AsOfDate.SecurityID 
				AND SecurityLiquidity.LiquidityDays = AsOfDate.LiquidityDays
		JOIN Security ON SecurityLiquidity.SecurityID = Security.SecurityID
	WHERE SecurityLiquidity.AsOfDate = AsOfDate
	UNION
	SELECT Security.SecurityID,
		Security.SecurityName,
		0 as LiquidityDays,
		NULL as LiquidityHardLockupDate,
		NULL as LiquiditySoftLockupDate,
		NULL as LiquiditySoftLockupPercent,
		NULL as LiquidityNoticePeriodDays,
		NULL as LiquidityGatePercent,
		NULL as LiquidityRedemptionPeriodID,
		NULL as LiquidityRedemptionPeriodName,
		'Black' as LiquidityColor,
		Security.SourceName as SecuritySourceName,
		Security.DataSourceID as SecurityDataSourceID,
		NULL as AsOfDate
	FROM Security 
	LEFT JOIN SecurityLiquidity ON SecurityLiquidity.SecurityID = Security.SecurityID
	WHERE ISNULL(SecurityLiquidity.LiquidityDays, -1) = -1
)

GO

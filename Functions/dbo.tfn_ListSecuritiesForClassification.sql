SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE FUNCTION [dbo].[tfn_ListSecuritiesForClassification] (@AsOfDate smalldatetime)

RETURNS @Classification TABLE (
	FamilyName varchar(100),
	EntityName varchar(100),
	AccountName varchar(100),
	SecurityID bigint,
	SecurityName varchar(100),
	SecurityTypeName varchar(100),
	RegionName varchar(100),
	SectorName varchar(100),
	AssetClassName varchar(100),
	LiquidityDays int,
	OPTIONAL_Fields_Follow varchar(100),
	LiquidityHardLockupDate smalldatetime,
	LiquiditySoftLockupDate smalldatetime,
	LiquiditySoftLockupPercent numeric(18,6),
	LiquidityNoticePeriodDays int,
	LiquidityGate numeric(18,6)
	) 

 AS
BEGIN

INSERT INTO @Classification (
	FamilyName,
	EntityName,
	AccountName,
	SecurityID,
	SecurityName,
	SecurityTypeName,
	RegionName,
	SectorName,
	AssetClassName,
	LiquidityDays,
	OPTIONAL_Fields_Follow,
	LiquidityHardLockupDate,
	LiquiditySoftLockupDate,
	LiquiditySoftLockupPercent,
	LiquidityNoticePeriodDays,
	LiquidityGate
)
SELECT Family.FamilyName,
	Entity.EntityName AS EntityName,--Entity.EntityName,
	Account.AccountName AS AccountName,--Account.AccountName,
	Security.SecurityID,
	Security.SecurityName,
	SecurityType.SecurityTypeName,
	Region.RegionName,
	Sector.SectorName,
	AssetClass.AssetClassName,
	Liquidity.LiquidityDays,
	'' as OPTIONAL_Fields_Follow,
	Liquidity.LiquidityHardLockupDate,
	Liquidity.LiquiditySoftLockupDate,
	Liquidity.LiquiditySoftLockupPercent,
	Liquidity.LiquidityNoticePeriodDays,
	Liquidity.LiquidityGatePercent
FROM Family
	JOIN FamilyEntity ON Family.FamilyID = FamilyEntity.FamilyID
	JOIN Entity ON FamilyEntity.EntityID = Entity.EntityID
	JOIN EntityAccount ON Entity.EntityID = EntityAccount.EntityID
	JOIN Account ON EntityAccount.AccountID = Account.AccountID
	JOIN AccountSecurity ON Account.AccountID = AccountSecurity.AccountID
	JOIN Security ON AccountSecurity.SecurityID = Security.SecurityID
	LEFT JOIN SecurityType ON Security.SecurityTypeID = SecurityType.SecurityTypeID
	LEFT JOIN [dbo].[tfn_Liquidity_By_Security] (@AsOfDate) as Liquidity ON Security.SecurityID = Liquidity.SecurityID
	LEFT JOIN [dbo].[tfn_Region_By_Security] (@AsOfDate) as Region ON Security.SecurityID = Region.SecurityID
	LEFT JOIN [dbo].[tfn_Sector_By_Security] (@AsOfDate) as Sector ON Security.SecurityID = Sector.SecurityID
	LEFT JOIN [dbo].[tfn_AssetClass_By_Security] (@AsOfDate) as AssetClass ON Security.SecurityID = AssetClass.SecurityID
WHERE Security.SecurityID <> 143
GROUP BY 
	Family.FamilyName,
	Entity.EntityName,
	Account.AccountName,
	Security.SecurityID,
	Security.SecurityName,
	SecurityType.SecurityTypeName,
	Region.RegionName,
	Sector.SectorName,
	AssetClass.AssetClassName,
	Liquidity.LiquidityDays,
	Liquidity.LiquidityHardLockupDate,
	Liquidity.LiquiditySoftLockupDate,
	Liquidity.LiquiditySoftLockupPercent,
	Liquidity.LiquidityNoticePeriodDays,
	Liquidity.LiquidityGatePercent


	RETURN;
END

GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[tfn_PeriodList_Months] (
	@BeginDate smalldatetime,
	@EndDate smalldatetime
	)
RETURNS @PeriodList TABLE
	(
	BeginDate smalldatetime,
	EndDate smalldatetime
	)
AS
BEGIN 

	WHILE @BeginDate <= @EndDate
	BEGIN
		INSERT INTO @PeriodList (
			BeginDate, 
			EndDate
			) 
		VALUES (
			[dbo].[sfn_MonthBegin](@BeginDate),
			[dbo].[sfn_MonthEnd](@BeginDate)
			);

		SET @BeginDate = DATEADD(month, 1, @BeginDate)
	END 

	RETURN;
END;


GO

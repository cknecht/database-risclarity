SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[tfn_PeriodList_Months_By_DateList] (@DateList datelist readonly)
RETURNS @PeriodList TABLE
	(
	BeginDate smalldatetime,
	EndDate smalldatetime
	)
AS
BEGIN 
	DECLARE @BeginDate smalldatetime
	DECLARE @EndDate smalldatetime

	--Set the BeginDate and EndDate based on the first and last entry in the @DateList table
	SELECT @BeginDate = MIN(AsOfDate),
		@EndDate = MAX(AsOfDate)
	FROM @DateList

	WHILE @BeginDate <= @EndDate
	BEGIN
		INSERT INTO @PeriodList (
			BeginDate, 
			EndDate
			) 
		VALUES (
			[dbo].[sfn_MonthBegin](@BeginDate),
			[dbo].[sfn_MonthEnd](@BeginDate)
			);

		SET @BeginDate = DATEADD(month, 1, @BeginDate)
	END 

	RETURN;
END;


GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[tfn_PeriodList_PerformanceReview] (
	@EndDate SMALLDATETIME,
	@MTD BIT,
	@QTD BIT,
	@YTD BIT,
	@1YR BIT,
	@3YR BIT,
	@5YR BIT,
	@10YR BIT,
	@ITD BIT
	)
RETURNS @PeriodList TABLE
	(
	BeginDate smalldatetime,
	EndDate smalldatetime
	)
AS
BEGIN 

	DECLARE @BeginDate smalldatetime
	DECLARE @Index int = 1
	DECLARE @MaxIndex int = 7

	WHILE @Index <= @MaxIndex
	BEGIN

	
		SELECT @BeginDate = CASE @Index
					--Month to Date
					WHEN 1 THEN 
						CASE WHEN @MTD = 1 THEN [dbo].[sfn_MonthBegin](@EndDate) END
					--Quarter to Date
					WHEN 2 THEN 
						CASE WHEN @QTD = 1 THEN [dbo].[sfn_QuarterBegin] (@EndDate) END
					--1 Year
					WHEN 3 THEN 
						CASE WHEN @1YR = 1 THEN [dbo].[sfn_PreviousYear](@EndDate, 1) END
					--3 Year
					WHEN 4 THEN 
						CASE WHEN @3YR = 1 THEN [dbo].[sfn_PreviousYear](@EndDate, 3) END
					--5 Year
					WHEN 5 THEN 
						CASE WHEN @5YR = 1 THEN [dbo].[sfn_PreviousYear](@EndDate, 5) END
					--10 Year
					WHEN 6 THEN 
						CASE WHEN @10YR = 1 THEN [dbo].[sfn_PreviousYear](@EndDate, 10) END
					-- YTD
					WHEN 7 THEN 
						CASE WHEN @YTD = 1 THEN [dbo].[sfn_YearBegin](@EndDate) END
					END

		
		IF @BeginDate IS NOT NULL
		BEGIN
		
			INSERT INTO @PeriodList (
				BeginDate, 
				EndDate
				) 
			SELECT
				InsertDates.BeginDate,
				InsertDates.EndDate
			FROM (SELECT @BeginDate AS BeginDate, 
				@EndDate AS EndDate) AS InsertDates
				LEFT JOIN @PeriodList AS PeriodList ON InsertDates.BeginDate = PeriodList.BeginDate
					AND InsertDates.EndDate = PeriodList.EndDate	
			WHERE ISNULL(PeriodList.BeginDate, '1/1/1900') = '1/1/1900' 
		END

		SELECT @Index = @Index + 1
	END 

	RETURN;
END;

GO

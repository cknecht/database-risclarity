SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE FUNCTION [dbo].[tfn_PortfolioEntity]
(	
	@AsOfDate datetime
)
RETURNS TABLE 
AS
RETURN 
(
	SELECT PortfolioEntity.PortfolioID,
		PortfolioEntity.EntityID,
		PortfolioEntity.PercentOwnership,
		PortfolioEntity.AsOfDate
	FROM PortfolioEntity 
		LEFT JOIN (SELECT PortfolioID, EntityID, MAX(AsOfDate) as MaxDate
			FROM PortfolioEntity
			WHERE AsOfDate <= @AsOfDate
			GROUP BY PortfolioID, EntityID) as AsOfDate ON PortfolioEntity.EntityID = AsOfDate.EntityID 
				AND PortfolioEntity.PortfolioID = AsOfDate.PortfolioID
	WHERE PortfolioEntity.AsOfDate = AsOfDate.MaxDate
	)
GO

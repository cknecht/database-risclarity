SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE FUNCTION [dbo].[tfn_PriceList] (
	@SecurityList securitylist READONLY, 
	@PeriodList periodlist READONLY,
	@TransactionList transactionlist READONLY
	)
RETURNS @Price TABLE
	(
	AsOfDate smalldatetime,
	SecurityID bigint,
	PriceAsOfDate smalldatetime,
	Price numeric(18,6),
	Price_CurrencyID int
	)
AS
BEGIN 

	DECLARE @BeginDate smalldatetime,
		@EndDate smalldatetime
	
	SELECT @BeginDate = DATEADD(day, -1, MIN(BeginDate)),
		@EndDate = MAX(EndDate)
	FROM @PeriodList

	INSERT INTO @Price (
		AsOfDate,
		SecurityID,
		PriceAsOfDate,
		Price,
		Price_CurrencyID
		)
	SELECT AsOfDate.AsOfDate,
		Price.SecurityID,
		Price.AsOfDate as PriceAsOfDate,
		Price.Price,
		Price.Price_CurrencyID
	FROM  (
			SELECT DateList.AsOfDate as AsOfDate,
					Price.SecurityID,
					MAX(Price.AsOfDate) as PriceAsOfDate 
				FROM @SecurityList as AccountSecurity
					CROSS JOIN (-- Make sure we have a beginning price
							SELECT DateAdd(Day, 1, @BeginDate) as AsOfDate
							UNION
							--Add all the EndDates from @PeriodList
							SELECT EndDate as AsOfDate
							FROM @PeriodList
							UNION
							--Add all the BeginDates from @PeriodList
							SELECT BeginDate as AsOfDate
							FROM @PeriodList
							UNION
							-- Add in any day we have a price entered
							SELECT Price.AsOfDate as AsOfDate
							FROM @SecurityList as AccountSecurity
								JOIN Price as Price ON AccountSecurity.SecurityID = Price.SecurityID
							WHERE Price.AsOfDate > @BeginDate
								AND Price.AsOfDate <= @EndDate
							UNION 
							--Add in any day we have a cash flow
							SELECT TradeDate as AsOfDate
							FROM @TransactionList as Price
							--WHERE (CalcCashFlow <> 0
							--	OR CalcIncome <> 0
							--	OR CalcFee <> 0)
							GROUP BY Price.TradeDate
							) as Datelist
					JOIN Price as Price ON AccountSecurity.SecurityID = Price.SecurityID
									AND Price.AsOfDate <= DateList.AsOfDate
				GROUP BY Price.SecurityID,
					DateList.AsOfDate
			) 
			AS AsOfDate 
			JOIN Price ON Price.SecurityID = AsOfDate.SecurityID 
						AND Price.AsOfDate = AsOfDate.PriceAsOfDate
						AND Price.AsOfDate >= @BeginDate
	GROUP BY AsOfDate.AsOfDate, 
		Price.SecurityID,
		Price.AsOfDate,
		Price.Price,
		Price.Price_CurrencyID
	ORDER BY PriceAsOfDate, SecurityID


	RETURN;
END;



GO

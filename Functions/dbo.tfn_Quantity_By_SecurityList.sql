SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[tfn_Quantity_By_SecurityList] (
	@SecurityList securitylist READONLY
	)

RETURNS @Quantity TABLE
	(
	AsOfDate smalldatetime,
	AccountSecurityID bigint,
	EntityID bigint,
	AccountID bigint,
	SecurityID bigint,
	Quantity numeric(18,6),
	QuantityOfOne bit,
	CashFlow numeric(18,6),
	Addition numeric(18,6),
	Withdrawal numeric(18,6),
	Income numeric(18,6),
	AccruedIncome numeric(18,6)
	)
AS
BEGIN 
	INSERT INTO @Quantity (
		AsOfDate,
		AccountSecurityID,
		EntityID,
		AccountID,
		SecurityID,
		Quantity,
		QuantityOfOne
		)
	SELECT Datelist.AsOfDate,
		SecurityList.AccountSecurityID,  
		SecurityList.EntityID,
		SecurityList.AccountID,
		SecurityList.SecurityID,
		CASE WHEN ISNULL(SecurityList.QuantityofOne, 0) = 1
			THEN 1
			ELSE SUM(Trans.Quantity * TransCode.CalcQuantity)
			END as Quantity,
		ISNULL(SecurityList.QuantityofOne, 0)
	FROM Trans as Trans
		JOIN TransCode ON Trans.TransCodeID = TransCode.TransCodeID
		JOIN @SecurityList as SecurityList ON Trans.AccountSecurityID = SecurityList.AccountSecurityID
		JOIN (
				SELECT Trans.TradeDate as AsOfDate, 
					SecurityList.SecurityID,
					SecurityList.AccountSecurityID
				FROM Trans as Trans
					JOIN @SecurityList as SecurityList ON Trans.AccountSecurityID = SecurityList.AccountSecurityID
				GROUP BY Trans.TradeDate,
					SecurityList.SecurityID,
					SecurityList.AccountSecurityID
				) as DateList ON Trans.AccountSecurityID = DateList.AccountSecurityID
						AND Trans.TradeDate <= DateList.AsOfDate
	WHERE ISNULL(SecurityList.QuantityOfOne, 0) = 0
	GROUP BY DateList.AsOfDate,
		SecurityList.AccountSecurityID,
		SecurityList.EntityID,
		SecurityList.AccountID,
		SecurityList.SecurityID,
		SecurityList.QuantityOfOne
	UNION
	SELECT Datelist.AsOfDate,
		SecurityList.AccountSecurityID,  
		SecurityList.EntityID,
		SecurityList.AccountID,
		SecurityList.SecurityID,
		(1 * SUM(TransCode.CalcQuantityOfOne)) + 1 as Quantity,
		SecurityList.QuantityofOne
	FROM Trans as Trans
		JOIN TransCode ON Trans.TransCodeID = TransCode.TransCodeID
		JOIN @SecurityList as SecurityList ON Trans.AccountSecurityID = SecurityList.AccountSecurityID
		JOIN (
				SELECT Trans.TradeDate as AsOfDate, 
					SecurityList.SecurityID,
					SecurityList.AccountSecurityID
				FROM Trans as Trans
					JOIN @SecurityList as SecurityList ON Trans.AccountSecurityID = SecurityList.AccountSecurityID
				GROUP BY Trans.TradeDate,
					SecurityList.SecurityID,
					SecurityList.AccountSecurityID
				) as DateList ON Trans.AccountSecurityID = DateList.AccountSecurityID
						AND Trans.TradeDate <= DateList.AsOfDate
	WHERE ISNULL(SecurityList.QuantityOfOne, 0) = 1
	GROUP BY DateList.AsOfDate,
		SecurityList.AccountSecurityID,
		SecurityList.EntityID,
		SecurityList.AccountID,
		SecurityList.SecurityID,
		SecurityList.QuantityOfOne


	RETURN;
END;


GO

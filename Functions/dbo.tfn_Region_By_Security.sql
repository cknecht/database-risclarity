SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE FUNCTION [dbo].[tfn_Region_By_Security] 
(	
	@AsOfDate datetime
)
RETURNS TABLE 
AS
RETURN 
(
	SELECT Security.SecurityID,
		Security.SecurityName,
		Region.RegionID,
		Region.RegionName,
		Region.SortOrder as RegionSortOrder,
		Region.RegionColor as RegionColor,
		Security.SourceName as SecuritySourceName,
		Security.DataSourceID as SecurityDataSourceID,
		Region.SourceName as regionSourceName,
		SecurityRegion.AsOfDate as AsOfDate
	FROM SecurityRegion
		 JOIN (SELECT SecurityRegion.SecurityID, 
			SecurityRegion.RegionID, 
			SecurityRegion.AsOfDate
				FROM SecurityRegion 
					JOIN (SELECT SecurityRegion.SecurityID, 
						MAX(AsOfDate) as MaxDate
					FROM SecurityRegion
					WHERE SecurityRegion.AsOfDate <= @AsOfDate
					GROUP BY SecurityRegion.SecurityID) as MaxAsOfDate ON SecurityRegion.SecurityID = MaxAsOfDate.SecurityID
						AND SecurityRegion.AsOfDate = MaxAsOfDate.MaxDate
					) AS AsOfDate ON SecurityRegion.SecurityID = AsOfDate.SecurityID 
						AND SecurityRegion.RegionID = AsOfDate.RegionID
		JOIN Region ON SecurityRegion.RegionID = Region.RegionID
		JOIN Security ON SecurityRegion.SecurityID = Security.SecurityID
	WHERE SecurityRegion.AsOfDate = AsOfDate.AsOfDate
	UNION
	SELECT Security.SecurityID,
		Security.SecurityName,
		0 as RegionID,
		'Unknown' as RegionName,
		0 as RegionSortOrder,
		'Black' as RegionColor,
		Security.SourceName as SecuritySourceName,
		Security.DataSourceID as SecurityDataSourceID,
		'Unknown' as RegionSourceName,
		NULL as AsOfDate
	FROM Security 
	LEFT JOIN SecurityRegion ON Securityregion.SecurityID = Security.SecurityID
	WHERE ISNULL(SecurityRegion.RegionID, -1) = -1
)


GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE FUNCTION [dbo].[tfn_Sector_By_Security]
(	
	@AsOfDate datetime
)
RETURNS TABLE 
AS
RETURN 
(
	SELECT Security.SecurityID,
		Security.SecurityName,
		Sector.SectorID,
		Sector.SectorName,
		Sector.SortOrder as SectorSortOrder,
		Sector.SectorColor as SectorColor,
		--Sector.Image256 as SectorImage256
		Security.SourceName as SecuritySourceName,
		Security.DataSourceID as SecurityDataSourceID,
		Sector.SourceName as SectorSourceName,
		SecuritySector.AsOfDate as AsOfDate
	FROM SecuritySector
		JOIN (SELECT SecuritySector.SecurityID, 
			SecuritySector.SectorID, 
			SecuritySector.AsOfDate
				FROM SecuritySector 
					JOIN (SELECT SecuritySector.SecurityID, 
						MAX(AsOfDate) as MaxDate
					FROM SecuritySector
					WHERE SecuritySector.AsOfDate <= @AsOfDate
					GROUP BY SecuritySector.SecurityID) as MaxAsOfDate ON SecuritySector.SecurityID = MaxAsOfDate.SecurityID
						AND SecuritySector.AsOfDate = MaxAsOfDate.MaxDate
					) AS AsOfDate ON SecuritySector.SecurityID = AsOfDate.SecurityID 
						AND SecuritySector.SectorID = AsOfDate.SectorID
		JOIN Sector ON SecuritySector.SectorID = Sector.SectorID
		JOIN Security ON SecuritySector.SecurityID = Security.SecurityID
	WHERE SecuritySector.AsOfDate = AsOfDate.AsOfDate
	UNION 
	SELECT Security.SecurityID,
		Security.SecurityName,
		0 as SectorID,
		'Unknown' as SectorName,
		0 as SectorSortOrder,
		'Black' as SectorColor,
		--'' as SectorImage256
		Security.SourceName as SecuritySourceName,
		Security.DataSourceID as SecurityDataSourceID,
		'Unknown' as SectorSourceName,
		NULL as AsOfDate
	FROM Security 
	LEFT JOIN SecuritySector ON SecuritySector.SecurityID = Security.SecurityID
	WHERE ISNULL(SecuritySector.SectorID, -1) = -1
)

GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[tfn_SecurityList_by_Level] (@Level varchar(10), @LevelID bigint, @AsOfDate SMALLDATETIME, @IncludeLinkedAccounts BIT)
RETURNS @SecurityList TABLE
	(
	FamilyID	bigint,
	FamilyName	varchar(max),
	EntityID	bigint,
	EntityName	varchar(max),
	AccountID	bigint,
	AccountName varchar(max),
	AccountSecurityID bigint,
	SecurityID	bigint,
	SecurityName	varchar(max),
	SecuritySymbol VARCHAR(100),
	SecurityCUSIP VARCHAR(100),
	SecuritySEDOL VARCHAR(100),
	SecurityISIN VARCHAR(100),
	SectorName varchar(155),
	SectorID	bigint,
	SectorSortOrder int,
	SectorColor varchar(50),
	RegionName varchar(255),
	RegionID bigint,
	RegionSortOrder int,
	RegionColor varchar(50),
	AssetClassName varchar(255),
	AssetClassID bigint,
	AssetClassSortOrder int,
	AssetClassColor varchar(50),
	QuantityOfOne BIT,
	PriceFactor NUMERIC(18,6),
	PercentOwnership NUMERIC(18,6),
	ExcludeFromPerformance BIT,
	AccountInceptionDate SMALLDATETIME,
	TaxLotReliefMethodID TINYINT,
	CalcTaxLots BIT,
	ContractSize NUMERIC(18,6)
	)
AS
BEGIN 

	IF @Level = 'H'
	BEGIN
		INSERT INTO @SecurityList 
		(
		FamilyID,
		FamilyName,
		EntityID,
		EntityName,
		AccountID,
		AccountName,
		AccountSecurityID,
		SecurityID,
		SecurityName,
		SecuritySymbol,
		SecurityCUSIP,
		SecuritySEDOL,
		SecurityISIN,
		SectorName,
		SectorID,
		SectorSortOrder,
		SectorColor,
		RegionName,
		RegionID,
		RegionSortOrder,
		RegionColor,
		AssetClassName,
		AssetClassID,
		AssetClassSortOrder,
		AssetClassColor,
		QuantityOfOne,
		PriceFactor,
		PercentOwnership,
		ExcludeFromPerformance,
		AccountInceptionDate,
		TaxLotReliefMethodID,
		CalcTaxLots,
		ContractSize
		)
	SELECT Family.FamilyID as FamilyID,
		Family.FamilyName as FamilyName,
		Entity.EntityID as EntityID,
		Entity.EntityName as EntityName,
		Account.AccountID as AccountID,
		Account.AccountName as AccountName,
		AccountSecurity.AccountSecurityID as AccountSecurityID,
		Security.SecurityID as SecurityID,
		Security.SecurityName as SecurityName,
		Security.Symbol AS SecuritySymbol,
		Security.CUSIP AS SecurityCUSIP,
		Security.SEDOL AS SecuritySEDOL,
		Security.ISIN AS SecurityISIN,
		Sector.SectorName,
		Sector.SectorID,
		Sector.SectorSortOrder,
		Sector.SectorColor,
		Region.RegionName,
		Region.RegionID,
		Region.RegionSortOrder,
		Region.RegionColor,
		AssetClass.AssetClassName,
		AssetClass.AssetClassID,
		AssetClass.AssetClassSortOrder,
		AssetClass.AssetClassColor,
		ISNULL(Security.QuantityOfOne, 0),
		ISNULL(Security.PriceFactor, 1),
		100 AS PercentOwnership,
		CASE WHEN ISNULL(Account.ExcludeFromPerformance, 0) = 1
		THEN 1
		ELSE CASE WHEN ISNULL(Security.ExcludeFromPerformance, 0) = 1
			THEN 1
			ELSE ISNULL(AccountSecurity.ExcludeFromPerformance, 0)
			END
		END AS ExcludeFromPerformance,
		Account.InceptionDate,
		AccountSecurity.TaxLotReliefMethodID,
		Security.CalcTaxLots,
		ISNULL(Security.ContractSize, 1)
	FROM AccountSecurity
		JOIN Account ON AccountSecurity.AccountID = Account.AccountID
		JOIN EntityAccount ON AccountSecurity.AccountID = EntityAccount.AccountID
		JOIN Entity ON EntityAccount.EntityID = Entity.EntityID
		JOIN FamilyEntity ON Entity.EntityID = FamilyEntity.EntityID
		JOIN Family ON FamilyEntity.FamilyID = Family.FamilyID
		JOIN Security ON AccountSecurity.SecurityID = Security.SecurityID
		LEFT JOIN [dbo].[tfn_Sector_By_Security](@AsOfDate) as Sector ON AccountSecurity.SecurityID = Sector.SecurityID
		LEFT JOIN [dbo].[tfn_Region_By_Security](@AsOfDate) as Region ON AccountSecurity.SecurityID = Region.SecurityID
		LEFT JOIN [dbo].[tfn_AssetClass_By_Security](@AsOfDate) as AssetClass ON AccountSecurity.SecurityID = AssetClass.SecurityID
	WHERE AccountSecurity.AccountSecurityID = @LevelID
	GROUP BY Family.FamilyID,
		Family.FamilyName,
		Entity.EntityID,
		Entity.EntityName,
		Account.AccountID,
		Account.AccountName,
		AccountSecurity.AccountSecurityID,
		Security.SecurityID,
		Security.SecurityName,
		Security.Symbol,
		Security.CUSIP,
		Security.SEDOL,
		Security.ISIN,
		Sector.SectorName,
		Sector.SectorID,
		Sector.SectorSortOrder,
		Sector.SectorColor,
		Region.RegionName,
		Region.RegionID,
		Region.RegionSortOrder,
		Region.RegionColor,
		AssetClass.AssetClassName,
		AssetClass.AssetClassID,
		AssetClass.AssetClassSortOrder,
		AssetClass.AssetClassColor,
		Security.QuantityOfOne,
		Security.PriceFactor,
		Account.ExcludeFromPerformance,
		Security.ExcludeFromPerformance,
		AccountSecurity.ExcludeFromPerformance,
		Account.InceptionDate,
		AccountSecurity.TaxLotReliefMethodID,
		Security.CalcTaxLots,
		Security.ContractSize

	END
	ELSE
	BEGIN

		-- Find all the Accounts in the Level
		DECLARE @AccountList AccountList

		INSERT INTO @AccountList (
			AccountID,
			AccountName,
			EntityID,
			FamilyID,
			AccountNumber,
			AccountDescription,
			PercentOwnership,
			ExcludeFromPerformance,
			InceptionDate,
			TaxLotReliefMethodID
			)
		SELECT AccountID, 
			AccountName,
			EntityID,
			FamilyID,
			AccountNumber,
			AccountDescription,
			PercentOwnership,
			ExcludeFromPerformance,
			InceptionDate,
			TaxLotReliefMethodID
		FROM dbo.[tfn_AccountList_by_Level] (@Level, @LevelID, @AsOfDate, @IncludeLinkedAccounts)

		-- Find all the Securities from the AccountList
		INSERT INTO @SecurityList 
			(
			FamilyID,
			FamilyName,
			EntityID,
			EntityName,
			AccountID,
			AccountName,
			AccountSecurityID,
			SecurityID,
			SecurityName,
			SecuritySymbol,
			SecurityCUSIP,
			SecuritySEDOL,
			SecurityISIN,
			SectorName,
			SectorID,
			SectorSortOrder,
			SectorColor,
			RegionName,
			RegionID,
			RegionSortOrder,
			RegionColor,
			AssetClassName,
			AssetClassID,
			AssetClassSortOrder,
			AssetClassColor,
			QuantityOfOne,
			PriceFactor,
			PercentOwnership,
			ExcludeFromPerformance,
			AccountInceptionDate,
			TaxLotReliefMethodID,
			CalcTaxLots,
			ContractSize
			)
		SELECT Family.FamilyID as FamilyID,
			Family.FamilyName as FamilyName,
			Entity.EntityID as EntityID,
			Entity.EntityName as EntityName,
			Account.AccountID as AccountID,
			Account.AccountName as AccountName,
			AccountSecurity.AccountSecurityID as AccountSecurityID,
			Security.SecurityID as SecurityID,
			Security.SecurityName as SecurityName,
			Security.Symbol AS SecuritySymbol,
			Security.CUSIP AS SecurityCUSIP,
			Security.SEDOL AS SecuritySEDOL,
			Security.ISIN AS SecurityISIN,
			Sector.SectorName,
			Sector.SectorID,
			Sector.SectorSortOrder,
			Sector.SectorColor,
			Region.RegionName,
			Region.RegionID,
			Region.RegionSortOrder,
			Region.RegionColor,
			AssetClass.AssetClassName,
			AssetClass.AssetClassID,
			AssetClass.AssetClassSortOrder,
			AssetClass.AssetClassColor,
			Security.QuantityOfOne,
			Security.PriceFactor,
			AccountList.PercentOwnership AS PercentOwnership,
			AccountList.ExcludeFromPerformance,
			AccountList.InceptionDate,
			CASE WHEN ISNULL(AccountSecurity.TaxLotReliefMethodID, 0) = 0
				THEN 1 --AccountList.TaxLotReliefMethodID,
				ELSE AccountSecurity.TaxLotReliefMethodID
				END AS TaxLotReliefMethodID,
			Security.CalcTaxLots,
			ISNULL(Security.ContractSize, 1)
		FROM AccountSecurity
			JOIN @AccountList as AccountList ON AccountSecurity.AccountID = AccountList.AccountID
			JOIN Account ON AccountList.AccountID = Account.AccountID
			JOIN EntityAccount ON AccountList.AccountID = EntityAccount.AccountID
			JOIN Entity ON EntityAccount.EntityID = Entity.EntityID
			JOIN FamilyEntity ON Entity.EntityID = FamilyEntity.EntityID
			JOIN Family ON FamilyEntity.FamilyID = Family.FamilyID
			JOIN Security ON AccountSecurity.SecurityID = Security.SecurityID
			LEFT JOIN [dbo].[tfn_Sector_By_Security](@AsOfDate) as Sector ON AccountSecurity.SecurityID = Sector.SecurityID
			LEFT JOIN [dbo].[tfn_Region_By_Security](@AsOfDate) as Region ON AccountSecurity.SecurityID = Region.SecurityID
			LEFT JOIN [dbo].[tfn_AssetClass_By_Security](@AsOfDate) as AssetClass ON AccountSecurity.SecurityID = AssetClass.SecurityID

		GROUP BY Family.FamilyID,
			Family.FamilyName,
			Entity.EntityID,
			Entity.EntityName,
			Account.AccountID,
			Account.AccountName,
			AccountSecurity.AccountSecurityID,
			Security.SecurityID,
			Security.SecurityName,
			Security.Symbol,
			Security.CUSIP,
			Security.SEDOL,
			Security.ISIN,
			Sector.SectorName,
			Sector.SectorID,
			Sector.SectorSortOrder,
			Sector.SectorColor,
			Region.RegionName,
			Region.RegionID,
			Region.RegionSortOrder,
			Region.RegionColor,
			AssetClass.AssetClassName,
			AssetClass.AssetClassID,
			AssetClass.AssetClassSortOrder,
			AssetClass.AssetClassColor,
			Security.QuantityOfOne,
			Security.PriceFactor,
			AccountList.ExcludeFromPerformance,
			AccountList.PercentOwnership,
			AccountList.InceptionDate,
			AccountSecurity.TaxLotReliefMethodID,
			Security.CalcTaxLots,
			Security.ContractSize
	END


	RETURN;
END;

GO

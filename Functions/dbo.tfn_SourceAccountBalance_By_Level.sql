SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[tfn_SourceAccountBalance_By_Level] (
	@Level varchar(1), 
	@LevelID bigint,
	@AsOfDate SMALLDATETIME,
	@IncludeLinkedAccounts bit
	)
RETURNS @SourceAccountBalance TABLE
	(
	AccountID bigint,
	AsOfDate smalldatetime,
	MarketValue numeric(18,6),
	CurrencyCode varchar(3),
	DateTimeEntered SMALLDATETIME,
	SourceName VARCHAR(100)
	)
AS
BEGIN 
	INSERT INTO @SourceAccountBalance
	        ( AsOfDate ,
	          AccountID ,
	          MarketValue ,
	          DateTimeEntered ,
	          CurrencyCode,
			  SourceName
	        )
	SELECT SourceAccountBalance.AsOfDate AS AsOfDate,
		AccountList.AccountID AS AccountID,
		MAX(SourceAccountBalance.MarketValue) AS MarketValue,
		SourceAccountBalance.DateTimeEntered AS DateTimeEntered,
		SourceAccountBalance.CurrencyCode AS CurrencyCode,
		SourceAccountBalance.SourceName AS SourceName
	FROM [dbo].[tfn_AccountList_by_Level] (@Level, @LevelID, @AsOfDate, @IncludeLinkedAccounts) AS AccountList
		JOIN dbo.SourceAccountBalance ON AccountList.AccountID = SourceAccountBalance.AccountID
		JOIN (SELECT MAX(AsOfDate) AS AsOfDate, 
				AccountID	
				FROM SourceAccountBalance 
				WHERE AsOfDate <= @AsOfDate
				GROUP BY AccountID
				) AS RecentDates ON SourceAccountBalance.AccountID = RecentDates.AccountID 
					AND SourceAccountBalance.AsOfDate = RecentDates.AsOfDate
	GROUP BY SourceAccountBalance.AsOfDate,
		AccountList.AccountID,
		SourceAccountBalance.DateTimeEntered,
		SourceAccountBalance.CurrencyCode,
		SourceAccountBalance.SourceName


	RETURN;
END

GO

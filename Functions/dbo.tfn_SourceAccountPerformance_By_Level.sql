SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[tfn_SourceAccountPerformance_By_Level] (
	@Level varchar(1), 
	@LevelID bigint,
	@AsOfDate SMALLDATETIME,
	@IncludeLinkedAccounts BIT,
	@PeriodList PeriodList READONLY
	)
RETURNS @SourceAccountPerformance TABLE
	(
	AccountID bigint,
	BeginDate smalldatetime,
	EndDate SMALLDATETIME,
	PeriodReturn numeric(18,6),
	PeriodText varchar(3),
	DateTimeEntered smalldatetime
	)
AS
BEGIN 
	INSERT INTO @SourceAccountPerformance
	        ( BeginDate ,
			  EndDate ,
	          AccountID ,
	          PeriodReturn ,
	          DateTimeEntered 
	        )
	SELECT SourceAccountPerformance.BeginDate AS BeginDate,
		SourceAccountPerformance.EndDate AS EndDate,
		AccountList.AccountID AS AccountID,
		MAX(SourceAccountPerformance.PeriodReturn) AS MarketValue,
		SourceAccountPerformance.DateTimeEntered AS DateTimeEntered
	FROM [dbo].[tfn_AccountList_by_Level] (@Level, @LevelID, @AsOfDate, @IncludeLinkedAccounts) AS AccountList
		JOIN dbo.SourceAccountPerformance ON AccountList.AccountID = SourceAccountPerformance.AccountID
		CROSS JOIN @PeriodList AS PeriodList
	WHERE PeriodList.BeginDate = SourceAccountPerformance.BeginDate
		AND PeriodList.EndDate = SourceAccountPerformance.EndDate
	GROUP BY SourceAccountPerformance.BeginDate,
		SourceAccountPerformance.EndDate,
		AccountList.AccountID,
		SourceAccountPerformance.DateTimeEntered


	RETURN;
END

GO

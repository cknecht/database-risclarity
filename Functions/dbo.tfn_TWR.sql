SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[tfn_TWR] (
	@SecurityList SecurityList Readonly,
	@PeriodList PeriodList Readonly
	)

RETURNS @TWR TABLE (
	TWR_ID bigint identity(1,1),
	BeginDate smalldatetime,
	EndDate smalldatetime,
	BeginMarketValue numeric(18,6),
	EndMarketValue numeric(18,6),
	CashFlow numeric(18,6),
	Income numeric(18,6),
	AccruedIncome numeric(18,6),
	Fees numeric(18,6)
	)

AS
BEGIN
	DECLARE @DateList DateList
	DECLARE @BeginDate smalldatetime
	DECLARE @EndDate smalldatetime

	SELECT @BeginDate = MIN(BeginDate),
		@EndDate = MAX(EndDate)
	FROM @PeriodList

	INSERT INTO @DateList (AsOfDate)
	SELECT AsOfDate
	FROM [tfn_DateList] (@BeginDate, @EndDate)

	INSERT INTO @TWR (
		EndDate,
		EndMarketValue,
		CashFlow,
		Fees,
		Income
		)
	SELECT MarketValue.AsOfDate, 
		SUM(MarketValue.MarketValue_Base) as EndMarketValue,
		ISNULL(Trans.CashFlow, 0) as CashFlow,
		ISNULL(Trans.Fees, 0) as Fees,
		ISNULL(Trans.Income, 0) as Income
	FROM tfn_DisplayMarketValue_By_SecurityList_By_PeriodList (
			@SecurityList,
			@PeriodList
			) as MarketValue
		LEFT JOIN (SELECT SUM(Amount_Base * CalcCashFlow) as CashFlow,
						SUM(Amount_Base * CalcIncome) as Income,
						SUM(Amount_Base * CalcFee) as Fees,
						TradeDate 
					FROM dbo.[tfn_Transactions_By_Level_By_DateList] (
								@SecurityList,
								@DateList
								) as Trans
					WHERE (Trans.CalcCashFlow <> 0 OR Trans.CalcFee <> 0 OR Trans.CalcIncome <> 0)
					GROUP BY TradeDate
					) as Trans ON MarketValue.AsOfDate = Trans.TradeDate
		--LEFT JOIN dbo.[tfn_CashFlow_By_SecurityList_By_DateList] (
		--			@SecurityList,
		--			@DateList
		--			) as CashFlow ON MarketValue.AsOfDate = CashFlow.AsOfDate
		--LEFT JOIN dbo.[tfn_Fees_By_SecurityList_By_DateList] (
		--			@SecurityList,
		--			@DateList
		--			) as Fees ON MarketValue.AsOfDate = Fees.AsOfDate
		--LEFT JOIN dbo.[tfn_Income_By_SecurityList_By_DateList] (
		--			@SecurityList,
		--			@DateList
		--			) as Income ON MarketValue.AsOfDate = Income.AsOfDate
	GROUP BY MarketValue.AsOfDate,
		Trans.CashFlow,
		Trans.Fees,
		Trans.Income
	ORDER BY MarketValue.AsOfDate

	-- Update all records except the first one
	UPDATE EndTWR
	SET BeginDate = DATEADD(day, 1, BeginTWR.EndDate),
		BeginMarketValue = BeginTWR.EndMarketValue
	FROM @TWR as BeginTWR, @TWR as EndTWR
	WHERE EndTWR.TWR_ID = (BeginTWR.TWR_ID + 1)

	--REMOVE EndDates that are before the BeginDate
	--These may exist because we need to know the 
	--market value at the beginning of the begindate
	--which is the end value on the day before

	DELETE FROM @TWR 
	WHERE EndDate < @BeginDate

	--If we still have a blank BeginMarketValue
	--That likely means this was the first 
	--Transaction in the account and begin value is zero

	RETURN;
END;


GO

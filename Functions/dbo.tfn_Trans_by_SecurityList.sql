SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[tfn_Trans_by_SecurityList] (@SecurityList SecurityList READONLY)
RETURNS @Trans TABLE
	(
	TransID bigint,
	Quantity numeric(18,6),
	UnitPrice_Base numeric(18,6),
	UnitPrice_Local numeric(18,6),
	Amount_Base numeric(18,6),
	Amount_Local numeric(18,6),
	TradeDate smalldatetime,
	SettleDate smalldatetime,
	CurrencyID_Base int,
	CurrencyID_Local int,
	TransCodeName varchar(100),
	TransCodeID int,
	EntityID bigint,
	EntityName varchar(100),
	AccountID bigint,
	AccountName varchar(100),
	AccountSecurityID bigint,
	SecurityID bigint,
	SecurityName varchar(100), 
	CalcCashFlow int,
	CalcDividend int,
	CalcInterest int,
	CalcFee int,
	CalcDisplay int,
	CalcIncome int,
	CalcQuantity int,
	CalcQuantityOfOne int,
	QuantityOfOne BIT,
	PercentOwnership NUMERIC(18,6)
	)

	-- This is pretty well tuned and runs very fast.
AS
BEGIN 

	INSERT INTO @Trans 
		(
		TransID,
		Quantity,
		UnitPrice_Base,
		UnitPrice_Local,
		Amount_Base,
		Amount_Local,
		TradeDate,
		SettleDate,
		CurrencyID_Base,
		CurrencyID_Local,
		TransCodeName,
		TransCodeID,
		EntityID,
		EntityName,
		AccountID,
		AccountName,
		AccountSecurityID,
		SecurityID,
		SecurityName,
		CalcCashFlow,
		CalcDividend,
		CalcInterest,
		CalcFee,
		CalcDisplay,
		CalcIncome,
		CalcQuantity,
		CalcQuantityOfOne,
		QuantityOfOne,
		PercentOwnership
		)
	SELECT Trans.TransID,
		--CASE WHEN ISNULL(Trans.Quantity, 0) = 0 AND Trans.UnitPrice_Local IN (1, 0)
		--	THEN Trans.Amount_Local
		--	ELSE Trans.Quantity
		--	END as Quantity,
		--TRANS.Quantity as Quantity,
		--CASE WHEN ISNULL(Trans.Quantity, 0) = 0 AND Trans.UnitPrice_Local IN (1, 0)
		--	THEN (Trans.Amount_Local) --* Trans.CalcQuantity
		--	ELSE (Trans.Quantity) --* Trans.CalcQuantity
		--	END as Quantity,
		Trans.Quantity * (ISNULL(SecurityList.PercentOwnership, 100) * .01) AS Quantity,
		Trans.UnitPrice_Base,
		Trans.UnitPrice_Local,
		Trans.Amount_Base * (ISNULL(SecurityList.PercentOwnership, 100) * .01) AS Amount_Base,
		Trans.Amount_Local * (ISNULL(SecurityList.PercentOwnership, 100) * .01) AS Amount_Local,
		Trans.TradeDate,
		Trans.SettleDate,
		Trans.CurrencyID_Base,
		Trans.CurrencyID_Local,
		Trans.TransCodeName,
		Trans.TransCodeID,
		Trans.EntityID,
		Trans.EntityName,
		Trans.AccountID,
		Trans.AccountName,
		Trans.AccountSecurityID,
		Trans.SecurityID,
		Trans.SecurityName,
		Trans.CalcCashFlow,
		Trans.CalcDividend,
		Trans.CalcInterest,
		Trans.CalcFee,
		Trans.CalcDisplay,
		Trans.CalcIncome,
		Trans.CalcQuantity,
		Trans.CalcQuantityOfOne,
		Trans.QuantityOfOne,
		SecurityList.PercentOwnership
	FROM vTrans as Trans
		JOIN @SecurityList as SecurityList on Trans.AccountSecurityID = SecurityList.AccountSecurityID

	RETURN;
END;


GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[tfn_Transactions_By_Level_By_DateList] (
	@SecurityList SecurityList Readonly,
	@DateList DateList Readonly
	)

RETURNS @Transactions TABLE (
	TransID bigint,
	Quantity numeric(18,6),
	UnitPrice_Base numeric(18,6),
	UnitPrice_Local numeric(18,6),
	Amount_Base numeric(18,6),
	Amount_Local numeric(18,6),
	TradeDate smalldatetime,
	SettleDate smalldatetime,
	CurrencyID_Base int,
	CurrencyID_Local int,
	TransCodeName varchar(100),
	EntityID bigint,
	AccountID bigint,
	AccountSecurityID bigint,
	SecurityID bigint,
	SecurityName varchar(100),
	CalcCashFlow int,
	CalcCashFlow_By_Group int,
	CalcDividend int,
	CalcInterest int,
	CalcFee int,
	CalcDisplay int,
	CalcIncome INT,
	CalcQuantity INT,
	CalcNetInvested int
	)

AS
BEGIN
	DECLARE @BeginDate smalldatetime
	DECLARE @EndDate smalldatetime

	SELECT @BeginDate = MIN(AsOfDate),
		@EndDate = MAX(AsOfDate)
	FROM @DateList
	
	INSERT INTO @Transactions (
		TransID,
		Quantity,
		UnitPrice_Base,
		UnitPrice_Local,
		Amount_Base,
		Amount_Local,
		TradeDate,
		SettleDate,
		CurrencyID_Base,
		CurrencyID_Local,
		TransCodeName,
		EntityID,
		AccountID,
		AccountSecurityID,
		SecurityID,
		SecurityName,
		CalcCashFlow,
		CalcCashFlow_By_Group,
		CalcDividend,
		CalcInterest,
		CalcFee,
		CalcDisplay,
		CalcIncome,
		CalcQuantity,
		CalcNetInvested
		)
	SELECT Trans.TransID,
		Trans.Quantity * (ISNULL(SecurityList.PercentOwnership, 100) * .01) AS Quantity, 
		Trans.UnitPrice_Base,
		Trans.UnitPrice_Local,
		Trans.Amount_Base * (ISNULL(SecurityList.PercentOwnership, 100) * .01) AS Amount_Base,
		Trans.Amount_Local * (ISNULL(SecurityList.PercentOwnership, 100) * .01) AS Amount_Local,
		Trans.TradeDate,
		Trans.SettleDate,
		Trans.CurrencyID_Base,
		Trans.CurrencyID_Local,
		Trans.TransCodeName, 
		SecurityList.EntityID, 
		SecurityList.AccountID, 
		SecurityList.AccountSecurityID, 
		SecurityList.SecurityID,
		Trans.SecurityName,
		Trans.CalcCashFlow,
		Trans.CalcCashFlow_By_Group,
		Trans.CalcDividend,
		Trans.CalcInterest,
		Trans.CalcFee,
		Trans.CalcDisplay,
		Trans.CalcIncome,
		Trans.CalcQuantity,
		Trans.CalcNetInvested
	FROM vTrans as Trans
		JOIN @SecurityList as SecurityList ON Trans.AccountSecurityID = SecurityList.AccountSecurityID
		--CROSS JOIN @DateList
	WHERE Trans.TradeDate >= @BeginDate
		AND Trans.TradeDate <= @EndDate
	


	RETURN;
END;


GO

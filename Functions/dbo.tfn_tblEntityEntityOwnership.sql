SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE FUNCTION [dbo].[tfn_tblEntityEntityOwnership]
(	
	@AsOfDate datetime
)
RETURNS TABLE 
AS
RETURN 
(
	SELECT EntityEntityOwnership.EntityPrimaryID,
		EntityEntityOwnership.EntitySubID,
		EntityEntityOwnership.AsOfDate,
		EntityEntityOwnership.PercentOwnership
	FROM EntityEntityOwnership
		JOIN (SELECT EntityEntityOwnership.EntityPrimaryID, 
			EntityEntityOwnership.EntitySubID, 
			EntityEntityOwnership.AsOfDate
				FROM EntityEntityOwnership 
					JOIN (SELECT EntityEntityOwnership.EntityPrimaryID,
							 EntityEntityOwnership.EntitySubID,
						MAX(AsOfDate) as MaxDate
					FROM EntityEntityOwnership
					WHERE EntityEntityOwnership.AsOfDate <= @AsOfDate
					GROUP BY EntityEntityOwnership.EntityPrimaryID,
						EntityEntityOwnership.EntitySubID) as MaxAsOfDate ON EntityEntityOwnership.EntityPrimaryID = MaxAsOfDate.EntityPrimaryID
						AND EntityEntityOwnership.EntitySubID = MaxAsOfDate.EntitySubID
						AND EntityEntityOwnership.AsOfDate = MaxAsOfDate.MaxDate
					) AS AsOfDate ON EntityEntityOwnership.EntitySubID = AsOfDate.EntitySubID 
						AND EntityEntityOwnership.EntityPrimaryID = AsOfDate.EntityPrimaryID
						AND EntityEntityOwnership.EntitySubID = AsOfDate.EntitySubID
	WHERE EntityEntityOwnership.AsOfDate = AsOfDate.AsOfDate
)

GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROC [dbo].[ActiveAccountsByEntity]

AS

SELECT Entity.EntityName,
	COUNT(Account.AccountID) AS AccountCount
FROM dbo.Entity
	JOIN EntityAccount ON Entity.EntityID = EntityAccount.EntityID
	JOIN Account ON EntityAccount.AccountID = Account.AccountID
WHERE ISNULL(Entity.Inactive, 0) = 0
	AND ISNULL(Account.Inactive, 0) = 0
GROUP BY Entity.EntityName

GO

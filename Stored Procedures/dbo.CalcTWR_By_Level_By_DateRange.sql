SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROC [dbo].[CalcTWR_By_Level_By_DateRange]
	@Level nvarchar(10) = 'E',    
	@LevelID bigint,
	@BeginDate smalldatetime,
	@EndDate smalldatetime,
	@NetOfFees bit = 0,
	@Annualize bit = 0,
	@RecordSet bit = 0,
	@TWR numeric(18,6) = 0 OUT,
	@CashFlow numeric(18,6) = 0 OUT,
	@Income numeric(18,6) = 0 OUT,
	@Fees numeric(18,6) = 0 OUT


AS
BEGIN
	BEGIN TRY
		
		--**************************************
		-- CREATE the table variable
		--**************************************
		DECLARE @SecurityList SecurityList

		-- CREATE a list of all securities to present
		INSERT @SecurityList (
			FamilyID, 
			EntityID, 
			AccountID, 
			AccountSecurityID, 
			SecurityID, 
			SecurityName
			)
		EXEC CreateSecurityList_By_Level
			@Level = @Level,
			@LevelID = @LevelID

		--Create the variables 
		DECLARE @ActualBeginDate smalldatetime
		DECLARE @ActualEndDate smalldatetime
		DECLARE @BeginMarketValue numeric(18,6)
		DECLARE @EndMarketValue numeric(18,6)

		--Find the BeginMarketValue
		SELECT @ActualBeginDate = AsOfDate.AsOfDate, @BeginMarketValue = SUM(Holdings.MarketValue_Base)
		FROM Holdings
			JOIN @SecurityList as SecurityList ON Holdings.AccountSecurityID = SecurityList.AccountSecurityID
			JOIN (SELECT MAX(AsOfDate) as AsOfDate 
					FROM Holdings 
					WHERE AsOfDate <= DATEADD(day, -1, @BeginDate)) as AsOfDate ON Holdings.AsOfDate = AsOfDate.AsOfDate
		GROUP BY AsOfDate.AsOfDate

		--Find the EndMarketValue
		SELECT @ActualEndDate = AsOfDate.AsOfDate, @EndMarketValue = SUM(Holdings.MarketValue_Base)
		FROM Holdings
			JOIN @SecurityList as SecurityList ON Holdings.AccountSecurityID = SecurityList.AccountSecurityID
			JOIN (SELECT MAX(AsOfDate) as AsOfDate 
					FROM Holdings 
					WHERE AsOfDate <= @EndDate) as AsOfDate ON Holdings.AsOfDate = AsOfDate.AsOfDate
		GROUP BY AsOfDate.AsOfDate

		--Find the CashFlows
		EXEC CashFlowForTWR_By_Level
			@SecurityList = @SecurityList,
			@BeginDate = @ActualBeginDate,
			@EndDate = @ActualEndDate,
			@CashFlows = @CashFlow OUT,
			@DetailRecordSet = 0

		--Find the Income
		EXEC Income_By_Level
			@SecurityList = @SecurityList,
			@BeginDate = @ActualBeginDate,
			@EndDate = @ActualEndDate,
			@Income = @Income OUT,
			@DetailRecordSet = 0

		--Find the Fees
		IF @NetOfFees = 1
		BEGIN
			EXEC FeesForTWR_By_Level
				@SecurityList = @SecurityList,
				@BeginDate = @ActualBeginDate,
				@EndDate = @ActualEndDate,
				@Fees = @Fees OUT
		END

		--Set values to 0 if null
		SET @BeginMarketValue = ISNULL(@BeginMarketValue, 0)
		SET @EndMarketValue = ISNULL(@EndMarketValue, 0)
		SET @CashFlow = ISNULL(@CashFlow, 0)
		SET @Fees = ISNULL(@Fees, 0)


		--Calculate the TWR
		IF @BeginMarketValue > 0
		BEGIN
			SELECT @TWR = (((@EndMarketValue + @CashFlow + @Fees) - @BeginMarketValue) / @BeginMarketValue)
		END
		ELSE
		BEGIN
			SELECT @TWR = 0
		END

		--Annualize
		IF @Annualize = 1
		BEGIN
			SELECT @TWR = dbo.sfn_AnnualizeReturn
							(
								@TWR,
								@ActualBeginDate,
								@ActualEndDate
							)
		END

		--Output the return
		IF @RecordSet = 1
		BEGIN
			SELECT @ActualBeginDate as ActualBeginDate,
				@ActualEndDate as ActualEndDate, 
				@BeginMarketValue as BeginMarketValue, 
				@EndMarketValue as EndMarketValue, 
				@CashFlow as CashFlow,
				@Fees as Fees,
				@Income as Income,
				@TWR as TWR
		END

	END TRY
	BEGIN CATCH
		DECLARE @ErrMsg nvarchar(4000) = ERROR_MESSAGE()
		DECLARE @ErrNum INT = ERROR_NUMBER()
		DECLARE @ErrProc nvarchar(126) = ERROR_PROCEDURE()
		DECLARE @DataError nvarchar(4000) = 'Error running PROC CalcTWR_By_Level_By_DateRange' +
			CONVERT(nvarchar(10), @errNum) + ', Error Details: ' + @ErrMsg
		RAISERROR (@DataError, 16,1)
	END CATCH
END
GO

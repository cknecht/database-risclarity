SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROC [dbo].[CalculateDataForActivity]											
	@SecurityList securitylist READONLY,
	@PeriodList PeriodList Readonly,
	@Level varchar(10) = 'E',
	@LevelID bigint = 0,
	@RecordSet bit = 0,
	@GroupBy VARCHAR(100) = '', --'Account, Entity',
	@Debugging bit = 0

AS
SET NOCOUNT ON 

BEGIN

	DECLARE @DateList DateList
	DECLARE @BeginDate smalldatetime
	DECLARE @EndDate smalldatetime

	IF @GroupBy = ''
	BEGIN
		IF @Level = 'A'
		BEGIN
			SET @GroupBy = 'Account'
		END
		ELSE IF @Level = 'E'
		BEGIN	
			SET @GroupBy = 'Entity'
		END
		ELSE IF @Level = 'F'
		BEGIN	
			SET @GroupBy = 'Entity'
		END
	END

	SELECT @BeginDate = MIN(BeginDate),
		@EndDate = MAX(EndDate)
	FROM @PeriodList

	-- Populate the list of Dates (All dates between, and including the begin and end dates)
	INSERT INTO @DateList (AsOfDate)
	SELECT AsOfDate
	FROM [tfn_DateList] (@BeginDate, @EndDate)

	-- CREATE the output temp table if it doesnt exist
	IF OBJECT_ID('tempdb..#Activity') IS NULL
	BEGIN
		CREATE TABLE #Activity (
			[Activity_ID] [bigint] IDENTITY(1,1) NOT NULL,
			[Level] nvarchar(1) NOT NULL,
			[LevelID] [bigint] NOT NULL,
			[AccountID] [bigint] NULL,
			[AccountName] varchar(100),
			[AccountNumber] varchar(100),
			[AccountInceptionDate] smalldatetime,
			[PeriodBeginDate] [smalldatetime] NULL,
			[PeriodEndDate] [smalldatetime] NULL,
			[BeginMarketValue] [numeric](18, 6) NULL,
			[EndMarketValue] [numeric](18, 6) NULL,
			[CashFlow] [numeric](18, 6) NULL,
			[Additions] [numeric](18, 6) NULL,
			[Withdrawals] [numeric](18, 6) NULL,
			[Income] [numeric](18, 6) NULL,
			[AccruedIncome] [numeric](18, 6) NULL,
			[Fees] [numeric](18, 6) NULL,
			[ExcludeFromPerformance] BIT
		)
	END

	-- Create the MarketValues Table
	CREATE TABLE #MarketValueData (
		AsOfDate smalldatetime,
		AccountSecurityID bigint,
		EntityID bigint,
		EntityName varchar(100),
		AccountID bigint,
		AccountName varchar(100),
		[AccountNumber] varchar(100),
		[AccountInceptionDate] smalldatetime,
		SecurityID bigint,
		SecurityName varchar(100),
		Quantity numeric(18,6),
		Price_Base numeric(18,6),
		Price_Local numeric(18,6),
		MarketValue_Base numeric(18,6),
		MarketValue_Local numeric(18,6),
		ExchangeRate numeric(18,6),
		ExcludeFromPerformance BIT,
		ContractSize NUMERIC(18,6)
		)

	-- Add an index to MarketValue
	CREATE INDEX IX_1 on #MarketValueData (AsOfDate)

	-- Populate the MarketValue Table with market values on any day there is a..

	INSERT INTO #MarketValueData (
		AsOfDate,
		AccountSecurityID,
		EntityID,
		EntityName,
		AccountID,
		AccountName,
		SecurityID,
		SecurityName,
		Quantity,
		Price_Base,
		Price_Local,
		MarketValue_Base,
		MarketValue_Local,
		ExchangeRate
		)
	EXEC DisplayMarketValue_By_SecurityList_By_PeriodList
		@SecurityList,
		@PeriodList 

	-- UPDATE #MarketValueData with AccountNumber and AccountInceptionDate
	UPDATE #MarketValueData
	SET AccountNumber = Account.AccountNumber,
		AccountInceptionDate = Account.InceptionDate,
		ExcludeFromPerformance = Account.ExcludeFromPerformance
	FROM #MarketValueData AS MarketValue
		JOIN Account AS Account ON MarketValue.AccountID = Account.AccountID

	-- Create the Trans Table
	CREATE TABLE #TransData (
		TransID bigint,
		Quantity numeric(18,6),
		UnitPrice_Base numeric(18,6),
		UnitPrice_Local numeric(18,6),
		Amount_Base numeric(18,6),
		Amount_Local numeric(18,6),
		TradeDate smalldatetime,
		SettleDate smalldatetime,
		CurrencyID_Base int,
		CurrencyID_Local int,
		TransCodeName varchar(100),
		EntityID bigint,
		AccountID bigint,
		AccountSecurityID bigint,
		SecurityID bigint,
		SecurityName varchar(100),
		CalcCashFlow int,
		CalcDividend int,
		CalcInterest int,
		CalcFee int,
		CalcDisplay int,
		CalcIncome int
		)

	CREATE INDEX IX_2 on #TransData (CalcCashFlow, CalcFee, CalcIncome)
	
	-- Populte the Trans Table
	INSERT INTO #TransData (
		TransID,
		Quantity,
		UnitPrice_Base,
		UnitPrice_Local,
		Amount_Base,
		Amount_Local,
		TradeDate,
		SettleDate,
		CurrencyID_Base,
		CurrencyID_Local,
		TransCodeName,
		EntityID,
		AccountID,
		AccountSecurityID,
		SecurityID,
		SecurityName,
		CalcCashFlow,
		CalcDividend,
		CalcInterest,
		CalcFee,
		CalcDisplay,
		CalcIncome
		)
	SELECT
		Trans.TransID,
		Trans.Quantity,
		Trans.UnitPrice_Base,
		Trans.UnitPrice_Local,
		Trans.Amount_Base,
		Trans.Amount_Local,
		Trans.TradeDate,
		Trans.SettleDate,
		Trans.CurrencyID_Base,
		Trans.CurrencyID_Local,
		Trans.TransCodeName,
		Trans.EntityID,
		Trans.AccountID,
		Trans.AccountSecurityID,
		Trans.SecurityID,
		Trans.SecurityName,
		Trans.CalcCashFlow,
		Trans.CalcDividend,
		Trans.CalcInterest,
		Trans.CalcFee,
		Trans.CalcDisplay,
		Trans.CalcIncome
	FROM dbo.[tfn_Transactions_By_Level_By_DateList] (
								@SecurityList,
								@DateList
								) AS Trans


	-- Find the CashFlow, Income and Fees
	CREATE TABLE #CashData (
		[Level] varchar(1),
		LevelID bigint,
		AccountID BIGINT,
		CashFlow numeric(18,6),
		Additions numeric(18,6),
		Withdrawals numeric(18,6),
		Income numeric(18,6),
		Fees numeric(18,6),
		TradeDate smalldatetime
		)

	INSERT INTO #CashData (
		Level,
		LevelID,
		CashFlow,
		Additions,
		Withdrawals,
		Income,
		Fees,
		TradeDate,
		AccountID
		)
	SELECT @Level,
			@LevelID,
			SUM(ABS(Amount_Base) * CalcCashFlow) as CashFlow,
			ABS(SUM((Amount_Base) * IIF(CalcCashFlow < 0, 1, 0))) as Additions,
			ABS(SUM((Amount_Base) * IIF(CalcCashFlow > 0, 1, 0))) as Withdrawals,
			SUM(ABS(Amount_Base) * CalcIncome) as Income,
			SUM(ABS(Amount_Base) * CalcFee) as Fees,
			TradeDate
			,
			CASE @GroupBy
					WHEN 'Account' THEN Trans.AccountID
					WHEN 'Entity' THEN Trans.EntityID
					ELSE EntityID
				END AS AccountID
			FROM #TransData as Trans
			WHERE (Trans.CalcCashFlow <> 0 OR Trans.CalcFee <> 0 OR Trans.CalcIncome <> 0)
				AND Trans.TransID > 0
			GROUP BY TradeDate
			,
				CASE @GroupBy
					WHEN 'Account' THEN Trans.AccountID
					WHEN 'Entity' THEN Trans.EntityID
					ELSE EntityID
				END

		INSERT INTO #Activity (
			[Level],
			[LevelID],
			PeriodBeginDate,
			PeriodEndDate,
			EndMarketValue
			)
		SELECT @Level,
			@LevelID,
			PeriodList.BeginDate,
			PeriodList.EndDate,
			MarketValueDays.MarketValue
		FROM  @PeriodList as PeriodList
			JOIN (SELECT AsOfDate,
					SUM(MarketValue_Base) AS MarketValue
				FROM #MarketValueData
				GROUP BY AsOfDate) AS MarketValueDays ON PeriodList.EndDate = MarketValueDays.AsOfDate

		--INSERT INTO #Activity (
		--	[Level],
		--	[LevelID],
		--	PeriodBeginDate,
		--	PeriodEndDate,
		--	CashFlow,
		--	Additions,
		--	Withdrawals,
		--	Income,
		--	Fees
		--	)
		--SELECT @Level,
		--	@LevelID,
		--	PeriodList.BeginDate,
		--	PeriodList.EndDate,
		--	SUM(ISNULL(Trans.CashFlow, 0)) as CashFlow,
		--	SUM(ISNULL(Trans.Additions, 0)) as Additions,
		--	SUM(ISNULL(Trans.Withdrawals, 0)) as Withdrawals,
		--	SUM(ISNULL(Trans.Fees, 0)) as Fees,
		--	SUM(ISNULL(Trans.Income, 0)) as Income
		--FROM  @PeriodList as PeriodList
		--	JOIN #CashData as Trans ON PeriodList.EndDate >= Trans.TradeDate
		--		AND PeriodList.BeginDate <= Trans.TradeDate
		--GROUP BY PeriodList.BeginDate,
		--	PeriodList.EndDate

		UPDATE #Activity
		SET BeginMarketValue = MarketValueDays.MarketValue
		FROM  #Activity as PeriodList
			JOIN (SELECT AsOfDate,
					SUM(MarketValue_Base) AS MarketValue
				FROM #MarketValueData
				GROUP BY AsOfDate) AS MarketValueDays ON DATEADD(day, -1, PeriodList.PeriodBeginDate) = MarketValueDays.AsOfDate

		UPDATE #Activity
		SET CashFlow = SumTrans.CashFlow,
			Additions = SumTrans.Additions,
			Withdrawals = SumTrans.Withdrawals,
			Income = SumTrans.Income,
			Fees = SumTrans.Fees
		FROM  #Activity as PeriodList
			JOIN (
				SELECT PeriodList.PeriodBeginDate,
					PeriodList.PeriodEndDate,
					SUM(ISNULL(Trans.CashFlow, 0)) as CashFlow,
					SUM(ISNULL(Trans.Additions, 0)) as Additions,
					SUM(ISNULL(Trans.Withdrawals, 0)) as Withdrawals,
					SUM(ISNULL(Trans.Fees, 0)) as Fees,
					SUM(ISNULL(Trans.Income, 0)) as Income
				FROM  #Activity as PeriodList
					LEFT JOIN #CashData as Trans ON PeriodList.PeriodEndDate >= Trans.TradeDate
						AND PeriodList.PeriodBeginDate <= Trans.TradeDate
				GROUP BY PeriodList.PeriodBeginDate,
					PeriodList.PeriodEndDate
					) as SumTrans ON PeriodList.PeriodEndDate = SumTrans.PeriodEndDate
				


		IF @RecordSet = 1
		BEGIN

			SELECT * FROM #Activity
			
		END



	DROP TABLE #MarketValueData
	DROP TABLE #TransData
	DROP TABLE #CashData

END;

--EXEC [DisplayAccountPerformanceReview]
--	@Level = 'E',
--	@LevelID = 44,
--	@EndDate = '6/30/13',
--	@CompareToSource = 0,
--	@IncludeEstimates = 0,
--	@IncludeLinkedAccounts = 0

GO

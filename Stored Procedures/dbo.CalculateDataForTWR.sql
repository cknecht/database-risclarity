SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROC [dbo].[CalculateDataForTWR] (
	@SecurityList SecurityList Readonly,
	@PeriodList PeriodList Readonly,
	@Level varchar(10) = 'E',
	@LevelID bigint = 0,
	@RecordSet bit = 0,
	@GroupBy VARCHAR(100) = '' --'Account, Entity'
	)

AS
SET NOCOUNT ON 

BEGIN
	DECLARE @DateList DateList
	DECLARE @BeginDate smalldatetime
	DECLARE @EndDate smalldatetime

	IF @GroupBy = ''
	BEGIN
		IF @Level = 'A'
		BEGIN
			SET @GroupBy = 'Account'
		END
		ELSE IF @Level = 'E'
		BEGIN	
			SET @GroupBy = 'Entity'
		END
		ELSE IF @Level = 'F'
		BEGIN	
			SET @GroupBy = 'Entity'
		END
	END

	SELECT @BeginDate = MIN(BeginDate),
		@EndDate = MAX(EndDate)
	FROM @PeriodList

	-- Populate the list of Dates (All dates between, and including the begin and end dates)
	INSERT INTO @DateList (AsOfDate)
	SELECT AsOfDate
	FROM [tfn_DateList] (@BeginDate, @EndDate)

	-- CREATE the output temp table if it doesnt exist
	IF OBJECT_ID('tempdb..#TWR') IS NULL
	BEGIN
		CREATE TABLE #TWR (
			[TWR_ID] [bigint] IDENTITY(1,1) NOT NULL,
			[Level] nvarchar(1) NOT NULL,
			[LevelID] [bigint] NOT NULL,
			[AccountID] [bigint] NULL,
			[AccountName] varchar(100),
			[AccountNumber] varchar(100),
			[AccountInceptionDate] smalldatetime,
			[PeriodBeginDate] [smalldatetime] NULL,
			[PeriodEndDate] [smalldatetime] NULL,
			[TWRBeginDate] [smalldatetime] NULL,
			[TWREndDate] [smalldatetime] NULL,
			[BeginMarketValue] [numeric](18, 6) NULL,
			[EndMarketValue] [numeric](18, 6) NULL,
			[EndNetInvested] [numeric](18, 6) NULL,
			[CashFlow] [numeric](18, 6) NULL,
			[Additions] [numeric](18, 6) NULL,
			[Withdrawals] [numeric](18, 6) NULL,
			[Income] [numeric](18, 6) NULL,
			[AccruedIncome] [numeric](18, 6) NULL,
			[Fees] [numeric](18, 6) NULL,
			[PeriodTWR] [numeric](18, 6) NULL,
			[PeriodTWR_Net_Of_Fees] [numeric](18, 6) NULL,
			[PeriodTWR_Plus_One] [numeric](18, 6) NULL,
			[PeriodTWR_Net_Of_Fees_Plus_One] [numeric](18, 6) NULL,
			[TWR] [numeric](18, 6) NULL,
			[AnnualizedTWR] [numeric](18, 6) NULL,
			[TWR_Net_Of_Fees] [numeric](18, 6) NULL,
			[AnnualizedTWR_Net_Of_Fees] [numeric](18, 6) NULL,
			[ExcludeFromPerformance] BIT
		)
	END
	
	-- Create the MarketValues Table
	CREATE TABLE #MarketValueData (
		AsOfDate smalldatetime,
		AccountSecurityID bigint,
		EntityID bigint,
		EntityName varchar(100),
		AccountID bigint,
		AccountName varchar(100),
		[AccountNumber] varchar(100),
		[AccountInceptionDate] smalldatetime,
		SecurityID bigint,
		SecurityName varchar(100),
		Quantity numeric(18,6),
		Price_Base numeric(18,6),
		Price_Local numeric(18,6),
		MarketValue_Base numeric(18,6),
		MarketValue_Local numeric(18,6),
		ExchangeRate numeric(18,6),
		ExcludeFromPerformance bit
		)

	-- Add an index to MarketValue
	CREATE INDEX IX_1 on #MarketValueData (AsOfDate)
	
	-- Populate the MarketValue Table with market values on any day there is a..

	INSERT INTO #MarketValueData (
		AsOfDate,
		AccountSecurityID,
		EntityID,
		EntityName,
		AccountID,
		AccountName,
		SecurityID,
		SecurityName,
		Quantity,
		Price_Base,
		Price_Local,
		MarketValue_Base,
		MarketValue_Local,
		ExchangeRate
		)
	EXEC DisplayMarketValue_By_SecurityList_By_PeriodList
		@SecurityList,
		@PeriodList 

	-- Pull out the marketvalues from the 'Consolidated' performance number
	-- If we are grouping by Account, we need to see each account
	-- and the market values, we will filter our the performance later

	IF @GroupBy <> 'Account' OR @Level = 'A'
	BEGIN
		DELETE #MarketValueData
		FROM #MarketValueData AS MarketValue
			JOIN Account AS Account ON MarketValue.AccountID = Account.AccountID
		WHERE ISNULL(Account.ExcludeFromPerformance, 0) = 1
	END

	-- UPDATE #MarketValueData with AccountNumber and AccountInceptionDate
	UPDATE #MarketValueData
	SET AccountNumber = Account.AccountNumber,
		AccountInceptionDate = Account.InceptionDate,
		ExcludeFromPerformance = Account.ExcludeFromPerformance
	FROM #MarketValueData AS MarketValue
		JOIN Account AS Account ON MarketValue.AccountID = Account.AccountID

	-- Create the Trans Table
	CREATE TABLE #TransData (
		TransID bigint,
		Quantity numeric(18,6),
		UnitPrice_Base numeric(18,6),
		UnitPrice_Local numeric(18,6),
		Amount_Base numeric(18,6),
		Amount_Local numeric(18,6),
		TradeDate smalldatetime,
		SettleDate smalldatetime,
		CurrencyID_Base int,
		CurrencyID_Local int,
		TransCodeName varchar(100),
		EntityID bigint,
		AccountID bigint,
		AccountSecurityID bigint,
		SecurityID bigint,
		SecurityName varchar(100),
		CalcCashFlow int,
		CalcDividend int,
		CalcInterest int,
		CalcFee int,
		CalcDisplay int,
		CalcIncome INT,
		CalcNetInvested INT
		)

	CREATE INDEX IX_2 on #TransData (CalcCashFlow, CalcFee, CalcIncome, CalcNetInvested)
	
	-- Populte the Trans Table
	INSERT INTO #TransData (
		TransID,
		Quantity,
		UnitPrice_Base,
		UnitPrice_Local,
		Amount_Base,
		Amount_Local,
		TradeDate,
		SettleDate,
		CurrencyID_Base,
		CurrencyID_Local,
		TransCodeName,
		EntityID,
		AccountID,
		AccountSecurityID,
		SecurityID,
		SecurityName,
		CalcCashFlow,
		CalcDividend,
		CalcInterest,
		CalcFee,
		CalcDisplay,
		CalcIncome
		)
	SELECT
		Trans.TransID,
		Trans.Quantity,
		Trans.UnitPrice_Base,
		Trans.UnitPrice_Local,
		Trans.Amount_Base,
		Trans.Amount_Local,
		Trans.TradeDate,
		Trans.SettleDate,
		Trans.CurrencyID_Base,
		Trans.CurrencyID_Local,
		Trans.TransCodeName,
		Trans.EntityID,
		Trans.AccountID,
		Trans.AccountSecurityID,
		Trans.SecurityID,
		Trans.SecurityName,
		Trans.CalcCashFlow,
		Trans.CalcDividend,
		Trans.CalcInterest,
		Trans.CalcFee,
		Trans.CalcDisplay,
		Trans.CalcIncome
	FROM dbo.[tfn_Transactions_By_Level_By_DateList] (
								@SecurityList,
								@DateList
								) AS Trans
		JOIN @SecurityList AS SecurityList ON Trans.AccountSecurityID = SecurityList.AccountSecurityID
			AND ISNULL(SecurityList.ExcludeFromPerformance, 0) = 0 -- Pull out the Accounts that are excluded from performance.

	-- Find the CashFlow, Income and Fees
	CREATE TABLE #CashData (
		[Level] varchar(1),
		LevelID bigint,
		AccountID BIGINT,
		CashFlow numeric(18,6),
		Additions numeric(18,6),
		Withdrawals numeric(18,6),
		Income numeric(18,6),
		Fees numeric(18,6),
		TradeDate smalldatetime
		)

	--IF @GroupBy = 'Account'
	--BEGIN
		CREATE UNIQUE CLUSTERED INDEX IX_3 on #CashData (TradeDate, AccountID)
	--END
	--ELSE 
	--BEGIN
	--	CREATE UNIQUE CLUSTERED INDEX IX_3 on #CashData (TradeDate)
	--END

	INSERT INTO #CashData (
		Level,
		LevelID,
		CashFlow,
		Additions,
		Withdrawals,
		Income,
		Fees,
		TradeDate,
		AccountID
		)
	SELECT @Level,
			@LevelID,
			SUM(ABS(Amount_Base) * CalcCashFlow) as CashFlow,
			ABS(SUM((Amount_Base) * IIF(CalcCashFlow < 0, 1, 0))) as Additions,
			ABS(SUM((Amount_Base) * IIF(CalcCashFlow > 0, 1, 0))) as Withdrawals,
			SUM(ABS(Amount_Base) * CalcIncome) as Income,
			SUM(ABS(Amount_Base) * CalcFee) as Fees,
			TradeDate
			,
			CASE @GroupBy
					WHEN 'Account' THEN Trans.AccountID
					WHEN 'Entity' THEN Trans.EntityID
					ELSE EntityID
				END AS AccountID
			FROM #TransData as Trans
			WHERE (Trans.CalcCashFlow <> 0 OR Trans.CalcFee <> 0 OR Trans.CalcIncome <> 0)
				AND Trans.TransID > 0
			GROUP BY TradeDate
			,
				CASE @GroupBy
					WHEN 'Account' THEN Trans.AccountID
					WHEN 'Entity' THEN Trans.EntityID
					ELSE EntityID
				END

	

	CREATE TABLE #TWR_InputsData (
		TWR_ID bigint identity(1,1),
		[AccountID] [bigint] NULL,
		[AccountName] varchar(100),
		[AccountNumber] varchar(100),
		[AccountInceptionDate] smalldatetime,
		BeginDate smalldatetime,
		EndDate smalldatetime,
		BeginMarketValue numeric(18,6),
		EndMarketValue numeric(18,6),
		CashFlow numeric(18,6),
		Additions numeric(18,6),
		Withdrawals numeric(18,6),
		Income numeric(18,6),
		AccruedIncome numeric(18,6),
		Fees numeric(18,6),
		TWR numeric(18,6),
		TWR_Net_Of_Fees numeric(18,6),
		TWR_plus_one numeric(18,6),
		TWR_plus_one_Net_Of_Fees numeric(18,6),
		TotalTWR numeric(18,6),
		AnnualizedTotalTWR numeric(18,6),
		TotalTWR_Net_Of_Fees numeric(18,6),
		AnnualizedTotalTWR_Net_Of_Fees numeric(18,6),
		ExcludeFromPerformance bit
		)

	IF @GroupBy <> 'Account' AND @Level <> 'A'
	BEGIN

		INSERT INTO #TWR_InputsData (
			EndDate,
			EndMarketValue,
			CashFlow,
			Additions,
			Withdrawals,
			Fees,
			Income,
			AccountID,
			ExcludeFromPerformance,
			AccountInceptionDate
			)
		SELECT MarketValue.AsOfDate, 
			SUM(MarketValue.MarketValue_Base) as EndMarketValue,
			(ISNULL(Trans.CashFlow, 0)) as CashFlow,
			(ISNULL(Trans.Additions, 0)) as Additions,
			(ISNULL(Trans.Withdrawals, 0)) as Withdrawals,
			(ISNULL(Trans.Fees, 0)) as Fees,
			(ISNULL(Trans.Income, 0)) as Income,
			0,
			Account.ExcludeFromPerformance,
			MIN(Account.Inceptiondate) -- Choose the first inception date 
		FROM #MarketValueData as MarketValue
			LEFT JOIN Account ON MarketValue.AccountID = Account.AccountID
			LEFT JOIN #CashData as Trans ON MarketValue.AsOfDate = Trans.TradeDate
		GROUP BY MarketValue.AsOfDate,
			Trans.CashFlow,
			Trans.Additions,
			Trans.Withdrawals,
			Trans.Fees,
			Trans.Income,
			Account.ExcludeFromPerformance
		ORDER BY  MarketValue.AsOfDate
	END
	ELSE
	BEGIN
			INSERT INTO #TWR_InputsData (
				EndDate,
				EndMarketValue,
				CashFlow,
				Additions,
				Withdrawals,
				Fees,
				Income,
				AccountID,
				ExcludeFromPerformance,
				AccountInceptionDate
				)
			SELECT MarketValue.AsOfDate, 
				SUM(MarketValue.MarketValue_Base) as EndMarketValue,
				(ISNULL(Trans.CashFlow, 0)) as CashFlow,
				(ISNULL(Trans.Additions, 0)) as Additions,
				(ISNULL(Trans.Withdrawals, 0)) as Withdrawals,
				(ISNULL(Trans.Fees, 0)) as Fees,
				(ISNULL(Trans.Income, 0)) as Income,
				MarketValue.AccountID,
				Account.ExcludeFromPerformance,
				Account.InceptionDate
			FROM #MarketValueData as MarketValue
				JOIN Account ON MarketValue.AccountID = Account.AccountID
				LEFT JOIN #CashData as Trans ON MarketValue.AsOfDate = Trans.TradeDate
					--AND ((@GroupBy <> 'Account') OR 
					AND Account.AccountID = Trans.AccountID
			GROUP BY MarketValue.AsOfDate,
				Trans.CashFlow,
				Trans.Additions,
				Trans.Withdrawals,
				Trans.Fees,
				Trans.Income,
				MarketValue.AccountID,
				Account.ExcludeFromPerformance,
				Account.InceptionDate,
				Account.ForceInceptionDateTo
			ORDER BY MarketValue.AccountID, 
				MarketValue.AsOfDate
	END

	-- Update all records except the first one
	UPDATE EndTWR
	SET BeginDate = DATEADD(day, 1, BeginTWR.EndDate),
		BeginMarketValue = BeginTWR.EndMarketValue
	FROM #TWR_InputsData as BeginTWR, #TWR_InputsData as EndTWR
	WHERE EndTWR.TWR_ID = (BeginTWR.TWR_ID + 1)
		AND ((@GroupBy <> 'Account') OR (EndTWR.AccountID = BeginTWR.AccountID))
	

	--REMOVE EndDates that are before the BeginDate
	--These may exist because we need to know the 
	--market value at the beginning of the begindate
	--which is the end value on the day before
	
	--DELETE FROM #TWR_InputsData
	--WHERE BeginDate > EndDate

	--IF there is an invesment made on the last day of the 
	--period, there wont be a begin date and that investmant 
	--will fall off the list.  e.g., Pregulman Equities, TSW on 9/13/2013

	UPDATE #TWR_InputsData
	SET BeginDate = DATEADD(day, -1, EndDate)
	WHERE BEGINDATE IS NULL
		AND EndDate = @EndDate
	
	DELETE FROM #TWR_InputsData
	WHERE BeginDate IS NULL

	--If we still have a blank BeginMarketValue
	--That likely means this was the first 
	--Transaction in the account and begin value is zero

	--Populate the period returns
	UPDATE #TWR_InputsData
	SET TWR = CASE WHEN BeginMarketValue <> 0 
			THEN (((EndMarketValue + ISNULL(CashFlow, 0)) - BeginMarketValue) / BeginMarketValue)
			ELSE 0
			END,
		TWR_plus_one = CASE WHEN BeginMarketValue <> 0
					THEN (((EndMarketValue + ISNULL(CashFlow, 0)) - BeginMarketValue) / BeginMarketValue) 
					ELSE 0
					END + 1,
		TWR_Net_Of_Fees = CASE WHEN BeginMarketValue <> 0
					THEN (((EndMarketValue + ISNULL(CashFlow, 0) + ISNULL(Fees, 0)) - BeginMarketValue) / BeginMarketValue)
					ELSE 0
					END,
		TWR_plus_one_Net_Of_Fees = CASE WHEN BeginMarketValue <> 0
				THEN (((EndMarketValue + ISNULL(CashFlow, 0) + ISNULL(Fees, 0)) - BeginMarketValue) / BeginMarketValue)
				ELSE 0
				END + 1
	WHERE EndMarketValue <> 0

	--SELECT *
	--FROM #TWR_InputsData

	--Declare variables
	DECLARE @TotalTWR numeric(18,6)
	DECLARE @TotalTWR_Net_Of_Fees numeric(18,6)

	INSERT INTO #TWR (
		[Level],
		[LevelID],
		[AccountID],
		[AccountName],
		[AccountNumber],
		[AccountInceptionDate],
		PeriodBeginDate, 
		PeriodEndDate,
		TWRBeginDate,
		TWREndDate,
		BeginMarketValue,
		EndMarketVAlue,
		CashFlow,
		Additions,
		Withdrawals,
		Income,
		AccruedIncome,
		Fees,
		PeriodTWR,
		PeriodTWR_Net_Of_Fees,
		PeriodTWR_Plus_One,
		PeriodTWR_Net_Of_Fees_Plus_One,
		ExcludeFromPerformance
		)
	SELECT @Level,
		@LevelID,
		[AccountID],
		[AccountName],
		[AccountNumber],
		[AccountInceptionDate],
		PeriodList.BeginDate, 
		PeriodList.EndDate,
		TWRDetail.BeginDate,
		TWRDetail.EndDate,
		TWRDetail.BeginMarketValue,
		TWRDetail.EndMarketVAlue,
		TWRDetail.CashFlow,
		TWRDetail.Additions,
		TWRDetail.Withdrawals,
		TWRDetail.Income,
		TWRDetail.AccruedIncome,
		TWRDetail.Fees,
		TWRDetail.TWR,
		TWRDetail.TWR_Net_Of_Fees,
		TWRDetail.TWR_plus_one,
		TWRDetail.TWR_plus_one_Net_Of_Fees,
		TWRDetail.ExcludeFromPerformance
	FROM @PeriodList as PeriodList
		JOIN #TWR_InputsData as TWRDetail ON PeriodList.EndDate >= TWRDetail.EndDate
			AND PeriodList.BeginDate <= TWRDetail.BeginDate
	--WHERE TWRDetail.BeginDate >= AccountInceptionDate 
	GROUP BY PeriodList.BeginDate, 
		PeriodList.EndDate,
		TWRDetail.BeginDate,
		TWRDetail.EndDate,
		TWRDetail.BeginMarketValue,
		TWRDetail.EndMarketVAlue,
		TWRDetail.CashFlow,
		TWRDetail.Additions,
		TWRDetail.Withdrawals,
		TWRDetail.Income,
		TWRDetail.AccruedIncome,
		TWRDetail.Fees,
		TWRDetail.TWR,
		TWRDetail.TWR_Net_Of_Fees,
		TWRDetail.TWR_plus_one,
		TWRDetail.TWR_plus_one_Net_Of_Fees,
		[AccountID],
		[AccountName],
		[AccountNumber],
		[AccountInceptionDate],
		TWRDetail.ExcludeFromPerformance

		IF @GroupBy = 'Account' AND @Level <> 'A'
		BEGIN
			UPDATE #TWR 
			SET TWR = (TotalTWR.TWR - 1)
			FROM #TWR as TWR
				JOIN (SELECT AccountID,
					PeriodBeginDate, 
					PeriodEndDate, 
					CASE
					   WHEN MinVal = 0 THEN 0
					   WHEN Neg % 2 = 1 THEN -1 * EXP(ABSMult)
					   ELSE EXP(ABSMult)
					END as TWR
					FROM  (
						SELECT AccountID,
							PeriodBeginDate,
							PeriodEndDate,
						   --log of +ve row values
						   SUM(LOG(ABS(NULLIF(PeriodTWR_plus_one, 0)))) AS ABSMult,
						   --count of -ve values. Even = +ve result.
						   SUM(SIGN(CASE WHEN PeriodTWR_plus_one < 0 THEN 1 ELSE 0 END)) AS Neg,
						   --anything * zero = zero
						   MIN(ABS(PeriodTWR_plus_one)) AS MinVal
						FROM #TWR	
						GROUP BY
							AccountID,
							PeriodBeginDate,
							PeriodEndDate
						) as TWR
					) as TotalTWR ON TWR.AccountID = TotalTWR.AccountID	
				AND TWR.PeriodBeginDate = TotalTWR.PeriodBeginDate
				AND TWR.PeriodEndDate = TotalTWR.PeriodEndDate 
		
		END
		ELSE 
		BEGIN
			UPDATE #TWR 
			SET TWR = (TotalTWR.TWR - 1)
			FROM #TWR as TWR
				JOIN (
				SELECT PeriodBeginDate, 
					PeriodEndDate, 
					CASE
					   WHEN MinVal = 0 THEN 0
					   WHEN Neg % 2 = 1 THEN -1 * EXP(ABSMult)
					   ELSE 
					   EXP(ABSMult) 
					END 
					as TWR
					FROM  (
						SELECT PeriodBeginDate,
							PeriodEndDate,
						   --log of +ve row values
						   SUM(LOG(ABS(NULLIF(PeriodTWR_plus_one, 0)))) AS ABSMult,
						   -- count of -ve values. Even = +ve result.
						   SUM(SIGN(CASE WHEN PeriodTWR_plus_one < 0 THEN 1 ELSE 0 END)) AS Neg,
						   --anything * zero = zero
						   MIN(ABS(PeriodTWR_plus_one)) AS MinVal
						FROM #TWR
						GROUP BY
							PeriodBeginDate,
							PeriodEndDate
						) as TWR
				) as TotalTWR ON TWR.PeriodBeginDate = TotalTWR.PeriodBeginDate
										AND TWR.PeriodEndDate = TotalTWR.PeriodEndDate
		END

		IF @RecordSet = 1
		BEGIN
			SELECT * FROM #TWR
		END

	DROP TABLE #MarketValueData
	DROP TABLE #TransData
	DROP TABLE #CashData
	DROp TABLE #TWR_InputsData

END;

GO

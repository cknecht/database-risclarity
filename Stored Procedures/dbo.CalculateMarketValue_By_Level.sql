SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROC [dbo].[CalculateMarketValue_By_Level]
	@Level varchar(3) = 'E',
	@LevelID bigint = 1,
	@PeriodList periodlist READONLY,
	@IncludeLinkedAccounts BIT = 0
AS

--SET NOCOUNT ON

DECLARE @SecurityList securitylist
DECLARE @BeginDate smalldatetime
DECLARE @EndDate smalldatetime

INSERT INTO @SecurityList (
	FamilyID,
	FamilyName,
	AccountSecurityID, 
	SecurityID, 
	SecurityName,
	QuantityOfOne, 
	AccountID, 
	AccountName,
	EntityID,
	EntityName,
	AssetClassID,
	RegionID,
	SectorID
	)
SELECT FamilyID,
	FamilyName, 
	AccountSecurityID, 
	SecurityID, 
	SecurityName,
	QuantityOfOne, 
	AccountID, 
	AccountName,
	EntityID,
	EntityName,
	AssetClassID,
	RegionID,
	SectorID
FROM [dbo].[tfn_SecurityList_by_Level] (
	@Level, 
	@LevelID, 
	@EndDate,
	@IncludeLinkedAccounts
	)

SELECT @BeginDate = MIN(BeginDate),
	@EndDate = MAX(EndDate)
FROM @PeriodList

CREATE TABLE #Quantity (
	AsOfDate smalldatetime,
	AccountSecurityID bigint,
	EntityID bigint,
	AccountID bigint,
	SecurityID bigint,
	Quantity numeric(18,6),
	QuantityOfOne bit
	)

CREATE INDEX QuantityIndex1 ON #Quantity (AsOfDate, SecurityID)
CREATE INDEX QuantityIndex2 ON #Quantity (AccountSecurityID)

	INSERT INTO #Quantity (
		AsOfDate,
		AccountSecurityID,
		EntityID,
		AccountID,
		SecurityID,
		Quantity,
		QuantityOfOne
		)
	SELECT AsOfDate,
		AccountSecurityID,
		EntityID,
		AccountID,
		SecurityID,
		Quantity,
		QuantityOfOne
	FROM [tfn_Quantity_By_SecurityList] (@SecurityList)
	WHERE AsOfDate >= DATEADD(day, -1, @BeginDate)

	CREATE TABLE #DateList (
		AsOfDate smalldatetime Primary Key
		)

	INSERT INTO #Datelist (AsOfDate)
	-- Make sure we have a beginning price
	SELECT @BeginDate as AsOfDate
	UNION
	--Add all the EndDates from @PeriodList
	SELECT EndDate as AsOfDate
	FROM @PeriodList
	UNION
	--Add all the BeginDates from @PeriodList
	SELECT BeginDate as AsOfDate
	FROM @PeriodList
	UNION
	--Add all the Day before BeginDates from @PeriodList
	SELECT DATEADD(day, -1, BeginDate) as AsOfDate
	FROM @PeriodList
	UNION
	-- Add in any day we have a price entered
	SELECT Price.AsOfDate as AsOfDate
	FROM @SecurityList as AccountSecurity
		JOIN Price as Price ON AccountSecurity.SecurityID = Price.SecurityID
	WHERE --Price.AsOfDate > @BeginDate
		 Price.AsOfDate <= @EndDate
	UNION 
	--Add in any day we have a cash flow
	SELECT Quantity.AsOfDate as AsOfDate
	FROM #Quantity as Quantity
	GROUP BY Quantity.AsOfDate		
	
	CREATE TABLE #Activity (
		AsOfDate smalldatetime,
		CashFlow numeric(18,6),
		Addition numeric(18,6),
		Withdrawal numeric(18,6),
		Fee numeric(18,6),
		Interest numeric(18,6),
		Dividend numeric(18,6),
		Income numeric(18,6),
		TradeDate smalldatetime,
		TransCodeName varchar(100),
		EntityID bigint,
		AccountID bigint,
		AccountSecurityID bigint,
		SecurityID bigint
		)
	CREATE INDEX ActivityIndex1 ON #Activity (AsOfDate, AccountSecurityID)

	INSERT INTO #Activity
	SELECT *
	FROM [tfn_Activity_By_SecurityList] 
				(@SecurityList)	 
				
	--SELECT *
	--FROM #Activity
	--ORDER BY AsOfDate, AccountSecurityID		

	SELECT @Level as Level,
		@LevelID as LevelID,
		Price.AsOfDate as AsOfDate,
		Quantity.AccountSecurityID,
		Quantity.SecurityID,
		Quantity.AccountID,
		Quantity.EntityID,
		Price.PriceAsOfDate,
		Quantity.QuantityAsOfDate,
		Price.Price,
		Quantity.QuantityOfOne,
		Quantity.Quantity,
		Price.Price * Quantity.Quantity as MarketValue,
		Activity.Income,
		Activity.CashFlow,
		Activity.Addition,
		Activity.Withdrawal,
		Activity.Interest,
		Activity.Dividend,
		Activity.Fee,
		SecurityList.SectorID,
		SecurityList.AssetClassID,
		SecurityList.RegionID
	FROM (
		SELECT AsOfDate.AsOfDate,
				Price.SecurityID,
				Price.AsOfDate as PriceAsOfDate,
				Price.Price,
				Price.Price_CurrencyID
		FROM (
			SELECT DateList.AsOfDate as AsOfDate,
					Price.SecurityID,
					MAX(Price.AsOfDate) as PriceAsOfDate
			FROM @SecurityList as AccountSecurity
				CROSS JOIN #DateList as Datelist
				JOIN Price as Price ON AccountSecurity.SecurityID = Price.SecurityID
			WHERE Price.AsOfDate <= DateList.AsOfDate
					--AND AccountSecurity.SecurityID = 9
			GROUP BY Price.SecurityID,
					DateList.AsOfDate
			) 
			AS AsOfDate 
			JOIN Price ON Price.SecurityID = AsOfDate.SecurityID 
						AND Price.AsOfDate = AsOfDate.PriceAsOfDate
		) as Price 
		JOIN (
			SELECT AsOfDate.AsOfDate,
				Quantity.SecurityID,
				Quantity.AccountSecurityID,
				Quantity.AccountID,
				Quantity.EntityID,
				Quantity.AsOfDate as QuantityAsOfDate,
				Quantity.Quantity,
				Quantity.QuantityOfOne
			FROM (
				SELECT DateList.AsOfDate as AsOfDate,
					Quantity.SecurityID,
					Quantity.AccountSecurityID,
					MAX(Quantity.AsOfDate) as QuantityAsOfDate 
				FROM @SecurityList as AccountSecurity
					CROSS JOIN #DateList as Datelist
					LEFT JOIN #Quantity as Quantity ON AccountSecurity.SecurityID = Quantity.SecurityID
				WHERE Quantity.AsOfDate <= DateList.AsOfDate	
				GROUP BY Quantity.SecurityID,
					Quantity.AccountSecurityID,
					DateList.AsOfDate
				) 
				AS AsOfDate 
				JOIN #Quantity as Quantity ON Quantity.AccountSecurityID = AsOfDate.AccountSecurityID 
						AND Quantity.AsOfDate = AsOfDate.QuantityAsOfDate
			--WHERE Quantity.Quantity <> 0
			) as Quantity ON Price.SecurityID = Quantity.SecurityID
						AND Price.AsOfDate = Quantity.AsOfDate
		JOIN @SecurityList as SecurityList ON Quantity.AccountSecurityID = SecurityList.AccountSecurityID
		LEFT JOIN #Activity as Activity ON Quantity.AccountSecurityID = Activity.AccountSecurityID
												AND Quantity.AsOfDate = Activity.AsOfDate
	--WHERE SecurityList.AccountID =221
	--ORDER BY Quantity.AsOfDate

DROP TABLE #Quantity
DROP TABLE #DateList
DROP TABLE #Activity


GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROC [dbo].[CalculateTWR_By_Level]
	@Level nvarchar(10),
	@LevelID bigint,
	@BeginDate smalldatetime,
	@EndDate smalldatetime

AS

BEGIN
	BEGIN TRY

		-- CREATE a list of all securities to present
		DECLARE @SecurityList SecurityList

		INSERT @SecurityList (
			FamilyID, 
			EntityID, 
			AccountID, 
			AccountSecurityID, 
			SecurityID, 
			SecurityName
			)
		EXEC CreateSecurityList_By_Level
			@Level = @Level,
			@LevelID = @LevelID


		-- Create a list of all the Cash Flows
		EXEC CashFlowForTWR_By_Level
			@SecurityList = @SecurityList,
			@BeginDate = @BeginDate,
			@EndDate = @EndDate

	END TRY
	BEGIN CATCH
		DECLARE @ErrMsg nvarchar(4000) = ERROR_MESSAGE()
		DECLARE @ErrNum INT = ERROR_NUMBER()
		DECLARE @ErrProc nvarchar(126) = ERROR_PROCEDURE()
		DECLARE @DataError nvarchar(4000) = 'Error running PROC CalculateTWR_By_Level' +
			CONVERT(nvarchar(10), @errNum) + ', Error Details: ' + @ErrMsg
		RAISERROR (@DataError, 16,1)
	END CATCH
END

GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROC [dbo].[CashFlowForTWR_By_Level]
	@SecurityList SecurityList Readonly,
	@BeginDate smalldatetime,
	@EndDate smalldatetime,
	@DetailRecordSet bit = 0,
	@CashFlows numeric(18,6) = 0 OUTPUT

AS 

CREATE TABLE #CashFlows (
	CashFlows numeric(18,6),
	TradeDate smalldatetime,
	TransCodeName varchar(100),
	EntityID bigint,
	AccountID bigint,
	AccountSecurityID bigint,
	SecurityID bigint
	)

INSERT INTO #CashFlows (
	CashFlows,
	TradeDate,
	TransCodeName,
	EntityID,
	AccountID,
	AccountSecurityID,
	SecurityID
	)
SELECT Trans.CashFlows, 
	Trans.TradeDate, 
	Trans.TransCodeName, 
	Trans.EntityID, 
	Trans.AccountID, 
	Trans.AccountSecurityID, 
	Trans.SecurityID
FROM [tfn_CashFlowForTWR_By_Level] (
	@SecurityList,
	@BeginDate,
	@EndDate
	) as Trans
ORDER BY Trans.TradeDate

IF @DetailRecordSet = 1
BEGIN
	SELECT * FROM #CashFlows
END

SELECT @CashFlows = SUM(CashFlows)
FROM #CashFlows


GO

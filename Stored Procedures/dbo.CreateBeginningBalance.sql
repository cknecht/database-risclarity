SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROC [dbo].[CreateBeginningBalance]
	@AccountID BIGINT = 0,
	@SecurityID BIGINT = 0,
	@SecurityName VARCHAR(1000),
	@Symbol VARCHAR(10),
	@CUSIP VARCHAR(10),
	@TradeDate SMALLDATETIME,
	@TransCodeID INT,
	@Quantity NUMERIC(18,6),
	@Price NUMERIC(18,6),
	@Amount NUMERIC(18,6),
	@Username VARCHAR(100)
AS

BEGIN TRANSACTION

	DECLARE @QuantityOfOne BIT
	DECLARE @AccountSecurityID bigint

	DECLARE @SecurityTable table(
		SecurityID bigint
		)

	DECLARE @AccountSecurityTable table(
		AccountSecurityID bigint
		)


IF ISNULL(@AccountID, 0) <> 0
BEGIN
	-- Lookup Security
	IF ISNULL(@SecurityID, 0) = 0
	BEGIN
		--Lookup by Symbol

		SELECT @SecurityID = Security.SecurityID
		FROM dbo.Security
		WHERE Symbol = @Symbol

		--Lookup by CUSIP

		IF ISNULL(@SecurityID, 0) = 0 
		BEGIN
			SELECT @SecurityID = Security.SecurityID
			FROM dbo.Security
			WHERE CUSIP = @CUSIP
		END

		--Lookup by SecurityName

		IF ISNULL(@SecurityID, 0) = 0 
		BEGIN
			SELECT @SecurityID = Security.SecurityID
			FROM dbo.Security
			WHERE SecurityName = @SecurityName
		END	

		--Insert new Security

		IF ISNULL(@SecurityID, 0) = 0 
		BEGIN

			IF @Quantity = 1
			BEGIN
				SET @QuantityOfOne = 1
			END

			INSERT INTO Security (
					SecurityName,
					Symbol,
					CUSIP,
					QuantityOfOne,
					LocalCurrencyID,
					EnteredBy,
					EnteredDateTime
					)
			OUTPUT INSERTED.SecurityID
			INTO @SecurityTable
			VALUES (@SecurityName,
					@Symbol,
					@CUSIP,
					@QuantityOfOne,
					234,
					@UserName,
					CONVERT(varchar(100), GETUTCDATE())
					)

			SELECT @SecurityID = SecurityID
			FROM @SecurityTable

		END
	END 

	--SELECT @AccountSecurityID
	SELECT @AccountSecurityID = @AccountSecurityID
	FROM AccountSecurity
	WHERE AccountID = @AccountID
		AND SecurityID = @SecurityID

	--INSERT INTO AccountSecurityID
	IF ISNULL(@AccountSecurityID, 0) = 0
	BEGIN
		INSERT INTO AccountSecurity (
			AccountID,
			SecurityID
			)
		OUTPUT INSERTED.AccountSecurityID
			INTO @AccountSecurityTable
		VALUES(
			@AccountID,
			@SecurityID
			)

		SELECT @AccountSecurityID = AccountSecurityID
		FROM @AccountSecurityTable
	END

	--INSERT PRICE														

	IF (SELECT COUNT(PriceID)
		FROM Price
		WHERE SecurityID = @SecurityID
			AND AsOfDate = @TradeDate) = 0
	BEGIN
		INSERT INTO dbo.Price
				( SecurityID ,
				  AsOfDate ,
				  Price ,
				  Estimate ,
				  DateTimeEntered ,
				  EnteredBy 
				)
		VALUES  ( @SecurityID, -- SecurityID - bigint
				  @Tradedate , -- AsOfDate - smalldatetime
				  @Price , -- Price - numeric
				  0 , -- Estimate - bit
				  GETUTCDATE() , -- DateTimeEntered - smalldatetime
				  @Username -- EnteredBy - varchar(255)
				)
	END 

	--INSERT TRANSACTION

	INSERT INTO dbo.Trans
	        ( AccountSecurityID ,
	          TradeDate ,
	          SettleDate ,
	          TransCodeID ,
	          Quantity ,
			  CurrencyID_Base,
			  CurrencyID_Local,
	          UnitPrice_Base ,
	          UnitPrice_Local ,
	          Amount_Base ,
	          Amount_Local ,
	          DateTimeEntered ,
	          DataSource ,
	          SourceTransactionCodeName ,
	          SourceName ,
	          SourceTransactionID ,
	          Notes
	        )
	VALUES  ( @AccountSecurityID, -- AccountSecurityID - bigint
	          @TradeDate , -- TradeDate - smalldatetime
	          @TradeDate , -- SettleDate - smalldatetime
	          @TransCodeID , -- TransCodeID - int
	          @Quantity , -- Quantity - numeric
			  234,
			  234,
	          @Price , -- UnitPrice_Base - numeric
	          @Price , -- UnitPrice_Local - numeric
	          @Amount , -- Amount_Base - numeric
	          @Amount , -- Amount_Local - numeric
	          GETUTCDATE() , -- DateTimeEntered - smalldatetime
	          @UserName, -- DataSource - varchar(max)
	          '' , -- SourceTransactionCodeName - varchar(max)
	          '' , -- SourceName - varchar(100)
	          0 , -- SourceTransactionID - bigint
	          ''  -- Notes - varchar(max)
	        )

END

COMMIT TRANSACTION
GO

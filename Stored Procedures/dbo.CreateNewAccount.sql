SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROC [dbo].[CreateNewAccount]
	@EntityID BIGINT = NULL,
	@AccountName VARCHAR(100),
	@AccountNumber VARCHAR(100),
	@AccountDescription VARCHAR(1000),
	@InceptionDate SMALLDATETIME,
	@DefaultCashSecurityID BIGINT = 0,
	@BaseCurrencyID INT = 234,
	@ExcludeFromPerformance BIT = 0,
	@Username VARCHAR(100)

AS

BEGIN TRANSACTION

	DECLARE @AccountTable table(
		AccountID bigint
		)

	IF @EntityID IS NOT NULL
	BEGIN

	INSERT INTO dbo.Account
			( AccountName ,
			  AccountNumber ,
			  AccountDescription ,
			  DefaultCashSecurityID ,
			  BaseCurrencyID ,
			  InceptionDate ,
			  EnteredBy ,
			  EnteredDateTime ,
			  DataSource ,
			  DataSourceID ,
			  Inactive ,
			  SourceName ,
			  NoCashAsset ,
			  ExcludeFromPerformance
			)
	OUTPUT INSERTED.AccountID
	INTO @AccountTable
	VALUES  ( @AccountName , -- AccountName - varchar(max)
			  @AccountNumber , -- AccountNumber - varchar(50)
			  @AccountDescription , -- AccountDescription - varchar(max)
			  @DefaultCashSecurityID , -- DefaultCashSecurityID - bigint
			  @BaseCurrencyID, -- BaseCurrencyID - int
			  @InceptionDate , -- InceptionDate - smalldatetime
			  @Username , -- EnteredBy - varchar(255)
			  GETUTCDATE() , -- EnteredDateTime - smalldatetime
			  'CreateNewAccount' , -- DataSource - varchar(max)
			  0 , -- DataSourceID - bigint
			  0 , -- Inactive - bit
			  NULL , -- SourceName - nvarchar(100)
			  NULL , -- NoCashAsset - bit
			  @ExcludeFromPerformance  -- ExcludeFromPerformance - bit
			)

	INSERT INTO EntityAccount (
		EntityID,
		AccountID,
		DateTimeEntered,
		DataSource)
	SELECT @EntityID,
		AccountID,
		GetUTCDate(),
		'CreateNewAccount by ' + @Username
	FROM @AccountTable

	END

COMMIT TRANSACTION
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROC [dbo].[CreateSecurityList_By_Level]
	@Level varchar(2),
	@LevelID bigint
AS

BEGIN

	SET NOCOUNT ON
	BEGIN TRY

		DECLARE @SecurityList SecurityList

		IF @Level = 'F'
		BEGIN
			INSERT INTO @SecurityList
			(
			FamilyID,
			EntityID,
			AccountID,
			AccountSecurityID,
			SecurityID,
			SecurityName
			)
			SELECT FamilyEntity.FamilyID,
			EntityAccount.EntityID,
			AccountSecurity.AccountID,
			AccountSecurity.AccountSecurityID,
			AccountSecurity.SecurityID,
			Security.SecurityName
			FROM FamilyEntity 
			JOIN EntityAccount ON FamilyEntity.EntityID = EntityAccount.EntityID
			JOIN AccountSecurity ON EntityAccount.AccountID = AccountSecurity.AccountID
			JOIN Security ON AccountSecurity.SecurityID = Security.SecurityID
			WHERE FamilyEntity.FamilyID = @LevelID
		END
		ELSE IF @Level = 'E'
		BEGIN
			INSERT INTO @SecurityList
			(
			FamilyID,
			EntityID,
			AccountID,
			AccountSecurityID,
			SecurityID,
			SecurityName
			)
			SELECT FamilyEntity.FamilyID,
			EntityAccount.EntityID,
			AccountSecurity.AccountID,
			AccountSecurity.AccountSecurityID,
			AccountSecurity.SecurityID,
			Security.SecurityName
			FROM EntityAccount
			JOIN AccountSecurity ON EntityAccount.AccountID = AccountSecurity.AccountID
			JOIN Security ON AccountSecurity.SecurityID = Security.SecurityID
			JOIN FamilyEntity ON EntityAccount.EntityID = FamilyEntity.EntityID
			WHERE EntityAccount.EntityID = @LevelID
		END
		ELSE IF @Level = 'A'
		BEGIN
			INSERT INTO @SecurityList
			(
			FamilyID,
			EntityID,
			AccountID,
			AccountSecurityID,
			SecurityID,
			SecurityName
			)
			SELECT FamilyEntity.FamilyID,
			EntityAccount.EntityID,
			AccountSecurity.AccountID,
			AccountSecurity.AccountSecurityID,
			AccountSecurity.SecurityID,
			Security.SecurityName
			FROM AccountSecurity 
			JOIN Security ON AccountSecurity.SecurityID = Security.SecurityID
			JOIN EntityAccount ON AccountSecurity.AccountID = EntityAccount.AccountID
			JOIN FamilyEntity ON EntityAccount.EntityID = FamilyEntity.EntityID
			WHERE AccountSecurity.AccountID = @LevelID
		END
		ELSE IF @Level = 'AS'
		BEGIN
			INSERT INTO @SecurityList
			(
			FamilyID,
			EntityID,
			AccountID,
			AccountSecurityID,
			SecurityID,
			SecurityName
			)
			SELECT FamilyEntity.FamilyID,
			EntityAccount.EntityID,
			AccountSecurity.AccountID,
			AccountSecurity.AccountSecurityID,
			AccountSecurity.SecurityID,
			Security.SecurityName
			FROM AccountSecurity 
			JOIN Security ON AccountSecurity.SecurityID = Security.SecurityID
			JOIN EntityAccount ON AccountSecurity.AccountID = EntityAccount.AccountID
			JOIN FamilyEntity ON EntityAccount.EntityID = FamilyEntity.EntityID
			WHERE AccountSecurity.AccountSecurityID = @LevelID
		END

	SELECT *
	FROM @SecurityList

	END TRY
	BEGIN CATCH
		DECLARE @ErrMsg nvarchar(4000) = ERROR_MESSAGE()
		DECLARE @ErrNum INT = ERROR_NUMBER()
		DECLARE @ErrProc nvarchar(126) = ERROR_PROCEDURE()
		DECLARE @DataError nvarchar(4000) = 'Error loading data to SecurityList table' +
			CONVERT(nvarchar(10), @errNum) + ', Error Details: ' + @ErrMsg
		RAISERROR (@DataError, 16,1)
	END CATCH
END
GO

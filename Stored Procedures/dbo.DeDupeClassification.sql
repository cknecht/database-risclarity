SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROC [dbo].[DeDupeClassification]

as

-- Dedupe Asset Class Table

UPDATE dbo.SecurityAssetClass
SET AsOFDate = '12/31/12'

delete t
from SecurityAssetClass t
inner join  
(
    select SecurityID, AsOfDate, min(SecurityAssetClassID) as min_id
    from SecurityAssetClass
    group by SecurityID, AsOfDate
    having count(*) > 1
) t2 on t.SecurityID = t2.SecurityID
	AND t.AsOFDate = t2.AsOfDate
where t.SecurityAssetClassID > t2.min_id

-- Dedupe Region Table

UPDATE dbo.SecurityRegion
SET AsOFDate = '12/31/12'

delete t
from SecurityRegion t
inner join  
(
    select SecurityID, AsOfDate, min(SecurityRegionID) as min_id
    from SecurityRegion
    group by SecurityID, AsOfDate
    having count(*) > 1
) t2 on t.SecurityID = t2.SecurityID
	AND t.AsOFDate = t2.AsOfDate
where t.SecurityRegionID > t2.min_id

-- Dedupe Sector Table

UPDATE dbo.SecuritySector
SET AsOFDate = '12/31/12'

delete t
from SecuritySector t
inner join  
(
    select SecurityID, AsOfDate, min(SecuritySectorID) as min_id
    from SecuritySector
    group by SecurityID, AsOfDate
    having count(*) > 1
) t2 on t.SecurityID = t2.SecurityID
	AND t.AsOFDate = t2.AsOfDate
where t.SecuritySectorID > t2.min_id

-- Dedupe Liquidity Table

UPDATE dbo.SecurityLiquidity
SET AsOFDate = '12/31/12'

delete t
from SecurityLiquidity t
inner join  
(
    select SecurityID, AsOfDate, min(SecurityLiquidityID) as min_id
    from SecurityLiquidity
    group by SecurityID, AsOfDate
    having count(*) > 1
) t2 on t.SecurityID = t2.SecurityID
	AND t.AsOFDate = t2.AsOfDate
where t.SecurityLiquidityID > t2.min_id
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROC [dbo].[DefaultCurrencyID_to_USD]

AS

SELECT     CurrencyID
FROM         Currency
WHERE     (CurrencyCode = 'USD')

GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROC [dbo].[DeleteTransaction]
	@TransID bigint,
	@Username varchar(255)

AS

DECLARE @Message varchar(max)

IF ISNUMERIC(@TransID) = 1
BEGIN
	DELETE FROM Trans
	WHERE TransID = @TransID

	SET @Message = 'TransID ' + CONVERT(varchar(10), @TransID) + ' Deleted'
END
ELSE
BEGIN
	SET @Message = 'Nothing Deleted'
END

SELECT @Message as [Message]
GO

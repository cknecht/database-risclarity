SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROC [dbo].[DisplayAccountPerformanceReview]
	@Level nvarchar(10) = 'E',
	@LevelID bigint,
	@EndDate SMALLDATETIME,
	@CompareToSource BIT = 0,
	@IncludeEstimates BIT = 1,
	@IncludeLinkedAccounts BIT = 1,
	@HideZeroAccounts BIT = 1,
	@GroupBy VARCHAR(20) = 'E',
	@MTD BIT = 0,
	@QTD BIT = 1,
	@YTD BIT = 1,
	@1YR BIT = 1,
	@3YR BIT = 0,
	@5YR BIT = 0,
	@10YR BIT = 1,
	@ITD BIT = 1

AS

BEGIN
	
	SET NOCOUNT ON

	--BEGIN TRY

		DECLARE @BeginDate smalldatetime = [dbo].[sfn_QuarterBegin](@EndDate)
		DECLARE @InceptionDate smalldatetime

		DECLARE @SecurityList securitylist

		INSERT INTO @SecurityList (
			AccountSecurityID,
			AccountID,
			AccountName,
			SecurityID,
			SecurityName,
			Pricefactor,
			ExcludeFromPerformance,
			PercentOwnership,
			EntityID,
			ContractSize
			)
		SELECT AccountSecurityID,
			AccountID,
			AccountName,
			SecurityID,
			SecurityName,
			PriceFactor,
			ExcludeFromPerformance,
			ISNULL(PercentOwnership, 100),
			EntityID,
			ContractSize
		FROM [dbo].[tfn_SecurityList_by_Level] (@Level, @LevelID, @EndDate, @IncludeLinkedAccounts)

		-- CREATE THE #Output Table that will be returned
		CREATE TABLE #Output (
			Level varchar(2),
			LevelID bigint,
			LevelName VARCHAR(100),
			LevelSortOrder INT,
			LevelColor VARCHAR(100),
			AccountID bigint,
			AccountName varchar(100),
			AccountNumber varchar(100),
			BeginDate smalldatetime,
			EndDate smalldatetime,
			Acct_BeginMarketValue numeric(18,6),
			Source_Acct_BeginMarketValue numeric(18,6),
			Acct_EndMarketValue numeric(18,6),
			Acct_NetInvested NUMERIC(18,6),
			Source_Acct_EndMarketValue numeric(18,6),
			LastUpdateDate SMALLDATETIME,
			Acct_Per_TWR FLOAT,
			Acct_YTD_TWR FLOAT,
			Acct_YR1_TWR FLOAT,
			Acct_YR3_TWR FLOAT,
			Acct_YR5_TWR FLOAT,
			Acct_YR10_TWR FLOAT,
			Acct_ITD_TWR FLOAT,
			Acct_InceptionDate smalldatetime,
			Acct_FirstTransDate SMALLDATETIME,
			SourceName VARCHAR(1000),
			Source_Acct_Per_TWR numeric(18,6),
			Source_Acct_YTD_TWR numeric(18,6),
			Source_Acct_YR1_TWR numeric(18,6),
			Source_Acct_YR3_TWR numeric(18,6),
			Source_Acct_YR5_TWR numeric(18,6),
			Source_Acct_YR10_TWR numeric(18,6),
			Source_Acct_ITD_TWR numeric(18,6),
			Level_BeginMarketValue numeric(18,6),
			Level_EndMarketValue numeric(18,6),
			Level_NetInvested NUMERIC(18,6),
			Level_Per_TWR FLOAT,
			Level_YTD_TWR FLOAT,
			Level_YR1_TWR FLOAT,
			Level_YR3_TWR FLOAT,
			Level_YR5_TWR FLOAT,
			Level_YR10_TWR FLOAT,
			Level_ITD_TWR FLOAT,
			Level_InceptionDate SMALLDATETIME,
			PercentOwnerShip NUMERIC(18,6),
			ExcludeFromPerformance BIT,
			EntityID BIT,
			EntityName VARCHAR(500),
			InceptionDate smalldatetime
			)

		-- CREATE the TWR temp table that will be used to calculate account and level independently
		CREATE TABLE #TWR (
			[TWR_ID] [bigint] identity(1,1) NOT NULL,
			[Level] nvarchar(1) NOT NULL,
			[LevelID] [bigint] NOT NULL,
			[AccountID] [bigint] NULL,
			[AccountName] varchar(100),
			[AccountNumber] varchar(100),
			[AccountInceptionDate] smalldatetime,
			[PeriodBeginDate] [smalldatetime] NULL,
			[PeriodEndDate] [smalldatetime] NULL,
			[TWRBeginDate] [smalldatetime] NULL,
			[TWREndDate] [smalldatetime] NULL,
			[BeginMarketValue] [numeric](18, 6) NULL,
			[EndMarketValue] [numeric](18, 6) NULL,
			[CashFlow] [numeric](18, 6) NULL,
			[Additions] [numeric](18, 6) NULL,
			[Withdrawals] [numeric](18, 6) NULL,
			[Income] [numeric](18, 6) NULL,
			[AccruedIncome] [numeric](18, 6) NULL,
			[Fees] [numeric](18, 6) NULL,
			[PeriodTWR] [numeric](18, 6) NULL,
			[PeriodTWR_Net_Of_Fees] [numeric](18, 6) NULL,
			[PeriodTWR_Plus_One] [numeric](18, 6) NULL,
			[PeriodTWR_Net_Of_Fees_Plus_One] [numeric](18, 6) NULL,
			[TWR] float NULL,
			[AnnualizedTWR] float NULL,
			[TWR_Net_Of_Fees] float NULL,
			[AnnualizedTWR_Net_Of_Fees] float NULL,
			[ExcludeFromPerformance] bit
		)

		CREATE INDEX TWRIndex1 ON #TWR (PeriodBeginDate, TWRBeginDate)
		CREATE INDEX TWRIndex2 ON #TWR (PeriodEndDate, TWREndDate)

		-- Create and populate the periodlist based on the type of report
		DECLARE @PeriodList periodlist
		INSERT INTO @PeriodList(
			BeginDate,
			EndDate
			)
		SELECT BeginDate,
			EndDate
		FROM [dbo].[tfn_PeriodList_PerformanceReview] (
			@EndDate,
			@MTD,
			@QTD,
			@YTD,
			@1YR,
			@3YR,
			@5YR,
			@10YR,
			@ITD
			)
		UNION
		SELECT BeginDate,
			EndDate
		FROM [dbo].[tfn_PeriodList_Months] (
			DATEADD(Month, -12, @BeginDate),
			@EndDate
			)
		UNION -- inception from '1/1/13'
		SELECT COALESCE(NULLIF(DATEADD(day, 0, ForceInceptionDateTo), '1/1/1900'), NULLIF(DATEADD(day, 0, InceptionDate), '1/1/1900'), '1/1/2000') AS InceptionDate,
			@EndDate
		FROM Account 
			JOIN @SecurityList AS SecurityList ON Account.AccountID = SecurityList.AccountID

		EXEC [CalculateDataForTWR]
				@SecurityList,
				@PeriodList,
				@RecordSet = 0,
				@Level = @Level,
				@GroupBy = 'Account'

		-- UPDATE THE ACCOUNT RETURNS
		INSERT INTO #Output (
			Level,
			LevelID,
			AccountID,
			AccountName,
			AccountNumber,
			BeginDate,
			EndDate,
			Acct_BeginMarketValue,
			PercentOwnership,
			EntityID,
			EntityName,
			ExcludeFromPerformance
			)
		SELECT @GroupBy,
			ISNULL(CASE @GroupBy
				WHEN 'E' THEN Entity.EntityID
				WHEN 'A' THEN Account.AccountID
				WHEN 'MS' THEN Account.ManagerStyleID
				END, 0) AS GroupByID,
			SecurityList.AccountID,
			SecurityList.AccountName,
			#TWR.AccountNumber,
			@BeginDate as BeginDate,
			@EndDate as EndDate,
			NULL AS Acct_BeginMarketValue,
			--BeginMarketValue as Acct_BeginMarketValue,
			SecurityList.PercentOwnership,
			SecurityList.EntityID,
			Entity.EntityName,
			SecurityList.ExcludeFromPerformance
		FROM #TWR
			LEFT JOIN @SecurityList AS SecurityList ON #TWR.AccountID = SecurityList.AccountID
			LEFT JOIN Entity ON SecurityList.EntityID = Entity.EntityID
			LEFT JOIN Account ON SecurityList.AccountID = Account.AccountID
		GROUP BY SecurityList.AccountID,
			SecurityList.AccountName,
			#TWR.AccountNumber,
			SecurityList.PercentOwnership,
			CASE @GroupBy
				WHEN 'E' THEN Entity.EntityID
				WHEN 'A' THEN Account.AccountID
				WHEN 'MS' THEN Account.ManagerStyleID
				END,
			SecurityList.EntityID,
			Entity.EntityName,
			SecurityList.ExcludeFromPerformance

		IF @GroupBy = 'MS'
		BEGIN
			UPDATE #Output
			SET LevelName = Managerstyle.ManagerstyleName,
				LevelSortOrder = Managerstyle.SortOrder,
				LevelColor = Managerstyle.Color
			FROM #Output
				JOIN ManagerStyle ON #Output.LevelID = Managerstyle.ManagerstyleID
		END
		ELSE 
		BEGIN
			UPDATE #Output
			SET LevelName = EntityName
		END
		
		-- UPDATE the Acct Begin Market Value
		UPDATE #Output 
		SET Acct_BeginMarketValue = BeginMarketValue
		FROM #Output as Output
			CROSS APPLY #TWR as TWR
		WHERE TWRBeginDate = DATEADD(DAY, 0, [dbo].[sfn_QuarterBegin](@EndDate))
			AND (PeriodBeginDate = DATEADD(DAY, 0, [dbo].[sfn_QuarterBegin](@EndDate)))
			AND Output.AccountID = TWR.AccountID

			--SELECT AccountID,
			--	MIN(TWRBeginDate)
			--FROM #TWR
			--	WHERE TWRBeginDate < PeriodBeginDate
			--GROUP BY AccountID


		-- UPDATE the Acct End Market Value
		UPDATE #Output 
		SET Acct_EndMarketValue = EndMarketValue
		FROM #Output as Output
			CROSS APPLY #TWR as TWR
		WHERE TWREndDate = @EndDate
			AND PeriodEndDate = @EndDate 
			AND Output.AccountID = TWR.AccountID

		UPDATE #Output
		SET LastUpdateDate = LastUpdate.AsOfDate
		FROM #Output
			JOIN (SELECT AsOfDate,
					AccountID
					FROM [tfn_LastAccountActivityDate_By_Level] (
						@Level, 
						@LevelID,
						@EndDate,
						@SecurityList,
						@IncludeEstimates,
						0)
				) as LastUpdate ON #OUTPUT.AccountID = LastUpdate.AccountID

		UPDATE #Output
		SET --Acct_InceptionDate = InceptionDate.InceptionDate,
			InceptionDate = InceptionDate.InceptionDate
		FROM #Output
			LEFT JOIN (SELECT COALESCE(ForceInceptionDateTo, InceptionDate, '1/1/2000') AS InceptionDate,
				Account.AccountID
				FROM dbo.Account
					JOIN @SecurityList AS SecurityList ON SecurityList.AccountID = Account.AccountID) AS InceptionDate
					 ON #OUTPUT.AccountID = Inceptiondate.AccountID

		-- Net Invested

		UPDATE #Output
		SET Acct_NetInvested = Trans.NetInvested
		FROM #Output
			JOIN (SELECT SUM(ABS(Amount_Base) * CalcNetInvested) AS NetInvested,
						Trans.AccountID AS AccountID
					FROM [dbo].[tfn_Transactions_By_Level] (@SecurityList, '1/1/1900', @EndDate) AS Trans
					WHERE ISNULL(Trans.CalcNetInvested, 0) <> 0
						AND Trans.TransID > 0
					GROUP BY Trans.AccountID
				) as Trans ON #Output.AccountID = Trans.AccountID
		

		-- INSERT the Source MarketValues and Performance for Comparison
		IF @CompareToSource = 1
		BEGIN
			-- Begin Market Value
			UPDATE #OutPut
			SET Source_Acct_BeginMarketValue = Compare.MarketValue,
				SourceName = Compare.SourceName
			FROM #Output
				JOIN tfn_SourceAccountBalance_By_Level (
						@Level, 
						@LevelID,
						@BeginDate,
						@IncludeLinkedAccounts
						) AS Compare ON #Output.AccountID = Compare.AccountID
			
			-- End Market Value
			UPDATE #OutPut
			SET Source_Acct_EndMarketValue = Compare.MarketValue
			FROM #Output
				JOIN tfn_SourceAccountBalance_By_Level (
						@Level, 
						@LevelID,
						@EndDate,
						@IncludeLinkedAccounts
						) AS Compare ON #Output.AccountID = Compare.AccountID

			-- Period Return
			UPDATE #Output
			SET Source_Acct_Per_TWR = (Compare.PeriodReturn * .01)
			FROM #Output
				JOIN tfn_SourceAccountPerformance_By_Level (
						@Level, 
						@LevelID,
						@EndDate,
						@IncludeLinkedAccounts,
						@periodList
						) AS Compare ON #Output.AccountID = Compare.AccountID
							AND Compare.BeginDate = @BeginDate
							AND Compare.EndDate = @EndDate

			-- YTD Return
			UPDATE #Output
			SET Source_Acct_YTD_TWR = (Compare.PeriodReturn * .01)
			FROM #Output
				JOIN tfn_SourceAccountPerformance_By_Level (
						@Level, 
						@LevelID,
						@EndDate,
						@IncludeLinkedAccounts,
						@periodList
						) AS Compare ON #Output.AccountID = Compare.AccountID
							AND Compare.BeginDate = [dbo].[sfn_YearBegin](@EndDate)
							AND Compare.EndDate = @EndDate

			-- 1YR Return
			UPDATE #Output
			SET Source_Acct_YR1_TWR = (Compare.PeriodReturn * .01)
			FROM #Output
				JOIN tfn_SourceAccountPerformance_By_Level (
						@Level, 
						@LevelID,
						@EndDate,
						@IncludeLinkedAccounts,
						@periodList
						) AS Compare ON #Output.AccountID = Compare.AccountID
							AND Compare.BeginDate = [dbo].[sfn_PreviousYear](@EndDate, 1)
							AND Compare.EndDate = @EndDate
			-- 3YR Return
			UPDATE #Output
			SET Source_Acct_YR3_TWR = (Compare.PeriodReturn * .01)
			FROM #Output
				JOIN tfn_SourceAccountPerformance_By_Level (
						@Level, 
						@LevelID,
						@EndDate,
						@IncludeLinkedAccounts,
						@periodList
						) AS Compare ON #Output.AccountID = Compare.AccountID
							AND Compare.BeginDate = [dbo].[sfn_PreviousYear](@EndDate, 3)
							AND Compare.EndDate = @EndDate

			-- 5YR Return
			UPDATE #Output
			SET Source_Acct_YR5_TWR = (Compare.PeriodReturn * .01)
			FROM #Output
				JOIN tfn_SourceAccountPerformance_By_Level (
						@Level, 
						@LevelID,
						@EndDate,
						@IncludeLinkedAccounts,
						@periodList
						) AS Compare ON #Output.AccountID = Compare.AccountID
							AND Compare.BeginDate = [dbo].[sfn_PreviousYear](@EndDate, 5)
							AND Compare.EndDate = @EndDate

			-- 10YR Return
			UPDATE #Output
			SET Source_Acct_YR10_TWR = (Compare.PeriodReturn * .01)
			FROM #Output
				JOIN tfn_SourceAccountPerformance_By_Level (
						@Level, 
						@LevelID,
						@EndDate,
						@IncludeLinkedAccounts,
						@periodList
						) AS Compare ON #Output.AccountID = Compare.AccountID
							AND Compare.BeginDate = [dbo].[sfn_PreviousYear](@EndDate, 10)
							AND Compare.EndDate = @EndDate
		END

		-- UPDATE the First Transactions Date
		UPDATE #Output 
		SET Acct_FirstTransDate = TWR.FirstTransDate
		FROM #Output as Output
			JOIN (SELECT MIN(TWRBeginDate) as FirstTransDate, 
					AccountID 
				FROM #TWR as TWR
				WHERE BeginMarketValue <> 0
				GROUP BY AccountID) as TWR ON Output.AccountID = TWR.AccountID

		---- UPDATE the Inception Date
		UPDATE #Output 
		SET Acct_InceptionDate = TWR.InceptionDate
		FROM #Output as Output
			JOIN (SELECT CASE WHEN AccountInceptionDate IS NULL
						THEN MIN(TWRBeginDate)
						ELSE AccountInceptionDate
						END as InceptionDate, 
					AccountID 
				FROM #TWR as TWR
				--WHERE BeginMarketValue <> 0
				GROUP BY AccountID,
					AccountInceptionDate) as TWR ON Output.AccountID = TWR.AccountID

		-- UPDATE the Acct Qtr TWR
		UPDATE #Output 
		SET Acct_Per_TWR = TWR
		FROM #Output as Output
			CROSS APPLY #TWR as TWR
		WHERE PeriodBeginDate = @BeginDate
			AND PeriodEndDate = @EndDate 
			AND Output.AccountID = TWR.AccountID
			AND ISNULL(TWR.ExcludeFromPerformance, 0) = 0
		-- END UPDATE the Acct Qtr TWR

		-- UPDATE the Acct YTD TWR
		UPDATE #Output 
		SET Acct_YTD_TWR = TWR
		FROM #Output as Output
			CROSS APPLY #TWR as TWR
		WHERE PeriodBeginDate = [dbo].[sfn_YearBegin](@EndDate) 
			AND PeriodEndDate = @EndDate 
			AND Output.AccountID = TWR.AccountID
			AND ISNULL(TWR.ExcludeFromPerformance, 0) = 0
		-- END UPDATE the Acct YTD TWR

		-- UPDATE the Acct 1Year TWR
		UPDATE #Output 
		SET Acct_YR1_TWR = TWR
		FROM #Output as Output
			CROSS APPLY #TWR as TWR
		WHERE PeriodBeginDate = [dbo].[sfn_PreviousYear](@EndDate, 1)
			AND PeriodEndDate = @EndDate 
			AND Output.AccountID = TWR.AccountID
			AND DATEADD(day, -1, OUTPUT.Acct_InceptionDate) <= [dbo].[sfn_PreviousYear](@EndDate, 1)
			AND ISNULL(TWR.ExcludeFromPerformance, 0) = 0
		-- END UPDATE the Acct 1Year TWR

		-- UPDATE the Acct 3Year TWR
		UPDATE #Output 
		SET Acct_YR3_TWR = [dbo].[sfn_AnnualizeReturn](TWR, [dbo].[sfn_PreviousYear](@EndDate, 3), @EndDate)
		FROM #Output as Output
			CROSS APPLY #TWR as TWR
		WHERE PeriodBeginDate = [dbo].[sfn_PreviousYear](@EndDate, 3)
			AND PeriodEndDate = @EndDate 
			AND Output.AccountID = TWR.AccountID
			AND DATEADD(day, -1, OUTPUT.Acct_InceptionDate) <= [dbo].[sfn_PreviousYear](@EndDate, 3)
			AND ISNULL(TWR.ExcludeFromPerformance, 0) = 0
		-- END UPDATE the Acct 3Year TWR

		-- UPDATE the Acct 5Year TWR
		UPDATE #Output 
		SET Acct_YR5_TWR = [dbo].[sfn_AnnualizeReturn](TWR, [dbo].[sfn_PreviousYear](@EndDate, 5), @EndDate)
		FROM #Output as Output
			CROSS APPLY #TWR as TWR
		WHERE PeriodBeginDate = [dbo].[sfn_PreviousYear](@EndDate, 5)
			AND PeriodEndDate = @EndDate 
			AND Output.AccountID = TWR.AccountID
			AND DATEADD(day, -1, OUTPUT.Acct_InceptionDate) <= [dbo].[sfn_PreviousYear](@EndDate, 5)
			AND ISNULL(TWR.ExcludeFromPerformance, 0) = 0
		-- END UPDATE the Acct 5Year TWR

		-- UPDATE the Acct 10Year TWR
		UPDATE #Output 
		SET Acct_YR10_TWR = [dbo].[sfn_AnnualizeReturn](TWR, [dbo].[sfn_PreviousYear](@EndDate, 10), @EndDate)
		FROM #Output as Output
			CROSS APPLY #TWR as TWR
		WHERE PeriodBeginDate = [dbo].[sfn_PreviousYear](@EndDate, 10)
			AND PeriodEndDate = @EndDate 
			AND Output.AccountID = TWR.AccountID
			AND DATEADD(day, -1, OUTPUT.Acct_InceptionDate) <= [dbo].[sfn_PreviousYear](@EndDate, 10)
			AND ISNULL(TWR.ExcludeFromPerformance, 0) = 0
		-- END UPDATE the Acct 10Year TWR

		---- UPDATE the Inception TWR
		UPDATE #Output 
		SET Acct_ITD_TWR = [dbo].[sfn_AnnualizeReturn] (TWR, InceptionDate, @EndDate) --(TWR, Acct_FirstTransDate, @EndDate)
		FROM #Output as Output
			CROSS APPLY #TWR as TWR
		WHERE PeriodBeginDate = Output.InceptionDate --[dbo].[sfn_PreviousYear](@EndDate, 10)
			AND PeriodEndDate = @EndDate 
			AND Output.AccountID = TWR.AccountID
			AND ISNULL(TWR.ExcludeFromPerformance, 0) = 0
		---- END UPDATE the Inception TWR

		IF @Level <> 'A' AND @GroupBy <> 'MS'
		BEGIN

			TRUNCATE TABLE #TWR

			EXEC [CalculateDataForTWR]
				@SecurityList,
				@PeriodList,
				@RecordSet = 0,
				@Level = @Level,
				@GroupBy = 'Entity'

			------ Annualize the values
			----UPDATE #TWR 
			----SET [AnnualizedTWR] = [dbo].[sfn_AnnualizeReturn](TWR, PeriodBeginDate, PeriodEndDate)

			-- UPDATE the Level Begin Market Value
			UPDATE #Output 
			SET Level_BeginMarketValue = BeginMarketValue 
			FROM #TWR
			WHERE TWRBeginDate = [dbo].[sfn_QuarterBegin](@EndDate) 
				AND PeriodBeginDate = [dbo].[sfn_QuarterBegin](@EndDate) 

			-- UPDATE the Level End Market Value
			UPDATE #Output 
			SET Level_EndMarketValue = EndMarketValue
			FROM #Output as Output
				CROSS APPLY #TWR as TWR
			WHERE TWREndDate = @EndDate
				AND PeriodEndDate = @EndDate

			-- UPDATE the Inception Date
			UPDATE #Output 
			SET Level_InceptionDate = TWR.InceptionDate
			FROM #Output as Output
				JOIN (SELECT CASE WHEN MIN(AccountInceptionDate) IS NULL
							THEN MIN(TWRBeginDate)
							ELSE MIN(AccountInceptionDate)
							END as InceptionDate, 
						LevelID
					FROM #TWR as TWR
					WHERE BeginMarketValue <> 0
					GROUP BY AccountInceptionDate,
						LevelID) as TWR ON Output.LevelID = TWR.LevelID

			-- UPDATE the Level Qtr TWR
			UPDATE #Output 
			SET Level_Per_TWR = TWR
			FROM #Output as Output
				CROSS APPLY #TWR as TWR
			WHERE PeriodBeginDate = [dbo].[sfn_QuarterBegin](@EndDate) 
				AND PeriodEndDate = @EndDate 
				AND ISNULL(TWR.ExcludeFromPerformance, 0) = 0
			-- END UPDATE the Level Qtr TWR

			-- UPDATE the Level YTD TWR
			UPDATE #Output 
			SET Level_YTD_TWR = TWR
			FROM #Output as Output
				CROSS APPLY #TWR as TWR
			WHERE PeriodBeginDate = [dbo].[sfn_YearBegin](@EndDate) 
				AND PeriodEndDate = @EndDate 
				--AND ISNULL(TWR.ExcludeFromPerformance, 0) = 0
			-- END UPDATE the Level YTD TWR

			-- UPDATE the Level 1Year TWR
			UPDATE #Output 
			SET Level_YR1_TWR = TWR
			FROM #Output as Output
				CROSS APPLY #TWR as TWR
			WHERE PeriodBeginDate = [dbo].[sfn_PreviousYear](@EndDate, 1)
				AND PeriodEndDate = @EndDate 
				--AND Output.LevelID = TWR.LevelID
				AND OUTPUT.Level_InceptionDate <= [dbo].[sfn_PreviousYear](@EndDate, 1)
			-- END UPDATE the Level 1Year TWR

			-- UPDATE the Level 3Year TWR
			UPDATE #Output 
			SET Level_YR3_TWR = [AnnualizedTWR]
			FROM #Output as Output
				CROSS APPLY #TWR as TWR
			WHERE PeriodBeginDate = [dbo].[sfn_PreviousYear](@EndDate, 3)
				AND PeriodEndDate = @EndDate 
				--AND Output.LevelID = TWR.LevelID
				AND OUTPUT.Level_InceptionDate <= [dbo].[sfn_PreviousYear](@EndDate, 3)
			-- END UPDATE the Level 3Year TWR

			-- UPDATE the Level 5Year TWR
			UPDATE #Output 
			SET Level_YR5_TWR = [AnnualizedTWR]
			FROM #Output as Output
				CROSS APPLY #TWR as TWR
			WHERE PeriodBeginDate = [dbo].[sfn_PreviousYear](@EndDate, 5)
				AND PeriodEndDate = @EndDate 
				--AND Output.LevelID = TWR.LevelID
				AND OUTPUT.Level_InceptionDate <= [dbo].[sfn_PreviousYear](@EndDate, 5)
			-- END UPDATE the Level 5Year TWR

			-- UPDATE the Level 10Year TWR
			UPDATE #Output 
			SET Level_YR10_TWR = [AnnualizedTWR]
			FROM #Output as Output
				CROSS APPLY #TWR as TWR
			WHERE PeriodBeginDate = [dbo].[sfn_PreviousYear](@EndDate, 10) 
				AND PeriodEndDate = @EndDate 
				--AND Output.LevelID = TWR.LevelID
				AND OUTPUT.Level_InceptionDate <= [dbo].[sfn_PreviousYear](@EndDate, 10)
			-- END UPDATE the Level 10Year TWR

			---- UPDATE the Inception TWR
			--UPDATE #Output 
			--SET Level_YR10_TWR = [AnnualizedTWR]
			--FROM #Output as Output
			--	CROSS APPLY #TWR as TWR
			--WHERE PeriodBeginDate = TWR.AccountInceptionDate
			--	AND PeriodEndDate = @EndDate 
			--	AND Output.LevelID = TWR.LevelID
			---- END UPDATE the Level Inception TWR		
		END
		
		IF @HideZeroAccounts = 1
		BEGIN
			SELECT * FROM #Output
			WHERE ISNULL(Acct_BeginMarketValue, 0) <> 0 
				OR ISNULL(Acct_EndMarketValue, 0) <> 0
		END
		ELSE
		BEGIN
			SELECT * FROM #Output
		END

		DROP TABLE #Output
		DROP TABLE #TWR

	--END TRY
	--BEGIN CATCH
	--	DECLARE @ErrMsg nvarchar(4000) = ERROR_MESSAGE()
	--	DECLARE @ErrNum INT = ERROR_NUMBER()
	--	DECLARE @ErrProc nvarchar(126) = ERROR_PROCEDURE()
	--	DECLARE @DataError nvarchar(4000) = 'Error running PROC [DisplayAccountPerformanceReview]' +
	--		CONVERT(nvarchar(10), @errNum) + ', Error Details: ' + @ErrMsg
	--	RAISERROR (@DataError, 16,1)
	--	--ROLLBACK TRANSACTION
	--END CATCH

END

GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROC [dbo].[DisplayActivity_By_Level]
	@Level VARCHAR(1),
	@LevelID BIGINT,
	@BeginDate SMALLDATETIME = NULL,
	@EndDate SMALLDATETIME,
	@IncludeLinkedAccounts BIT = 0


AS

IF ISNULL(@BeginDate, '1/1/1900') = '1/1/1900'
BEGIN
	SET @BeginDate = [dbo].[sfn_QuarterBegin] (@EndDate)
END

CREATE TABLE #Output (
	LEVEL VARCHAR(1),
	LevelID BIGINT,
	BeginDate SMALLDATETIME,
	EndDate SMALLDATETIME,
	Period_BeginMarketValue NUMERIC(18,6),
	Period_EndMarketValue NUMERIC(18,6),
	Period_Additions NUMERIC(18,6),
	Period_Withdrawals NUMERIC(18,6),
	Period_Fees NUMERIC(18,6),
	Period_Income NUMERIC(18,6),
	Period_MarketChanges NUMERIC(18,6),
	YTD_BeginMarketValue NUMERIC(18,6),
	YTD_EndMarketValue NUMERIC(18,6),
	YTD_Additions NUMERIC(18,6),
	YTD_Withdrawals NUMERIC(18,6),
	YTD_Fees NUMERIC(18,6),
	YTD_Income NUMERIC(18,6),
	YTD_MarketChanges NUMERIC(18,6),
	)


	DECLARE @SecurityList securitylist

	INSERT INTO @SecurityList (
		AccountSecurityID,
		AccountID,
		AccountName,
		SecurityID,
		SecurityName,
		Pricefactor,
		ExcludeFromPerformance,
		PercentOwnership,
		EntityID,
		ContractSize
		)
	SELECT AccountSecurityID,
		AccountID,
		AccountName,
		SecurityID,
		SecurityName,
		PriceFactor,
		ExcludeFromPerformance,
		ISNULL(PercentOwnership, 100),
		EntityID,
		ContractSize
	FROM [dbo].[tfn_SecurityList_by_Level] (@Level, @LevelID, @EndDate, @IncludeLinkedAccounts)

			-- CREATE the TWR temp table that will be used to calculate account and level independently
		CREATE TABLE #Activity (
			[Activity_ID] [bigint] IDENTITY(1,1) NOT NULL,
			[Level] nvarchar(1) NOT NULL,
			[LevelID] [bigint] NOT NULL,
			[AccountID] [bigint] NULL,
			[AccountName] varchar(100),
			[AccountNumber] varchar(100),
			[AccountInceptionDate] smalldatetime,
			[PeriodBeginDate] [smalldatetime] NULL,
			[PeriodEndDate] [smalldatetime] NULL,
			[BeginMarketValue] [numeric](18, 6) NULL,
			[EndMarketValue] [numeric](18, 6) NULL,
			[CashFlow] [numeric](18, 6) NULL,
			[Additions] [numeric](18, 6) NULL,
			[Withdrawals] [numeric](18, 6) NULL,
			[Income] [numeric](18, 6) NULL,
			[AccruedIncome] [numeric](18, 6) NULL,
			[Fees] [numeric](18, 6) NULL,
			[ExcludeFromPerformance] BIT
		)

		-- Create and populate the periodlist based on the type of report
		DECLARE @PeriodList periodlist
		INSERT INTO @PeriodList(
			BeginDate,
			EndDate
			)
		SELECT @BeginDate,
			@EndDate
		-- New Code
		--UNION all
		--SELECT [dbo].[sfn_YearBegin](@EndDate),
		--	@EndDate
	

		-- Calculate the Period Activity
		EXEC CalculateDataForActivity
				@SecurityList,
				@PeriodList,
				@RecordSet = 0,
				@Level = @Level

		INSERT INTO #Output (
			Level,
			LevelID,
			BeginDate,
			EndDate,
			Period_BeginMarketValue,
			Period_EndMarketValue,
			Period_Additions,
			Period_Withdrawals,
			Period_Fees,
			Period_Income,
			Period_MarketChanges
			)
		SELECT @Level,
			@LevelID,
			@BeginDate,
			@EndDate,
			ISNULL(BeginMarketValue, 0),
			ISNULL(EndMarketValue, 0),
			Additions,
			Withdrawals,
			Fees * -1,
			Income, 
			(ISNULL(BeginMarketValue, 0) + Additions - Withdrawals + (Fees * -1) + Income - ISNULL(EndMarketValue, 0)) * -1
		FROM #Activity

		-- Calculate the YTD Activity

		DECLARE @YTDList periodlist

		INSERT INTO @YTDList(
			BeginDate,
			EndDate
			)
		SELECT [dbo].[sfn_YearBegin](@EndDate),
			@EndDate

		TRUNCATE TABLE #Activity

		EXEC CalculateDataForActivity
				@SecurityList,
				@YTDList,
				@RecordSet = 0,
				@Level = @Level

		UPDATE #Output 
		SET Level = @Level,
			LevelID = @LevelID,
			BeginDate = @BeginDate,
			EndDate = @EndDate,
			YTD_BeginMarketValue = ISNULL(BeginMarketValue, 0),
			YTD_EndMarketValue = ISNULL(EndMarketValue, 0),
			YTD_Additions = Additions,
			YTD_Withdrawals = Withdrawals,
			YTD_Fees = Fees * -1,
			YTD_Income = Income,
			YTD_MarketChanges = (ISNULL(BeginMarketValue, 0) + Additions - Withdrawals + (Fees * -1) + Income - ISNULL(EndMarketValue, 0)) * -1
		FROM #Activity

	SELECT *
	FROM #Output

GO

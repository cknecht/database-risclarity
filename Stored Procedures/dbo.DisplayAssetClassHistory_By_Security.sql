SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/****** Object:  StoredProcedure [dbo].[DisplayPrices_By_Security]    Script Date: 2/2/2014 7:05:00 PM ******/
CREATE PROC [dbo].[DisplayAssetClassHistory_By_Security]
	@UserName varchar(100) = '',
	@SecurityID BIGINT = 0

AS

IF @SecurityID <> 0
BEGIN

	SELECT SecurityAssetClass.SecurityAssetClassID,
		Security.SecurityName,
		Security.SecurityID,
		AssetClass.AssetClassID,
		AssetClass.AssetClassName,
		SecurityAssetClass.AsOfDate
	FROM dbo.SecurityAssetClass
		JOIN Security ON Security.SecurityID = SecurityAssetClass.SecurityID
		JOIN AssetClass ON SecurityAssetClass.AssetClassID = AssetClass.AssetClassID
	WHERE SecurityAssetClass.SecurityID = @SecurityID
END

GO

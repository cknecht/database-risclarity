SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROC [dbo].[DisplayAvailableMonths] 
	@InceptionDate smalldatetime = '1/1/2000'

AS

SELECT CONVERT(varchar(1000), EndDate, 107) as EndDateName,
	EndDate as EndDate
FROM [dbo].[tfn_PeriodList_Months] (
	@InceptionDate,
	CURRENT_TIMESTAMP)
ORDER BY EndDate desc

GO

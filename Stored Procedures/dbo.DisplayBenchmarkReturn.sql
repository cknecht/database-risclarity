SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROC [dbo].[DisplayBenchmarkReturn]
	@BenchmarkID bigint = 1,
	@BeginDate	smalldatetime,
	@EndDate SMALLDATETIME

AS

SELECT Benchmark.BenchmarkName,
	@BeginDate AS BeginDate,
	@EndDate AS EndDate,
	BenchmarkReturn.BenchmarkReturn
FROM Benchmark
	JOIN BenchmarkReturn ON Benchmark.BenchmarkID = BenchmarkReturn.BenchmarkID
WHERE Benchmark.BenchmarkID = @BenchmarkID
	AND BeginDate = @BeginDate
	AND EndDate = @EndDate

GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROC [dbo].[DisplayCategoryPerformance_By_Level]
	@Level VARCHAR(1),
	@LevelID BIGINT,
	@BeginDate SMALLDATETIME = NULL,
	@EndDate SMALLDATETIME,
	@IncludeLinkedAccounts BIT = 0

AS

DECLARE @Category VARCHAR(10) = 'AC'

IF ISNULL(@BeginDate, '1/1/1900') = '1/1/1900'
BEGIN
	SET @BeginDate = DATEADD(DAY, -1, [dbo].[sfn_QuarterBegin] (@EndDate))
END

CREATE TABLE #Output (
	LEVEL VARCHAR(1),
	LevelID BIGINT,
	CategoryName VARCHAR(100),
	CategoryID BIGINT,
	BeginDate SMALLDATETIME,
	EndDate SMALLDATETIME,
	BeginMarketValue NUMERIC(18,6),
	CashFlow NUMERIC(18,6),
	EndMarketValue NUMERIC(18,6),
	EndMarketValueWithNonPerformingAssets NUMERIC(18,6),
	DisplayName VARCHAR(100),
	PeriodReturn NUMERIC(18,6),
	QTDReturn NUMERIC(18,6),
	YTDReturn NUMERIC(18,6),
	PercentOwnership NUMERIC(18,5),
	ExcludeFromPerformance BIT,
	CategorySortOrder int,
	CategoryColor VARCHAR(20)
	)


	DECLARE @SecurityList securitylist

	INSERT INTO @SecurityList (
		AccountSecurityID,
		AccountID,
		AccountName,
		SecurityID,
		SecurityName,
		Pricefactor,
		ExcludeFromPerformance,
		PercentOwnership,
		EntityID,
		AssetClassName,
		AssetClassID,
		AssetClassSortOrder,
		AssetClassColor,
		ContractSize
		)
	SELECT AccountSecurityID,
		AccountID,
		AccountName,
		SecurityID,
		SecurityName,
		PriceFactor,
		ExcludeFromPerformance,
		ISNULL(PercentOwnership, 100),
		EntityID,
		AssetClassName,
		AssetClassID,
		AssetClassSortOrder,
		AssetClassColor,
		ContractSize
	FROM [dbo].[tfn_SecurityList_by_Level] (@Level, @LevelID, @EndDate, @IncludeLinkedAccounts)

	--SELECT * FROM @SecurityList

			-- CREATE the TWR temp table that will be used to calculate account and level independently
		CREATE TABLE #TWR (
			[TWR_ID] [bigint] IDENTITY(1,1) NOT NULL,
			[Level] nvarchar(5) NOT NULL,
			[LevelID] [bigint] NOT NULL,
			CategoryID BIGINT,
			CategoryName VARCHAR(100),
			[PeriodBeginDate] [smalldatetime] NULL,
			[PeriodEndDate] [smalldatetime] NULL,
			[TWRBeginDate] [smalldatetime] NULL,
			[TWREndDate] [smalldatetime] NULL,
			[BeginMarketValue] [numeric](18, 6) NULL,
			[EndMarketValue] [numeric](18, 6) NULL,
			[CashFlow] [numeric](18, 6) NULL,
			[Additions] [numeric](18, 6) NULL,
			[Withdrawals] [numeric](18, 6) NULL,
			[Income] [numeric](18, 6) NULL,
			[AccruedIncome] [numeric](18, 6) NULL,
			[Fees] [numeric](18, 6) NULL,
			[PeriodTWR] [numeric](18, 6) NULL,
			[PeriodTWR_Net_Of_Fees] [numeric](18, 6) NULL,
			[PeriodTWR_Plus_One] [numeric](18, 6) NULL,
			[PeriodTWR_Net_Of_Fees_Plus_One] [numeric](18, 6) NULL,
			[TWR] [numeric](18, 6) NULL,
			[AnnualizedTWR] [numeric](18, 6) NULL,
			[TWR_Net_Of_Fees] [numeric](18, 6) NULL,
			[AnnualizedTWR_Net_Of_Fees] [numeric](18, 6) NULL,
			[ExcludeFromPerformance] BIT,
			CategorySortOrder INT,
			CategoryColor VARCHAR(20)
		)

		CREATE INDEX TWRIndex1 ON #TWR (PeriodBeginDate, TWRBeginDate)
		CREATE INDEX TWRIndex2 ON #TWR (PeriodEndDate, TWREndDate)

		-- Create and populate the periodlist based on the type of report
		DECLARE @PeriodList periodlist
		INSERT INTO @PeriodList(
			BeginDate,
			EndDate
			)
		SELECT @BeginDate,
			@EndDate
		UNION
		SELECT [dbo].[sfn_YearBegin](@EndDate),
			@EndDate
		UNION
		SELECT [dbo].[sfn_QuarterBegin](@EndDate),
			@EndDate
	
		-- Calculate the Account Level Performance
		EXEC [dbo].[CalculateDataForTWR_with_Group] 
			@SecurityList,
			@PeriodList,
			@Level = 'AC',
			@RecordSet = 0

		IF @Category = 'AC'
		BEGIN

			INSERT INTO #Output (
				Level,
				LevelID,
				CategoryID,
				CategoryName,
				BeginDate,
				EndDate,
				CategorySortOrder,
				CategoryColor,
				EndMarketValueWithNonPerformingAssets
				)
			SELECT @Level,
				@LevelID,
				AssetClassID,
				AssetClassName,
				@BeginDate,
				@EndDate,
				AssetClassSortOrder,
				AssetClassColor,
				SUM(MarketValue_Base)
			FROM [dbo].[tfn_Holdings_by_Level] (@Level, @LevelID, @EndDate, @IncludeLinkedAccounts, 0)
			GROUP BY AssetClassName,
				AssetClassID,
				AssetClassSortOrder,
				AssetClassColor
		END

		--INSERT INTO #Output (
		--	Level,
		--	LevelID,
		--	CategoryID,
		--	CategoryName,
		--	BeginDate,
		--	EndDate,
		--	--PercentOwnership,
		--	ExcludeFromPerformance,
		--	CategorySortOrder,
		--	CategoryColor
		--	)
		--SELECT @Level,
		--	@LevelID,
		--	SecurityList.AssetClassID,
		--	SecurityList.AssetClassName,
		--	[dbo].[sfn_QuarterBegin](@EndDate) as BeginDate,
		--	@EndDate as EndDate,
		--	--SecurityList.PercentOwnership,
		--	SecurityList.ExcludeFromPerformance,
		--	#TWR.CategorySortOrder,
		--	#TWR.CategoryColor
		--FROM #TWR
		--	LEFT JOIN @SecurityList AS SecurityList ON #TWR.CategoryID = SecurityList.AssetClassID
		--GROUP BY SecurityList.AssetClassID,
		--	SecurityList.AssetClassName,
		--	--SecurityList.PercentOwnership,
		--	SecurityList.ExcludeFromPerformance,
		--	#TWR.CategorySortOrder,
		--	#TWR.CategoryColor
		
		-- UPDATE the Acct Begin Market Value
		UPDATE #Output 
		SET BeginMarketValue = TWR.BeginMarketValue
		FROM #Output as Output
			CROSS APPLY #TWR as TWR
		WHERE TWRBeginDate = DATEADD(DAY, 0, [dbo].[sfn_QuarterBegin](@EndDate))
			AND (PeriodBeginDate = DATEADD(DAY, 0, [dbo].[sfn_QuarterBegin](@EndDate)))
			AND Output.CategoryID = TWR.CategoryID

		-- UPDATE the Cash Flow
		 
		--UPDATE #Output
		--SET CashFlow = CashFlow.CashFlow
		--FROM #Output AS Output
		--	JOIN (
		--	SELECT SUM(TWR.CashFlow) AS CashFlow,
		--		TWR.CategoryID,
		--		Output.CategoryName,
		--		[dbo].[sfn_QuarterBegin](@EndDate) AS BeginDate
		--	FROM #Output as Output
		--		CROSS APPLY #TWR as TWR
		--	WHERE PeriodBeginDate <= [dbo].[sfn_QuarterBegin](@EndDate)
		--		AND Output.CategoryID = TWR.CategoryID
		--	GROUP BY TWR.CategoryID,
		--		Output.CategoryName
		--		) AS CashFlow ON Output.BeginDate = CashFlow.BeginDate
		--					AND Output.CategoryID = CashFlow.CategoryID


		-- UPDATE the Acct End Market Value
		UPDATE #Output 
		SET EndMarketValue = TWR.EndMarketValue
		FROM #Output as Output
			CROSS APPLY #TWR as TWR
		WHERE TWREndDate = @EndDate
			AND PeriodEndDate = @EndDate 
			AND Output.CategoryID = TWR.CategoryID

		-- UPDATE the  Qtr TWR
		UPDATE #Output 
		SET QTDReturn = TWR
		FROM #Output as Output
			CROSS APPLY #TWR as TWR
		WHERE PeriodBeginDate = [dbo].[sfn_QuarterBegin](@EndDate) 
			AND PeriodEndDate = @EndDate 
			AND Output.CategoryID = TWR.CategoryID
			--AND ISNULL(TWR.ExcludeFromPerformance, 0) = 0
		-- END UPDATE the Acct Qtr TWR

		-- UPDATE the  YTD TWR
		UPDATE #Output 
		SET YTDReturn = TWR
		FROM #Output as Output
			CROSS APPLY #TWR as TWR
		WHERE PeriodBeginDate = [dbo].[sfn_YearBegin](@EndDate) 
			AND PeriodEndDate = @EndDate 
			AND Output.CategoryID = TWR.CategoryID
			--AND ISNULL(TWR.ExcludeFromPerformance, 0) = 0
		-- END UPDATE the Acct YTD TWR

	SELECT *
	FROM #Output
	--WHERE CategoryID = 163

GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROC [dbo].[DisplayColorScheme]
	@UserName varchar(100) = 'Default',
	@SchemeID int = 0

AS

IF @SchemeID = 0
BEGIN
	SELECT @SchemeID = ISNULL(Users.ColorSchemeID, 1) FROM Users WHERE UserName = @Username
END

IF @SchemeID = 0
BEGIN
	SET @SchemeID = 1
END

	DECLARE @RawData table (
		RowID int identity(1,1),
		ElementName varchar(100),
		HexColor varchar(100),
		FontColor varchar(100)
		)

	CREATE TABLE #SchemeColors (
		SchemeID int,
		TitleText varchar(10),
		ThreeDots varchar(10),
		Header varchar(10),
		Header_FontColor varchar(10),
		SubHeader varchar(10),
		SubHeader_FontColor varchar(10),
		Account varchar(10),
		Entity varchar(10),
		Family varchar(10),
		Portfolio varchar(10),
		PosNumber varchar(10),
		NegNumber varchar(10),
		ZeroNumber varchar(10),
		GraphAxisText varchar(10),
		GraphGridLines varchar(10),
		GraphGridLines_FontColor varchar(10),
		Income varchar(10),
		CashFlow varchar(10),
		MarketValue varchar(10),
		Performance varchar(10),
		Activity varchar(10),
		BodyText varchar(10),
		BodySubText varchar(10),
		RegionUSCanada varchar(10),
		RegionEurope varchar(10),
		RegionLatinAmerica varchar(10),
		RegionSouthEastAsia varchar(10),
		RegionOceana varchar(10),
		RegionCentralAsia varchar(10),
		RegionSubSaharian varchar(10),
		RegionMiddleEastNorthAfrica varchar(10),
		RegionGlobal varchar(10)

		)

		INSERT INTO @RawData (
			ElementName,
			HexColor,
			FontColor
			)
		SELECT ColorReportElements.ColorElementName,
			CASE ISNULL(ColorJoin.ColorElementID, 0)
				WHEN 0 THEN 'Black' 
				WHEN -1 THEN 'Silver'
				ELSE ColorSchemeElements.ElementHEX
				END as ElementHex,
				ColorSchemeElements.FontColor as FontColor
			--ColorJoin.ColorElementID,
			--ColorJoin.ReportElementID
		FROM ColorReportElementJoin as ColorJoin
			LEFT JOIN ColorReportElements ON ColorJoin.ReportElementID = ColorReportElements.ColorElementID
			LEFT JOIN ColorSchemeElements ON ColorJoin.ColorElementID = ColorSchemeElements.ElementNumber
				AND ColorJoin.SchemeID = ColorSchemeElements.SchemeID
		WHERE ColorJoin.SchemeID = @SchemeID

		DECLARE @RowID int = 1
		DECLARE @ElementName varchar(100)
		DECLARE @HexColor varchar(20)
		DECLARE @FontColor varchar(20)
		DECLARE @SQLText varchar(Max)

		INSERT INTO #SchemeColors (
			SchemeID
			)
		SELECT @SchemeID

		--Loop throught the reportelements
		WHILE @RowID <= (SELECT MAX(RowID) FROM @RawData)
		BEGIN
			SELECT @ElementName = ElementName,
				@HexColor = HexColor,
				@FontColor = FontColor
			FROM @RawData
			WHERE RowID = @RowID

			IF EXISTS (SELECT * FROM TempDB.INFORMATION_SCHEMA.COLUMNS WHERE COLUMN_NAME = @ElementName AND TABLE_NAME LIKE '#SchemeColors%')

			--UPDATE each column with the color
			IF EXISTS (SELECT * FROM TempDB.INFORMATION_SCHEMA.COLUMNS WHERE COLUMN_NAME = @ElementName AND TABLE_NAME LIKE '#SchemeColors%')
			BEGIN
				SET @SQLText = 'UPDATE #SchemeColors ' +
								'SET ' + @ElementName + ' = ''' + @HexColor + ''''

				Print @SQLText
				EXEC (@SQLText)
			END

			--SELECT COL_LENGTH('#SchemeColors', @ElementName + '_FontColor')

			--UPDATE each column with the FONT color
			IF EXISTS (SELECT * FROM TempDB.INFORMATION_SCHEMA.COLUMNS WHERE COLUMN_NAME = @ElementName + '_FontColor' AND TABLE_NAME LIKE '#SchemeColors%')
			BEGIN
				SET @SQLText = 'UPDATE #SchemeColors ' +
								'SET ' + @ElementName + '_FontColor' + ' = ''' + @FontColor + ''''

				Print @SQLText
				EXEC (@SQLText)
			END
			
			--SELECT @ElementName, @HexColor

			SET @RowID = @RowID + 1
		END
		
		
		SELECT *
		FROM #SchemeColors

		DROP TABLE #SchemeColors


GO

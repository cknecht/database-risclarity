SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROC [dbo].[DisplayCurrentPrice_By_SecurityList_By_PeriodList] (
	@SecurityList securitylist readonly,
	@PeriodList periodlist readonly
	)

AS

DECLARE @Prices TABLE (
	AsOfDate smalldatetime NOT NULL,
	SecurityID bigint NOT NULL,
	PriceAsOfDate smalldatetime NOT NULL,
	Price numeric(18,6) NOT NULL,
	Price_CurrencyID int NOT NULL,
	PriceFactor NUMERIC(18,6)
	)

	DECLARE @BeginDate smalldatetime,
		@EndDate smalldatetime
	
	SELECT @BeginDate = DATEADD(day, -1, MIN(BeginDate)),
		@EndDate = MAX(EndDate)
	FROM @PeriodList

	DECLARE @DateList DateList

	INSERT INTO @DateList (AsOfDate)
	SELECT AsOfDate
	FROM [tfn_DateList] (@BeginDate, @EndDate)

	--CREATE A TEMP TABLE of All PRICES we may need
	CREATE TABLE #PriceTable (
		SecurityID BIGINT NOT NULL,
		AsOfDate SMALLDATETIME NOT NULL,
		Price NUMERIC(18,6) NOT NULL,
		Price_CurrencyID INT NOT NULL
		)

	INSERT INTO #PriceTable (
		SecurityID,
		AsOfDate,
		Price,
		Price_CurrencyID
		)
	SELECT Price.SecurityID,
		Price.AsOfDate,
		Price.Price,
		Price.Price_CurrencyID
	FROM (SELECT DISTINCT SecurityID FROM @SecurityList) AS SecurityList 
		JOIN dbo.Price AS Price ON Price.SecurityID = SecurityList.SecurityID
	WHERE Price.AsOfDate <= @EndDate
	-- Add prices from transactions when not in the price table 
	UNION
	SELECT --Trans.SecurityName,
		Trans.SecurityID,
		Trans.TradeDate AS AsOfDate,
		MAX(COALESCE(Trans.UnitPrice_Base, (Trans.Amount_Base / Trans.Quantity))) AS Price, --(Take the greatest price if more than one price)
		Trans.CurrencyID_Base as Price_CurrencyID
	FROM (SELECT DISTINCT AccountSecurityID FROM @SecurityList) AS SecurityList 
		JOIN dbo.vTrans AS Trans ON Trans.AccountSecurityID = SecurityList.AccountSecurityID
	WHERE Trans.TradeDate <= @EndDate
		AND (Trans.CalcQuantity <> 0 OR Trans.CalcQuantityOfOne <> 0)
	GROUP BY Trans.SecurityName,
		Trans.SecurityID,
		Trans.TradeDate,
		--Trans.UnitPrice_Base, 
		--Trans.Amount_Base, 
		--Trans.Quantity,
		Trans.CurrencyID_Base





	CREATE CLUSTERED INDEX IX_TestTable_TestCol1 ON #PriceTable (SecurityID, AsOfDate, Price_CurrencyID)
	CREATE INDEX IX_1 on #PriceTable (AsOfDate, SecurityID)

	CREATE TABLE #DateList (
		AsOfDate SMALLDATETIME NOT NULL PRIMARY KEY
		)


	--CREATE a list of dates
	INSERT INTO #DateList (
		AsOfDate
		)
	-- Make sure we have a beginning price
	SELECT @BeginDate as AsOfDate
	UNION 
	--Add all the EndDates from @PeriodList
	SELECT EndDate as AsOfDate
	FROM @PeriodList
	UNION 
	--Add all the BeginDates from @PeriodList
	SELECT BeginDate as AsOfDate
	FROM @PeriodList
	UNION 
	-- Add in any day we have a price entered
	SELECT Price.AsOfDate as AsOfDate
	FROM #PriceTable AS Price
	WHERE Price.AsOfDate > @BeginDate
		AND Price.AsOfDate <= @EndDate
	UNION 
	--Add in any day we have a cash flow
	SELECT TradeDate as AsOfDate
	FROM [tfn_Transactions_By_Level_By_DateList] (
		@SecurityList,
		@DateList
		) as Price
	WHERE CalcCashFlow <> 0
		OR CalcIncome <> 0
		OR CalcFee <> 0
	GROUP BY Price.TradeDate

	--CREATE AsOfDates of all prices
	CREATE TABLE #AsOfDate (
		AsOfDate SMALLDATETIME NOT NULL,
		SecurityID BIGINT,
		PriceAsOfDate SMALLDATETIME NOT null
		)

	CREATE INDEX IX_1 on #AsOfDate (AsOfDate, SecurityID, PriceAsOfDate)

	INSERT INTO #AsOfDate (
		AsOfDate,
		SecurityID,
		PriceAsOfDate
		)
	SELECT DateList.AsOfDate as AsOfDate,
		Price.SecurityID,
		MAX(Price.AsOfDate) as PriceAsOfDate 
	FROM (SELECT DISTINCT SecurityID FROM @SecurityList) as AccountSecurity
		JOIN #PriceTable as Price ON AccountSecurity.SecurityID = Price.SecurityID
		JOIN #DateList as Datelist ON Price.AsOfDate <= DateList.AsOfDate --This is important so as not to get prices greater than the asofdate
	GROUP BY Price.SecurityID,
		DateList.AsOfDate
			 


	--Put it all together 
	INSERT INTO @Prices (
		AsOfDate,
		SecurityID,
		PriceAsOfDate,
		Price,
		Price_CurrencyID,
		PriceFactor
		)
	SELECT AsOfDate.AsOfDate,
		Price.SecurityID,
		Price.AsOfDate as PriceAsOfDate,
		Price.Price * ISNULL(SecurityList.PriceFactor, 1) AS Price,
		Price.Price_CurrencyID,
		ISNULL(SecurityList.PriceFactor, 1) AS PriceFactor
	FROM (SELECT DISTINCT SecurityID, AccountID, PriceFactor FROM @SecurityList) AS SecurityList
		JOIN #AsOfDate AS AsOfDate ON AsOfDate.SecurityID = SecurityList.SecurityID
		JOIN Account ON SecurityList.AccountID = Account.AccountID 
			AND DATEADD(d, -1, ISNULL(Account.InceptionDate, '1/2/1900')) <= AsOfDate.AsOfDate
		JOIN Price AS Price ON Price.SecurityID = SecurityList.SecurityID 
			AND Price.AsOfDate = AsOfDate.PriceAsOfDate
	GROUP BY AsOfDate.AsOfDate,
		Price.SecurityID,
		Price.AsOfDate,
		Price.Price,
		Price.Price_CurrencyID,
		SecurityList.PriceFactor 

	--SELECT * FROM @Prices

GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROC [dbo].[DisplayDefaultLevel]
	@Username VARCHAR(100)

AS

DECLARE @Level CHAR(1)
DECLARE @LevelID bigint

--************************************************************
--If Access to More than one Family, Select 'D' for Advisor
--************************************************************
IF (SELECT COUNT(UsersFamily.FamilyID) AS Count
	FROM Users	
		JOIN UsersFamily ON Users.UserID = UsersFamily.UserID
	WHERE Users.Username = @UserName) > 1
BEGIN 
	SET @Level = 'F'

	SELECT @LevelID = (UsersFamily.FamilyID)
	FROM Users	
		JOIN UsersFamily ON Users.UserID = UsersFamily.UserID
	WHERE Users.Username = @UserName
		AND UsersFamily.SelectDefault = 1

	IF @LevelID IS NULL
	BEGIN
		SELECT @LevelID = MIN(UsersFamily.FamilyID)
		FROM Users	
			JOIN UsersFamily ON Users.UserID = UsersFamily.UserID
		WHERE Users.Username = @UserName
	END
END
--************************************************************
--If Access to only one Family, Select 'F' for Family
--************************************************************
ELSE IF (SELECT COUNT(UsersFamily.FamilyID) AS Count
	FROM Users	
		JOIN UsersFamily ON Users.UserID = UsersFamily.UserID
	WHERE Users.Username = @UserName) = 1
BEGIN
	SET @Level = 'F'

	SELECT @LevelID = MIN(UsersFamily.FamilyID)
	FROM Users	
		JOIN UsersFamily ON Users.UserID = UsersFamily.UserID
	WHERE Users.Username = @UserName
END
--************************************************************
-- Otherwise Select 'E' for Entity
--************************************************************
ELSE
BEGIN
	SET @Level = 'E'
END

SELECT @Level AS DefaultLevel, 
	@LevelID AS DefaultLevelID

GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROC [dbo].[DisplayEntitiesByFamily]
	@FamilyID bigint,
	@EndDate smalldatetime

AS

BEGIN
	
	SET NOCOUNT ON

	BEGIN TRY

		CREATE TABLE #Entities (
			ID int identity(1,1),
			FamilyID bigint,
			FamilyName varchar(100),
			EntityID	bigint,
			EntityName	varchar(100),
			EndMarketValue numeric(18,6),
			AsOfDate	smalldatetime,
			)


		INSERT INTO #Entities (
			FamilyID,
			FamilyName,
			EntityID,
			EntityName
			)
		SELECT Family.FamilyID,
			Family.FamilyName,
			Entity.EntityID,
			Entity.EntityName
		FROM FamilyEntity
			JOIN Family ON FamilyEntity.FamilyID = Family.FamilyID
			JOIN Entity ON FamilyEntity.EntityID = Entity.EntityID
		WHERE FamilyEntity.FamilyID = 2

		--CREATE TABLE #NPeriods (
		--	ID int identity(1,1),
		--	BeginDate smalldatetime,
		--	EndDate smalldatetime 
		--	)

		----Populate Periods
		--DECLARE @Index int = 0
		--DECLARE @BeginDate smalldatetime
		--DECLARE @TWR numeric(18,6)
		--DECLARE @CashFlow numeric(18,6)
		--DECLARE @Income numeric(18,6) = 0
		--DECLARE @Fees numeric(18,6)

		--WHILE @Index < @NumMonths
		--BEGIN
		--	INSERT INTO #NPeriods (
		--		EndDate,
		--		BeginDate
		--		)
		--	SELECT DATEADD(M, -1 * (@Index), @EndDate) as EndDate,
		--		dbo.sfn_MonthBegin(DATEADD(M, -1 * (@Index), @EndDate)) as BeginDate

		--	SET @Index = @Index + 1
		--END

		--DECLARE @CurrentEndDate smalldatetime
		--DECLARE @CurrentBeginDate smalldatetime

		--SELECT @Index = MAX(ID)
		--FROM #NPeriods

		--WHILE  @Index > 1
		--BEGIN
		--	SELECT Top 1 @CurrentBeginDate = BeginDate,
		--		@CurrentEndDate = EndDate
		--	FROM #NPeriods
		--	WHERE ID = @Index

		--	EXEC CalcTWR_By_Level_By_DateRange
		--		@Level = @Level,
		--		@LevelID = @LevelID,
		--		@BeginDate = @CurrentBeginDate,
		--		@EndDate = @CurrentEndDate,
		--		@NetOfFees = @NetOfFees,
		--		@Annualize = 0,
		--		@RecordSet = 0,
		--		@TWR = @TWR OUT,
		--		@CashFlow = @CashFlow OUT,
		--		@Income = @Income OUT,
		--		@Fees = @Fees OUT

		--	INSERT INTO #TrailingNMonth (
		--		BeginDate,
		--		EndDate,
		--		TWR,
		--		CashFlow,
		--		Income,
		--		Fees
		--		)
		--	SELECT @CurrentBeginDate,
		--		@CurrentEndDate,
		--		@TWR,
		--		ISNULL(@CashFlow, 0),
		--		ISNULL(@Income, 0),
		--		@Fees

		--	SELECT @Index = @Index - 1

		--END

	SELECT * FROM #Entities

	END TRY
	BEGIN CATCH
		DECLARE @ErrMsg nvarchar(4000) = ERROR_MESSAGE()
		DECLARE @ErrNum INT = ERROR_NUMBER()
		DECLARE @ErrProc nvarchar(126) = ERROR_PROCEDURE()
		DECLARE @DataError nvarchar(4000) = 'Error running PROC [DisplayTrailingMonthsTWR]' +
			CONVERT(nvarchar(10), @errNum) + ', Error Details: ' + @ErrMsg
		RAISERROR (@DataError, 16,1)
		--ROLLBACK TRANSACTION
	END CATCH

END

GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROC [dbo].[DisplayEntityAndAccountPerformance]
	@Level nvarchar(10) = 'E',
	@LevelID bigint,
	@EndDate SMALLDATETIME,
	@CompareToSource BIT = 0,
	@IncludeEstimates BIT = 1,
	@IncludeLinkedAccounts BIT = 1,
	@MTD BIT = 0,
	@QTD BIT = 1,
	@YTD BIT = 0,
	@1YR BIT = 0,
	@3YR BIT = 0,
	@5YR BIT = 0,
	@10YR BIT = 0,
	@ITD BIT = 0

AS

BEGIN
	
	SET NOCOUNT ON

	--BEGIN TRY

		DECLARE @BeginDate smalldatetime = [dbo].[sfn_QuarterBegin](@EndDate)

		DECLARE @SecurityList securitylist

		INSERT INTO @SecurityList (
			AccountSecurityID,
			AccountID,
			AccountName,
			SecurityID,
			SecurityName,
			ExcludeFromPerformance,
			PercentOwnership,
			EntityID
			)
		SELECT AccountSecurityID,
			AccountID,
			AccountName,
			SecurityID,
			SecurityName,
			ExcludeFromPerformance,
			ISNULL(PercentOwnership, 100),
			EntityID
		FROM [dbo].[tfn_SecurityList_by_Level] (@Level, @LevelID, @EndDate, @IncludeLinkedAccounts)

		-- CREATE THE #Output Table that will be returned
		CREATE TABLE #Output (
			Level varchar(1),
			LevelID bigint,
			AccountID bigint,
			AccountName varchar(100),
			AccountNumber varchar(100),
			BeginDate smalldatetime,
			EndDate smalldatetime,
			Acct_BeginMarketValue numeric(18,6),
			Acct_EndMarketValue numeric(18,6),
			LastUpdateDate SMALLDATETIME,
			Acct_Per_TWR FLOAT,
			Acct_YTD_TWR FLOAT,
			Acct_InceptionDate smalldatetime,
			EntityID BIT,
			EntityName VARCHAR(500),
			Entity_BeginMarketValue numeric(18,6),
			Entity_EndMarketValue numeric(18,6),
			Entity_Per_TWR FLOAT,
			Entity_YTD_TWR FLOAT,
			Level_InceptionDate SMALLDATETIME,
			PercentOwnerShip NUMERIC(18,6),
			ExcludeFromPerformance BIT
			)

		-- CREATE the TWR temp table that will be used to calculate account and level independently
		CREATE TABLE #TWR (
			[TWR_ID] [bigint] identity(1,1) NOT NULL,
			[Level] nvarchar(1) NOT NULL,
			[LevelID] [bigint] NOT NULL,
			[AccountID] [bigint] NULL,
			[AccountName] varchar(100),
			[AccountNumber] varchar(100),
			[AccountInceptionDate] smalldatetime,
			[PeriodBeginDate] [smalldatetime] NULL,
			[PeriodEndDate] [smalldatetime] NULL,
			[TWRBeginDate] [smalldatetime] NULL,
			[TWREndDate] [smalldatetime] NULL,
			[BeginMarketValue] [numeric](18, 6) NULL,
			[EndMarketValue] [numeric](18, 6) NULL,
			[CashFlow] [numeric](18, 6) NULL,
			[Additions] [numeric](18, 6) NULL,
			[Withdrawals] [numeric](18, 6) NULL,
			[Income] [numeric](18, 6) NULL,
			[AccruedIncome] [numeric](18, 6) NULL,
			[Fees] [numeric](18, 6) NULL,
			[PeriodTWR] [numeric](18, 6) NULL,
			[PeriodTWR_Net_Of_Fees] [numeric](18, 6) NULL,
			[PeriodTWR_Plus_One] [numeric](18, 6) NULL,
			[PeriodTWR_Net_Of_Fees_Plus_One] [numeric](18, 6) NULL,
			[TWR] float NULL,
			[AnnualizedTWR] float NULL,
			[TWR_Net_Of_Fees] float NULL,
			[AnnualizedTWR_Net_Of_Fees] float NULL,
			[ExcludeFromPerformance] bit
		)

		CREATE INDEX TWRIndex1 ON #TWR (PeriodBeginDate, TWRBeginDate)
		CREATE INDEX TWRIndex2 ON #TWR (PeriodEndDate, TWREndDate)

		-- Create and populate the periodlist based on the type of report
		DECLARE @PeriodList periodlist
		INSERT INTO @PeriodList(
			BeginDate,
			EndDate
			)
		SELECT BeginDate,
			EndDate
		FROM [dbo].[tfn_PeriodList_PerformanceReview] (
			@EndDate,
			@MTD,
			@QTD,
			@YTD,
			@1YR,
			@3YR,
			@5YR,
			@10YR,
			@ITD
			)

		EXEC [CalculateDataForTWR]
				@SecurityList,
				@PeriodList,
				@RecordSet = 0,
				@Level = @Level,
				@GroupBy = 'Account'

		-- UPDATE THE ACCOUNT RETURNS
		INSERT INTO #Output (
			Level,
			LevelID,
			AccountID,
			AccountName,
			AccountNumber,
			BeginDate,
			EndDate,
			Acct_BeginMarketValue,
			PercentOwnership,
			EntityID,
			EntityName,
			ExcludeFromPerformance
			)
		SELECT @Level,
			@LevelID,
			SecurityList.AccountID,
			SecurityList.AccountName,
			#TWR.AccountNumber,
			@BeginDate as BeginDate,
			@EndDate as EndDate,
			0 AS Acct_BeginMarketValue,
			--BeginMarketValue as Acct_BeginMarketValue,
			SecurityList.PercentOwnership,
			SecurityList.EntityID,
			Entity.EntityName,
			SecurityList.ExcludeFromPerformance
		FROM #TWR
			LEFT JOIN @SecurityList AS SecurityList ON #TWR.AccountID = SecurityList.AccountID
			LEFT JOIN Entity ON SecurityList.EntityID = Entity.EntityID
		GROUP BY SecurityList.AccountID,
			SecurityList.AccountName,
			#TWR.AccountNumber,
			SecurityList.PercentOwnership,
			SecurityList.EntityID,
			Entity.EntityName,
			SecurityList.ExcludeFromPerformance

		-- UPDATE the Acct Begin Market Value
		UPDATE #Output 
		SET Acct_BeginMarketValue = BeginMarketValue
		FROM #Output as Output
			CROSS APPLY #TWR as TWR
		WHERE TWRBeginDate = [dbo].[sfn_QuarterBegin](@EndDate)
			AND (PeriodBeginDate = [dbo].[sfn_QuarterBegin](@EndDate))
			AND Output.AccountID = TWR.AccountID

		-- UPDATE the Acct End Market Value
		UPDATE #Output 
		SET Acct_EndMarketValue = EndMarketValue
		FROM #Output as Output
			CROSS APPLY #TWR as TWR
		WHERE TWREndDate = @EndDate
			AND PeriodEndDate = @EndDate 
			AND Output.AccountID = TWR.AccountID

		UPDATE #Output
		SET LastUpdateDate = LastUpdate.AsOfDate
		FROM #Output
			JOIN (SELECT AsOfDate,
					AccountID
					FROM [tfn_LastAccountActivityDate_By_Level] (
						@Level, 
						@LevelID,
						@EndDate,
						@SecurityList,
						@IncludeEstimates,
						1)
				) as LastUpdate ON #OUTPUT.AccountID = LastUpdate.AccountID
					AND ISNULL(#Output.ExcludeFromPerformance, 0) = 0

		-- UPDATE the Inception Date
		UPDATE #Output 
		SET Acct_InceptionDate = TWR.InceptionDate
		FROM #Output as Output
			JOIN (SELECT CASE WHEN AccountInceptionDate IS NULL
						THEN MIN(TWRBeginDate)
						ELSE AccountInceptionDate
						END as InceptionDate, 
					AccountID 
				FROM #TWR as TWR
				WHERE BeginMarketValue <> 0
				GROUP BY AccountID,
					AccountInceptionDate) as TWR ON Output.AccountID = TWR.AccountID

		-- UPDATE the Acct Qtr TWR
		
		IF @QTD = 1 
		BEGIN
			UPDATE #Output 
			SET Acct_Per_TWR = TWR
			FROM #Output as Output
				CROSS APPLY #TWR as TWR
			WHERE PeriodBeginDate = @BeginDate
				AND PeriodEndDate = @EndDate 
				AND Output.AccountID = TWR.AccountID
				AND ISNULL(TWR.ExcludeFromPerformance, 0) = 0
			-- END UPDATE the Acct Qtr TWR
		END

		-- UPDATE the Acct YTD TWR
		IF @YTD = 1 
		BEGIN
			UPDATE #Output 
			SET Acct_YTD_TWR = TWR
			FROM #Output as Output
				CROSS APPLY #TWR as TWR
			WHERE PeriodBeginDate = [dbo].[sfn_YearBegin](@EndDate) 
				AND PeriodEndDate = @EndDate 
				AND Output.AccountID = TWR.AccountID
				AND ISNULL(TWR.ExcludeFromPerformance, 0) = 0
			-- END UPDATE the Acct YTD TWR
		END


		IF @Level <> 'A' AND 1=0
		BEGIN

			TRUNCATE TABLE #TWR

			EXEC [CalculateDataForTWR]
				@SecurityList,
				@PeriodList,
				@RecordSet = 0,
				@Level = @Level,
				@GroupBy = 'Entity'

			------ Annualize the values
			----UPDATE #TWR 
			----SET [AnnualizedTWR] = [dbo].[sfn_AnnualizeReturn](TWR, PeriodBeginDate, PeriodEndDate)

			-- UPDATE the Level Begin Market Value
			UPDATE #Output 
			SET Level_BeginMarketValue = BeginMarketValue 
			FROM #TWR
			WHERE TWRBeginDate = [dbo].[sfn_QuarterBegin](@EndDate) 
				AND PeriodBeginDate = [dbo].[sfn_QuarterBegin](@EndDate) 

			-- UPDATE the Level End Market Value
			UPDATE #Output 
			SET Level_EndMarketValue = EndMarketValue
			FROM #Output as Output
				CROSS APPLY #TWR as TWR
			WHERE TWREndDate = @EndDate
				AND PeriodEndDate = @EndDate

			-- UPDATE the Inception Date
			UPDATE #Output 
			SET Level_InceptionDate = TWR.InceptionDate
			FROM #Output as Output
				JOIN (SELECT CASE WHEN MIN(AccountInceptionDate) IS NULL
							THEN MIN(TWRBeginDate)
							ELSE MIN(AccountInceptionDate)
							END as InceptionDate, 
						LevelID
					FROM #TWR as TWR
					WHERE BeginMarketValue <> 0
					GROUP BY AccountInceptionDate,
						LevelID) as TWR ON Output.LevelID = TWR.LevelID

			-- UPDATE the Level Qtr TWR
			UPDATE #Output 
			SET Level_Per_TWR = TWR
			FROM #Output as Output
				CROSS APPLY #TWR as TWR
			WHERE PeriodBeginDate = [dbo].[sfn_QuarterBegin](@EndDate) 
				AND PeriodEndDate = @EndDate 
				AND ISNULL(TWR.ExcludeFromPerformance, 0) = 0
			-- END UPDATE the Level Qtr TWR

			-- UPDATE the Level YTD TWR
			UPDATE #Output 
			SET Level_YTD_TWR = TWR
			FROM #Output as Output
				CROSS APPLY #TWR as TWR
			WHERE PeriodBeginDate = [dbo].[sfn_YearBegin](@EndDate) 
				AND PeriodEndDate = @EndDate 
				--AND ISNULL(TWR.ExcludeFromPerformance, 0) = 0
			-- END UPDATE the Level YTD TWR

			---- UPDATE the Inception TWR
			--UPDATE #Output 
			--SET Level_YR10_TWR = [AnnualizedTWR]
			--FROM #Output as Output
			--	CROSS APPLY #TWR as TWR
			--WHERE PeriodBeginDate = TWR.AccountInceptionDate
			--	AND PeriodEndDate = @EndDate 
			--	AND Output.LevelID = TWR.LevelID
			---- END UPDATE the Level Inception TWR		
		END

		SELECT * FROM #Output

		DROP TABLE #Output
		DROP TABLE #TWR

	--END TRY
	--BEGIN CATCH
	--	DECLARE @ErrMsg nvarchar(4000) = ERROR_MESSAGE()
	--	DECLARE @ErrNum INT = ERROR_NUMBER()
	--	DECLARE @ErrProc nvarchar(126) = ERROR_PROCEDURE()
	--	DECLARE @DataError nvarchar(4000) = 'Error running PROC [DisplayAccountPerformanceReview]' +
	--		CONVERT(nvarchar(10), @errNum) + ', Error Details: ' + @ErrMsg
	--	RAISERROR (@DataError, 16,1)
	--	--ROLLBACK TRANSACTION
	--END CATCH

END

GO

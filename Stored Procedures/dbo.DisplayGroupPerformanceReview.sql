SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROC [dbo].[DisplayGroupPerformanceReview]
	@Level nvarchar(10) = 'E',
	@LevelID bigint,
	@EndDate SMALLDATETIME,
	@CompareToSource BIT = 0,
	@IncludeEstimates BIT = 1,
	@IncludeLinkedAccounts BIT = 1,
	@MTD BIT = 1,
	@QTD BIT = 0,
	@YTD BIT = 0,
	@1YR BIT = 0,
	@3YR BIT = 0,
	@5YR BIT = 0,
	@10YR BIT = 0,
	@ITD BIT = 0

AS

BEGIN
	
	SET NOCOUNT ON

	--BEGIN TRY

		--DECLARE @BeginDate smalldatetime = [dbo].[sfn_QuarterBegin](@EndDate)
		DECLARE @BeginDate smalldatetime = [dbo].[sfn_MonthBegin](@EndDate)

		DECLARE @SecurityList securitylist

		INSERT INTO @SecurityList (
			AccountSecurityID,
			AccountID,
			AccountName,
			SecurityID,
			SecurityName,
			AssetClassID,
			AssetClassName,
			Pricefactor,
			ExcludeFromPerformance,
			PercentOwnership,
			EntityID,
			ContractSize
			)
		SELECT AccountSecurityID,
			AccountID,
			AccountName,
			SecurityID,
			SecurityName,
			AssetClassID,
			AssetClassName,
			PriceFactor,
			ExcludeFromPerformance,
			ISNULL(PercentOwnership, 100),
			EntityID,
			ContractSize
		FROM [dbo].[tfn_SecurityList_by_Level] (@Level, @LevelID, @EndDate, @IncludeLinkedAccounts)

		-- CREATE THE #Output Table that will be returned
		CREATE TABLE #Output (
			Level varchar(1),
			LevelID bigint,
			GroupID BIGINT,
			GroupName VARCHAR(100),
			BeginDate smalldatetime,
			EndDate smalldatetime,
			Group_BeginMarketValue numeric(18,6),
			Group_EndMarketValue numeric(18,6),
			LastUpdateDate SMALLDATETIME,
			Group_Per_TWR FLOAT,
			Group_YTD_TWR FLOAT,
			Group_YR1_TWR FLOAT,
			Group_YR3_TWR FLOAT,
			Group_YR5_TWR FLOAT,
			Group_YR10_TWR FLOAT,
			Group_ITD_TWR FLOAT,
			Group_InceptionDate smalldatetime,
			SourceName VARCHAR(1000),
			Level_BeginMarketValue numeric(18,6),
			Level_EndMarketValue numeric(18,6),
			Level_Per_TWR FLOAT,
			Level_YTD_TWR FLOAT,
			Level_YR1_TWR FLOAT,
			Level_YR3_TWR FLOAT,
			Level_YR5_TWR FLOAT,
			Level_YR10_TWR FLOAT,
			Level_ITD_TWR FLOAT,
			Level_InceptionDate SMALLDATETIME,
			PercentOwnerShip NUMERIC(18,6),
			ExcludeFromPerformance BIT,
			EntityID BIT,
			EntityName VARCHAR(500)
			)

		-- CREATE the TWR temp table that will be used to calculate account and level independently
		CREATE TABLE #TWR (
			[TWR_ID] [bigint] identity(1,1) NOT NULL,
			[Level] nvarchar(1) NOT NULL,
			[LevelID] [bigint] NOT NULL,
			[AccountID] [bigint] NULL,
			[AccountName] varchar(100),
			[AccountNumber] varchar(100),
			GroupID BIGINT,
			GroupName VARCHAR(100),
			[AccountInceptionDate] smalldatetime,
			[PeriodBeginDate] [smalldatetime] NULL,
			[PeriodEndDate] [smalldatetime] NULL,
			[TWRBeginDate] [smalldatetime] NULL,
			[TWREndDate] [smalldatetime] NULL,
			[BeginMarketValue] [numeric](18, 6) NULL,
			[EndMarketValue] [numeric](18, 6) NULL,
			[CashFlow] [numeric](18, 6) NULL,
			[Additions] [numeric](18, 6) NULL,
			[Withdrawals] [numeric](18, 6) NULL,
			[Income] [numeric](18, 6) NULL,
			[AccruedIncome] [numeric](18, 6) NULL,
			[Fees] [numeric](18, 6) NULL,
			[PeriodTWR] [numeric](18, 6) NULL,
			[PeriodTWR_Net_Of_Fees] [numeric](18, 6) NULL,
			[PeriodTWR_Plus_One] [numeric](18, 6) NULL,
			[PeriodTWR_Net_Of_Fees_Plus_One] [numeric](18, 6) NULL,
			[TWR] float NULL,
			[AnnualizedTWR] float NULL,
			[TWR_Net_Of_Fees] float NULL,
			[AnnualizedTWR_Net_Of_Fees] float NULL,
			[ExcludeFromPerformance] bit
		)

		CREATE INDEX TWRIndex1 ON #TWR (PeriodBeginDate, TWRBeginDate)
		CREATE INDEX TWRIndex2 ON #TWR (PeriodEndDate, TWREndDate)

		-- Create and populate the periodlist based on the type of report
		DECLARE @PeriodList periodlist
		INSERT INTO @PeriodList(
			BeginDate,
			EndDate
			)
		SELECT BeginDate,
			EndDate
		FROM [dbo].[tfn_PeriodList_PerformanceReview] (
			@EndDate,
			@MTD,
			@QTD,
			@YTD,
			@1YR,
			@3YR,
			@5YR,
			@10YR,
			@ITD
			)
		---- New Code
		--UNION
		--SELECT BeginDate,
		--	EndDate
		--FROM [dbo].[tfn_PeriodList_Months] (
		--	DATEADD(Month, -1, @BeginDate),
		--	@EndDate
		--	)


		EXEC [CalculateDataForTWR_with_Group]
				@SecurityList,
				@PeriodList,
				@RecordSet = 0,			--**************			
				@Level = @Level,
				@GroupBy = 'AssetClass'


		-- UPDATE THE ACCOUNT RETURNS
		INSERT INTO #Output (
			Level,
			LevelID,
			GroupID,
			GroupName,
			BeginDate,
			EndDate,
			Group_BeginMarketValue,
			PercentOwnership,
			EntityID,
			EntityName,
			ExcludeFromPerformance
			)
		SELECT @Level,
			@LevelID,
			SecurityList.AssetClassID,
			SecurityList.AssetClassName,
			@BeginDate as BeginDate,
			@EndDate as EndDate,
			NULL AS Group_BeginMarketValue,
			--BeginMarketValue as Group_BeginMarketValue,
			SecurityList.PercentOwnership,
			SecurityList.EntityID,
			Entity.EntityName,
			SecurityList.ExcludeFromPerformance
		FROM #TWR
			LEFT JOIN @SecurityList AS SecurityList ON #TWR.GroupID = SecurityList.AssetClassID
			LEFT JOIN Entity ON SecurityList.EntityID = Entity.EntityID
		GROUP BY SecurityList.AssetClassID,
			SecurityList.AssetClassName,
			SecurityList.PercentOwnership,
			SecurityList.EntityID,
			Entity.EntityName,
			SecurityList.ExcludeFromPerformance

		-- UPDATE the Group Begin Market Value
		UPDATE #Output 
		SET Group_BeginMarketValue = BeginMarketValue
		FROM #Output as Output
			CROSS APPLY #TWR as TWR
		WHERE TWREndDate = DATEADD(DAY, -1, [dbo].[sfn_QuarterBegin](@EndDate))
			AND (PeriodEndDate = DATEADD(DAY, -1, [dbo].[sfn_QuarterBegin](@EndDate)))
			AND Output.GroupID = TWR.GroupID

			--SELECT GroupID,
			--	MIN(TWRBeginDate)
			--FROM #TWR
			--	WHERE TWRBeginDate < PeriodBeginDate
			--GROUP BY GroupID


		-- UPDATE the Acct End Market Value
		UPDATE #Output 
		SET Group_EndMarketValue = EndMarketValue
		FROM #Output as Output
			CROSS APPLY #TWR as TWR
		WHERE TWREndDate = @EndDate
			AND PeriodEndDate = @EndDate 
			AND Output.GroupID = TWR.GroupID

		--UPDATE #Output
		--SET LastUpdateDate = LastUpdate.AsOfDate
		--FROM #Output
		--	JOIN (SELECT AsOfDate,
		--			GroupID
		--			FROM [tfn_LastAccountActivityDate_By_Level] (
		--				@Level, 
		--				@LevelID,
		--				@EndDate,
		--				@SecurityList,
		--				@IncludeEstimates,
		--				0)
		--		) as LastUpdate ON #OUTPUT.GroupID = LastUpdate.GroupID

		---- UPDATE the Inception Date
		--UPDATE #Output 
		--SET Group_InceptionDate = TWR.InceptionDate
		--FROM #Output as Output
		--	JOIN (SELECT CASE WHEN AccountInceptionDate IS NULL
		--				THEN MIN(TWRBeginDate)
		--				ELSE AccountInceptionDate
		--				END as InceptionDate, 
		--			GroupID 
		--		FROM #TWR as TWR
		--		WHERE BeginMarketValue <> 0
		--		GROUP BY GroupID,
		--			AccountInceptionDate) as TWR ON Output.GroupID = TWR.GroupID

		-- UPDATE the Group Qtr TWR
		UPDATE #Output 
		SET Group_Per_TWR = TWR
		FROM #Output as Output
			CROSS APPLY #TWR as TWR
		WHERE PeriodBeginDate = @BeginDate
			AND PeriodEndDate = @EndDate 
			AND Output.GroupID = TWR.GroupID
			--AND ISNULL(TWR.ExcludeFromPerformance, 0) = 0
		-- END UPDATE the Acct Qtr TWR

		-- UPDATE the Acct YTD TWR
		UPDATE #Output 
		SET Group_YTD_TWR = TWR
		FROM #Output as Output
			CROSS APPLY #TWR as TWR
		WHERE PeriodBeginDate = [dbo].[sfn_YearBegin](@EndDate) 
			AND PeriodEndDate = @EndDate 
			AND Output.GroupID = TWR.GroupID
			AND ISNULL(TWR.ExcludeFromPerformance, 0) = 0
		-- END UPDATE the Acct YTD TWR

		-- UPDATE the Acct 1Year TWR
		UPDATE #Output 
		SET Group_YR1_TWR = TWR
		FROM #Output as Output
			CROSS APPLY #TWR as TWR
		WHERE PeriodBeginDate = [dbo].[sfn_PreviousYear](@EndDate, 1)
			AND PeriodEndDate = @EndDate 
			AND Output.GroupID = TWR.GroupID
			AND OUTPUT.Group_InceptionDate <= [dbo].[sfn_PreviousYear](@EndDate, 1)
			AND ISNULL(TWR.ExcludeFromPerformance, 0) = 0
		-- END UPDATE the Acct 1Year TWR

		-- UPDATE the Acct 3Year TWR
		UPDATE #Output 
		SET Group_YR3_TWR = [dbo].[sfn_AnnualizeReturn](TWR, [dbo].[sfn_PreviousYear](@EndDate, 3), @EndDate)
		FROM #Output as Output
			CROSS APPLY #TWR as TWR
		WHERE PeriodBeginDate = [dbo].[sfn_PreviousYear](@EndDate, 3)
			AND PeriodEndDate = @EndDate 
			AND Output.GroupID = TWR.GroupID
			AND OUTPUT.Group_InceptionDate <= [dbo].[sfn_PreviousYear](@EndDate, 3)
			AND ISNULL(TWR.ExcludeFromPerformance, 0) = 0
		-- END UPDATE the Acct 3Year TWR

		-- UPDATE the Acct 5Year TWR
		UPDATE #Output 
		SET Group_YR5_TWR = [dbo].[sfn_AnnualizeReturn](TWR, [dbo].[sfn_PreviousYear](@EndDate, 5), @EndDate)
		FROM #Output as Output
			CROSS APPLY #TWR as TWR
		WHERE PeriodBeginDate = [dbo].[sfn_PreviousYear](@EndDate, 5)
			AND PeriodEndDate = @EndDate 
			AND Output.GroupID = TWR.GroupID
			AND OUTPUT.Group_InceptionDate <= [dbo].[sfn_PreviousYear](@EndDate, 5)
			AND ISNULL(TWR.ExcludeFromPerformance, 0) = 0
		-- END UPDATE the Acct 5Year TWR

		-- UPDATE the Acct 10Year TWR
		UPDATE #Output 
		SET Group_YR10_TWR = [dbo].[sfn_AnnualizeReturn](TWR, [dbo].[sfn_PreviousYear](@EndDate, 10), @EndDate)
		FROM #Output as Output
			CROSS APPLY #TWR as TWR
		WHERE PeriodBeginDate = [dbo].[sfn_PreviousYear](@EndDate, 10)
			AND PeriodEndDate = @EndDate 
			AND Output.GroupID = TWR.GroupID
			AND OUTPUT.Group_InceptionDate <= [dbo].[sfn_PreviousYear](@EndDate, 10)
			AND ISNULL(TWR.ExcludeFromPerformance, 0) = 0
		-- END UPDATE the Acct 10Year TWR

		---- UPDATE the Inception TWR
		--UPDATE #Output 
		--SET Group_YR10_TWR = [AnnualizedTWR]
		--FROM #Output as Output
		--	CROSS APPLY #TWR as TWR
		--WHERE PeriodBeginDate = TWR.AccountInceptionDate
		--	AND PeriodEndDate = @EndDate 
		--	AND Output.GroupID = TWR.GroupID
		--	AND ISNULL(OUTPUT.ExcludeFromPerformance, 0) = 0
		---- END UPDATE the Inception TWR

		--IF @Level <> 'A'
		--BEGIN

		--	TRUNCATE TABLE #TWR

		--	EXEC [CalculateDataForTWR]
		--		@SecurityList,
		--		@PeriodList,
		--		@RecordSet = 0,
		--		@Level = @Level,
		--		@GroupBy = 'Entity'

		--	------ Annualize the values
		--	----UPDATE #TWR 
		--	----SET [AnnualizedTWR] = [dbo].[sfn_AnnualizeReturn](TWR, PeriodBeginDate, PeriodEndDate)

		--	-- UPDATE the Level Begin Market Value
		--	UPDATE #Output 
		--	SET Level_BeginMarketValue = BeginMarketValue 
		--	FROM #TWR
		--	WHERE TWRBeginDate = [dbo].[sfn_QuarterBegin](@EndDate) 
		--		AND PeriodBeginDate = [dbo].[sfn_QuarterBegin](@EndDate) 

		--	-- UPDATE the Level End Market Value
		--	UPDATE #Output 
		--	SET Level_EndMarketValue = EndMarketValue
		--	FROM #Output as Output
		--		CROSS APPLY #TWR as TWR
		--	WHERE TWREndDate = @EndDate
		--		AND PeriodEndDate = @EndDate

		--	-- UPDATE the Inception Date
		--	UPDATE #Output 
		--	SET Level_InceptionDate = TWR.InceptionDate
		--	FROM #Output as Output
		--		JOIN (SELECT CASE WHEN MIN(AccountInceptionDate) IS NULL
		--					THEN MIN(TWRBeginDate)
		--					ELSE MIN(AccountInceptionDate)
		--					END as InceptionDate, 
		--				LevelID
		--			FROM #TWR as TWR
		--			WHERE BeginMarketValue <> 0
		--			GROUP BY AccountInceptionDate,
		--				LevelID) as TWR ON Output.LevelID = TWR.LevelID

		--	-- UPDATE the Level Qtr TWR
		--	UPDATE #Output 
		--	SET Level_Per_TWR = TWR
		--	FROM #Output as Output
		--		CROSS APPLY #TWR as TWR
		--	WHERE PeriodBeginDate = [dbo].[sfn_QuarterBegin](@EndDate) 
		--		AND PeriodEndDate = @EndDate 
		--		AND ISNULL(TWR.ExcludeFromPerformance, 0) = 0
		--	-- END UPDATE the Level Qtr TWR

		--	-- UPDATE the Level YTD TWR
		--	UPDATE #Output 
		--	SET Level_YTD_TWR = TWR
		--	FROM #Output as Output
		--		CROSS APPLY #TWR as TWR
		--	WHERE PeriodBeginDate = [dbo].[sfn_YearBegin](@EndDate) 
		--		AND PeriodEndDate = @EndDate 
		--		--AND ISNULL(TWR.ExcludeFromPerformance, 0) = 0
		--	-- END UPDATE the Level YTD TWR

		--	-- UPDATE the Level 1Year TWR
		--	UPDATE #Output 
		--	SET Level_YR1_TWR = TWR
		--	FROM #Output as Output
		--		CROSS APPLY #TWR as TWR
		--	WHERE PeriodBeginDate = [dbo].[sfn_PreviousYear](@EndDate, 1)
		--		AND PeriodEndDate = @EndDate 
		--		--AND Output.LevelID = TWR.LevelID
		--		AND OUTPUT.Level_InceptionDate <= [dbo].[sfn_PreviousYear](@EndDate, 1)
		--	-- END UPDATE the Level 1Year TWR

		--	-- UPDATE the Level 3Year TWR
		--	UPDATE #Output 
		--	SET Level_YR3_TWR = [AnnualizedTWR]
		--	FROM #Output as Output
		--		CROSS APPLY #TWR as TWR
		--	WHERE PeriodBeginDate = [dbo].[sfn_PreviousYear](@EndDate, 3)
		--		AND PeriodEndDate = @EndDate 
		--		--AND Output.LevelID = TWR.LevelID
		--		AND OUTPUT.Level_InceptionDate <= [dbo].[sfn_PreviousYear](@EndDate, 3)
		--	-- END UPDATE the Level 3Year TWR

		--	-- UPDATE the Level 5Year TWR
		--	UPDATE #Output 
		--	SET Level_YR5_TWR = [AnnualizedTWR]
		--	FROM #Output as Output
		--		CROSS APPLY #TWR as TWR
		--	WHERE PeriodBeginDate = [dbo].[sfn_PreviousYear](@EndDate, 5)
		--		AND PeriodEndDate = @EndDate 
		--		--AND Output.LevelID = TWR.LevelID
		--		AND OUTPUT.Level_InceptionDate <= [dbo].[sfn_PreviousYear](@EndDate, 5)
		--	-- END UPDATE the Level 5Year TWR

		--	-- UPDATE the Level 10Year TWR
		--	UPDATE #Output 
		--	SET Level_YR10_TWR = [AnnualizedTWR]
		--	FROM #Output as Output
		--		CROSS APPLY #TWR as TWR
		--	WHERE PeriodBeginDate = [dbo].[sfn_PreviousYear](@EndDate, 10) 
		--		AND PeriodEndDate = @EndDate 
		--		--AND Output.LevelID = TWR.LevelID
		--		AND OUTPUT.Level_InceptionDate <= [dbo].[sfn_PreviousYear](@EndDate, 10)
		--	-- END UPDATE the Level 10Year TWR

		--	---- UPDATE the Inception TWR
		--	--UPDATE #Output 
		--	--SET Level_YR10_TWR = [AnnualizedTWR]
		--	--FROM #Output as Output
		--	--	CROSS APPLY #TWR as TWR
		--	--WHERE PeriodBeginDate = TWR.AccountInceptionDate
		--	--	AND PeriodEndDate = @EndDate 
		--	--	AND Output.LevelID = TWR.LevelID
		--	---- END UPDATE the Level Inception TWR		
		--END

		SELECT * FROM #Output

		DROP TABLE #Output
		DROP TABLE #TWR

	--END TRY
	--BEGIN CATCH
	--	DECLARE @ErrMsg nvarchar(4000) = ERROR_MESSAGE()
	--	DECLARE @ErrNum INT = ERROR_NUMBER()
	--	DECLARE @ErrProc nvarchar(126) = ERROR_PROCEDURE()
	--	DECLARE @DataError nvarchar(4000) = 'Error running PROC [DisplayAccountPerformanceReview]' +
	--		CONVERT(nvarchar(10), @errNum) + ', Error Details: ' + @ErrMsg
	--	RAISERROR (@DataError, 16,1)
	--	--ROLLBACK TRANSACTION
	--END CATCH

END

GO

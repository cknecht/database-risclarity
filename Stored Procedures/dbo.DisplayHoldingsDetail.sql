SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROC [dbo].[DisplayHoldingsDetail]
	@Level varchar(1) = 'E',
	@LevelID int,
	@AsOfDate smalldatetime,
	@IncludeLinkedAccounts BIT = 0,
	@ShowQuantityOfZero BIT = 1

AS
BEGIN
	SELECT *
	FROM [dbo].[tfn_Holdings_by_Level] (@Level, @LevelID, @AsOfDate, @IncludeLinkedAccounts, @ShowQuantityOfZero)
END


GO

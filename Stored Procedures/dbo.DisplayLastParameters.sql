SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROC [dbo].[DisplayLastParameters]
	@Username VARCHAR(100)

AS

DECLARE @Level CHAR(1)
DECLARE @LevelID BIGINT
DECLARE @AsOfDate SMALLDATETIME
DECLARE @BeginDate SMALLDATETIME

SELECT @Level = ISNULL(Users.LastLevel, 'F'),
	@LevelID = Users.LastLevelID,
	@AsOfDate = ISNULL(Users.LastAsOfDate, CURRENT_TIMESTAMP),
	@BeginDate = ISNULL(users.LastBeginDate, '1/1/1900')
FROM dbo.Users
WHERE Users.Username = @UserName

IF ISNULL(@LevelID, '') = ''
BEGIN
	--************************************************************
	--If Access to More than one Family, Select 'D' for Advisor
	--************************************************************
	IF (SELECT COUNT(UsersFamily.FamilyID) AS Count
		FROM Users	
			JOIN UsersFamily ON Users.UserID = UsersFamily.UserID
		WHERE Users.Username = @UserName) > 1
	BEGIN 

		SELECT @LevelID = (UsersFamily.FamilyID)
		FROM Users	
			JOIN UsersFamily ON Users.UserID = UsersFamily.UserID
		WHERE Users.Username = @UserName
			AND UsersFamily.SelectDefault = 1

		IF @LevelID IS NULL
		BEGIN
			SELECT @LevelID = MIN(UsersFamily.FamilyID)
			FROM Users	
				JOIN UsersFamily ON Users.UserID = UsersFamily.UserID
			WHERE Users.Username = @UserName
		END
	END
	--************************************************************
	--If Access to only one Family, Select 'F' for Family
	--************************************************************
	ELSE IF (SELECT COUNT(UsersFamily.FamilyID) AS Count
		FROM Users	
			JOIN UsersFamily ON Users.UserID = UsersFamily.UserID
		WHERE Users.Username = @UserName) = 1
	BEGIN

		SELECT @LevelID = MIN(UsersFamily.FamilyID)
		FROM Users	
			JOIN UsersFamily ON Users.UserID = UsersFamily.UserID
		WHERE Users.Username = @UserName
	END
	--************************************************************
	-- Otherwise Select 'E' for Entity
	--************************************************************
	ELSE
	BEGIN
		SET @Level = 'E'
	END
END

SELECT @Level AS LastLevel, 
	@LevelID AS LastLevelID,
	@AsOfDate AS LastAsOfDate,
	@BeginDate AS LastBeginDate

GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROC [dbo].[DisplayLevelID_By_Level]
	@Level varchar(1) = 'E',
	@Username VARCHAR(100)
AS

DECLARE @UserID bigint

EXEC UpdateLastLogin 
	@Username = @Username,
	@UserID = @UserID OUTPUT

IF @Level = 'D' 
BEGIN
	SELECT Advisor.AdvisorName AS LevelIDName,
		Advisor.AdvisorID AS LevelID
	FROM Users	
		JOIN UsersAdvisor ON Users.UserID = UsersAdvisor.UserID
		JOIN Advisor ON UsersAdvisor.AdvisorID = Advisor.AdvisorID
	WHERE Users.Username = @UserName
	GROUP BY Advisor.AdvisorName,
		Advisor.AdvisorID
END
ELSE IF @Level = 'F' 
BEGIN
	SELECT Family.FamilyName AS LevelIDName,
		Family.FamilyID AS LevelID
	FROM Users	
		JOIN UsersFamily ON Users.UserID = UsersFamily.UserID
		JOIN Family ON UsersFamily.FamilyID = Family.FamilyID
	WHERE Users.Username = @UserName
	GROUP BY Family.FamilyName,
		Family.FamilyID
		
END
ELSE IF @Level = 'P' 
BEGIN
	SELECT Portfolio.PortfolioName AS LevelIDName,
		Portfolio.PortfolioID AS LevelID
	FROM Users	
		JOIN UsersFamily ON Users.UserID = UsersFamily.UserID
		JOIN Portfolio ON UsersFamily.FamilyID = Portfolio.FamilyID
	WHERE Users.Username = @UserName
	GROUP BY Portfolio.PortfolioName,
		Portfolio.PortfolioID
		
END
ELSE IF @Level = 'E' 
BEGIN
	SELECT EntityName as LevelIDName,
		Entity.EntityID as LevelID
	FROM dbo.Entity
		JOIN dbo.FamilyEntity ON Entity.EntityID = FamilyEntity.EntityID
		JOIN UsersFamily ON FamilyEntity.FamilyID = UsersFamily.FamilyID
		JOIN Users ON UsersFamily.UserID = Users.UserID
	WHERE Entity.Inactive = 0
		AND Users.UserName = @Username
	GROUP BY EntityName,
		Entity.EntityID 
	ORDER BY EntityName
		
END
ELSE IF @Level = 'A' 
BEGIN
	SELECT Entity.EntityName + ' - ' + Account.AccountName as LevelIDName,
		Account.AccountID as LevelID
	FROM Account
		JOIN EntityAccount ON Account.AccountID = EntityAccount.AccountID
		JOIN Entity ON EntityAccount.EntityID = Entity.EntityID
		JOIN dbo.FamilyEntity ON Entity.EntityID = FamilyEntity.EntityID
		JOIN UsersFamily ON FamilyEntity.FamilyID = UsersFamily.FamilyID
		JOIN Users ON UsersFamily.UserID = Users.UserID
	WHERE Account.Inactive = 0
		AND Entity.Inactive = 0
		AND Users.UserName = @Username
	GROUP BY Entity.EntityName,
		Account.AccountName,
		Account.AccountID 
	ORDER BY Entity.EntityName, Account.AccountName
		
END
ELSE IF @Level = 'H' 
BEGIN
	SELECT Entity.EntityName + ' - ' + Account.AccountName + ' - ' + Security.SecurityName as LevelIDName,
		AccountSecurity.AccountSecurityID as LevelID
	FROM Account
		JOIN EntityAccount ON Account.AccountID = EntityAccount.AccountID
		JOIN AccountSecurity ON EntityAccount.AccountID = AccountSecurity.AccountID
		JOIN Security ON AccountSecurity.SecurityID = Security.SecurityID
		JOIN Entity ON EntityAccount.EntityID = Entity.EntityID
		JOIN dbo.FamilyEntity ON Entity.EntityID = FamilyEntity.EntityID
		JOIN UsersFamily ON FamilyEntity.FamilyID = UsersFamily.FamilyID
		JOIN Users ON UsersFamily.UserID = Users.UserID
	WHERE Account.Inactive = 0
		AND Entity.Inactive = 0
		AND Users.UserName = @Username
	GROUP BY Entity.EntityName,
		Account.AccountName,
		AccountSecurity.AccountSecurityID,
		Security.SecurityName
	ORDER BY Entity.EntityName, Account.AccountName, Security.SecurityName
		
END

GO

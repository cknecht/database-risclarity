SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROC [dbo].[DisplayLiquidityHistory_By_Security]
	@UserName varchar(100) = '',
	@SecurityID BIGINT = 0

AS

IF @SecurityID <> 0
BEGIN

	SELECT SecurityLiquidity.SecurityLiquidityID,
		Security.SecurityName,
		Security.SecurityID,
		SecurityLiquidity.LiquidityDays,
		SecurityLiquidity.AsOfDate
	FROM dbo.SecurityLiquidity
		JOIN Security ON Security.SecurityID = SecurityLiquidity.SecurityID
	WHERE SecurityLiquidity.SecurityID = @SecurityID
END
GO

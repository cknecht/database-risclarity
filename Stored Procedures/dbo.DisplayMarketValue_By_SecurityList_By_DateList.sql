SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROC [dbo].[DisplayMarketValue_By_SecurityList_By_DateList]
	@SecurityList securitylist readonly,
	@DateList datelist readonly,
	@TransactionList transactionlist readonly

AS

SET NOCOUNT ON

BEGIN

	CREATE TABLE #Price (
		AsOfDate smalldatetime,
		SecurityID bigint,
		PriceAsOfDate smalldatetime,
		Price numeric(18,6),
		Price_CurrencyID int,
		PRIMARY KEY (PriceAsOfDate, AsOfDate, SecurityID),
		UNIQUE (PriceAsOfDate, AsOFDate, SecurityID)
		)

	CREATE INDEX IX_1 on #Price (AsOfDate, SecurityID)

	INSERT INTO #Price (
		AsOfDate,
		SecurityID,
		PriceAsOfDate,
		Price,
		Price_CurrencyID
		)
	SELECT AsOfDate,
		SecurityID,
		PriceAsOfDate,
		Price,
		Price_CurrencyID
	FROM dbo.tfn_DisplayCurrentPrice_By_SecurityList_By_DateList (
				@SecurityList,
				@DateList
				) 
	--FROM dbo.tfn_DisplayCurrentPrice_By_SecurityList_By_PeriodList (
	--			@SecurityList,
	--			@PeriodList
	--			) 
	GROUP BY AsOfDate, 
		SecurityID,
		PriceAsOfDate,
		Price,
		Price_CurrencyID
	ORDER BY PriceAsOfDate, SecurityID

	CREATE TABLE #Trans (
		RowID int not null identity(1,1),
		Quantity numeric(18,6),
		UnitPrice_Base numeric(18,6),
		UnitPrice_Local numeric(18,6),
		Amount_Base numeric(18,6),
		Amount_Local numeric(18,6),
		TradeDate smalldatetime,
		SettleDate smalldatetime,
		CurrencyID_Base int,
		CurrencyID_Local int,
		TransCodeName varchar(100),
		EntityID bigint,
		EntityName varchar(100),
		AccountID bigint,
		AccountName varchar(100),
		AccountSecurityID bigint,
		SecurityID bigint,
		SecurityName varchar(100),
		CalcCashFlow int,
		CalcDividend int,
		CalcInterest int,
		CalcFee int,
		CalcDisplay int,
		CalcIncome int,
		CalcQuantity int,
		CalcQuantityOfOne int,
		QuantityOfOne bit,
		PRIMARY KEY (RowID, SecurityID, TradeDate),
		UNIQUE (SecurityID, TradeDate, RowID)

		)

	CREATE INDEX IX_2 on #Trans (SecurityID, TradeDate)


	INSERT INTO #Trans (
		Quantity,
		UnitPrice_Base,
		UnitPrice_Local,
		Amount_Base,
		Amount_Local,
		TradeDate,
		SettleDate,
		CurrencyID_Base,
		CurrencyID_Local,
		TransCodeName,
		EntityID,
		EntityName,
		AccountID,
		AccountName,
		AccountSecurityID,
		SecurityID,
		SecurityName,
		CalcCashFlow,
		CalcDividend,
		CalcInterest,
		CalcFee,
		CalcDisplay,
		CalcIncome,
		CalcQuantity,
		CalcQuantityOfOne,
		QuantityOfOne
		)
	SELECT Quantity,
		UnitPrice_Base,
		UnitPrice_Local,
		Amount_Base,
		Amount_Local,
		TradeDate,
		SettleDate,
		CurrencyID_Base,
		CurrencyID_Local,
		TransCodeName,
		EntityID,
		EntityName,
		AccountID,
		AccountName,
		AccountSecurityID,
		SecurityID,
		SecurityName,
		CalcCashFlow,
		CalcDividend,
		CalcInterest,
		CalcFee,
		CalcDisplay,
		CalcIncome,
		CalcQuantity,
		CalcQuantityOfOne,
		QuantityOfOne
	--FROM [dbo].[tfn_Trans_by_SecurityList] (
	--			@SecurityList
	--			) 
	FROM @TransactionList
	
	SELECT Price.AsOfDate,
		Trans.AccountSecurityID, 
		Trans.EntityID,
		Trans.EntityName,
		Trans.AccountID,
		Trans.AccountName,
		Trans.SecurityID,
		Trans.SecurityName, 
		CASE WHEN ISNULL(Trans.QuantityofOne, 1) = 1
			THEN (1 * SUM(Trans.CalcQuantityOfOne)) + 1
			ELSE SUM(Trans.Quantity * Trans.CalcQuantity)
			END as Quantity,
		Price.Price as Price_Base,
		Price.Price as Price_Local,
		CASE WHEN ISNULL(Trans.QuantityofOne, 1) = 1
			THEN ((1 * SUM(Trans.CalcQuantityOfOne)) + 1)
			ELSE SUM(Trans.Quantity * Trans.CalcQuantity)
			END * Price.Price as MarketValue_Base,
		CASE WHEN ISNULL(Trans.QuantityofOne, 1) = 1
			THEN ((1 * SUM(Trans.CalcQuantityOfOne)) + 1)
			ELSE SUM(Trans.Quantity * Trans.CalcQuantity)
			END * Price.Price as MarketValue_Local,
			1 as ExchangeRate
	FROM #Price as Price
		JOIN #Trans as Trans ON Trans.SecurityID = Price.SecurityID
						AND Trans.TradeDate <= Price.AsOfDate
	GROUP BY Price.AsOfDate,
		Trans.AccountSecurityID,
		Trans.EntityID,
		Trans.EntityName,
		Trans.AccountID,
		Trans.AccountName,
		Trans.SecurityID,
		Trans.SecurityName,
		Trans.SecurityName,
		Trans.QuantityOfOne,
		Price.Price

	DROP TABLE #Price
	DROP TABLE #Trans
END


GO

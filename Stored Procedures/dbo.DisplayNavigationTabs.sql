SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROC [dbo].[DisplayNavigationTabs]

AS

SELECT TabName, 
	TabID, 
	ReportName,
	TabOrder,
	Disable
FROM NavigationTabs
ORDER BY TabOrder

GO

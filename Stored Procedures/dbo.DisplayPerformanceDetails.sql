SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROC [dbo].[DisplayPerformanceDetails]
	@Level CHAR(1),
	@LevelID BIGINT,
	@BeginDate SMALLDATETIME,
	@EndDate SMALLDATETIME,
	@IncludeLinkedAccounts BIT = 0



AS

DECLARE @MTD BIT = 0
DECLARE @QTD BIT = 1
DECLARE @YTD bit = 0
DECLARE @1YR bit = 0
DECLARE @3YR bit = 0
DECLARE @5YR bit = 0
DECLARE @10YR bit = 0
DECLARE @ITD bit = 0


DECLARE @SecurityList securitylist

INSERT INTO @SecurityList (
	AccountSecurityID,
	AccountID,
	AccountName,
	SecurityID,
	SecurityName,
	Pricefactor,
	ExcludeFromPerformance,
	PercentOwnership,
	EntityID
	)
SELECT AccountSecurityID,
	AccountID,
	AccountName,
	SecurityID,
	SecurityName,
	PriceFactor,
	ExcludeFromPerformance,
	ISNULL(PercentOwnership, 100),
	EntityID
FROM [dbo].[tfn_SecurityList_by_Level] (@Level, @LevelID, @EndDate, @IncludeLinkedAccounts)

-- Create and populate the periodlist based on the type of report
		DECLARE @PeriodList periodlist
		INSERT INTO @PeriodList(
			BeginDate,
			EndDate
			)
		SELECT BeginDate,
			EndDate
		FROM [dbo].[tfn_PeriodList_PerformanceReview] (
			@EndDate,
			@MTD,
			@QTD,
			@YTD,
			@1YR,
			@3YR,
			@5YR,
			@10YR,
			@ITD
			)


EXEC [CalculateDataForTWR] 
	@SecurityList = @SecurityList,
	@PeriodList = @Periodlist,
	@Level = @Level,
	@LevelID = @LevelID,
	@RecordSet = 1

GO

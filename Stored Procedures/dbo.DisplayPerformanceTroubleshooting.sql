SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROC [dbo].[DisplayPerformanceTroubleshooting]
	@Level VARCHAR(1),
	@LevelID BIGINT,
	@BeginDate SMALLDATETIME,
	@EndDate SMALLDATETIME,
	@IncludeLinkedAccounts BIT = 0


AS

CREATE TABLE #Output (
	LEVEL VARCHAR(1),
	LevelID BIGINT,
	BeginDate SMALLDATETIME,
	EndDate SMALLDATETIME,
	IsPortfolio BIT,
	IsBenchmark BIT,
	BenchmarkID INT,
	DisplayName VARCHAR(100),
	PeriodReturn NUMERIC(18,6),
	YTDReturn NUMERIC(18,6)
	)


	DECLARE @SecurityList securitylist

	INSERT INTO @SecurityList (
		AccountSecurityID,
		AccountID,
		AccountName,
		SecurityID,
		SecurityName,
		Pricefactor,
		ExcludeFromPerformance,
		PercentOwnership,
		EntityID
		)
	SELECT AccountSecurityID,
		AccountID,
		AccountName,
		SecurityID,
		SecurityName,
		PriceFactor,
		ExcludeFromPerformance,
		ISNULL(PercentOwnership, 100),
		EntityID
	FROM [dbo].[tfn_SecurityList_by_Level] (@Level, @LevelID, @EndDate, @IncludeLinkedAccounts)

			-- CREATE the TWR temp table that will be used to calculate account and level independently
		CREATE TABLE #TWR (
			[TWR_ID] [bigint] identity(1,1) NOT NULL,
			[Level] nvarchar(1) NOT NULL,
			[LevelID] [bigint] NOT NULL,
			[AccountID] [bigint] NULL,
			[AccountName] varchar(100),
			[AccountNumber] varchar(100),
			[AccountInceptionDate] smalldatetime,
			[PeriodBeginDate] [smalldatetime] NULL,
			[PeriodEndDate] [smalldatetime] NULL,
			[TWRBeginDate] [smalldatetime] NULL,
			[TWREndDate] [smalldatetime] NULL,
			[BeginMarketValue] [numeric](18, 6) NULL,
			[EndMarketValue] [numeric](18, 6) NULL,
			[CashFlow] [numeric](18, 6) NULL,
			[Additions] [numeric](18, 6) NULL,
			[Withdrawals] [numeric](18, 6) NULL,
			[Income] [numeric](18, 6) NULL,
			[AccruedIncome] [numeric](18, 6) NULL,
			[Fees] [numeric](18, 6) NULL,
			[PeriodTWR] [numeric](18, 6) NULL,
			[PeriodTWR_Net_Of_Fees] [numeric](18, 6) NULL,
			[PeriodTWR_Plus_One] [numeric](18, 6) NULL,
			[PeriodTWR_Net_Of_Fees_Plus_One] [numeric](18, 6) NULL,
			[TWR] float NULL,
			[AnnualizedTWR] float NULL,
			[TWR_Net_Of_Fees] float NULL,
			[AnnualizedTWR_Net_Of_Fees] float NULL,
			[ExcludeFromPerformance] bit
		)

		CREATE INDEX TWRIndex1 ON #TWR (PeriodBeginDate, TWRBeginDate)
		CREATE INDEX TWRIndex2 ON #TWR (PeriodEndDate, TWREndDate)

		-- Create and populate the periodlist based on the type of report
		DECLARE @PeriodList periodlist
		INSERT INTO @PeriodList(
			BeginDate,
			EndDate
			)
		SELECT @BeginDate,
			@EndDate
		UNION all
		SELECT [dbo].[sfn_YearBegin](@EndDate),
			@EndDate
	

		-- Calculate the Account Level Performance
		EXEC [CalculateDataForTWR]
				@SecurityList,
				@PeriodList,
				@RecordSet = 1,
				@Level = @Level

	--	INSERT INTO #Output (
	--		Level,
	--		LevelID,
	--		BeginDate,
	--		EndDate,
	--		IsPortfolio,
	--		IsBenchmark
	--		)
	--	SELECT @Level,
	--		@LevelID,
	--		@BeginDate,
	--		@EndDate,
	--		1 AS IsPortfolio,
	--		0 AS IsBenchmark

	--	UPDATE #Output 
	--	SET PeriodReturn = TWR
	--	FROM #Output as Output
	--		CROSS APPLY #TWR as TWR
	--	WHERE PeriodBeginDate = @BeginDate
	--		AND PeriodEndDate = @EndDate 
	--		AND ISNULL(TWR.ExcludeFromPerformance, 0) = 0

	----UPDATE the YTD TWR
	--	UPDATE #Output 
	--	SET YTDReturn = TWR
	--	FROM #Output as Output
	--		CROSS APPLY #TWR as TWR
	--	WHERE PeriodBeginDate = [dbo].[sfn_YearBegin](@EndDate)
	--		AND PeriodEndDate = @EndDate 
	--		AND ISNULL(TWR.ExcludeFromPerformance, 0) = 0

	--DECLARE @Index int = 1

	--WHILE @Index <= 3
	--BEGIN

	--INSERT INTO #Output (
	--		Level,
	--		LevelID,
	--		BeginDate,
	--		EndDate,
	--		IsPortfolio,
	--		IsBenchmark,
	--		DisplayName,
	--		BenchmarkID
	--		)
	--	SELECT @Level,
	--		@LevelID,
	--		@BeginDate,
	--		@EndDate,
	--		0 AS IsPortfolio,
	--		1 AS IsBenchmark,
	--		BenchmarkName,
	--		@Index AS BenchmarkID
	--	FROM Benchmark
	--	WHERE BenchmarkID = @Index

	--	UPDATE #Output
	--	SET PeriodReturn = TWR.BenchmarkReturn
	--	FROM #Output as Output
	--		CROSS APPLY dbo.tfn_DisplayBenchmarkReturn 
	--			(@Index,
	--			@BeginDate,
	--			@EndDate
	--			) AS TWR
	--	WHERE Output.IsBenchmark = 1
	--		 AND Output.BenchmarkID = TWR.BenchmarkID
		
	--	UPDATE #Output
	--	SET YTDReturn = TWR.BenchmarkReturn
	--	FROM #Output as Output
	--		CROSS APPLY dbo.tfn_DisplayBenchmarkReturn 
	--			(@Index,
	--			[dbo].[sfn_YearBegin](@EndDate) ,
	--			@EndDate
	--			) AS TWR
	--	WHERE Output.IsBenchmark = 1
	--		 AND Output.BenchmarkID = TWR.BenchmarkID

	--	SET @Index = @Index + 1

	--END

	--SELECT *
	--FROM #Output

GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROC [dbo].[DisplayPeriodPerformanceByCategory]
	@Level varchar(1),
	@LevelID bigint,
	@Category varchar(20),
	@BeginDate SMALLDATETIME = '1/1/1900',
	@EndDate SMALLDATETIME,
	@IncludeLinkedAccounts BIT = 0

AS

BEGIN

	IF ISNULL(@BeginDate, '1/1/1900') = '1/1/1900'
	BEGIN
		SET @BeginDate = DATEADD(DAY, -1, [dbo].[sfn_QuarterBegin] (@EndDate))
	END

	SET NOCOUNT ON
	BEGIN TRY

	--Create the variables 
	DECLARE @NetOfFees bit = 0
	DECLARE @Annualize bit = 0
	DECLARE @TWR numeric(18,6)
	DECLARE @CategorySecurityList Securitylist

	DECLARE @ActualBeginDate smalldatetime
	DECLARE @ActualEndDate smalldatetime
	DECLARE @BeginMarketValue numeric(18,6)
	DECLARE @EndMarketValue numeric(18,6)
	DECLARE @CashFlows numeric(18,6)
	DECLARE @Fees numeric(18,6)
	DECLARE @Income numeric(18,6)

	-- CREATETemporary @AllSecurityList 
	CREATE TABLE #AllSecurityList (
		FamilyID	bigint,
		FamilyName	varchar(max),
		EntityID	bigint,
		EntityName	varchar(max),
		AccountID	bigint,
		AccountName varchar(max),
		AccountSecurityID bigint,
		SecurityID	bigint,
		SecurityName	varchar(max),
		SectorName varchar(155),
		SectorID	bigint,
		SectorSortOrder int,
		SectorColor varchar(50),
		RegionName varchar(255),
		RegionID bigint,
		RegionSortOrder int,
		RegionColor varchar(50),
		AssetClassName varchar(255),
		AssetClassID bigint,
		AssetClassSortOrder int,
		AssetClassColor varchar(50)
		)

	-- CREATE Temp Table #Category
	CREATE TABLE #Category (
		ID int identity(1,1),
		CategoryGroup varchar(20),
		CategoryID int,
		CategoryName varchar(100),
		CategoryBeginMarketValue numeric(18,6),
		CategoryEndMarketValue numeric(18,6),
		CategoryCashFlows numeric(18,6),
		CategoryTWR numeric(18,6),
		CategoryIncome numeric(18,6),
		CategoryColor varchar(100),
		CategoryImage varchar(100),
		CategorySortOrder int,
		)	

	--Populate the #AllSecurityList with all the securities in the Level by LevelID
	INSERT INTO #AllSecurityList (
		AccountSecurityID,
		SecurityID,
		SecurityName,
		SectorName,
		SectorID,
		SectorSortOrder,
		SectorColor,
		RegionName,
		RegionID,
		RegionSortOrder,
		RegionColor,
		AssetClassName,
		AssetClassID,
		AssetClassSortOrder,
		AssetClassColor
		)
	SELECT 	AccountSecurityID,
		SecurityID,
		SecurityName,
		SectorName,
		SectorID,
		SectorSortOrder,
		SectorColor,
		RegionName,
		RegionID,
		RegionSortOrder,
		RegionColor,
		AssetClassName,
		AssetClassID,
		AssetClassSortOrder,
		AssetClassColor
	FROM tfn_SecurityList_by_Level (@Level, @LevelID, @EndDate, @IncludeLinkedAccounts)

	--Populate Category Table
	IF UPPER(@Category) = 'AC' or UPPER(@Category) LIKE 'ASSET%'
	BEGIN
		INSERT INTO #Category (
			CategoryGroup,
			CategoryID,
			CategoryName,
			CategoryColor,
			CategorySortOrder
			)
		SELECT 'AssetClass',
			AssetClass.AssetClassID, 
			AssetClass.AssetClassName,
			AssetClass.AssetClassColor,
			AssetClass.AssetClassSortOrder
		FROM #AllSecurityList as AssetClass
		GROUP BY AssetClass.AssetClassID,
			AssetClass.AssetClassName,
			AssetClass.AssetClassColor,
			AssetClass.AssetClassSortOrder
	END
	ELSE IF UPPER(@Category) = 'SR' OR UPPER(@Category) = 'SECTOR'
	BEGIN
		INSERT INTO #Category (
			CategoryGroup,
			CategoryID,
			CategoryName,
			CategoryColor,
			CategorySortOrder
			)
		SELECT 'Sector',
			Sector.SectorID, 
			Sector.SectorName,
			Sector.SectorColor,
			Sector.SectorSortOrder
		FROM #AllSecurityList as Sector
		GROUP BY Sector.SectorID, 
			Sector.SectorName,
			Sector.SectorColor,
			Sector.SectorSortOrder
	END
	ELSE IF UPPER(@Category) = 'RN' OR UPPER(@Category) = 'REGION'
	BEGIN
		INSERT INTO #Category (
			CategoryGroup,
			CategoryID,
			CategoryName,
			CategoryColor,
			CategorySortOrder
			)
		SELECT 'Region',
			Region.RegionID, 
			Region.RegionName,
			Region.RegionColor,
			Region.RegionSortOrder
		FROM #AllSecurityList as Region
		GROUP BY Region.RegionID, 
			Region.RegionName,
			Region.RegionColor,
			Region.RegionSortOrder
	END

	--DECLARE the Looping Variables
	DECLARE @Index int
	DECLARE @CategoryName varchar(100)
	DECLARE @CategoryID int
	DECLARE @CategoryGroup varchar(20)

	--SET The Max Category Index
	SELECT @Index = MAX(ID)
	FROM #Category

	-- Loop Through the Categories
	WHILE @Index > 0
	BEGIN
		SELECT @CategoryName = CategoryName,
			@CategoryID = CategoryID,
			@CategoryGroup = CategoryGroup
		FROM #Category
		WHERE ID = @Index

		-- Clean out the Temp Table
		DELETE  FROM @CategorySecurityList

		--Populate the Temp Table
		INSERT @CategorySecurityList (
			 AccountSecurityID
			)
		SELECT SecurityList.AccountSecurityID
		FROM #AllSecurityList as SecurityList
		WHERE @CategoryID = (	CASE @CategoryGroup
								WHEN 'AssetClass' THEN AssetClassID
								WHEN 'Sector' THEN SectorID
								WHEN 'Region' THEN RegionID
								ELSE -1
								END)
		GROUP BY SecurityList.AccountSecurityID

		--RESET all the variables to null
		SET @BeginMarketValue = NULL
		SET @EndMarketValue = NULL
		SET @CashFlows = NULL
		SET @Fees = NULL
		SET @Income = NULL


		--Find the BeginMarketValue
		SELECT @ActualBeginDate = @BeginDate, 
			@BeginMarketValue = SUM(Holdings.MarketValue_Base)
		FROM [dbo].[tfn_Holdings_by_Level](@Level, @LevelID, @BeginDate, @IncludeLinkedAccounts, 0) AS Holdings
			JOIN @CategorySecurityList as SecurityList ON Holdings.AccountSecurityID = SecurityList.AccountSecurityID
		--	JOIN (SELECT MAX(AsOfDate) as AsOfDate 
		--			FROM Holdings 
		--			WHERE AsOfDate <= DATEADD(day, -1, @BeginDate)) as AsOfDate ON Holdings.AsOfDate = AsOfDate.AsOfDate
		--GROUP BY AsOfDate.AsOfDate

		--Find the EndMarketValue
		SELECT @ActualEndDate = @EndDate, 
			@EndMarketValue = SUM(Holdings.MarketValue_Base)
		FROM [dbo].[tfn_Holdings_by_Level](@Level, @LevelID, @EndDate, @IncludeLinkedAccounts, 0) AS Holdings
			JOIN @CategorySecurityList as SecurityList ON Holdings.AccountSecurityID = SecurityList.AccountSecurityID
			--JOIN (SELECT MAX(AsOfDate) as AsOfDate 
			--		FROM Holdings 
			--		WHERE AsOfDate <= @EndDate) as AsOfDate ON Holdings.AsOfDate = AsOfDate.AsOfDate
		--GROUP BY AsOfDate.AsOfDate

		--Find the CashFlows
		SELECT Trans.CashFlows, 
			Trans.TradeDate, 
			Trans.TransCodeName, 
			Trans.EntityID, 
			Trans.AccountID, 
			Trans.AccountSecurityID, 
			Trans.SecurityID
		FROM [tfn_CashFlowForTWR_By_Level] (
			@CategorySecurityList,
			@ActualBeginDate,
			@ActualEndDate
			) as Trans
		ORDER BY Trans.TradeDate

		SELECT *
		FROM @CategorySecurityList

		--EXEC CashFlowForTWR_By_Level
		--	@SecurityList = @CategorySecurityList,
		--	@BeginDate = @ActualBeginDate,
		--	@EndDate = @ActualEndDate,
		--	@CashFlows = @CashFlows OUT,
		--	@DetailRecordSet = 1

		--Find the Income
		--EXEC Income_By_Level
		--	@SecurityList = @CategorySecurityList,
		--	@BeginDate = @ActualBeginDate,
		--	@EndDate = @ActualEndDate,
		--	@Income = @Income OUT,
		--	@DetailRecordSet = 0

		--Find the Fees
		IF @NetOfFees = 1
		BEGIN
			EXEC FeesForTWR_By_Level
				@SecurityList = @CategorySecurityList,
				@BeginDate = @ActualBeginDate,
				@EndDate = @ActualEndDate,
				@Fees = @Fees OUT
		END

		--Set values to 0 if null
		SET @BeginMarketValue = ISNULL(@BeginMarketValue, 0)
		SET @EndMarketValue = ISNULL(@EndMarketValue, 0)
		SET @CashFlows = ISNULL(@CashFlows, 0)
		SET @Fees = ISNULL(@Fees, 0)
		SET @Income = ISNULL(@Income, 0)

		--Calculate the TWR
		IF @BeginMarketValue > 0
		BEGIN
			SELECT @TWR = (((@EndMarketValue + @CashFlows + @Fees) - @BeginMarketValue) / @BeginMarketValue)
		END
		ELSE
		BEGIN
			SELECT @TWR = 0
		END

		--Annualize
		IF @Annualize = 1
		BEGIN
			SELECT @TWR = dbo.sfn_AnnualizeReturn
							(
								@TWR,
								@ActualBeginDate,
								@ActualEndDate
							)
		END

		--SELECT @BeginMarketValue, 
		--	@EndMarketValue,
		--	@CashFlows,
		--	@CategoryID

		UPDATE #Category
		SET CategoryBeginMarketValue = @BeginMarketValue, 
			CategoryEndMarketValue = @EndMarketValue, 
			CategoryCashFlows = @CashFlows,
			CategoryTWR = @TWR,
			CategoryIncome = @Income
		WHERE CategoryID = @CategoryID

		SELECT @Index = @Index - 1
	END

	--DELETE Entries that have a zero balance for EndMarketValue
	DELETE FROM #Category
	WHERE CategoryEndMarketValue = 0

	SELECT * FROM #Category
	ORDER BY CategorySortOrder
	
	END TRY
	BEGIN CATCH
		DECLARE @ErrMsg nvarchar(4000) = ERROR_MESSAGE()
		DECLARE @ErrNum INT = ERROR_NUMBER()
		DECLARE @ErrProc nvarchar(126) = ERROR_PROCEDURE()
		DECLARE @DataError nvarchar(4000) = '[DisplayPeriodPerformanceByCategory]' +
			CONVERT(nvarchar(10), @errNum) + ', Error Details: ' + @ErrMsg
		RAISERROR (@DataError, 16,1)
	END CATCH
END

GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROC [dbo].[DisplayPrices_By_Security]
	@UserName varchar(100) = 'Default',
	@AccountSecurityID BIGINT = 0,
	@SecurityID BIGINT = 0

AS

IF @AccountSecurityID <> 0
BEGIN

	SELECT @AccountSecurityID AS AccountSecurityID,
		Security.SecurityName,
		Security.SecurityID,
		Security.Symbol,
		Security.CUSIP,
		Price.PriceID, 
		Price.Price,
		Price.Price_CurrencyID,
		Price.AsOfDate,
		Price.Estimate,
		Price.DataSource,
		Price.DateTimeEntered,
		Price.EnteredBy,
		Price.SourceName
	FROM dbo.Price
		JOIN AccountSecurity ON Price.SecurityID = AccountSecurity.SecurityID
		JOIN Security ON Security.SecurityID = Price.SecurityID
	WHERE AccountSecurity.AccountSecurityID = @AccountSecurityID
END

GO

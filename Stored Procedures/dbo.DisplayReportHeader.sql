SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROC [dbo].[DisplayReportHeader]
	@Level varchar(1),
	@LevelID BIGINT,
	@Username VARCHAR(100) = ''

AS

CREATE TABLE #ReportHeader (
	Level varchar(1),
	LevelName varchar(100),
	LevelID bigint,
	HeaderName varchar(100),
	ClientLongName VARCHAR(100),
	ClientShortName VARCHAR(100),
	ColorSchemeID INT,
	IsAdmin bit
	)


IF UPPER(@Level) = 'D'
BEGIN
	INSERT INTO #ReportHeader (
		Level,
		LevelName,
		LevelID,
		HeaderName,
		ClientLongName,
		ClientShortName,
		ColorSchemeID,
		IsAdmin
		)
	SELECT @Level,
		'Advisor',
		@LevelID,
		AdvisorName,
		Client.ClientLongName,
		Client.ClientShortName,
		ISNULL(Users.ColorSchemeID, 1),
		Users.Admin
	FROM Advisor
		CROSS APPLY  (SELECT TOP 1 * FROM Client) AS Client
		CROSS APPLY (SELECT TOP 1 * FROM Users WHERE UserName = @Username) as Users 
	WHERE AdvisorID = @LevelID
END
ELSE IF UPPER(@Level) = 'F'
BEGIN
	INSERT INTO #ReportHeader (
		Level,
		LevelName,
		LevelID,
		HeaderName,
		ClientLongName,
		ClientShortName,
		ColorSchemeID,
		IsAdmin
		)
	SELECT @Level,
		'Family',
		@LevelID,
		FamilyName,
		Client.ClientLongName,
		Client.ClientShortName,
		ISNULL(Users.ColorSchemeID, 1),
		Users.Admin
	FROM Family
		CROSS APPLY (SELECT TOP 1 * FROM Client) AS Client
		CROSS APPLY (SELECT TOP 1 * FROM Users WHERE UserName = @Username) as Users 
	WHERE FamilyID = @LevelID
END
ELSE IF UPPER(@Level) = 'P'
BEGIN
	INSERT INTO #ReportHeader (
		Level,
		LevelName,
		LevelID,
		HeaderName,
		ClientLongName,
		ClientShortName,
		ColorSchemeID,
		IsAdmin
		)
	SELECT @Level,
		'Portfolio',
		@LevelID,
		PortfolioName,
		Client.ClientLongName,
		Client.ClientShortName,
		ISNULL(Users.ColorSchemeID, 1),
		Users.Admin
	FROM Portfolio
		CROSS APPLY (SELECT TOP 1 * FROM Client) AS Client
		CROSS APPLY (SELECT TOP 1 * FROM Users WHERE UserName = @Username) as Users 
	WHERE PortfolioID = @LevelID
END
ELSE IF UPPER(@Level) = 'E'
BEGIN
	INSERT INTO #ReportHeader (
		Level,
		LevelName,
		LevelID,
		HeaderName,
		ClientLongName,
		ClientShortName,
		ColorSchemeID,
		IsAdmin
		)
	SELECT @Level,
		'Entity',
		@LevelID,
		EntityName,
		Client.ClientLongName,
		Client.ClientShortName,
		ISNULL(Users.ColorSchemeID, 1),
		Users.Admin
	FROM Entity
		CROSS APPLY  (SELECT TOP 1 * FROM Client) AS Client
		CROSS APPLY (SELECT TOP 1 * FROM Users WHERE UserName = @Username) as Users 
	WHERE EntityID = @LevelID
END
ELSE IF UPPER(@Level) = 'A'
BEGIN
	INSERT INTO #ReportHeader (
		Level,
		LevelName,
		LevelID,
		HeaderName,
		ClientLongName,
		ClientShortName,
		ColorSchemeID,
		IsAdmin
		)
	SELECT @Level,
		'Account',
		@LevelID,
		Account.AccountName,
		Client.ClientLongName,
		Client.ClientShortName,
		ISNULL(Users.ColorSchemeID, 1),
		Users.Admin
	FROM Account
		JOIN EntityAccount ON Account.AccountID = EntityAccount.AccountID
		JOIN Entity ON EntityAccount.EntityID = Entity.EntityID
		CROSS APPLY  (SELECT TOP 1 * FROM Client) AS Client
		CROSS APPLY (SELECT TOP 1 * FROM Users WHERE UserName = @Username) as Users 
	WHERE Account.AccountID = @LevelID
END
ELSE IF UPPER(@Level) = 'H'
BEGIN
	INSERT INTO #ReportHeader (
		Level,
		LevelName,
		LevelID,
		HeaderName,
		ClientLongName,
		ClientShortName,
		ColorSchemeID,
		IsAdmin
		)
	SELECT @Level,
		'Holding',
		@LevelID,
		Security.SecurityName + ' in ' + Account.AccountName,
		Client.ClientLongName,
		Client.ClientShortName,
		ISNULL(Users.ColorSchemeID, 1),
		Users.Admin
	FROM AccountSecurity
		JOIN Account ON AccountSecurity.AccountID = Account.AccountID
		JOIN Security ON AccountSecurity.SecurityID = Security.SecurityID
		CROSS APPLY  (SELECT TOP 1 * FROM Client) AS Client
		CROSS APPLY (SELECT TOP 1 * FROM Users WHERE UserName = @Username) as Users 
	WHERE AccountSecurity.AccountSecurityID = @LevelID
END

SELECT *
FROM #ReportHeader

DROP TABLE #ReportHeader

GO

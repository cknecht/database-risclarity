SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROC [dbo].[DisplaySecurities_By_Level]
	@Level varchar(1) = 'E',
	@LevelID BIGINT,
	@AsOfDate SMALLDATETIME,
	@Username VARCHAR(100) = ''
AS

SELECT * FROM tfn_SecurityList_by_Level (@Level, @LevelID, @AsOfDate, 0)

GO

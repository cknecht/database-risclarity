SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROC [dbo].[DisplaySecurityDetails]
	@UserName VARCHAR(100) = '',
	@PriceID BIGINT = 0,
	@AccountSecurityID BIGINT = 0,
	@SecurityID BIGINT = 0

AS

IF @PriceID <> 0
BEGIN
	SELECT Security.*
	FROM dbo.Security
		JOIN Price ON Security.SecurityID = Price.SecurityID
	WHERE Price.PriceID = @PriceID
END
ELSE IF @AccountSecurityID <> 0
BEGIN
	SELECT Security.*
	FROM dbo.Security
		JOIN AccountSecurity ON Security.SecurityID = AccountSecurity.SecurityID
	WHERE AccountSecurityID = @AccountSecurityID
END
ELSE IF @SecurityID <> 0
BEGIN
	SELECT Security.*
	FROM dbo.Security
	WHERE SecurityID = @SecurityID
END
GO

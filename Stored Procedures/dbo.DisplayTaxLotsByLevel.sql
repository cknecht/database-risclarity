SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROC [dbo].[DisplayTaxLotsByLevel]
	@Level varchar(1), -- A = Account, E = Entity, H = Holding
	@LevelID bigint,
	@EndDate SMALLDATETIME,
	@IncludeLinkedAccounts BIT = 0,
	@ReportOutput VARCHAR(20) = 'Open Tax Lots' -- 'Realized', 'Adjustments', 'Everything'

AS

BEGIN

	SET NOCOUNT ON
	--BEGIN TRY

	DECLARE @BeginDate SMALLDATETIME = '1/1/1900'

	--Create the variables 
	DECLARE @SecurityList Securitylist

	--Populate the @SecurityList with all the securities in the Level by LevelID
	INSERT INTO @SecurityList (
		AccountSecurityID,
		SecurityID,
		SecurityName,
		AccountID,
		AccountName,
		EntityID,
		EntityName,
		PriceFactor,
		TaxLotReliefMethodID,
		QuantityOfOne,
		CalcTaxLots,
		PercentOwnership,
		AssetClassID,
		AssetClassName,
		AssetClassSortOrder,
		AssetClassColor,
		ContractSize
		)
	SELECT 	AccountSecurityID,
		SecurityID,
		SecurityName,
		AccountID,
		AccountName,
		EntityID,
		EntityName,
		PriceFactor,
		TaxLotReliefMethodID,
		QuantityOfOne,
		CalcTaxLots,
		PercentOwnership,
		AssetClassID,
		AssetClassName,
		AssetClassSortOrder,
		AssetClassColor,
		ContractSize
	FROM tfn_SecurityList_by_Level (@Level, @LevelID, @EndDate, @IncludeLinkedAccounts)

	-- Create a table to house the TaxLotReliefMethod Labels
	CREATE TABLE #TaxLotReliefMethod (
		TaxLotReliefMethodID tinyint,
		TaxLotReliefMethodLongName VARCHAR(100),
		TaxLotReliefMethodShortName VARCHAR(10)
		)

	-- Populate the table to house the TaxLotReliefMethod Labels
	INSERT INTO #TaxLotReliefMethod (
		TaxLotReliefMethodID,
		TaxLotReliefMethodLongName,
		TaxLotReliefMethodShortName
		) 
	SELECT TaxLotReliefMethodID,
		TaxLotReliefMethodLongName,
		TaxLotReliefMethodShortName
	FROM TaxLotReliefMethod

	--CREATE #Transactions Temp TABLE
	CREATE TABLE #Transactions (
		[TransID] [bigint] NOT NULL,
		[Quantity] [numeric](18, 6) NULL,
		[UnitPrice_Base] [numeric](18, 6) NULL,
		[UnitPrice_Local] [numeric](18, 6) NULL,
		[Amount_Base] [numeric](18, 6) NULL,
		[Amount_Local] [numeric](18, 6) NULL,
		[Cost_Basis_Base] [numeric](18, 6) NULL,
		[Cost_Basis_Local] [numeric](18, 6) NULL,
		[TradeDate] [smalldatetime] NULL,
		[SettleDate] [smalldatetime] NULL,
		[CurrencyID_Base] [int] NULL,
		[CurrencyID_Local] [int] NULL,
		[TransCodeName] [varchar](100) NULL,
		[TransCodeID] [int] NULL,
		[EntityID] [bigint] NULL,
		[EntityName] [varchar](100) NULL,
		[AccountID] [bigint] NULL,
		[AccountName] [varchar](100) NULL,
		[AccountSecurityID] [bigint] NULL,
		[SecurityID] [bigint] NULL,
		[SecurityName] [varchar](100) NULL,
		[CUSIP] VARCHAR(20) NULL,
		[Symbol] VARCHAR(20) NULL,
		[SEDOL] varchar(20) NULL,
		[ISIN] VARCHAR(20) NULL,
		[PriceFactor] NUMERIC(18,6),
		[CalcCashFlow] [int] NULL,
		[CalcDividend] [int] NULL,
		[CalcInterest] [int] NULL,
		[CalcFee] [int] NULL,
		[CalcDisplay] [int] NULL,
		[CalcIncome] [int] NULL,
		[CalcQuantity] [int] NULL,
		[CalcQuantityOfOne] [int] NULL,
		[CalcNetInvested] [int] NULL,
		[CalcCostBasisQuantity] [int] NULL,
		[CalcCostBasisAmount] [int] NULL,
		[CalcShort] [int] NULL,
		[QuantityOfOne] [bit] NULL,
		[Notes] VARCHAR(8000),
		[TaxLotReliefMethodID] TINYINT,
		[CalcTaxLots] BIT,
		[PercentOwnership] NUMERIC(18,6),
		[AssetClassID] INT,
		[AssetClassName] VARCHAR(255),
		[AssetClassSortOrder] INT,
		[AssetClassColor] VARCHAR(50)
	)

	-- CREATE the final output table
	CREATE TABLE #TaxLots (
		PurchaseTransID BIGINT,
		SaleTransID BIGINT,
		EntityName VARCHAR(100),
		EntityID BIGINT,
		AccountName VARCHAR(100),
		AccountID BIGINT,
		AccountSecurityID BIGINT,
		SecurityName VARCHAR(100),
		SecurityID BIGINT,
		CUSIP VARCHAR(20),
		Symbol VARCHAR(20),
		ISIN VARCHAR(20),
		SEDOL VARCHAR(20),
		Quantity NUMERIC(18,6),
		Quantity_Original NUMERIC(18,6),
		UnitPrice_Base numeric(18, 6),
		UnitPrice_Local numeric(18, 6),
		Amount_Base numeric(18, 6),
		Amount_Local numeric(18, 6),
		Amount_Base_Original numeric(18, 6),
		Amount_Local_Original numeric(18, 6),
		Cost_Basis_Base numeric(18, 6),
		Cost_Basis_Local numeric(18, 6),
		Cost_Basis_Base_Original numeric(18, 6),
		Cost_Basis_Local_Original numeric(18, 6),
		AcquireDate smalldatetime,
		SaleDate smalldatetime,
		Proceeds_Amount_Base NUMERIC(18,6),
		Proceeds_Amount_Local NUMERIC(18,6),
		CurrencyID_Base int,
		CurrencyID_Local int,
		TransCodeName varchar(100),
		TransCodeID int,
		MarketPrice_AsOfDate SMALLDATETIME,
		MarketPrice_Base NUMERIC(18,6),
		MarketPrice_Local NUMERIC(18,6),
		PriceFactor NUMERIC(18,6),
		MarketValue_Base NUMERIC(18,6),
		MarketValue_Local NUMERIC(18,6),
		UnRealizedGL_Base NUMERIC(18,6),
		UnRealizedGL_Local NUMERIC(18,6),
		HoldingDays INT,
		HoldingPeriod VARCHAR(20),
		CalcCashFlow INT,
		CalcCostBasisQuantity int,
		CalcCostBasisAmount int,
		CalcShort INT,
		IsTaxLot BIT,
		IsSale BIT,
		IsAdjustment BIT,
		Notes VARCHAR(8000),
		TaxLotReliefMethodID TINYINT,
		TaxLotReliefMethodLongName VARCHAR(100),
		TaxLotReliefMethodShortName VARCHAR(10),
		QuantityOfOne bit,
		PercentOwnership VARCHAR(255),
		CalcTaxLots TINYINT,
		AssetClassID int,
		AssetClassName VARCHAR(255),
		AssetClassSortOrder INT,
		AssetClassColor varchar(50)
		)

	--Populate the #Transactions table with all transactions that affect tax lots
	INSERT INTO #Transactions (
		TransID,
		Quantity,
		UnitPrice_Base,
		UnitPrice_Local,
		Amount_Base,
		Amount_Local,
		Cost_Basis_Base,
		Cost_Basis_Local,
		TradeDate,
		SettleDate,
		CurrencyID_Base,
		CurrencyID_Local,
		TransCodeName,
		TransCodeID,
		EntityName,
		EntityID,
		AccountName,
		AccountID,
		AccountSecurityID,
		SecurityID,
		SecurityName,
		CUSIP,
		Symbol,
		ISIN,
		SEDOL,
		CalcCashFlow,
		CalcDividend,
		CalcInterest,
		CalcFee,
		CalcDisplay,
		CalcIncome,
		CalcCostBasisAmount,
		CalcCostBasisQuantity,
		CalcShort,
		Notes,
		TaxLotReliefMethodID,
		QuantityOfOne,
		CalcTaxLots,
		PercentOwnership,
		AssetClassID,
		AssetClassName,
		AssetClassSortOrder,
		AssetClassColor
		)
	SELECT Trans.TransID,
		Trans.Quantity,
		Trans.UnitPrice_Base,
		Trans.UnitPrice_Local,
		Trans.Amount_Base,
		Trans.Amount_Local,
		COALESCE(Trans.TaxCost_Base, Trans.Amount_Base),
		COALESCE(Trans.TaxCost_Local, Trans.Amount_Local),
		Trans.TradeDate,
		Trans.SettleDate,
		Trans.CurrencyID_Base,
		Trans.CurrencyID_Local,
		Trans.TransCodeName,
		Trans.TransCodeID,
		Trans.EntityName,
		Trans.EntityID,
		Trans.AccountName,
		Trans.AccountID,
		Trans.AccountSecurityID,
		Trans.SecurityID,
		Trans.SecurityName,
		Trans.CUSIP,
		Trans.Symbol,
		Trans.ISIN,
		Trans.SEDOL,
		Trans.CalcCashFlow,
		Trans.CalcDividend,
		Trans.CalcInterest,
		Trans.CalcFee,
		Trans.CalcDisplay,
		Trans.CalcIncome,
		Trans.CalcCostBasisAmount,
		Trans.CalcCostBasisQuantity,
		Trans.CalcShort,
		Trans.Notes,
		Trans.TaxLotReliefMethodID,
		Security.QuantityOfOne,
		Trans.CalcTaxLots,
		Security.PercentOwnership,
		Security.AssetClassID,
		Security.AssetClassName,
		Security.AssetClassSortOrder,
		Security.AssetClassColor
	FROM [dbo].[tfn_Transactions_By_Level] (
		@SecurityList,
		@BeginDate,
		@EndDate
		) AS Trans
		JOIN @SecurityList AS Security ON Trans.AccountSecurityID = Security.AccountSecurityID
	WHERE ISNULL(CalcCostBasisQuantity, 0) <> 0
		OR ISNULL(CalcCostBasisAmount, 0) <> 0
	ORDER BY TradeDate

	-- POPULATE THE TAX LOT TABLE
	INSERT INTO #TaxLots (
		PurchaseTransID,
		EntityName,
		EntityID,
		AccountName,
		AccountID,
		AccountSecurityID,
		SecurityName,
		SecurityID,
		CUSIP,
		Symbol,
		ISIN,
		SEDOL,
		PriceFactor,
		Quantity,
		Quantity_Original,
		UnitPrice_Base,
		UnitPrice_Local,
		Amount_Base,
		Amount_Local,
		Amount_Base_Original,
		Amount_Local_Original,
		Cost_Basis_Base,
		Cost_Basis_Local,
		Cost_Basis_Base_Original,
		Cost_Basis_Local_Original,
		AcquireDate,
		CurrencyID_Base,
		CurrencyID_Local,
		TransCodeName,
		TransCodeID,
        CalcCashFlow,
		CalcCostBasisQuantity,
		CalcCostBasisAmount,
		CalcShort,
		IsTaxLot,
		Notes,
		TaxLotReliefMethodID,
		QuantityOfOne,
		PercentOwnership,
		CalcTaxLots,
		AssetClassID,
		AssetClassName,
		AssetClassSortOrder,
		AssetClassColor
		)
	-- Quantity of One is 0 (Not Quantity Of One)
	SELECT TransID,
		EntityName,
		EntityID,
		AccountName,
		AccountID,
		AccountSecurityID,
		SecurityName,
		SecurityID,
		CUSIP,
		Symbol,
		ISIN,
		SEDOL,
		PriceFactor,
		CASE WHEN ISNULL(QuantityOfOne, 0) = 1 THEN 0 ELSE Quantity END,
		CASE WHEN ISNULL(QuantityOfOne, 0) = 1 THEN 0 ELSE Quantity END AS Quantity_Original,
		UnitPrice_Base,
		UnitPrice_Local,
		Amount_Base,
		Amount_Local,
		Amount_Base AS Amount_Base_Original,
		Amount_Local AS Amount_Local_Original,
		Cost_Basis_Base,
		Cost_Basis_Local,
		Cost_Basis_Base AS Cost_Basis_Base_Original,
		Cost_Basis_Local AS Cost_Basis_Local_Original,
		TradeDate,
		CurrencyID_Base,
		CurrencyID_Local,
		TransCodeName,
		TransCodeID,
		CalcCashFlow,
		CalcCostBasisQuantity,
		CalcCostBasisAmount,
		CalcShort,
		1 AS IsTaxLot,
		Notes,
		TaxLotReliefMethodID,
		QuantityOfOne,
		PercentOwnership,
		CalcTaxLots,
		AssetClassID,
		AssetClassName,
		AssetClassSortOrder,
		AssetClassColor
	FROM #Transactions
	WHERE ((CalcCostBasisQuantity > 0 AND CalcShort = 0) OR (CalcCostBasisQuantity < 0 AND CalcShort != 0))
		AND ISNULL(CalcTaxLots, 1) = 1
		AND ISNULL(QuantityOfOne, 0) = 0
		--AND Quantity <> Amount_Base
	UNION
	-- Quantity of One is 1 (Quantity Of One) OR Cash/Equivalent
	SELECT 0 AS TransID,
		MAX(EntityName),
		MAX(EntityID),
		MAX(AccountName),
		MAX(AccountID),
		AccountSecurityID,
		MAX(SecurityName),
		MAX(SecurityID),
		MAX(CUSIP),
		MAX(Symbol),
		MAX(ISIN),
		MAX(SEDOL),
		MAX(PriceFactor),
		CASE WHEN QuantityOfOne = 1 THEN 1 ELSE SUM(Quantity * CalcQuantity) END As Quantity,
		CASE WHEN QuantityOfOne = 1 THEN 1 ELSE SUM(Quantity * CalcQuantity) END AS Quantity_Original,
		SUM(Amount_Base) AS UnitPrice_Base,
		SUM(Amount_Local) AS UnitPrice_Local,
		SUM(Amount_Base),
		SUM(Amount_Local),
		SUM(Amount_Base) AS Amount_Base_Original,
		SUM(Amount_Local) AS Amount_Local_Original,
		SUM(Cost_Basis_Base),
		SUM(Cost_Basis_Local),
		SUM(Cost_Basis_Base) AS Cost_Basis_Base_Original,
		SUM(Cost_Basis_Local) AS Cost_Basis_Local_Original,
		MIN(TradeDate),
		MIN(CurrencyID_Base),
		MIN(CurrencyID_Local),
		'' AS TransCodeName,
		0 AS TransCodeID,
		CalcCashFlow,
		MIN(CalcCostBasisQuantity),
		MIN(CalcCostBasisAmount),
		CalcShort,
		1 AS IsTaxLot,
		'' AS Notes,
		0 AS TaxLotReliefMethodID,
		QuantityOfOne AS QuantityOfOne,
		CalcTaxLots,
		MIN(PercentOwnership),
		MIN(AssetClassID),
		MIN(AssetClassName),
		MIN(AssetClassSortOrder),
		MIN(AssetClassColor)
	FROM #Transactions
	WHERE  (ISNULL(QuantityOfOne, 0) = 1
			OR CalcTaxLots = 0)
	GROUP BY AccountSecurityID,
		QuantityofOne,
		CalcTaxLots,
		CalcCashFlow,
		CalcShort

	--********************************************************************************************
	--LOOP through the sales and decrement the TaxLots and add the Realized Gain/Loss
	--********************************************************************************************

	DECLARE @AccountSecurityID BIGINT
	DECLARE @MaxAccountSecurityID BIGINT
	DECLARE @Quantity NUMERIC(18,6)
	DECLARE @SaleProceeds_Base NUMERIC(18,6)

	CREATE TABLE #Purchases (
		OrderID INT IDENTITY(1,1),
		AccountSecurityID BIGINT,
		TransID bigint,
		AcquiredDate smalldatetime,
		Quantity numeric(18,6),
		UnitPrice numeric(18,6),
		CostBasis_Local NUMERIC(18,6),
		CostBasis_Base NUMERIC(18,6),
		CalcSort int
		)

	CREATE TABLE #SalesAdjustments (
		OrderID INT IDENTITY(1,1),
		AccountSecurityID bigint,
		TransID BIGINT,
		SaleDate SMALLDATETIME,
		Quantity NUMERIC(18,6),
		Amount NUMERIC(18,6),
		TaxLotReliefMethodID TINYINT,
		CalcShort INT
		)

	SELECT @MaxAccountSecurityID = ISNULL(MAX(AccountSecurityID), 0)
	FROM #Transactions
	WHERE (CalcCostBasisQuantity < 0 AND CalcShort = 0) 
		OR (CalcCostBasisQuantity < 0 AND CalcShort != 0)
		OR CalcCostBasisAmount <> 0
	
	SELECT @AccountSecurityID = ISNULL(MIN(AccountSecurityID), 0)
	FROM #Transactions
	WHERE (CalcCostBasisQuantity < 0 AND CalcShort = 0) 
		OR (CalcCostBasisQuantity < 0 AND CalcShort != 0)
		OR CalcCostBasisAmount <> 0

	--********************************
	-- INNER Loop Variables
	--********************************

	DECLARE @SalesOrderID INT
    DECLARE @MaxSalesOrderID INT
	DECLARE @SalesQuantity NUMERIC(18,6)
	DECLARE @SaleDate SMALLDATETIME
	DECLARE @SaleTransID bigint
    DECLARE @TaxLotReliefMethodID tinyint
	DECLARE @TotalSalesQuantity NUMERIC(18,6)
	DECLARE @RemainingSalesQuantity NUMERIC(18,6)
	DECLARE @AdjustmentAmount NUMERIC(18,6)
	DECLARE @TotalCost_Base NUMERIC(18,6)
	DECLARE @TotalCost_Local NUMERIC(18,6)

    DECLARE @PurchasesOrderID INT
    DECLARE @MaxPurchasesOrderID INT
    DECLARE @PurchaseQuantity NUMERIC(18,6)

	WHILE 0 = 0
	BEGIN

		--CLEAR THE #Purchases and #Sales Table
		TRUNCATE TABLE #Purchases
		TRUNCATE TABLE #SalesAdjustments

		--Find the Tax Lot Relief Method for the Asset (AccountSecurity)

		SELECT @TaxLotReliefMethodID = MIN(TaxLotReliefMethodID)
		FROM #Transactions
		WHERE AccountSecurityID = @AccountSecurityID

		--Populate Purchases in order of use (FIFO for default)
		
		
		INSERT INTO #Purchases (
			TransID,
			AccountSecurityID,
			AcquiredDate,
			Quantity,
			UnitPrice
			)
		SELECT TransID,
			AccountSecurityID,
			TradeDate,
			ABS(Quantity) * ISNULL(NULLIF(ABS(CalcShort) * -1, 0), 1) AS Quantity,
			ABS(Cost_Basis_Base) / ISNULL(NULLIF(ABS(Quantity), 0), 1) AS UnitPrice
		FROM #Transactions
		WHERE AccountSecurityID = @AccountSecurityID
			AND ((CalcCostBasisQuantity > 0 AND CalcShort = 0) OR (CalcCostBasisQuantity < 0 AND CalcShort != 0))
			AND ISNULL(TransID, 0) > 0
			AND ISNULL(QuantityOfOne, 0) = 0
		ORDER BY CASE @TaxLotReliefMethodID
			WHEN 1 THEN CONVERT(NUMERIC(18,6),TradeDate) -- (FIFO)
			WHEN 2 THEN (ABS(Cost_Basis_Base) / ABS(Quantity) * -1) -- (Highest Cost)
			WHEN 3 THEN  CONVERT(NUMERIC(18,6),TradeDate) * -1 -- (LIFO)
			WHEN 4 THEN  (ABS(Cost_Basis_Base) / ABS(Quantity)) -- (Lowest Cost)
			ELSE CONVERT(NUMERIC(18,6),TradeDate)
			END ASC,
			Quantity DESC

		--Populate Sales in chronological order
		INSERT INTO #SalesAdjustments (
			TransID,
			AccountSecurityID,
			SaleDate,
			Quantity,
			Amount,
			TaxLotReliefMethodID
			)
		SELECT Transactions.TransID,
			 Transactions.AccountSecurityID,
			 Transactions.TradeDate,
			(ABS(Transactions.Quantity) * ISNULL(NULLIF(ABS(Transactions.CalcShort) * -1, 0), 1) * (Transactions.CalcCostBasisQuantity)) AS Quantity,
			(ABS(Transactions.Amount_Base) * ISNULL(NULLIF(ABS(Transactions.CalcShort) * -1, 0), 1) *  Transactions.CalcCostBasisAmount) AS Amount,
			 Transactions.TaxLotReliefMethodID
		FROM #Transactions AS Transactions
			JOIN #TaxLotReliefMethod AS TaxLotReliefMethod ON Transactions.TaxLotReliefMethodID = TaxLotReliefMethod.TaxLotReliefMethodID
		WHERE AccountSecurityID = @AccountSecurityID
			AND ((CalcCostBasisQuantity < 0 AND CalcShort = 0)
				OR (CalcCostBasisQuantity > 0 AND CalcShort != 0)
				OR CalcCostBasisAmount <> 0)
			AND ISNULL(TransID, 0) > 0
		ORDER BY TradeDate ASC,
			Quantity DESC
        
		SELECT @SalesOrderID = 1
		SELECT @MaxSalesOrderID = ISNULL(MAX(OrderID), 0)
		FROM #SalesAdjustments

		--************************************************
		-- LOOP Through the sales and adjustments
		--************************************************

		WHILE @SalesOrderID <= @MaxSalesOrderID
		BEGIN
			SELECT @SalesQuantity = Quantity * -1,
				@AdjustmentAmount = Amount,
				@TotalSalesQuantity = Quantity * -1,
				@RemainingSalesQuantity = Quantity * -1,
				@SaleDate = SaleDate,
				@TaxLotReliefMethodID = TaxLotReliefMethodID,
				@SaleTransID = TransID
			FROM #SalesAdjustments
			WHERE OrderID = @SalesOrderID

				IF @AdjustmentAmount <> 0
				BEGIN
					--Find the total cost

					SELECT @TotalCost_Base = SUM(Cost_Basis_Base),
						@TotalCost_Local = SUM(Cost_Basis_Local)
					FROM #TaxLots
					WHERE AccountSecurityID = @AccountSecurityID
						AND CalcCostBasisQuantity > 0
						AND AcquireDate <= @SaleDate

					UPDATE #TaxLots
					SET Cost_Basis_Base = Cost_Basis_Base + ((Cost_Basis_Base / @TotalCost_Base) * @AdjustmentAmount)
					WHERE AccountSecurityID = @AccountSecurityID
						AND CalcCostBasisQuantity > 0
						AND @TotalCost_Base <> 0
						AND AcquireDate <= @SaleDate 

				END
				ELSE IF @SalesQuantity <> 0
				BEGIN

					--************************************************
					-- Sell Specific Tax Lots First
					--************************************************
					IF (SELECT ISNULL(SUM(Quantity), 0) FROM SpecificTaxLots WHERE SaleTransID = @SaleTransID) > 0
					BEGIN
						UPDATE #Purchases
						SET Quantity = Purchases.Quantity - SpecificTaxLots.Quantity
						--SELECT LotTransID, Purchases.Quantity - SpecificTaxLots.Quantity
						FROM (
							SELECT SpecificTaxLots.LotTransID AS LotTransID,
								SpecificTaxLots.SaleTransID AS SaleTransID,
								SUM(SpecificTaxLots.Quantity) AS Quantity
							FROM SpecificTaxLots
								JOIN #SalesAdjustments AS Sales ON Sales.TransID = SpecificTaxLots.SaleTransID
							WHERE Sales.TransID = @SaleTransID	
							GROUP BY SpecificTaxLots.LotTransID,
								SpecificTaxLots.SaleTransID
							) as SpecificTaxLots 
							JOIN #Purchases AS Purchases ON SpecificTaxLots.LotTransID = Purchases.TransID

						UPDATE #TaxLots
						SET Quantity = Purchases.Quantity - SpecificTaxLots.Quantity
						--SELECT LotTransID, Purchases.Quantity - SpecificTaxLots.Quantity
						FROM (
							SELECT SpecificTaxLots.LotTransID AS LotTransID,
								SpecificTaxLots.SaleTransID AS SaleTransID,
								SUM(SpecificTaxLots.Quantity) AS Quantity
							FROM SpecificTaxLots
								JOIN #SalesAdjustments AS Sales ON Sales.TransID = SpecificTaxLots.SaleTransID
							WHERE Sales.TransID = @SaleTransID	
							GROUP BY SpecificTaxLots.LotTransID,
								SpecificTaxLots.SaleTransID
							) as SpecificTaxLots 
							JOIN #TaxLots AS Purchases ON SpecificTaxLots.LotTransID = Purchases.PurchaseTransID 

						SELECT @RemainingSalesQuantity = @SalesQuantity - SUM(SpecificTaxLots.Quantity)
						FROM (
							SELECT SpecificTaxLots.LotTransID AS LotTransID,
								SpecificTaxLots.SaleTransID AS SaleTransID,
								SUM(SpecificTaxLots.Quantity) AS Quantity
							FROM SpecificTaxLots
								JOIN #SalesAdjustments AS Sales ON Sales.TransID = SpecificTaxLots.SaleTransID
							WHERE Sales.TransID = @SaleTransID
							GROUP BY SpecificTaxLots.LotTransID,
								SpecificTaxLots.SaleTransID
							) as SpecificTaxLots 
							JOIN #SalesAdjustments AS Sales ON Sales.TransID = SpecificTaxLots.SaleTransID
						WHERE Sales.OrderID = @SalesOrderID	

						--Populate the #TaxLots Table with Sales/Realized GL
						INSERT INTO #TaxLots (
							AccountSecurityID,
							Proceeds_Amount_Base,
							Proceeds_Amount_Local,
							Cost_Basis_Base,
							Cost_Basis_Local,
							Quantity,
							IsTaxLot,
							IsSale,
							PurchaseTransID,
							SaleTransID,
							EntityName,
							EntityID,
							AccountName,
							AccountID,
							SecurityName,
							SecurityID,
							CUSIP,
							Symbol,
							ISIN,
							SEDOL,
							AcquireDate,
							SaleDate,
							TransCodeName,
							TransCodeID,
							CurrencyID_Base,
							CurrencyID_Local,
							TaxLotReliefMethodID,
							CalcTaxLots,
							CalcCashFlow
							)
						SELECT @AccountSecurityID AS AccountSecurityID,
							CASE WHEN Transactions.CalcShort = 0
								THEN ABS(SpecificTaxLots.Quantity / ABS(Transactions.Quantity) * Transactions.Amount_Base)
								ELSE NULL
								END as SaleProceeds_Base,
							CASE WHEN Transactions.CalcShort = 0
								THEN ABS(SpecificTaxLots.Quantity / ABS(Transactions.Quantity) * Transactions.Amount_Local)
								ELSE NULL
								END as SaleProceeds_Local,
							CASE WHEN Transactions.CalcShort != 0
								THEN ABS(SpecificTaxLots.Quantity / ABS(Transactions.Quantity) * Transactions.Amount_Base)
								ELSE NULL
								END as Cost_Basis_Base,
							CASE WHEN Transactions.CalcShort != 0
								THEN ABS(SpecificTaxLots.Quantity / ABS(Transactions.Quantity) * Transactions.Amount_Local)
								ELSE NULL
								END as Cost_Basis_Local,
							SpecificTaxLots.Quantity AS Quantity,
							0 AS IsTaxLot,
							1 AS IsSale,
							Purchases.TransID AS PurchaseTransID,
							Sales.TransID AS SaleTransID,
							Transactions.EntityName,
							Transactions.EntityID,
							Transactions.AccountName,
							Transactions.AccountID,
							Transactions.SecurityName,
							Transactions.SecurityID,
							Transactions.CUSIP,
							Transactions.Symbol,
							Transactions.ISIN,
							Transactions.SEDOL,
							CASE WHEN Transactions.CalcShort = 0
								THEN Purchases.AcquiredDate
								ELSE Sales.SaleDate
								END AS AcquiredDate,
							CASE WHEN Transactions.CalcShort = 0
								THEN Sales.SaleDate
								ELSE Purchases.AcquiredDate
								END AS SaleDate,
							Transactions.TransCodeName,
							Transactions.TransCodeID,
							Transactions.CurrencyID_Base,
							Transactions.CurrencyID_Local,
							0,
							Transactions.CalcTaxLots,
							Transactions.CalcCashFlow
						FROM #SalesAdjustments AS Sales
							JOIN #Transactions AS Transactions ON Sales.TransID = Transactions.TransID
							JOIN SpecificTaxLots ON Sales.TransID = SpecificTaxLots.SaleTransID
							JOIN #Purchases AS Purchases ON  Purchases.TransID = SpecificTaxLots.LotTransID
						WHERE Sales.TransID = @SaleTransID

						--UPDATE the #TaxLots Sales Table with the Cost Basis
						UPDATE TaxLots
						SET	Cost_Basis_Base = CASE WHEN Purchases.Quantity <> 0 
									THEN (Purchases.Cost_Basis_Base / Purchases.Quantity) * TaxLots.Quantity
									ELSE 0
									END,
							Cost_Basis_Local = CASE WHEN Purchases.Quantity <> 0 
									THEN (Purchases.Cost_Basis_Local / Purchases.Quantity) * TaxLots.Quantity
									ELSE 0
									END
						FROM #TaxLots AS TaxLots
							JOIN #TaxLots AS Purchases ON TaxLots.PurchaseTransID = Purchases.PurchaseTransID
							JOIN #SalesAdjustments AS Sales ON TaxLots.SaleTransID = Sales.TransID
						WHERE ISNULL(TaxLots.IsSale, 0) = 1	
							AND Sales.TransID = @SaleTransID
							AND TaxLots.Cost_Basis_Base IS NULL	

					END

					--************************************************
					-- LOOP Through the purchases
					--************************************************
				
					SELECT @PurchasesOrderID = MIN(OrderID),
						@MaxPurchasesOrderID = MAX(OrderID)
					FROM #Purchases
					WHERE DATEDIFF(Day, AcquiredDate, @SaleDate) >= 0

					WHILE @PurchasesOrderID <= @MaxPurchasesOrderID AND @RemainingSalesQuantity <> 0
					BEGIN
		
						SELECT @PurchaseQuantity = Quantity 
						FROM #Purchases 
						WHERE OrderID = @PurchasesOrderID
					
						--********************************************************************************************
						-- There is enough of the purchase to sell the entire quantity
						--********************************************************************************************
						
						IF @PurchaseQuantity >= @RemainingSalesQuantity
						BEGIN
							UPDATE #Purchases
							SET Quantity = Quantity - @RemainingSalesQuantity
							FROM #Purchases
							WHERE OrderID = @PurchasesOrderID
								AND AcquiredDate <= @SaleDate

									--Populate the #TaxLots Table with Sales/Realized GL
									INSERT INTO #TaxLots (
										AccountSecurityID,
										Proceeds_Amount_Base,
										Proceeds_Amount_Local,
										Cost_Basis_Base,
										Cost_Basis_Local,
										Quantity,
										IsTaxLot,
										IsSale,
										PurchaseTransID,
										SaleTransID,
										EntityName,
										EntityID,
										AccountName,
										AccountID,
										SecurityName,
										SecurityID,
										CUSIP,
										Symbol,
										ISIN,
										SEDOL,
										AcquireDate,
										SaleDate,
										TransCodeName,
										TransCodeID,
										CurrencyID_Base,
										CurrencyID_Local,
										TaxLotReliefMethodID,
										CalcTaxLots,
										CalcCashFlow
										)
									SELECT @AccountSecurityID AS AccountSecurityID,
										CASE WHEN Transactions.CalcShort = 0
											THEN ABS(@RemainingSalesQuantity / ABS(Transactions.Quantity) * Transactions.Amount_Base)
											ELSE NULL
											END as SaleProceeds_Base,
										CASE WHEN Transactions.CalcShort = 0
											THEN ABS(@RemainingSalesQuantity / ABS(Transactions.Quantity) * Transactions.Amount_Local)
											ELSE NULL
											END as SaleProceeds_Local,
										CASE WHEN Transactions.CalcShort != 0
											THEN ABS(@RemainingSalesQuantity / ABS(Transactions.Quantity) * Transactions.Amount_Base)
											ELSE NULL
											END as Cost_Basis_Base,
										CASE WHEN Transactions.CalcShort != 0
											THEN ABS(@RemainingSalesQuantity / ABS(Transactions.Quantity) * Transactions.Amount_Local)
											ELSE NULL
											END as Cost_Basis_Local,	
										ABS(@RemainingSalesQuantity) AS Quantity,
										0 AS IsTaxLot,
										1 AS IsSale,
										Purchases.TransID AS PurchaseTransID,
										Sales.TransID AS SaleTransID,
										Transactions.EntityName,
										Transactions.EntityID,
										Transactions.AccountName,
										Transactions.AccountID,
										Transactions.SecurityName,
										Transactions.SecurityID,
										Transactions.CUSIP,
										Transactions.Symbol,
										Transactions.ISIN,
										Transactions.SEDOL,
										CASE WHEN Transactions.CalcShort = 0
											THEN Purchases.AcquiredDate
											ELSE Sales.SaleDate
											END AS AcquiredDate,
										CASE WHEN Transactions.CalcShort = 0
											THEN Sales.SaleDate
											ELSE Purchases.AcquiredDate
											END AS SaleDate,
										Transactions.TransCodeName,
										Transactions.TransCodeID,
										Transactions.CurrencyID_Base,
										Transactions.CurrencyID_Local,
										@TaxLotReliefMethodID,
										Transactions.CalcTaxLots,
										Transactions.CalcCashFlow
									FROM #SalesAdjustments AS Sales
										JOIN #Transactions AS Transactions ON Sales.TransID = Transactions.TransID
										CROSS APPLY (SELECT TransID, AcquiredDate FROM #Purchases WHERE OrderID = @PurchasesOrderID) AS Purchases
									WHERE Sales.OrderID = @SalesOrderID						

							SET @RemainingSalesQuantity = 0 --Nothing is left over
						END
						ELSE
						BEGIN
							--********************************************************************************************
							-- There is still some of the sale to apply to the next purchase
							--********************************************************************************************
							
							SET @SalesQuantity = @PurchaseQuantity 
					
							UPDATE #Purchases
							SET Quantity = Quantity - @SalesQuantity
							FROM #Purchases
							WHERE OrderID = @PurchasesOrderID
						

							SELECT @RemainingSalesQuantity = @RemainingSalesQuantity - @SalesQuantity

								--Populate the #TaxLots Table with Sales/Realized GL
								INSERT INTO #TaxLots (
									AccountSecurityID,
									Proceeds_Amount_Base,
									Proceeds_Amount_Local,
									Cost_Basis_Base,
									Cost_Basis_Local,
									Quantity,
									IsTaxLot,
									IsSale,
									PurchaseTransID,
									SaleTransID,
									EntityName,
									EntityID,
									AccountName,
									AccountID,
									SecurityName,
									SecurityID,
									CUSIP,
									Symbol,
									ISIN,
									SEDOL,
									AcquireDate,
									SaleDate,
									TransCodeName,
									TransCodeID,
									CurrencyID_Base,
									CurrencyID_Local,
									TaxLotReliefMethodID,
									CalcTaxLots,
									CalcCashFlow
									)
								SELECT @AccountSecurityID,
									CASE WHEN Transactions.CalcShort = 0
										THEN ABS(@PurchaseQuantity / ABS(Transactions.Quantity) * Transactions.Amount_Base) 
										ELSE NULL
										END as SaleProceeds_Base,
									CASE WHEN Transactions.CalcShort = 0
										THEN ABS(@PurchaseQuantity / ABS(Transactions.Quantity) * Transactions.Amount_Local) 
										ELSE NULL
										END as SaleProceeds_Local,
									CASE WHEN Transactions.CalcShort != 0
										THEN ABS(@PurchaseQuantity / ABS(Transactions.Quantity) * Transactions.Amount_Base) 
										ELSE NULL
										END as Cost_Basis_Base,
									CASE WHEN Transactions.CalcShort != 0
										THEN ABS(@PurchaseQuantity / ABS(Transactions.Quantity) * Transactions.Amount_Local) 
										ELSE NULL
										END as Cost_Basis_Local,								
									ABS(@PurchaseQuantity) AS Quantity,
									0 AS IsTaxLot,
									1 AS IsSale,
									Purchases.TransID AS PurchaseTransID,
									Sales.TransID AS SaleTransID,
									Transactions.EntityName,
									Transactions.EntityID,
									Transactions.AccountName,
									Transactions.AccountID,
									Transactions.SecurityName,
									Transactions.SecurityID,
									Transactions.CUSIP,
									Transactions.Symbol,
									Transactions.ISIN,
									Transactions.SEDOL,
									CASE WHEN Transactions.CalcShort = 0
										THEN Purchases.AcquiredDate
										ELSE Sales.SaleDate
										END AS AcquiredDate,
									CASE WHEN Transactions.CalcShort = 0
										THEN Sales.SaleDate
										ELSE Purchases.AcquiredDate
										END AS SaleDate,
									Transactions.TransCodeName,
									Transactions.TransCodeID,
									Transactions.CurrencyID_Base,
									Transactions.CurrencyID_Local,
									@TaxLotReliefMethodID,
									Transactions.CalcTaxLots,
									Transactions.CalcCashFlow
								FROM #SalesAdjustments AS Sales
									JOIN #Transactions AS Transactions ON Sales.TransID = Transactions.TransID
									CROSS APPLY (SELECT TransID, AcquiredDate FROM #Purchases WHERE OrderID = @PurchasesOrderID) AS Purchases
								WHERE Sales.OrderID = @SalesOrderID	
						END

						--SELECT Cost_Basis_Base = CASE WHEN Purchases.Quantity <> 0 
						--			THEN (Purchases.Cost_Basis_Base / Purchases.Quantity) * TaxLots.Quantity
						--			ELSE 0
						--			END,
						--	Cost_Basis_Local = CASE WHEN Purchases.Quantity <> 0 
						--			THEN (Purchases.Cost_Basis_Local / Purchases.Quantity) * TaxLots.Quantity
						--			ELSE 0
						--			END
						--FROM #TaxLots AS TaxLots
						--	JOIN #TaxLots AS Purchases ON TaxLots.PurchaseTransID = Purchases.PurchaseTransID
						--	--LEFT JOIN #SalesAdjustments AS Sales ON TaxLots.SaleTransID = Sales.TransID
						--WHERE ISNULL(TaxLots.IsSale, 0) = 1	
						--	--AND Sales.OrderID = @SalesOrderID
						--	AND TaxLots.Cost_Basis_Base IS NULL	
						--	AND TaxLots.SecurityId = 1898

						--UPDATE the #TaxLots Sales Table with the new Purchase Quantity for NON-Short Sales
						UPDATE TaxLots
						SET	Cost_Basis_Base = CASE WHEN Purchases.Quantity <> 0
									THEN (Purchases.Cost_Basis_Base / Purchases.Quantity) * TaxLots.Quantity 
									ELSE Purchases.Cost_Basis_Base
									END,
							Cost_Basis_Local = CASE WHEN Purchases.Quantity <> 0
									THEN (Purchases.Cost_Basis_Local / Purchases.Quantity) * TaxLots.Quantity
									ELSE Purchases.Cost_Basis_Local
									END
						FROM #TaxLots AS TaxLots
							JOIN #TaxLots AS Purchases ON TaxLots.PurchaseTransID = Purchases.PurchaseTransID AND ISNULL(Purchases.IsTaxLot, 0) = 1 
						WHERE ISNULL(TaxLots.IsSale, 0) = 1	
							AND TaxLots.Cost_Basis_Base IS NULL 
							AND ISNULL(TaxLots.CalcShort, 0) = 0
						
						----UPDATE the #TaxLots Sales Table with the new Purchase Quantity for Short Sales
						UPDATE TaxLots
						SET	Proceeds_Amount_Base = CASE WHEN Purchases.Quantity <> 0
									THEN (Purchases.Cost_Basis_Base / Purchases.Quantity) * ABS(TaxLots.Quantity )
									ELSE Purchases.Cost_Basis_Base
									END,
							Proceeds_Amount_Local = CASE WHEN Purchases.Quantity <> 0
									THEN (Purchases.Cost_Basis_Local / Purchases.Quantity) * ABS(TaxLots.Quantity)
									ELSE Purchases.Cost_Basis_Local
									END
						FROM #TaxLots AS TaxLots
							JOIN #TaxLots AS Purchases ON TaxLots.PurchaseTransID = Purchases.PurchaseTransID AND ISNULL(Purchases.IsTaxLot, 0) = 1 
						WHERE ISNULL(TaxLots.IsSale, 0) = 1	
							AND TaxLots.Proceeds_Amount_Base IS NULL 
							AND ISNULL(Purchases.CalcShort, 0) != 0

						--UPDATE the #TaxLots Table with the new Purchase Quantity
						UPDATE #TaxLots
						SET Quantity = Purchases.Quantity,
							Cost_Basis_Base = CASE WHEN TaxLots.Quantity <> 0 
									THEN (TaxLots.Cost_Basis_Base / TaxLots.Quantity) * Purchases.Quantity
									ELSE 0
									END,
							Cost_Basis_Local = CASE WHEN TaxLots.Quantity > 0 
									THEN (TaxLots.Cost_Basis_Local / TaxLots.Quantity) * Purchases.Quantity
									ELSE 0
									END
						FROM #TaxLots AS TaxLots
							JOIN #Purchases AS Purchases ON TaxLots.PurchaseTransID = Purchases.TransID
						WHERE Purchases.OrderID = @PurchasesOrderID
							AND ISNULL(IsTaxLot, 0) = 1


					SET @PurchasesOrderID = @PurchasesOrderID + 1
				END

			END

			SET @SalesOrderID = @SalesOrderID + 1
		END

		IF @AccountSecurityID = @MaxAccountSecurityID
		BEGIN
			BREAK
		END

		SELECT @AccountSecurityID = MIN(AccountSecurityID)
		FROM #Transactions
		WHERE (CalcCostBasisQuantity < 0 OR CalcCostBasisAmount <> 0)
			AND AccountSecurityID > @AccountSecurityID
	END

	--Add in MarketPrice, MarketValue, Calculate the GL

	UPDATE #TaxLots
	SET MarketPrice_AsOfDate = Price.PriceAsOfDate,
		MarketPrice_Base = Price.Price_Base,
		MarketPrice_Local = Price.Price_Base,
		PriceFactor = Price.PriceFactor,
		MarketValue_Base = (Price.Price_Base * ISNULL(Price.PriceFactor, 1)) * TaxLots.Quantity * Price.ContractSize,
		MarketValue_Local = (Price.Price_Base * ISNULL(Price.PriceFactor, 1)) * TaxLots.Quantity * Price.ContractSize,
		UnRealizedGL_Base = ((Price.Price_Base * ISNULL(Price.PriceFactor, 1)) * TaxLots.Quantity * Price.ContractSize) - TaxLots.Cost_Basis_Base,
		UnRealizedGL_Local = ((Price.Price_Base * ISNULL(Price.PriceFactor, 1)) * TaxLots.Quantity * Price.ContractSize) - TaxLots.Cost_Basis_Local,
		Amount_Base = (TaxLots.Amount_Base_Original / ISNULL(NULLIF(TaxLots.Quantity_Original, 0), 1)) * TaxLots.Quantity,
		Amount_Local = (TaxLots.Amount_Local_Original / ISNULL(NULLIF(TaxLots.Quantity_Original, 0), 1)) * TaxLots.Quantity
	FROM  #TaxLots AS TaxLots
		JOIN tfn_Holdings_by_Level (@Level, @LevelID, @EndDate, @IncludeLinkedAccounts, 1) AS Price ON TaxLots.SecurityID = Price.SecurityID
	WHERE IsTaxLot = 1 
		--AND ISNULL(QuantityOfOne, 0) = 0

	-- UPDATE HoldingDays and HoldingPeriod for Open Tax Lots
	
	UPDATE #TaxLots
	SET HoldingDays = DATEDIFF(DAY, AcquireDate, @EndDate),
		HoldingPeriod = CASE WHEN (DATEDIFF(DAY, AcquireDate, @EndDate)) > 365 THEN 'Long Term' ELSE 'Short Term' END
	WHERE IsTaxLot = 1

	-- UPDATE HoldingDays and HoldingPeriod for Sales
	
	UPDATE #TaxLots
	SET HoldingDays = DATEDIFF(DAY, AcquireDate, SaleDate),
		HoldingPeriod = CASE WHEN (DATEDIFF(DAY, AcquireDate, SaleDate)) > 365 THEN 'Long Term' ELSE 'Short Term' END
	WHERE IsSale = 1

	-- Update TaxLotReliefMethod Labels

	UPDATE #TaxLots
	SET TaxLotReliefMethodLongName = TaxLotReliefMethod.TaxLotReliefMethodLongName,
		TaxLotReliefMethodShortName = TaxLotReliefMethod.TaxLotReliefMethodShortName
	FROM #TaxLots AS TaxLots
		JOIN #TaxLotReliefMethod as TaxLotReliefMethod ON TaxLots.TaxLotReliefMethodID = TaxLotReliefMethod.TaxLotReliefMethodID

	-- SELECT Final RecordSet
	IF @ReportOutput = 'Open Tax Lots'
	BEGIN
		SELECT *
		FROM #TaxLots
		WHERE ISNULL(MarketPrice_Base, 0) <> 1
			AND (ISNULL(Quantity, 0) <> 0)
			AND ISNULL(IsTaxLot, 0) = 1
		ORDER BY SecurityName, AcquireDate
	END
	ELSE IF @ReportOutput = 'Realized'
	BEGIN
		SELECT *
		FROM #TaxLots
		WHERE ISNULL(IsSale, 0) = 1
			AND ISNULL(CalcTaxLots, 1) = 1
			AND ISNULL(CalcCashFlow, 0) = 0
			--AND ABS(Quantity) <> ABS(Proceeds_Amount_Base)
		ORDER BY SecurityName, AcquireDate
	END
	ELSE IF @ReportOutput = 'Holdings Composite'
	BEGIN
		SELECT EntityName,
			EntityId,
			AccountName,
			AccountID,
			AccountSecurityID,
			SecurityName,
			SecurityId,
			CUSIP,
			Symbol,
			ISIN,
			SEDOL,
			Quantity,
			UnitPrice_Base,
			UnitPrice_Local,
			Amount_Base,
			Amount_Local,
			Cost_Basis_Base,
			Cost_Basis_Local,
			AcquireDate,
			CurrencyID_Base,
			CurrencyID_Local,
			TransCodeName,
			TransCodeID,
			MarketPrice_AsOfDate,
			MarketPrice_Base,
			MarketPrice_Local,
			PriceFactor,
			MarketValue_Base,
			MarketValue_Local,
			UnRealizedGL_Base,
			UnRealizedGL_Local,
			QuantityOfOne,
			IsTaxLot,
			PercentOwnership,
			AssetClassID,
			AssetClassName,
			AssetClassSortOrder,
			AssetClassColor
		FROM #TaxLots
		WHERE (ISNULL(Quantity, 0) <> 0)
			AND ISNULL(IsTaxLot, 0) = 1
		UNION ALL
		SELECT EntityName,
			EntityId,
			AccountName,
			AccountID,
			AccountSecurityID,
			SecurityName,
			SecurityId,
			CUSIP,
			Symbol,
			ISIN,
			SEDOL,
			Quantity,
			Price_Base AS UnitPrice_Base,
			Price_Base AS UnitPrice_Local,
			MarketValue_Base AS Amount_Base,
			MarketValue_Base AS Amount_Local,
			MarketValue_Base AS Cost_Basis_Base,
			MarketValue_Base AS Cost_Basis_Local,
			'' AS AcquireDate,
			0 AS CurrencyID_Base,
			0 AS CurrencyID_Local,
			'' AS TransCodeName,
			0 AS TransCodeID,
			PriceAsOfDate AS MarketPrice_AsOfDate,
			Price_Base AS MarketPrice_Base,
			Price_Base AS MarketPrice_Local,
			1 AS PriceFactor,
			MarketValue_Base,
			MarketValue_Base AS MarketValue_Local,
			0 as UnRealizedGL_Base,
			0 AS UnRealizedGL_Local,
			0 AS QuantityOfOne,
			1 AS IsTaxLot,
			PercentOwnership,
			AssetClassID,
			AssetClassName,
			AssetClassSortOrder,
			AssetClassColor
		FROM tfn_Holdings_by_Level (@Level, @LevelID, @EndDate, @IncludeLinkedAccounts)
		--WHERE Quantity = MarketValue_Base
		WHERE AccountSecurityID NOT IN (SELECT DISTINCT AccountSecurityID FROM #TaxLots
					WHERE (ISNULL(Quantity, 0) <> 0)
						AND ISNULL(IsTaxLot, 0) = 1)
		ORDER BY SecurityName, AcquireDate
	END
	ELSE 
	BEGIN
		SELECT *
		FROM #TaxLots
		--WHERE ABS(Quantity) <> ABS(Proceeds_Amount_Base)
		--WHERE SecurityID = 1898
		ORDER BY SecurityName, AcquireDate
	END

	--END TRY
	--BEGIN CATCH
	--	DECLARE @ErrMsg nvarchar(4000) = ERROR_MESSAGE()
	--	DECLARE @ErrNum INT = ERROR_NUMBER()
	--	DECLARE @ErrProc nvarchar(126) = ERROR_PROCEDURE()
	--	DECLARE @DataError nvarchar(4000) = '[DisplayTransactionsByLevel]' +
	--		CONVERT(nvarchar(10), @errNum) + ', Error Details: ' + @ErrMsg
	--	RAISERROR (@DataError, 16,1)
	--END CATCH
END


GO

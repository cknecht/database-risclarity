SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROC [dbo].[DisplayTrailingMonthsTWR]
	@Level nvarchar(10) = 'E',
	@LevelID bigint,
	@EndDate smalldatetime,
	@NumMonths int = 12,
	@NetOfFees bit = 0,
	@IncludeLinkedAccounts BIT = 1,
	@Debugging BIT = 0

AS

BEGIN
	
	SET NOCOUNT ON

	--BEGIN TRY

		CREATE TABLE #TrailingNMonth (
			BeginDate smalldatetime,
			EndDate smalldatetime,
			TWR numeric(18,6),
			BeginMarketValue numeric(18,6),
			EndMarketValue numeric(18,6),
			CashFlow numeric(18,6),
			Additions numeric(18,6),
			Withdrawals numeric(18,6),
			Income numeric(18,6),
			Fees numeric(18,6)
			)

		-- CREATE the output temp table
		CREATE TABLE #TWR (
			[TWR_ID] [bigint]  NULL,
			[Level] nvarchar(1)  NULL,
			[LevelID] [bigint]  NULL,
			[AccountID] [bigint] NULL,
			[AccountName] varchar(100),
			[AccountNumber] varchar(100),
			[AccountInceptionDate] smalldatetime,
			[PeriodBeginDate] [smalldatetime] NULL,
			[PeriodEndDate] [smalldatetime] NULL,
			[TWRBeginDate] [smalldatetime] NULL,
			[TWREndDate] [smalldatetime] NULL,
			[BeginMarketValue] [numeric](18, 6) NULL,
			[EndMarketValue] [numeric](18, 6) NULL,
			[CashFlow] [numeric](18, 6) NULL,
			[Additions] [numeric](18, 6) NULL,
			[Withdrawals] [numeric](18, 6) NULL,
			[Income] [numeric](18, 6) NULL,
			[AccruedIncome] [numeric](18, 6) NULL,
			[Fees] [numeric](18, 6) NULL,
			[PeriodTWR] [numeric](18, 6) NULL,
			[PeriodTWR_Net_Of_Fees] [numeric](18, 6) NULL,
			[PeriodTWR_Plus_One] [numeric](18, 6) NULL,
			[PeriodTWR_Net_Of_Fees_Plus_One] [numeric](18, 6) NULL,
			[TWR] [numeric](18, 6) NULL,
			[AnnualizedTWR] [numeric](18, 6) NULL,
			[TWR_Net_Of_Fees] [numeric](18, 6) NULL,
			[AnnualizedTWR_Net_Of_Fees] [numeric](18, 6) NULL,
			[ExcludeFromPerformance] bit
		)

		CREATE TABLE #Activity (
			[Activity_ID] [bigint] IDENTITY(1,1) NOT NULL,
			[Level] nvarchar(1) NOT NULL,
			[LevelID] [bigint] NOT NULL,
			[AccountID] [bigint] NULL,
			[AccountName] varchar(100),
			[AccountNumber] varchar(100),
			[AccountInceptionDate] smalldatetime,
			[PeriodBeginDate] [smalldatetime] NULL,
			[PeriodEndDate] [smalldatetime] NULL,
			[BeginMarketValue] [numeric](18, 6) NULL,
			[EndMarketValue] [numeric](18, 6) NULL,
			[CashFlow] [numeric](18, 6) NULL,
			[Additions] [numeric](18, 6) NULL,
			[Withdrawals] [numeric](18, 6) NULL,
			[Income] [numeric](18, 6) NULL,
			[AccruedIncome] [numeric](18, 6) NULL,
			[Fees] [numeric](18, 6) NULL,
			[ExcludeFromPerformance] BIT
		)

		DECLARE @PeriodList periodlist
		DECLARE @SecurityList securitylist
		DECLARE @BeginDate smalldatetime

		INSERT INTO @SecurityList (
			AccountSecurityID,
			AccountID,
			AccountName,
			SecurityID,
			SecurityName,
			PriceFactor,
			ExcludeFromPerformance,
			PercentOwnership,
			EntityID,
			ContractSize
			)
		SELECT AccountSecurityID,
			AccountID,
			AccountName,
			SecurityID,
			SecurityName,
			PriceFactor,
			ExcludeFromPerformance,
			ISNULL(PercentOwnership, 100),
			EntityID,
			ContractSize
		FROM [dbo].[tfn_SecurityList_by_Level] (@Level, @LevelID, @EndDate, @IncludeLinkedAccounts)

		SET @BeginDate = [dbo].[sfn_MonthBegin](DATEADD(Month, ((@NumMonths - 1) * -1), @EndDate))


		INSERT INTO @PeriodList(
			BeginDate,
			EndDate
			)
		SELECT BeginDate,
			EndDate
		FROM [dbo].[tfn_PeriodList_Months] (
			@BeginDate,
			@EndDate
			)
		
		EXEC [CalculateDataForTWR]
				@SecurityList,
				@PeriodList,
				@RecordSet = 0,
				@Level = @Level

		IF @Debugging = 1 
		BEGIN
			SELECT * FROM #TWR
		END

		INSERT INTO #TrailingNMonth (
			BeginDate,
			EndDate
			)
		SELECT BeginDate as BeginDate,
			EndDate as EndDate
		FROM @PeriodList
		
		UPDATE #TrailingNMonth 
		SET TWR = TWR.TWR
		FROM #TWR as TWR
			JOIN #TrailingNMonth as TrailingNMonth ON TWR.PeriodBeginDate = TrailingNMonth.BeginDate
					AND TWR.PeriodEndDate = TWR.PeriodEndDate 

		-- Find the activity, seperate from the performance activity
		-- since there may be accounts excluded from performance
		-- and we want to see all activity.

		EXEC [dbo].[CalculateDataForActivity]
				@SecurityList,
				@PeriodList,
				@RecordSet = 0,
				@Level = @Level

		UPDATE #TrailingNMonth 
		SET BeginMarketValue = Activity.BeginMarketValue,
			EndMarketValue = Activity.EndMarketValue,
			CashFlow = Activity.CashFlow,
			Additions = Activity.Additions,
			Withdrawals = Activity.Withdrawals,
			Income = Activity.Income,
			Fees = Activity.Fees
		FROM #Activity as Activity
			JOIN #TrailingNMonth as TrailingNMonth ON Activity.PeriodBeginDate = TrailingNMonth.BeginDate
					AND Activity.PeriodEndDate = Activity.PeriodEndDate 

		UPDATE #TrailingNMonth
		SET BeginMarketValue = 0
		WHERE BeginMarketValue IS NULL 
			AND EndMarketValue IS NOT NULL

	DROP TABLE #TWR
	DROP TABLE #Activity

	INSERT INTO #TrailingNMonth (
		BeginDate,
		EndDate,
		TWR,
		BeginMarketValue,
		EndMarketValue,
		CashFlow,
		Additions,
		Withdrawals,
		Income,
		Fees
		)
	SELECT PeriodList.BeginDate,
		PeriodList.EndDate,
		NULL AS TWR,
		NULL AS BeginMarketValue,
		NULL AS EndMArketValue,
		NULL AS CashFlow,
		NULL AS Additions,
		NULL AS Withdrawals,
		NULL AS income,
		NULL AS Fees
	FROM @PeriodList AS PeriodList
		LEFT JOIN  #TrailingNMonth ON #TrailingNMonth.BeginDate = PeriodList.BeginDate
			AND #TrailingNMonth.EndDate = PeriodList.EndDate
	WHERE #TrailingNMonth.BeginDate IS NULL

	SELECT * 
	FROM #TrailingNMonth
	ORDER BY BeginDate
		

	DROP TABLE #TrailingNMonth

	--END TRY
	--BEGIN CATCH
	--	DECLARE @ErrMsg nvarchar(4000) = ERROR_MESSAGE()
	--	DECLARE @ErrNum INT = ERROR_NUMBER()
	--	DECLARE @ErrProc nvarchar(126) = ERROR_PROCEDURE()
	--	DECLARE @DataError nvarchar(4000) = 'Error running PROC [DisplayTrailingMonthsTWR]' +
	--		CONVERT(nvarchar(10), @errNum) + ', Error Details: ' + @ErrMsg
	--	RAISERROR (@DataError, 16,1)
	--	--ROLLBACK TRANSACTION
	--END CATCH

END

GO

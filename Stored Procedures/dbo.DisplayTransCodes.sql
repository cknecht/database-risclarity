SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROC [dbo].[DisplayTransCodes]
	@AccountSecurity BIGINT = 0

AS

SELECT *
FROM dbo.TransCode
ORDER BY TransCodeName

GO

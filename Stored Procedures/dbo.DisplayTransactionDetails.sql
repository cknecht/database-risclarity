SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROC [dbo].[DisplayTransactionDetails]
	@UserName VARCHAR(100) = '',
	@TransactionID BIGINT = 0

AS

IF @TransactionID <> 0
BEGIN
	SELECT Trans.*
	FROM vTrans AS Trans
	WHERE TransID = @TransactionID
END
GO

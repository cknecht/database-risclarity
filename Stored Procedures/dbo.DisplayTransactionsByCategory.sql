SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROC [dbo].[DisplayTransactionsByCategory]
	@Level varchar(1),
	@LevelID bigint,
	@Category varchar(20),
	@CategoryID int = -1,
	@BeginDate smalldatetime,
	@EndDate SMALLDATETIME,
	@IncludeLinkedAccounts BIT = 0

AS

BEGIN

	SET NOCOUNT ON
	BEGIN TRY

	--Create the variables 
	DECLARE @CategorySecurityList Securitylist

	-- CREATETemporary @AllSecurityList 
	CREATE TABLE #AllSecurityList (
		FamilyID	bigint,
		FamilyName	varchar(max),
		EntityID	bigint,
		EntityName	varchar(max),
		AccountID	bigint,
		AccountName varchar(max),
		AccountSecurityID bigint,
		SecurityID	bigint,
		SecurityName	varchar(max),
		SectorName varchar(155),
		SectorID	bigint,
		SectorSortOrder int,
		SectorColor varchar(50),
		RegionName varchar(255),
		RegionID bigint,
		RegionSortOrder int,
		RegionColor varchar(50),
		AssetClassName varchar(255),
		AssetClassID bigint,
		AssetClassSortOrder int,
		AssetClassColor varchar(50)
		)

	-- CREATE Temp Table #Category
	CREATE TABLE #Category (
		ID int identity(1,1),
		CategoryGroup varchar(20),
		CategoryID int,
		CategoryName varchar(100),
		CategoryBeginMarketValue numeric(18,6),
		CategoryEndMarketValue numeric(18,6),
		CategoryCashFlows numeric(18,6),
		CategoryTWR numeric(18,6),
		CategoryColor varchar(100),
		CategoryImage varchar(100),
		CategorySortOrder int,
		)	

	CREATE Table #Transactions (
		CategoryName varchar(100),
		CategoryID	int,
		CategorySortOrder int,
		CategoryColor varchar(100),
		Quantity numeric(18,6),
		UnitPrice_Base numeric(18,6),
		UnitPrice_Local numeric(18,6),
		Amount_Base numeric(18,6),
		Amount_Local numeric(18,6),
		TradeDate smalldatetime,
		SettleDate smalldatetime,
		CurrencyID_Base int,
		CurrencyID_Local int,
		TransCodeName varchar(100),
		EntityID bigint,
		AccountID bigint,
		AccountSecurityID bigint,
		SecurityID bigint,
		SecurityName varchar(100)
		)

	--Populate the #AllSecurityList with all the securities in the Level by LevelID
	INSERT INTO #AllSecurityList (
		AccountSecurityID,
		SecurityID,
		SecurityName,
		SectorName,
		SectorID,
		SectorSortOrder,
		SectorColor,
		RegionName,
		RegionID,
		RegionSortOrder,
		RegionColor,
		AssetClassName,
		AssetClassID,
		AssetClassSortOrder,
		AssetClassColor
		)
	SELECT 	AccountSecurityID,
		SecurityID,
		SecurityName,
		SectorName,
		SectorID,
		SectorSortOrder,
		SectorColor,
		RegionName,
		RegionID,
		RegionSortOrder,
		RegionColor,
		AssetClassName,
		AssetClassID,
		AssetClassSortOrder,
		AssetClassColor
	FROM tfn_SecurityList_by_Level (@Level, @LevelID, @EndDate, @IncludeLinkedAccounts)

	--Populate Category Table
	IF UPPER(@Category) = 'AC' or UPPER(@Category) LIKE 'ASSET%'
	BEGIN
		INSERT INTO #Category (
			CategoryGroup,
			CategoryID,
			CategoryName,
			CategoryColor,
			CategorySortOrder
			)
		SELECT 'AssetClass',
			AssetClass.AssetClassID, 
			AssetClass.AssetClassName,
			AssetClass.AssetClassColor,
			AssetClass.AssetClassSortOrder
		FROM #AllSecurityList as AssetClass
		GROUP BY AssetClass.AssetClassID,
			AssetClass.AssetClassName,
			AssetClass.AssetClassColor,
			AssetClass.AssetClassSortOrder
	END
	ELSE IF UPPER(@Category) = 'SR' OR UPPER(@Category) = 'SECTOR'
	BEGIN
		INSERT INTO #Category (
			CategoryGroup,
			CategoryID,
			CategoryName,
			CategoryColor,
			CategorySortOrder
			)
		SELECT 'Sector',
			Sector.SectorID, 
			Sector.SectorName,
			Sector.SectorColor,
			Sector.SectorSortOrder
		FROM #AllSecurityList as Sector
		GROUP BY Sector.SectorID, 
			Sector.SectorName,
			Sector.SectorColor,
			Sector.SectorSortOrder
	END
	ELSE IF UPPER(@Category) = 'RN' OR UPPER(@Category) = 'REGION'
	BEGIN
		INSERT INTO #Category (
			CategoryGroup,
			CategoryID,
			CategoryName,
			CategoryColor,
			CategorySortOrder
			)
		SELECT 'Region',
			Region.RegionID, 
			Region.RegionName,
			Region.RegionColor,
			Region.RegionSortOrder
		FROM #AllSecurityList as Region
		GROUP BY Region.RegionID, 
			Region.RegionName,
			Region.RegionColor,
			Region.RegionSortOrder
	END

	--DECLARE the Looping Variables
	DECLARE @Index int
	DECLARE @MinIndex int
	DECLARE @CategoryName varchar(100)
	DECLARE @LoopCategoryID int
	DECLARE @CategoryGroup varchar(20)
	DECLARE @CategorySortOrder int
	DECLARE @CategoryColor varchar(100)

	--Remove all categories other than the one passed in
	IF @CategoryID > -1
	BEGIN
		DELETE FROM #Category
		WHERE CategoryID <> @CategoryID
		
		SELECT @Index = MAX(ID)
		FROM #Category
		WHERE CategoryID = @CategoryID

		SELECT @MinIndex = @Index - 1
	END
	ELSE
	BEGIN
		--SET The Max Category Index
		SELECT @Index = MAX(ID)
		FROM #Category

		SELECT @MinIndex = 0
	END

	-- Loop Through the Categories
	WHILE @Index > @MinIndex
	BEGIN
		SELECT @CategoryName = CategoryName,
			@LoopCategoryID = CategoryID,
			@CategoryGroup = CategoryGroup,
			@CategorySortOrder = CategorySortOrder,
			@CategoryColor = CategoryColor
		FROM #Category
		WHERE ID = @Index

		-- Clean out the Temp Table
		DELETE  FROM @CategorySecurityList

		--Populate the Temp Table
		INSERT @CategorySecurityList (
			 AccountSecurityID,
			 SecurityID,
			 AccountID
			)
		SELECT SecurityList.AccountSecurityID,
			SecurityList.SecurityID,
			SecurityList.AccountID
		FROM #AllSecurityList as SecurityList
		WHERE @LoopCategoryID = (	CASE @CategoryGroup
								WHEN 'AssetClass' THEN AssetClassID
								WHEN 'Sector' THEN SectorID
								WHEN 'Region' THEN RegionID
								ELSE -1
								END)
		GROUP BY SecurityList.AccountSecurityID,
			SecurityList.SecurityID,
			SecurityList.AccountID

		INSERT INTO #Transactions (
			CategoryName,
			CategoryID,
			CategorySortOrder,
			CategoryColor,
			Quantity,
			UnitPrice_Base,
			UnitPrice_Local,
			Amount_Base,
			Amount_Local,
			TradeDate,
			SettleDate,
			CurrencyID_Base,
			CurrencyID_Local,
			TransCodeName,
			EntityID,
			AccountID,
			AccountSecurityID,
			SecurityID,
			SecurityName
			)
		SELECT @CategoryName,
			@LoopCategoryID,
			@CategorySortOrder,
			@CategoryColor,
			Quantity,
			UnitPrice_Base,
			UnitPrice_Local,
			Amount_Base,
			Amount_Local,
			TradeDate,
			SettleDate,
			CurrencyID_Base,
			CurrencyID_Local,
			TransCodeName,
			EntityID,
			AccountID,
			AccountSecurityID,
			SecurityID,
			SecurityName
		FROM [dbo].[tfn_Transactions_By_Level] (
			@CategorySecurityList,
			@BeginDate,
			@EndDate
			)

		SELECT @Index = @Index - 1
	END

	--DELETE Entries that have a zero balance for EndMarketValue
	--DELETE FROM #Category
	--WHERE CategoryEndMarketValue <= 0

	SELECT * FROM #Transactions
	ORDER BY TradeDate
	
	END TRY
	BEGIN CATCH
		DECLARE @ErrMsg nvarchar(4000) = ERROR_MESSAGE()
		DECLARE @ErrNum INT = ERROR_NUMBER()
		DECLARE @ErrProc nvarchar(126) = ERROR_PROCEDURE()
		DECLARE @DataError nvarchar(4000) = '[DisplayPeriodPerformanceByCategory]' +
			CONVERT(nvarchar(10), @errNum) + ', Error Details: ' + @ErrMsg
		RAISERROR (@DataError, 16,1)
	END CATCH
END



GO

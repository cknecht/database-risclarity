SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROC [dbo].[DisplayTransactionsByLevel]
	@Level varchar(1), -- A = Account, E = Entity, H = Holding
	@LevelID bigint,
	@BeginDate smalldatetime,
	@EndDate SMALLDATETIME,
	@IncludeLinkedAccounts BIT = 0

AS

BEGIN

	SET NOCOUNT ON
	BEGIN TRY

	--Create the variables 
	DECLARE @SecurityList Securitylist

	--Populate the @SecurityList with all the securities in the Level by LevelID
	INSERT INTO @SecurityList (
		AccountSecurityID,
		SecurityID,
		SecurityName,
		AccountID,
		AccountName,
		EntityID,
		EntityName
		)
	SELECT 	AccountSecurityID,
		SecurityID,
		SecurityName,
		AccountID,
		AccountName,
		EntityID,
		EntityName
	FROM tfn_SecurityList_by_Level (@Level, @LevelID, @EndDate, @IncludeLinkedAccounts)

	
	SELECT TransID,
		Quantity,
		UnitPrice_Base,
		UnitPrice_Local,
		Amount_Base,
		Amount_Local,
		TaxCost_Base,
		TaxCost_Local,
		TradeDate,
		SettleDate,
		CurrencyID_Base,
		CurrencyID_Local,
		TransCodeName,
		TransCodeID,
		EntityName,
		EntityID,
		AccountName,
		AccountID,
		AccountSecurityID,
		SecurityID,
		SecurityName,
		CUSIP,
		Symbol,
		ISIN,
		SEDOL,
		CalcCashFlow,
		CalcDividend,
		CalcInterest,
		CalcFee,
		CalcDisplay,
		CalcIncome,
		Notes
	FROM [dbo].[tfn_Transactions_By_Level] (
		@SecurityList,
		@BeginDate,
		@EndDate
		)
	ORDER BY TradeDate


	END TRY
	BEGIN CATCH
		DECLARE @ErrMsg nvarchar(4000) = ERROR_MESSAGE()
		DECLARE @ErrNum INT = ERROR_NUMBER()
		DECLARE @ErrProc nvarchar(126) = ERROR_PROCEDURE()
		DECLARE @DataError nvarchar(4000) = '[DisplayTransactionsByLevel]' +
			CONVERT(nvarchar(10), @errNum) + ', Error Details: ' + @ErrMsg
		RAISERROR (@DataError, 16,1)
	END CATCH
END


GO

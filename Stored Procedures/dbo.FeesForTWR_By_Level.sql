SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROC [dbo].[FeesForTWR_By_Level]
	@SecurityList SecurityList Readonly,
	@BeginDate smalldatetime,
	@EndDate smalldatetime,
	@DetailRecordSet bit = 0,
	@Fees numeric(18,6) = 0 OUTPUT

AS 

CREATE TABLE #Fees (
	Fees numeric(18,6),
	TradeDate smalldatetime,
	TransCodeName varchar(100),
	EntityID bigint,
	AccountID bigint,
	AccountSecurityID bigint,
	SecurityID bigint
	)

INSERT INTO #Fees (
	Fees,
	TradeDate,
	TransCodeName,
	EntityID,
	AccountID,
	AccountSecurityID,
	SecurityID
	)
SELECT (Trans.Amount_Base * TransCode.CalcFee) as Fees, 
	TradeDate, 
	TransCodeName, 
	SecurityList.EntityID, 
	SecurityList.AccountID, 
	SecurityList.AccountSecurityID, 
	SecurityList.SecurityID
FROM Trans
	JOIN TransCode ON Trans.TransCodeID = TransCode.TransCodeID
	JOIN @SecurityList as SecurityList ON Trans.AccountSecurityID = SecurityList.AccountSecurityID
WHERE TransCode.CalcFee <> 0
	AND Trans.TradeDate >= @BeginDate
	AND Trans.TradeDate <= @EndDate
ORDER BY TradeDate

IF @DetailRecordSet = 1
BEGIN
	SELECT * FROM #Fees
END

SELECT @Fees = SUM(Fees)
FROM #Fees


GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROC [dbo].[Income_By_Level]
	@SecurityList SecurityList Readonly,
	@BeginDate smalldatetime,
	@EndDate smalldatetime,
	@DetailRecordSet bit = 0,
	@Income numeric(18,6) = 0 OUTPUT

AS 

CREATE TABLE #Income (
	Income numeric(18,6),
	TradeDate smalldatetime,
	TransCodeName varchar(100),
	EntityID bigint,
	AccountID bigint,
	AccountSecurityID bigint,
	SecurityID bigint
	)

INSERT INTO #Income (
	Income,
	TradeDate,
	TransCodeName,
	EntityID,
	AccountID,
	AccountSecurityID,
	SecurityID
	)
SELECT Income.Income, 
	Income.TradeDate, 
	Income.TransCodeName, 
	Income.EntityID, 
	Income.AccountID, 
	Income.AccountSecurityID, 
	Income.SecurityID
FROM [tfn_Income_By_Level] (
	@SecurityList,
	@BeginDate,
	@EndDate
	) as Income
WHERE Income.TradeDate >= @BeginDate
	AND Income.TradeDate <= @EndDate
ORDER BY TradeDate

IF @DetailRecordSet = 1
BEGIN
	SELECT * FROM #Income
END

SELECT @Income = SUM(Income)
FROM #Income


GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROC [dbo].[InsertReportIntoLog]
	@ReportURL varchar(4000),
	@Username VARCHAR(1000) = '',
	@DateTime DATETIME = '',
	@RenderFormatName VARCHAR(100) = '',
	@ReportName VARCHAR(100) = ''
AS

INSERT INTO Report_Run_Log (
	Username,
	ReportURL,
	[DateTime],
	RenderFormatName,
	ReportName
	)
SELECT @Username,
	@ReportURL,
	@DateTime,
	@RenderFormatName,
	@ReportName

SELECT @ReportURL AS ReportURL

GO

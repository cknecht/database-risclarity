SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROC [dbo].[ListAccountsByLevel] (
	@Level CHAR(1), 
	@LevelID BIGINT
	)

AS

SELECT Account.AccountID,
	Account.AccountName,
	AccountList.EntityID,
	AccountList.FamilyID,
	Account.AccountNumber,
	Account.AccountDescription,
	AccountList.PercentOwnership,
	Account.ExcludeFromPerformance,
	Account.InceptionDate
FROM [dbo].[tfn_AccountList_by_Level] (@Level, @LevelID, CURRENT_TIMESTAMP, 0) AS AccountList
	JOIN Account ON AccountList.AccountID = Account.AccountID
GO

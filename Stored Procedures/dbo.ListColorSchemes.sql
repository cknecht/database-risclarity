SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROC [dbo].[ListColorSchemes]

AS

SELECT ColorScheme.SchemeID,
	ColorScheme.SchemeName,
	ColorScheme.DefaultScheme
FROM ColorScheme
	JOIN ColorReportElementJoin ON ColorScheme.SchemeID = ColorReportElementJoin.SchemeID
GROUP BY ColorScheme.SchemeID,
	ColorScheme.SchemeName,
	ColorScheme.DefaultScheme
ORDER BY ColorScheme.DefaultScheme desc,
	ColorScheme.SchemeName

GO

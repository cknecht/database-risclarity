SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROC [dbo].[ListCurrencies] 

AS

SELECT CurrencyName,
	CurrencyCode,
	CurrencyID
FROM dbo.Currency
WHERE CurrencyCode = 'USD'
UNION
SELECT CurrencyName,
	CurrencyCode,
	CurrencyID
FROM dbo.Currency
WHERE CurrencyCode <>'USD'

GO

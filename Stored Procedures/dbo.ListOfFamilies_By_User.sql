SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROC [dbo].[ListOfFamilies_By_User]
	@UserName VARCHAR(100)

AS

IF ISNULL(@Username, '') <> ''
BEGIN
	SELECT Family.FamilyID,
		Family.FamilyName,
		UsersFamily.SelectDefault as SelectDefault
	FROM UsersFamily
		JOIN Family ON UsersFamily.FamilyID = Family.FamilyID
		JOIN dbo.Users ON UsersFamily.UserID = Users.UserID
	WHERE Users.Username = @UserName

END


GO

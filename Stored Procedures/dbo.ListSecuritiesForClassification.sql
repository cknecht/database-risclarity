SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROC [dbo].[ListSecuritiesForClassification]
	@FamilyID BIGINT = 2,
	@AsOfDate SMALLDATETIME = '1/1/1900',
	@UnknownsOnly BIT = 0

AS

IF @AsOfDate = '1/1/1900'
BEGIN
	SET @AsOfDate = CURRENT_TIMESTAMP
END

SELECT @FamilyID AS FamilyID,
	'' AS EntityName,--Entity.EntityName,
	'' AS AccountName,--Account.AccountName,
	Security.SecurityID,
	Security.SecurityName,
	SecurityType.SecurityTypeName,
	Region.RegionName,
	Sector.SectorName,
	AssetClass.AssetClassName,
	Liquidity.LiquidityDays,
	'' as OPTIONAL_Fields_Follow,
	Liquidity.LiquidityHardLockupDate,
	Liquidity.LiquiditySoftLockupDate,
	Liquidity.LiquiditySoftLockupPercent,
	Liquidity.LiquidityNoticePeriodDays,
	Liquidity.LiquidityGatePercent
FROM Family
	JOIN FamilyEntity ON Family.FamilyID = FamilyEntity.FamilyID
	JOIN Entity ON FamilyEntity.EntityID = Entity.EntityID
	JOIN EntityAccount ON Entity.EntityID = EntityAccount.EntityID
	JOIN Account ON EntityAccount.AccountID = Account.AccountID
	JOIN AccountSecurity ON Account.AccountID = AccountSecurity.AccountID
	JOIN Security ON AccountSecurity.SecurityID = Security.SecurityID
	LEFT JOIN SecurityType ON Security.SecurityTypeID = SecurityType.SecurityTypeID
	LEFT JOIN [dbo].[tfn_Liquidity_By_Security] (@AsOfDate) as Liquidity ON Security.SecurityID = Liquidity.SecurityID
	LEFT JOIN [dbo].[tfn_Region_By_Security] (@AsOfDate) as Region ON Security.SecurityID = Region.SecurityID
	LEFT JOIN [dbo].[tfn_Sector_By_Security] (@AsOfDate) as Sector ON Security.SecurityID = Sector.SecurityID
	LEFT JOIN [dbo].[tfn_AssetClass_By_Security] (@AsOfDate) as AssetClass ON Security.SecurityID = AssetClass.SecurityID
WHERE Security.SecurityID <> 143
	AND Family.FamilyID = @FamilyID
	AND ((1 = @UnknownsOnly AND AssetClass.AssetClassName = 'Unknown') OR (0 = @UnknownsOnly))
GROUP BY 
	Security.SecurityID,
	Security.SecurityName,
	SecurityType.SecurityTypeName,
	Region.RegionName,
	Sector.SectorName,
	AssetClass.AssetClassName,
	Liquidity.LiquidityDays,
	Liquidity.LiquidityHardLockupDate,
	Liquidity.LiquiditySoftLockupDate,
	Liquidity.LiquiditySoftLockupPercent,
	Liquidity.LiquidityNoticePeriodDays,
	Liquidity.LiquidityGatePercent
ORDER BY Security.SecurityName

GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROC [dbo].[MaskClientData]

AS

-- Pregulman

UPDATE dbo.Family
SET FamilyName = REPLACE(FamilyName, 'Pregulman', 'Williams')


UPDATE dbo.Entity
SET entityName = REPLACE(entityName, 'Pregulman', 'Williams')

UPDATE dbo.Account
SET AccountName = REPLACE(AccountName, 'Pregulman', 'Williams')

UPDATE dbo.Security
SET SecurityName = REPLACE(SecurityName, 'Pregulman', 'Williams')

-- Nathanson
UPDATE dbo.Entity
SET entityName = REPLACE(entityName, 'Nathanson', 'Davis')

UPDATE dbo.Account
SET AccountName = REPLACE(AccountName, 'Nathanson', 'Davis')

UPDATE dbo.Security
SET SecurityName = REPLACE(SecurityName, 'Nathanson', 'Davis')

-- Matilda
UPDATE dbo.Entity
SET entityName = REPLACE(entityName, 'Matilda', 'Marion')

UPDATE dbo.Account
SET AccountName = REPLACE(AccountName, 'Matilda', 'Marion')

UPDATE dbo.Security
SET SecurityName = REPLACE(SecurityName, 'Matilda', 'Marion')

-- Goldie
UPDATE dbo.Entity
SET entityName = REPLACE(entityName, 'Goldie', 'Jackie')

UPDATE dbo.Account
SET AccountName = REPLACE(AccountName, 'Goldie', 'Jackie')

UPDATE dbo.Security
SET SecurityName = REPLACE(SecurityName, 'Goldie', 'Jackie')

-- Alyssa
UPDATE dbo.Entity
SET entityName = REPLACE(entityName, 'Alyssa', 'Casey')

UPDATE dbo.Account
SET AccountName = REPLACE(AccountName, 'Alyssa', 'Casey')

UPDATE dbo.Security
SET SecurityName = REPLACE(SecurityName, 'Alyssa', 'Casey')

-- Jonah
UPDATE dbo.Entity
SET entityName = REPLACE(entityName, 'Jonah', 'Daniel')

UPDATE dbo.Account
SET AccountName = REPLACE(AccountName, 'Jonah', 'Daniel')

UPDATE dbo.Security
SET SecurityName = REPLACE(SecurityName, 'Jonah', 'Daniel')

-- Robert
UPDATE dbo.Entity
SET entityName = REPLACE(entityName, 'Robert', 'Richard')

UPDATE dbo.Account
SET AccountName = REPLACE(AccountName, 'Robert', 'Richard')

UPDATE dbo.Security
SET SecurityName = REPLACE(SecurityName, 'Robert', 'Richard')

-- Merv
UPDATE dbo.Entity
SET entityName = REPLACE(entityName, 'Merv', 'Max')

UPDATE dbo.Account
SET AccountName = REPLACE(AccountName, 'Merv', 'Max')

UPDATE dbo.Security
SET SecurityName = REPLACE(SecurityName, 'Merv', 'Max')

-- RobMer
UPDATE dbo.Entity
SET entityName = REPLACE(entityName, 'Robmer', 'Tiger')

UPDATE dbo.Account
SET AccountName = REPLACE(AccountName, 'Robmer', 'Tiger')

UPDATE dbo.Security
SET SecurityName = REPLACE(SecurityName, 'Robmer', 'Tiger')

-- Mindy
UPDATE dbo.Entity
SET entityName = REPLACE(entityName, 'Mindy', 'MacKenzie')

UPDATE dbo.Account
SET AccountName = REPLACE(AccountName, 'Mindy', 'MacKenzie')

UPDATE dbo.Security
SET SecurityName = REPLACE(SecurityName, 'Mindy', 'MacKenzie')

-- Downing
UPDATE dbo.Entity
SET entityName = REPLACE(entityName, 'Downing', 'Main Street')

UPDATE dbo.Account
SET AccountName = REPLACE(AccountName, 'Downing', 'Main Street')

UPDATE dbo.Security
SET SecurityName = REPLACE(SecurityName, 'Downing', 'Main Street')

-- Preg
UPDATE dbo.Entity
SET entityName = REPLACE(entityName, 'Preg', 'Primary')

UPDATE dbo.Account
SET AccountName = REPLACE(AccountName, 'Preg', 'Primary')

UPDATE dbo.Security
SET SecurityName = REPLACE(SecurityName, 'Preg', 'Primary')

-- Citywide
UPDATE dbo.Entity
SET entityName = REPLACE(entityName, 'Citywide', 'Summit Bank')

UPDATE dbo.Account
SET AccountName = REPLACE(AccountName, 'Citywide', 'Summit Bank')

UPDATE dbo.Security
SET SecurityName = REPLACE(SecurityName, 'Citywide', 'Summit Bank')

GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROC [dbo].[MergeAccounts]
@FromAccountID bigint = 0,
@ToAccountID bigint = 0


AS

CREATE TABLE #AccountSecurityIDs (
FromAcountID bigint,
AccountSecurityID bigint,
)
BEGIN TRANSACTION
--Merge Securities

	--Update the AccountID Where the Security is only associated with the From Account
	UPDATE AccountSecurity
	SET AccountID = @ToAccountID
	FROM (
			SELECT FromAccountSecurity.SecurityID as SecurityID,
				FromAccountSecurity.AccountSecurityID as AccountSecurityID
			FROM (
				SELECT AccountSecurity.AccountID,
					AccountSecurity.SecurityID,
					AccountSecurity.AccountSecurityID
				FROM Security
					JOIN AccountSecurity ON Security.SecurityID = AccountSecurity.SecurityID
				WHERE AccountSecurity.AccountID = @FromAccountID
				) as FromAccountSecurity
				WHERE FromAccountSecurity.SecurityID NOT IN (SELECT AccountSecurity.SecurityID			
															FROM AccountSecurity
															WHERE AccountID = @ToAccountID)
	) as ChangeAccountID
	JOIN AccountSecurity ON ChangeAccountID.AccountSecurityID = AccountSecurity.AccountSecurityID

	-- Update the AccountID Where the Security is only associated with the From Account
	UPDATE Trans
	SET AccountSecurityID = ToAccountSecurityID
	FROM (
			SELECT FromAccountSecurity.SecurityID as SecurityID,
				FromAccountSecurity.AccountSecurityID as FromAccountSecurityID,
				ToAccountSecurity.AccountSecurityID as ToAccountSecurityID
			FROM (
				SELECT AccountSecurity.AccountID,
					AccountSecurity.AccountSecurityID, 
					AccountSecurity.SecurityID
				FROM Security
					JOIN AccountSecurity ON Security.SecurityID = AccountSecurity.SecurityID
				WHERE AccountSecurity.AccountID = @FromAccountID
				) as FromAccountSecurity
			 JOIN AccountSecurity as ToAccountSecurity ON ToAccountSecurity.AccountID = @ToAccountID
				AND FromAccountSecurity.SecurityID = ToAccountSecurity.SecurityID
	) as ChangeAccountID
		JOIN Trans ON ChangeAccountID.FromAccountSecurityID = Trans.AccountSecurityID

	-- DELETE the orphaned AccountSecurityID
	DELETE FROM AccountSecurity
	FROM AccountSecurity
	WHERE AccountSecurity.AccountSecurityID IN  (
						SELECT DeleteAccountSecurity.AccountSecurityID
						FROM (
							SELECT FromAccountSecurity.AccountSecurityID as AccountSecurityID
							FROM (
								SELECT AccountSecurity.AccountID,
									AccountSecurity.AccountSecurityID, 
									AccountSecurity.SecurityID
								FROM Security
									JOIN AccountSecurity ON Security.SecurityID = AccountSecurity.SecurityID
								WHERE AccountSecurity.AccountID = @FromAccountID
								) as FromAccountSecurity
							 JOIN AccountSecurity as ToAccountSecurity ON ToAccountSecurity.AccountID = @ToAccountID
								AND FromAccountSecurity.SecurityID = ToAccountSecurity.SecurityID
								) as DeleteAccountSecurity
							LEFT JOIN Trans ON DeleteAccountSecurity.AccountSecurityID = Trans.AccountSecurityID
							GROUP BY DeleteAccountSecurity.AccountSecurityID
							HAVING COUNT(Trans.TransID) = 0
						) 
	

COMMIT TRANSACTION


--Merge Transactions




--Merge Holdings

GO

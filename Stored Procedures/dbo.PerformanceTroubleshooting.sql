SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROC [dbo].[PerformanceTroubleshooting]
	@Level nvarchar(10) = 'E',
	@LevelID bigint,
	@BeginDate SMALLDATETIME,
	@EndDate SMALLDATETIME,
	@IncludeEstimates BIT = 1,
	@IncludeLinkedAccounts BIT = 1,
	@GroupBy VARCHAR(100) = 'Level'

AS

BEGIN
	
	SET NOCOUNT ON

	--BEGIN TRY

		SET @GroupBy = REPLACE(@GroupBy, ' ', '')

		DECLARE @SecurityList securitylist

		INSERT INTO @SecurityList (
			AccountSecurityID,
			AccountID,
			AccountName,
			SecurityID,
			SecurityName,
			ExcludeFromPerformance,
			PercentOwnership,
			EntityID,
			AssetClassID
			)
		SELECT AccountSecurityID,
			AccountID,
			AccountName,
			SecurityID,
			SecurityName,
			ExcludeFromPerformance,
			ISNULL(PercentOwnership, 100),
			EntityID,
			AssetClassID
		FROM [dbo].[tfn_SecurityList_by_Level] (@Level, @LevelID, @EndDate, @IncludeLinkedAccounts)

		-- CREATE the TWR temp table that will be used to calculate account and level independently
		--CREATE TABLE #TWR (
		--	[TWR_ID] [bigint] identity(1,1) NOT NULL,
		--	[Level] nvarchar(1) NOT NULL,
		--	[LevelID] [bigint] NOT NULL,
		--	[AccountID] [bigint] NULL,
		--	[AccountName] varchar(100),
		--	[AccountNumber] varchar(100),
		--	[AccountInceptionDate] smalldatetime,
		--	[PeriodBeginDate] [smalldatetime] NULL,
		--	[PeriodEndDate] [smalldatetime] NULL,
		--	[TWRBeginDate] [smalldatetime] NULL,
		--	[TWREndDate] [smalldatetime] NULL,
		--	[BeginMarketValue] [numeric](18, 6) NULL,
		--	[EndMarketValue] [numeric](18, 6) NULL,
		--	[CashFlow] [numeric](18, 6) NULL,
		--	[Additions] [numeric](18, 6) NULL,
		--	[Withdrawals] [numeric](18, 6) NULL,
		--	[Income] [numeric](18, 6) NULL,
		--	[AccruedIncome] [numeric](18, 6) NULL,
		--	[Fees] [numeric](18, 6) NULL,
		--	[PeriodTWR] [numeric](18, 6) NULL,
		--	[PeriodTWR_Net_Of_Fees] [numeric](18, 6) NULL,
		--	[PeriodTWR_Plus_One] [numeric](18, 6) NULL,
		--	[PeriodTWR_Net_Of_Fees_Plus_One] [numeric](18, 6) NULL,
		--	[TWR] float NULL,
		--	[AnnualizedTWR] float NULL,
		--	[TWR_Net_Of_Fees] float NULL,
		--	[AnnualizedTWR_Net_Of_Fees] float NULL,
		--	[ExcludeFromPerformance] bit
		--)

		--CREATE INDEX TWRIndex1 ON #TWR (PeriodBeginDate, TWRBeginDate)
		--CREATE INDEX TWRIndex2 ON #TWR (PeriodEndDate, TWREndDate)

		-- Create and populate the periodlist based on the type of report
		DECLARE @PeriodList periodlist
		INSERT INTO @PeriodList(
			BeginDate,
			EndDate
			)
		SELECT @BeginDate,
			@EndDate

		IF @GroupBy = 'Level'
		BEGIN
			EXEC [CalculateDataForTWR]
					@SecurityList,
					@PeriodList,
					@RecordSet = 0,
					@Level = @Level
		END
		ELSE
		BEGIN
			EXEC [CalculateDataForTWR_with_Group]
				@SecurityList,
				@PeriodList,
				@RecordSet = 1,		
				@Level = @Level,
				@GroupBy = @GroupBy
		END

	--END TRY
	--BEGIN CATCH
	--	DECLARE @ErrMsg nvarchar(4000) = ERROR_MESSAGE()
	--	DECLARE @ErrNum INT = ERROR_NUMBER()
	--	DECLARE @ErrProc nvarchar(126) = ERROR_PROCEDURE()
	--	DECLARE @DataError nvarchar(4000) = 'Error running PROC [DisplayAccountPerformanceReview]' +
	--		CONVERT(nvarchar(10), @errNum) + ', Error Details: ' + @ErrMsg
	--	RAISERROR (@DataError, 16,1)
	--	--ROLLBACK TRANSACTION
	--END CATCH

END

GO

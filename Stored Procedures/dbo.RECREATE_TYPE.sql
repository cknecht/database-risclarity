SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[RECREATE_TYPE]
    @schema     VARCHAR(100),       -- the schema name for the existing type
    @typ_nme    VARCHAR(128),       -- the type-name (without schema name)
    @sql        VARCHAR(MAX)        -- the SQL to create a type WITHOUT the "CREATE TYPE schema.typename" part

--EXEC [RECREATE_TYPE]
--    @schema     = 'dbo',
--    @typ_nme    = 'SecurityList',
--    @sql   = 'AS TABLE ([FamilyID] [bigint] NULL,
--	[FamilyName] [varchar](100) NULL,
--	[EntityID] [bigint] NULL,
--	[EntityName] [varchar](100) NULL,
--	[AccountID] [bigint] NULL,
--	[AccountName] [varchar](100) NULL,
--	[AccountSecurityID] [bigint] NULL,
--	[SecurityID] [bigint] NULL,
--	[SecurityName] [varchar](max) NULL,
--	[SecuritySymbol] [varchar](10) NULL,
--	[SecurityCUSIP] [varchar](10) NULL,
--	[SecuritySEDOL] [varchar](10) NULL,
--	[SecurityISIN] [varchar](10) NULL,
--	[QuantityOfOne] [bit] NULL,
--	[PriceFactor] [numeric](18, 6) NULL,
--	[CurrencyID] [int] NULL,
--	[CurrencyName] [varchar](100) NULL,
--	[CurrencyCode] [varchar](10) NULL,
--	[AssetClassID] [int] NULL,
--	[AssetClassPercent] [numeric](18, 6) NULL,
--	[AssetClassName] [varchar](255) NULL,
--	[AssetClassSortOrder] [int] NULL,
--	[AssetClassColor] [varchar](50) NULL,
--	[SectorID] [int] NULL,
--	[SectorPercent] [numeric](18, 6) NULL,
--	[SectorName] [varchar](155) NULL,
--	[SectorSortOrder] [int] NULL,
--	[SectorColor] [varchar](50) NULL,
--	[RegionID] [int] NULL,
--	[RegionPercent] [numeric](18, 6) NULL,
--	[RegionName] [varchar](255) NULL,
--	[RegionSortOrder] [int] NULL,
--	[RegionColor] [varchar](50) NULL,
--	[CountryID] [int] NULL,
--	[CountryPercent] [numeric](18, 6) NULL,
--	[CountryName] [varchar](255) NULL,
--	[CountryCode] [varchar](50) NULL,
--	[ExcludeFromPerformance] [bit] NULL,
--	[IncludeAccruedIncome] [bit] NULL,
--	[LiquidityInDays] [int] NULL,
--	[LiquidityColor] [varchar](20) NULL,
--	[PercentOwnership] [numeric](18, 6) NULL,
--	[TaxLotReliefMethodID] [int] NULL,
--	[CalcTaxLots] [bit] NULL)'

-- RUN exec recompile_prog a couple of times to make sure all dependencies are recompiled...

AS DECLARE
    @scid       BIGINT,
    @typ_id     BIGINT,
    @temp_nme   VARCHAR(1000),
    @msg        VARCHAR(200)
BEGIN
    -- find the existing type by schema and name
    SELECT @scid = [SCHEMA_ID] FROM sys.schemas WHERE UPPER(name) = UPPER(@schema);
    IF (@scid IS NULL) BEGIN
        SET @msg = 'Schema ''' + @schema + ''' not found.';
        RAISERROR (@msg, 1, 0);
    END;
    SELECT @typ_id = system_type_id FROM sys.types WHERE UPPER(name) = UPPER(@typ_nme);
    SET @temp_nme = @typ_nme + '_rcrt1'; -- temporary name for the existing type

    -- if the type-to-be-recreated actually exists, then rename it (give it a temporary name)
    -- if it doesn't exist, then that's OK, too.
    IF (@typ_id IS NOT NULL) BEGIN
        exec sp_rename @objname=@typ_nme, @newname= @temp_nme, @objtype='USERDATATYPE'
    END;    

    -- now create the new type
    SET @sql = 'CREATE TYPE ' + @schema + '.' + @typ_nme + ' ' + @sql;
    exec sp_sqlexec @sql;

    -- if we are RE-creating a type (as opposed to just creating a brand-spanking-new type)...
    IF (@typ_id IS NOT NULL) BEGIN
        exec recompile_prog;    -- then recompile all stored procs (that may have used the type)
        exec sp_droptype @typename=@temp_nme;   -- and drop the temporary type which is now no longer referenced
    END;    
END

GO

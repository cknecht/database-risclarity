SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROC [dbo].[ReturnAccountID]	
	@EntityID bigint,
	@AccountName varchar(max),
	@RecordSet bit = 0,
	@AccountID bigint = 0 OUTPUT

AS

BEGIN TRY
	BEGIN TRANSACTION

	SELECT @AccountID = MIN(Account.AccountID)
	FROM EntityAccount
		JOIN Account ON EntityAccount.AccountID = Account.AccountID
	WHERE EntityAccount.EntityID = @EntityID
		AND Account.AccountName = @AccountName


	IF ISNULL(@AccountID, 0) = 0
	BEGIN
		DECLARE @InsertedRows TABLE (AccountID bigint, AccountName varchar(max))

		INSERT INTO Account
		(AccountName)
			OUTPUT inserted.AccountID,
					inserted.AccountName
			INTO @InsertedRows
		VALUES (@AccountName)

		SELECT @AccountID = AccountID
		FROM @InsertedRows

		INSERT INTO EntityAccount
			(
			EntityID, 
			AccountID
			)
			VALUES (
			@EntityID,
			@AccountID
			)
	END

	IF @RecordSet = 1
	BEGIN
		SELECT @AccountID as AccountID
	END

COMMIT TRANSACTION
END TRY

BEGIN CATCH
	-- There is an error
	ROLLBACK TRANSACTION

	-- Re throw the exception
	THROW
END CATCH

GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROC [dbo].[ReturnCountryID]	
	@CountryName varchar(50) = '',
	@CountryCode2 varchar(2) = '',
	@CountryCode3 varchar(3) = '',
	@RecordSet bit = 0,
	@CountryID bigint = 0 OUTPUT

AS

BEGIN TRY
	BEGIN TRANSACTION

	SELECT @CountryID = MIN(Country.CountryID)
	FROM Country
	WHERE Country.CountryName = @CountryName
		OR Country.CountryCode2 = @CountryCode2
		OR Country.CountryCode3 = @CountryCode3

	IF @RecordSet = 1
	BEGIN
		SELECT @CountryID as CountryID
	END

COMMIT TRANSACTION
END TRY

BEGIN CATCH
	-- There is an error
	ROLLBACK TRANSACTION

	-- Re throw the exception
	THROW
END CATCH

GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROC [dbo].[ReturnCurrencyID]	
	@CurrencyName varchar(50) = '',
	@CurrencyCode varchar(3) = '',
	@RecordSet bit = 0,
	@CurrencyID bigint = 0 OUTPUT

AS

BEGIN TRY
	BEGIN TRANSACTION

	SELECT @CurrencyID = MIN(Currency.CurrencyID)
	FROM Currency
	WHERE Currency.CurrencyName = @CurrencyName
		OR Currency.CurrencyCode = @CurrencyCode

	IF @RecordSet = 1
	BEGIN
		SELECT @CurrencyID as CurrencyID
	END

COMMIT TRANSACTION
END TRY

BEGIN CATCH
	-- There is an error
	ROLLBACK TRANSACTION

	-- Re throw the exception
	THROW
END CATCH

GO

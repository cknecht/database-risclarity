SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROC [dbo].[ReturnEntityID]	
	@FamilyID bigint,
	@EntityName varchar(max),
	@RecordSet bit = 0,
	@EntityID bigint = 0 OUTPUT

AS

BEGIN TRY
	BEGIN TRANSACTION

	SELECT @EntityID = MIN(Entity.EntityID)
	FROM FamilyEntity
		JOIN Entity ON FamilyEntity.EntityID = Entity.EntityID
	WHERE FamilyEntity.FamilyID = @FamilyID
		AND Entity.EntityName = @EntityName


	IF ISNULL(@EntityID, 0) = 0
	BEGIN
		DECLARE @InsertedRows TABLE (EntityID bigint, EntityName varchar(max))

		INSERT INTO Entity
		(EntityName)
			OUTPUT inserted.EntityID,
					inserted.EntityName
			INTO @InsertedRows
		VALUES (@EntityName)

		SELECT @EntityID = EntityID
		FROM @InsertedRows

		INSERT INTO FamilyEntity
			(
			FamilyID, 
			EntityID
			)
			VALUES (
			@FamilyID,
			@EntityID
			)
	END

	IF @RecordSet = 1
	BEGIN
		SELECT @EntityID as EntityID
	END

COMMIT TRANSACTION
END TRY

BEGIN CATCH
	-- There is an error
	ROLLBACK TRANSACTION

	-- Re throw the exception
	THROW
END CATCH

GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROC [dbo].[ReturnSecurityID_bySecurityName]	
	@AccountID bigint,
	@SecurityName varchar(max),
	@RecordSet bit = 0,
	@SecurityID bigint = 0 OUTPUT,
	@AccountSecurityID bigint = 0 OUTPUT

AS

BEGIN TRY
	BEGIN TRANSACTION

	SELECT @SecurityID = MIN(Security.SecurityID)
	FROM Security 
	WHERE Security.SecurityName = @SecurityName

	IF ISNULL(@SecurityID, 0) = 0
	BEGIN
		DECLARE @InsertedRows TABLE (SecurityID bigint, SecurityName varchar(max))

		INSERT INTO Security
		(SecurityName)
			OUTPUT inserted.SecurityID,
					inserted.SecurityName
			INTO @InsertedRows
		VALUES (@SecurityName)

		SELECT @SecurityID = SecurityID
		FROM @InsertedRows
	END

	SELECT @AccountSecurityID = MIN(AccountSecurity.AccountSecurityID)
	FROM AccountSecurity
	WHERE AccountSecurity.AccountID = @AccountID
		AND AccountSecurity.SecurityID = @SecurityID

	IF ISNULL(@AccountSecurityID, 0) = 0
	BEGIN
		DECLARE @InsertedAccountSecurity TABLE (AccountSecurityID bigint, SecurityID bigint, AccountID bigint)

		INSERT INTO AccountSecurity
			(AccountID, SecurityID)
			OUTPUT inserted.AccountSecurityID,
					inserted.SecurityID,
					inserted.AccountID
			INTO @InsertedAccountSecurity
		VALUES (@AccountID, @SecurityID)

		SELECT @AccountSecurityID = AccountSecurityID
		FROM @InsertedAccountSecurity
	END

	IF @RecordSet = 1
	BEGIN
		SELECT @SecurityID as SecurityID,
			@AccountSecurityID as AccountSecurityID
	END

COMMIT TRANSACTION
END TRY

BEGIN CATCH
	-- There is an error
	ROLLBACK TRANSACTION

	-- Re throw the exception
	THROW
END CATCH

GO

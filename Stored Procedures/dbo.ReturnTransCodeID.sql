SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROC [dbo].[ReturnTransCodeID]	
	@TransCodeSourceName varchar(max), --Orginal Transaction Code
	@TransCodeSource varchar(max), --Source of Transaction (System or Institution)
	@TransSign int = 0,
	@RecordSet bit = 0,
	@TransCodeID int = -1 OUTPUT

AS

--BEGIN TRY
	--BEGIN TRANSACTION

	SELECT @TransCodeID = TransCodeMap.TransCodeID
	FROM TransCodeMap
	WHERE RTRIM(TransCodeMap.TransCodeSource) = RTRIM(@TransCodeSource)
		AND TransCodeMap.TransCodeSourceName = @TransCodeSourceName

	IF @TransCodeID IS NULL
	BEGIN

		SET @TransCodeID = 0

		INSERT INTO TransCodeMap
		(TransCodeSource, TransCodeSourceName, TransCodeID, TransSign)
		VALUES (RTRIM(@TransCodeSource), @TransCodeSourceName, @TransCodeID, @TransSign)

	END

	IF @RecordSet = 1
	BEGIN
		SELECT @TransCodeID as TransCodeID
	END

--COMMIT TRANSACTION
--END TRY

--BEGIN CATCH
--	-- There is an error
--	ROLLBACK TRANSACTION

--	-- Re throw the exception
--	THROW
--END CATCH

GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROC [dbo].[ShowChangeIfMultipleFamilies]
	@Username VARCHAR(1000)
AS

SELECT COUNT(FamilyID) AS CountFamilyID
FROM Users
	JOIN UsersFamily ON Users.UserID = UsersFamily.UserID
WHERE Users.Username = @Username

GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROC [dbo].[UpdateAssetClassHistory]
	@Username VARCHAR(100),
	@SecurityAssetClassID BIGINT = 0,
	@AsOfDate SMALLDATETIME,
	@AssetClassID INT,
	@Action VARCHAR(100) = '',
	@SecurityID BIGINT = 0

AS

IF @Action = 'Update' AND @SecurityID IS NOT NULL
BEGIN
	UPDATE dbo.SecurityAssetClass
	SET AssetClassID = @AssetClassID,
		AsOfDate = @AsOfDate,
		DataSource = '[UpdateAssetClassHistory] by ' + @Username,
		DateTimeEntered = CURRENT_TIMESTAMP
	WHERE SecurityAssetClassID = @SecurityAssetClassID

	SELECT @Action AS ACTION,
		*
	FROM dbo.SecurityAssetClass
	WHERE SecurityAssetClassID = @SecurityAssetClassID
END
ELSE IF @Action = 'Insert' AND @SecurityID IS NOT NULL AND @AsOfDate IS NOT NULL AND ISNULL(@SecurityAssetClassID, 0) = 0
BEGIN
		INSERT INTO dbo.SecurityAssetClass 
				( SecurityID ,
				  AsOfDate ,
				  AssetClassID ,
				  DateTimeEntered ,
				  DataSource ,
				  SourceName
				)
		VALUES  ( @SecurityID,
				  @AsOfDate,
				  @AssetClassID,
				  CURRENT_TIMESTAMP,
				  '[UpdateAssetClassHistory] by ' + @Username,
				  '[UpdateAssetClassHistory] by ' + @Username
				)

	SELECT @Action AS ACTION,
		*
	FROM dbo.SecurityAssetClass
	WHERE SecurityAssetClassID = @SecurityAssetClassID
END
ELSE IF @Action = 'Delete' AND ISNULL(@SecurityAssetClassID, 0) <> 0
BEGIN
	DELETE FROM dbo.SecurityAssetClass
	WHERE SecurityAssetClassID = @SecurityAssetClassId
END

GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROC [dbo].[UpdateDefaultFamily_By_User]
	@FamilyID BIGINT,
	@UserName VARCHAR(100)

AS

IF (SELECT COUNT(UsersFamily.ID) AS CountID 
	FROM dbo.Users
		JOIN UsersFamily ON Users.UserID = UsersFamily.UserID	
			AND UsersFamily.FamilyID = @FamilyID) > 0
BEGIN
	BEGIN TRANSACTION
		UPDATE dbo.UsersFamily
		SET SelectDefault = 1
		FROM dbo.Users
			JOIN UsersFamily ON Users.UserID = UsersFamily.UserID	
				AND UsersFamily.FamilyID = @FamilyID

		UPDATE dbo.UsersFamily
		SET SelectDefault = 0
		FROM dbo.Users
			JOIN UsersFamily ON Users.UserID = UsersFamily.UserID	
				AND UsersFamily.FamilyID <> @FamilyID
	COMMIT TRANSACTION
END
GO

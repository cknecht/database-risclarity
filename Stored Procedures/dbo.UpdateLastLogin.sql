SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROC [dbo].[UpdateLastLogin] (
	@Username VARCHAR(100),
	@RecordSet BIT = 0,
	@UserID bigint = NULL OUTPUT
	)
AS

SELECT @UserID = UserID
FROM dbo.Users
WHERE Username = @Username

IF @UserID IS NULL
BEGIN
	INSERT INTO Users (Username)
	VALUES(@Username)

	SELECT @UserID = SCOPE_IDENTITY()
END

UPDATE Users
SET LastLoginDateTime = GETUTCDATE()
WHERE UserID = @UserID

--IF @RecordSet = 1
--BEGIN
--	SELECT @UserID AS UserID
--END
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROC [dbo].[UpdateLastParameters]
	@UserName VARCHAR(100) = '',
	@Level VARCHAR(1),
	@LevelID BIGINT,
	@AsOfDate SMALLDATETIME,
	@BeginDate SMALLDATETIME = NULL,
	@ReportName VARCHAR(100),
	@Action VARCHAR(100),
	@ActionLevel VARCHAR(1),
	@ActionLevelID BIGINT

AS

SELECT @BeginDate = ISNULL(@BeginDate, LastBeginDate)
FROM dbo.Users
WHERE UserName = @UserName

UPDATE dbo.Users
SET LastLevel = @Level,
	LastLevelID = @LevelID,
	LastAsOfDate = @AsOfDate,
	LastBeginDate = @BeginDate,
	LastReportName = @ReportName,
	LastAction = @Action,
	LastActionLevel = @ActionLevel,
	LastActionLevelID = @ActionLevelID
WHERE UserName = @UserName
GO

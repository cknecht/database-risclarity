SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROC [dbo].[UpdateLiquidityHistory]
	@Username VARCHAR(100),
	@SecurityLiquidityID BIGINT = 0,
	@AsOfDate SMALLDATETIME,
	@LiquidityDays INT,
	@Action VARCHAR(100) = '',
	@SecurityID BIGINT = 0

AS

IF @Action = 'Update' AND @SecurityID IS NOT NULL
BEGIN
	UPDATE dbo.SecurityLiquidity
	SET LiquidityDays = @LiquidityDays,
		AsOfDate = @AsOfDate,
		DataSource = '[UpdateLiquidityHistory] by ' + @Username,
		DateTimeEntered = CURRENT_TIMESTAMP
	WHERE SecurityLiquidityID = @SecurityLiquidityID

	SELECT @Action AS ACTION,
		*
	FROM dbo.SecurityLiquidity
	WHERE SecurityLiquidityID = @SecurityLiquidityID
END
ELSE IF @Action = 'Insert' AND @SecurityID IS NOT NULL AND @AsOfDate IS NOT NULL AND ISNULL(@SecurityLiquidityID, 0) = 0
BEGIN
		INSERT INTO dbo.SecurityLiquidity 
				( SecurityID ,
				  AsOfDate ,
				  LiquidityDays ,
				  DateTimeEntered ,
				  DataSource ,
				  SourceName
				)
		VALUES  ( @SecurityID,
				  @AsOfDate,
				  @LiquidityDays,
				  CURRENT_TIMESTAMP,
				  '[UpdateLiquidityHistory] by ' + @Username,
				  '[UpdateLiquidityHistory] by ' + @Username
				)

	SELECT @Action AS ACTION,
		*
	FROM dbo.SecurityLiquidity
	WHERE SecurityLiquidityID = @SecurityLiquidityID
END
ELSE IF @Action = 'Delete' AND ISNULL(@SecurityLiquidityID, 0) <> 0
BEGIN
	DELETE FROM dbo.SecurityLiquidity
	WHERE SecurityLiquidityID = @SecurityLiquidityId
END

GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROC [dbo].[UpdatePrice]
	@Username VARCHAR(100),
	@PriceID BIGINT = 0,
	@AsOfDate SMALLDATETIME,
	@Price NUMERIC(18,6) = NULL,
	@Action VARCHAR(100) = '',
	@AccountSecurityID BIGINT = 0,
	@SecurityID BIGINT = 0


AS

DECLARE @Inserted_PriceIDs TABLE ([PriceID] BIGINT);

IF @Action = 'Update' AND @Price IS NOT NULL
BEGIN
	UPDATE dbo.Price
	SET Price = @Price,
		AsOfDate = @AsOfDate
	WHERE PriceID = @PriceID

	SELECT @Action AS ACTION,
		*
	FROM dbo.Price
	WHERE PriceID = @PriceID
END
ELSE IF @Action = 'Insert' AND @Price IS NOT NULL AND @AsOfDate IS NOT NULL AND ISNULL(@PriceID, 0) = 0
BEGIN

	IF ISNULL(@AccountSecurityID, 0) <> 0
	BEGIN
		SELECT @SecurityID = AccountSecurity.SecurityID
		FROM AccountSecurity
		WHERE AccountSecurity.AccountSecurityID = @AccountSecurityID
	END

	IF ISNULL(@SecurityID, 0) <> 0
	BEGIN

		INSERT INTO dbo.Price 
				( SecurityID ,
				  AsOfDate ,
				  Price ,
				  Price_CurrencyID ,
				  Estimate ,
				  DateTimeEntered ,
				  DataSource ,
				  EnteredBy ,
				  SourceName
				)
		OUTPUT INSERTED.[PriceId] INTO @Inserted_PriceIDs
		VALUES  ( @SecurityID,
				  @AsOfDate,
				  @Price,
				  234,
				  0,
				  CURRENT_TIMESTAMP,
				  'UpdatePrice', 
				  @Username,
				  'Stored Procedure'
				)
	END

	SELECT TOP 1 @PriceID = (PriceID)
	FROM @Inserted_PriceIDs

	SELECT @Action AS ACTION,
		*
	FROM dbo.Price
	WHERE PriceID = @PriceID
END
ELSE IF @Action = 'Delete' AND ISNULL(@PriceID, 0) <> 0
BEGIN
	DELETE FROM dbo.Price
	WHERE PriceID = @PriceID
END

GO

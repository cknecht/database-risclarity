SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROC [dbo].[UpdateSecurity]
	@Username VARCHAR(100),
	@SecurityID BIGINT = 0,
	@SecurityName varchar(100),
	@Symbol varchar(100) = NULL,
	@CUSIP varchar(100) = NULL,
	@PriceFactor NUMERIC(18,6),
	@QuantityOfOne bit,
	@ContractSize NUMERIC(18,6) = 1,
	@Action VARCHAR(100) = ''

AS

IF @Action = 'Update'
BEGIN
	
	UPDATE dbo.Security
	SET SecurityName = @SecurityName,
		Symbol = @Symbol,
		CUSIP = @CUSIP,
		PriceFactor = @PriceFactor,
		QuantityOfOne = @QuantityOfOne,
		ContractSize = @ContractSize
	WHERE SecurityID = @SecurityID

	SELECT @Action AS ACTION,
		*
	FROM dbo.Security
	WHERE SecurityID = @SecurityID
END

GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROC [dbo].[UpdateTransaction]
	@Username VARCHAR(100),
	@TransID BIGINT = 0,
	@TradeDate SMALLDATETIME,
	@TransCodeID INT,
	@Price NUMERIC(18,6) = NULL,
	@Quantity NUMERIC(18,6) = NULL,
	@Amount NUMERIC(18,6) = NULL,
	@TaxCost NUMERIC(18,6) = NULL,
	@Action VARCHAR(100) = '',
	@AccountSecurityID BIGINT = 0


AS

DECLARE @PriceFactor NUMERIC(18,6)

IF @AccountSecurityID <> 0
BEGIN
	SELECT @PriceFactor = Security.PriceFactor
	FROM dbo.Security
		JOIN AccountSecurity ON Security.SecurityId = AccountSecurity.SecurityID
	WHERE AccountSecurity.AccountSecurityID = @AccountSecurityID
END
ELSE
BEGIN
	SET @PriceFactor = 1
END

IF @Amount IS NULL 
	BEGIN
		SET @Amount = @Price * @Quantity * @PriceFactor
	END

IF @Action = 'Update'
BEGIN
	
	UPDATE dbo.Trans
	SET TradeDate = @TradeDate,
		SettleDate = @TradeDate,
		UnitPrice_Base = @Price,
		UnitPrice_Local = @Price,
		Amount_Base = @Amount,
		Amount_Local = @Amount,
		TaxCost_Base = @TaxCost,
		TaxCost_Local = @TaxCost,
		Quantity = @Quantity,
		TransCodeID = @TransCodeID
	WHERE TransID = @TransID

	SELECT @Action AS ACTION,
		*
	FROM dbo.vTrans
	WHERE TransID = @TransID
END
ELSE IF @Action = 'Insert' AND @TradeDAte IS NOT NULL AND @Price IS NOT NULL AND ISNULL(@AccountSecurityID, 0) <> 0
BEGIN

	DECLARE @IDs TABLE(ID INT);

	INSERT INTO dbo.Trans
	        ( AccountSecurityID ,
	          TradeDate ,
	          SettleDate ,
	          TransCodeID ,
	          Quantity ,
	          UnitPrice_Base ,
	          UnitPrice_Local ,
	          Amount_Base ,
	          Amount_Local ,
	          CurrencyID_Base ,
	          CurrencyID_Local ,
	          DateTimeEntered ,
	          DataSource ,
	          SourceTransactionCodeName ,
	          SourceName ,
	          SourceTransactionID ,
	          Notes
	        )
		OUTPUT inserted.TransID INTO @IDs(ID)
	VALUES  ( @AccountSecurityID,
	          @TradeDate,
	          @TradeDate,
	          @TransCodeID,
	          @Quantity,
			  @Price,
	          @Price,
	          @Amount,
	          @Amount,
	          234 , -- CurrencyID_Base - int
	          234 , -- CurrencyID_Local - int
	          CURRENT_TIMESTAMP , -- DateTimeEntered - smalldatetime
	          '[UpdateTransaction]' , -- DataSource - varchar(max)
	          NULL , -- SourceTransactionCodeName - varchar(max)
	          'Stored Procedure' , -- SourceName - varchar(100)
	          0 , -- SourceTransactionID - bigint
	          NULL  -- Notes - varchar(max)
	        ) 

	--Find new TransID 

	SELECT @TransID = ID FROM @IDs;

	SELECT @Action AS ACTION,
		*
	FROM dbo.vTrans
	WHERE TransID = @TransID
END
ELSE IF @Action = 'Delete' AND ISNULL(@TransID, 0) <> 0
BEGIN
	DELETE FROM dbo.Trans
	WHERE TransID = @TransID
END

GO

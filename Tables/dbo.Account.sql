CREATE TABLE [dbo].[Account]
(
[AccountID] [bigint] NOT NULL IDENTITY(1, 1),
[AccountName] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[AccountNumber] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AccountDescription] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DefaultCashSecurityID] [bigint] NULL CONSTRAINT [DF_Account_DefaultCashSecurityID] DEFAULT ((58)),
[BaseCurrencyID] [int] NULL CONSTRAINT [DF_Account_BaseCurrencyID] DEFAULT ((234)),
[InceptionDate] [smalldatetime] NULL,
[EnteredBy] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EnteredDateTime] [smalldatetime] NULL,
[DataSource] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DataSourceID] [bigint] NULL,
[Inactive] [bit] NULL CONSTRAINT [DF_Account_Inactive] DEFAULT ((0)),
[SourceName] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NoCashAsset] [bit] NULL CONSTRAINT [DF_Account_NoCashAsset] DEFAULT ((0)),
[ExcludeFromPerformance] [bit] NOT NULL CONSTRAINT [DF_Account_ExcludeFromPerformance] DEFAULT ((0)),
[ImportFromDate] [smalldatetime] NULL,
[ManagerStyleID] [int] NULL,
[ForceInceptionDateTo] [smalldatetime] NULL,
[TaxLotReliefMethodID] [tinyint] NOT NULL CONSTRAINT [DF_Account_TaxLotReliefMethodID] DEFAULT ((1)),
[CustodianID] [int] NULL,
[ManagerID] [int] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[Account] ADD CONSTRAINT [PK_Account] PRIMARY KEY CLUSTERED  ([AccountID]) WITH (FILLFACTOR=80) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [NonClusteredIndex-20131127-114526] ON [dbo].[Account] ([AccountID], [InceptionDate]) ON [PRIMARY]
GO

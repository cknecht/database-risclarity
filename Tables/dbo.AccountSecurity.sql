CREATE TABLE [dbo].[AccountSecurity]
(
[AccountSecurityID] [bigint] NOT NULL IDENTITY(1, 1),
[AccountID] [bigint] NOT NULL,
[SecurityID] [bigint] NOT NULL,
[EnteredDateTime] [smalldatetime] NOT NULL CONSTRAINT [DF_AccountSecurity_EnteredDateTime] DEFAULT (getdate()),
[EnteredBy] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LiquidityPeriodDays] [int] NULL,
[LiquidityHardLockupDate] [smalldatetime] NULL,
[LiquiditySoftLockupDate] [smalldatetime] NULL,
[LiquiditySoftLockupPercent] [numeric] (18, 6) NULL,
[LiquidityNoticePeriodDays] [int] NULL,
[LiquidityGatePercent] [numeric] (18, 6) NULL,
[LiquidityRedemptionPeriodID] [tinyint] NULL CONSTRAINT [DF_AccountSecurity_LiquidityRedemptionPeriodID] DEFAULT ((0)),
[DateTimeEntered] [smalldatetime] NULL,
[DataSource] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SourceName] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DataSourceID] [bigint] NULL,
[ExcludeFromPerformance] [bit] NOT NULL CONSTRAINT [DF_AccountSecurity_ExcludeFromPerformance] DEFAULT ((0)),
[TaxLotReliefMethodID] [tinyint] NOT NULL CONSTRAINT [DF_AccountSecurity_TaxLotReliefMethodID] DEFAULT ((0))
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[AccountSecurity] ADD CONSTRAINT [PK_AccountSecurity] PRIMARY KEY CLUSTERED  ([AccountSecurityID]) WITH (FILLFACTOR=80) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [NonClusteredIndex-20131127-114444] ON [dbo].[AccountSecurity] ([AccountID], [SecurityID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [NonClusteredIndex-20140116-092431] ON [dbo].[AccountSecurity] ([AccountID], [SecurityID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[AccountSecurity] ADD CONSTRAINT [FK_AccountSecurity_Account] FOREIGN KEY ([AccountID]) REFERENCES [dbo].[Account] ([AccountID]) ON DELETE CASCADE ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[AccountSecurity] ADD CONSTRAINT [FK_AccountSecurity_Security] FOREIGN KEY ([SecurityID]) REFERENCES [dbo].[Security] ([SecurityID]) ON DELETE CASCADE
GO
EXEC sp_addextendedproperty N'MS_Description', N'This is a measure used to restrict investors from redeeming more than a particular % of the fund’s total assets (NAV).', 'SCHEMA', N'dbo', 'TABLE', N'AccountSecurity', 'COLUMN', N'LiquidityGatePercent'
GO
EXEC sp_addextendedproperty N'MS_Description', N'This is the most common term used for liquidity. Funds enforce a hard lockup to restrict investors from redeeming their investment during a particular time period.

This period is generally one year to three years, depending on fund’s investments (in liquid securities or illiquid securities)', 'SCHEMA', N'dbo', 'TABLE', N'AccountSecurity', 'COLUMN', N'LiquidityHardLockupDate'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Investors have to give prior intimation/notice of their redemption. This is usually 60‐90 days, so investors have to inform the fund 60‐90 days in advance before the specified redemption date.', 'SCHEMA', N'dbo', 'TABLE', N'AccountSecurity', 'COLUMN', N'LiquidityNoticePeriodDays'
GO
EXEC sp_addextendedproperty N'MS_Description', N'The field contains the number of days before this asset can be redeamed.  Unknown is 0, Cash is 1, Stocks are typically 3 clearing days', 'SCHEMA', N'dbo', 'TABLE', N'AccountSecurity', 'COLUMN', N'LiquidityPeriodDays'
GO
EXEC sp_addextendedproperty N'MS_Description', N'In this, a period is specified when investors can redeem their investment by paying fees on the redemption amount. This fee is usually specified as 1% to 5% on the redemption amount. Sometime it is specified as different fees for different time periods or different share classes.', 'SCHEMA', N'dbo', 'TABLE', N'AccountSecurity', 'COLUMN', N'LiquiditySoftLockupDate'
GO
EXEC sp_addextendedproperty N'MS_Description', N'In this, a period is specified when investors can redeem their investment by paying fees on the redemption amount. This fee is usually specified as 1% to 5% on the redemption amount. Sometime it is specified as different fees for different time periods or different share classes.', 'SCHEMA', N'dbo', 'TABLE', N'AccountSecurity', 'COLUMN', N'LiquiditySoftLockupPercent'
GO

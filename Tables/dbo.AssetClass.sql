CREATE TABLE [dbo].[AssetClass]
(
[AssetClassID] [int] NOT NULL IDENTITY(0, 1),
[AssetClassName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[SortOrder] [smallint] NULL,
[AssetClassColor] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EnteredBy] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EnteredDateTime] [smalldatetime] NULL,
[DataSource] [varchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SourceName] [varchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[AssetClass] ADD CONSTRAINT [PK_AssetClass] PRIMARY KEY CLUSTERED  ([AssetClassID]) WITH (FILLFACTOR=80) ON [PRIMARY]
GO

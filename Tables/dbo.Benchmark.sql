CREATE TABLE [dbo].[Benchmark]
(
[BenchmarkID] [bigint] NOT NULL IDENTITY(1, 1),
[BenchmarkName] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[BenchmarkReturn]
(
[BenchmarkReturnID] [bigint] NOT NULL IDENTITY(1, 1),
[BenchmarkID] [bigint] NOT NULL,
[BenchmarkReturn] [numeric] (18, 6) NOT NULL,
[BeginDate] [smalldatetime] NOT NULL,
[EndDate] [smalldatetime] NOT NULL
) ON [PRIMARY]
GO

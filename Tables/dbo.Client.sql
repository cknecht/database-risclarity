CREATE TABLE [dbo].[Client]
(
[ClientID] [bigint] NOT NULL IDENTITY(1, 1),
[ClientShortName] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ClientLongName] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BillingAddress1] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BillingAddress2] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BillingCity] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BillingStateCode] [nvarchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BillingStateName] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BillingCountryName] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BillingPostalCode] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DatabaseServerName] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DataBaseName] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[ColorReportElementJoin]
(
[ColorElementJoinID] [bigint] NOT NULL IDENTITY(1, 1),
[ReportElementID] [int] NULL,
[ColorElementID] [int] NULL,
[SchemeID] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ColorReportElementJoin] ADD CONSTRAINT [PK_ColorReportElementJoin] PRIMARY KEY CLUSTERED  ([ColorElementJoinID]) WITH (FILLFACTOR=80) ON [PRIMARY]
GO

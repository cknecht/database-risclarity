CREATE TABLE [dbo].[ColorReportElements]
(
[ColorElementID] [int] NOT NULL IDENTITY(1, 1),
[ColorElementName] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ColorReportElements] ADD CONSTRAINT [PK_ColorReportElements] PRIMARY KEY CLUSTERED  ([ColorElementID]) WITH (FILLFACTOR=80) ON [PRIMARY]
GO

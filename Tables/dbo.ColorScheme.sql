CREATE TABLE [dbo].[ColorScheme]
(
[SchemeID] [int] NOT NULL IDENTITY(1, 1),
[SchemeName] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DefaultScheme] [bit] NOT NULL CONSTRAINT [DF_ColorScheme_DefaultScheme] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ColorScheme] ADD CONSTRAINT [PK_ColorScheme] PRIMARY KEY CLUSTERED  ([SchemeID]) WITH (FILLFACTOR=80) ON [PRIMARY]
GO

CREATE TABLE [dbo].[ColorSchemeElements]
(
[ElementID] [bigint] NOT NULL IDENTITY(1, 1),
[SchemeID] [int] NOT NULL,
[ElementNumber] [int] NOT NULL,
[ElementHEX] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ElementRGB] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FontColor] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ColorSchemeElements] ADD CONSTRAINT [PK_ColorSchemeElements] PRIMARY KEY CLUSTERED  ([ElementID]) WITH (FILLFACTOR=80) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ColorSchemeElements] ADD CONSTRAINT [FK_ColorSchemeElements_ColorScheme] FOREIGN KEY ([SchemeID]) REFERENCES [dbo].[ColorScheme] ([SchemeID]) ON DELETE CASCADE
GO

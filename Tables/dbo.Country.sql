CREATE TABLE [dbo].[Country]
(
[CountryID] [smallint] NOT NULL IDENTITY(1, 1),
[CountryName] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CountryCode2] [varchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CountryCode3] [varchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CountryCodeNumber] [smallint] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Country] ADD CONSTRAINT [PK_Country] PRIMARY KEY CLUSTERED  ([CountryID]) WITH (FILLFACTOR=80) ON [PRIMARY]
GO

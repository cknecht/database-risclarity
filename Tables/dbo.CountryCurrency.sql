CREATE TABLE [dbo].[CountryCurrency]
(
[CountryCurrencyID] [smallint] NOT NULL IDENTITY(1, 1),
[CountryID] [smallint] NOT NULL,
[CurrencyID] [smallint] NOT NULL,
[EffectiveDate] [smalldatetime] NULL,
[ExpirationDate] [smalldatetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CountryCurrency] ADD CONSTRAINT [PK_CountryCurrency] PRIMARY KEY CLUSTERED  ([CountryCurrencyID]) WITH (FILLFACTOR=80) ON [PRIMARY]
GO

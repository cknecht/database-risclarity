CREATE TABLE [dbo].[Currency]
(
[CurrencyID] [smallint] NOT NULL IDENTITY(1, 1),
[CurrencyName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CurrencyCode] [varchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CurrencyCodeNumber] [smallint] NULL,
[EffectiveDate] [smalldatetime] NULL,
[ExpirationDate] [smalldatetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Currency] ADD CONSTRAINT [PK_Currency] PRIMARY KEY CLUSTERED  ([CurrencyID]) WITH (FILLFACTOR=80) ON [PRIMARY]
GO

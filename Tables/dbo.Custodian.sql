CREATE TABLE [dbo].[Custodian]
(
[CustodianID] [int] NULL,
[CustodianName] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO

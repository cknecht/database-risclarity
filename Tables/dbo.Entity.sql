CREATE TABLE [dbo].[Entity]
(
[EntityID] [bigint] NOT NULL IDENTITY(1, 1),
[EntityName] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[EntityDescription] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EnteredBy] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EnteredDateTime] [datetime] NULL,
[DataSource] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DataSourceID] [bigint] NULL,
[Inactive] [bit] NULL CONSTRAINT [DF_Entity_Inactive] DEFAULT ((1)),
[SourceName] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EntityShortName] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[Entity] ADD CONSTRAINT [PK_Entity] PRIMARY KEY CLUSTERED  ([EntityID]) WITH (FILLFACTOR=80) ON [PRIMARY]
GO

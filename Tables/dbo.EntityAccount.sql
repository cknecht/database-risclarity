CREATE TABLE [dbo].[EntityAccount]
(
[EntityAccountID] [bigint] NOT NULL IDENTITY(1, 1),
[EntityID] [bigint] NOT NULL,
[AccountID] [bigint] NOT NULL,
[DateTimeEntered] [smalldatetime] NULL,
[DataSource] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[EntityAccount] ADD CONSTRAINT [PK_EntityAccount] PRIMARY KEY CLUSTERED  ([EntityAccountID]) WITH (FILLFACTOR=80) ON [PRIMARY]
GO
ALTER TABLE [dbo].[EntityAccount] ADD CONSTRAINT [FK_EntityAccount_Account] FOREIGN KEY ([AccountID]) REFERENCES [dbo].[Account] ([AccountID]) ON DELETE CASCADE ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[EntityAccount] ADD CONSTRAINT [FK_EntityAccount_Entity] FOREIGN KEY ([EntityID]) REFERENCES [dbo].[Entity] ([EntityID])
GO

CREATE TABLE [dbo].[EntityEntityOwnership]
(
[EntityOwnerID] [bigint] NOT NULL IDENTITY(1, 1),
[EntitySubID] [bigint] NOT NULL,
[EntityPrimaryID] [bigint] NOT NULL,
[PercentOwnership] [numeric] (18, 6) NOT NULL,
[AsOfDate] [smalldatetime] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[EntityEntityOwnership] ADD CONSTRAINT [PK_EntityEntityOwnership] PRIMARY KEY CLUSTERED  ([EntityOwnerID]) WITH (FILLFACTOR=80) ON [PRIMARY]
GO

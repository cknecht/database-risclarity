CREATE TABLE [dbo].[Family]
(
[FamilyID] [bigint] NOT NULL IDENTITY(1, 1),
[FamilyName] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[FamilyDescription] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EnteredBy] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[UpdatedBy] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EnteredDateTime] [datetime] NULL,
[UpdatedDateTime] [datetime] NULL,
[DataSource] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DataSourceID] [bigint] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[Family] ADD CONSTRAINT [PK_Family] PRIMARY KEY CLUSTERED  ([FamilyID]) WITH (FILLFACTOR=80) ON [PRIMARY]
GO

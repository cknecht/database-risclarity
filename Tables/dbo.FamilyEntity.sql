CREATE TABLE [dbo].[FamilyEntity]
(
[FamilyEntityID] [bigint] NOT NULL IDENTITY(1, 1),
[FamilyID] [bigint] NOT NULL,
[EntityID] [bigint] NOT NULL,
[DateTimeEntered] [smalldatetime] NULL,
[DataSource] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[FamilyEntity] ADD CONSTRAINT [PK_FamilyEntity] PRIMARY KEY CLUSTERED  ([FamilyEntityID]) WITH (FILLFACTOR=80) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_FamilyEntity] ON [dbo].[FamilyEntity] ([FamilyEntityID]) WITH (FILLFACTOR=80) ON [PRIMARY]
GO
ALTER TABLE [dbo].[FamilyEntity] ADD CONSTRAINT [FK_FamilyEntity_Entity] FOREIGN KEY ([EntityID]) REFERENCES [dbo].[Entity] ([EntityID])
GO
ALTER TABLE [dbo].[FamilyEntity] ADD CONSTRAINT [FK_FamilyEntity_Family] FOREIGN KEY ([FamilyID]) REFERENCES [dbo].[Family] ([FamilyID])
GO

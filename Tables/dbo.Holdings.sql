CREATE TABLE [dbo].[Holdings]
(
[HoldingsID] [bigint] NOT NULL IDENTITY(1, 1),
[AccountSecurityID] [bigint] NOT NULL,
[AsOfDate] [smalldatetime] NOT NULL,
[Quantity] [numeric] (18, 6) NOT NULL,
[DateAcquired] [smalldatetime] NULL,
[CurrencyID_Base] [tinyint] NULL,
[Price_Base] [numeric] (18, 6) NULL,
[MarketValue_Base] [numeric] (18, 6) NULL,
[Cost_Base] [numeric] (18, 6) NULL,
[GainLoss_Base] [numeric] (18, 6) NULL,
[CurrencyID_Local] [tinyint] NULL,
[Price_Local] [numeric] (18, 6) NULL,
[MarketValue_Local] [numeric] (18, 6) NULL,
[Cost_Local] [numeric] (18, 6) NULL,
[GainLoss_Local] [numeric] (18, 6) NULL,
[DateLongTerm] [smalldatetime] NULL,
[EnteredDateTime] [datetime] NULL CONSTRAINT [DF_Holdings_DateTimeEntered] DEFAULT (getdate()),
[UpdatedDateTime] [datetime] NULL,
[EnteredBy] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[UpdatedBy] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DataSource] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[Holdings] ADD CONSTRAINT [PK_Holdings] PRIMARY KEY CLUSTERED  ([HoldingsID]) WITH (FILLFACTOR=80) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Holdings] ADD CONSTRAINT [FK_Holdings_AccountSecurity] FOREIGN KEY ([AccountSecurityID]) REFERENCES [dbo].[AccountSecurity] ([AccountSecurityID]) ON DELETE CASCADE ON UPDATE CASCADE
GO

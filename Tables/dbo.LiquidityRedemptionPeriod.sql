CREATE TABLE [dbo].[LiquidityRedemptionPeriod]
(
[LiquidityRedemptionPeriodID] [int] NOT NULL IDENTITY(0, 1),
[LiquidityRedemptionPeriodName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[LiquidityRedemptionPeriod] ADD CONSTRAINT [PK_LiquidityRedemptionPeriod] PRIMARY KEY CLUSTERED  ([LiquidityRedemptionPeriodID]) WITH (FILLFACTOR=80) ON [PRIMARY]
GO

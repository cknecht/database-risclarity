CREATE TABLE [dbo].[Log_SecurityClassification]
(
[LogID] [bigint] NOT NULL IDENTITY(1, 1),
[SecurityID] [bigint] NULL,
[SecurityName] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[AssetClass] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Sector] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Country] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Region] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Datasource] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EnteredDateTime] [smalldatetime] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[Log_SecurityClassification] ADD CONSTRAINT [PK_Log_SecurityClassification] PRIMARY KEY CLUSTERED  ([LogID]) WITH (FILLFACTOR=80) ON [PRIMARY]
GO

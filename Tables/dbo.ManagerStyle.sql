CREATE TABLE [dbo].[ManagerStyle]
(
[ManagerStyleID] [smallint] NOT NULL IDENTITY(0, 1),
[ManagerStyleName] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Performing] [bit] NULL CONSTRAINT [DF_ManagerStyle_Performing] DEFAULT ((1)),
[SortOrder] [smallint] NULL,
[Color] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO

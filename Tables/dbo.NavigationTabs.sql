CREATE TABLE [dbo].[NavigationTabs]
(
[TabID] [smallint] NOT NULL IDENTITY(1, 1),
[TabName] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TabOrder] [smallint] NULL,
[ReportName] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Disable] [bit] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[NavigationTabs] ADD CONSTRAINT [PK_NavigationTabs] PRIMARY KEY CLUSTERED  ([TabID]) ON [PRIMARY]
GO

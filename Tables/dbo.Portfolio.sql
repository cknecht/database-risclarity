CREATE TABLE [dbo].[Portfolio]
(
[PortfolioID] [bigint] NOT NULL IDENTITY(1, 1),
[PortfolioName] [varchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FamilyID] [bigint] NULL
) ON [PRIMARY]
GO

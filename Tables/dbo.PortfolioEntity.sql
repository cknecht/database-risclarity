CREATE TABLE [dbo].[PortfolioEntity]
(
[PortfolioEntityID] [bigint] NOT NULL IDENTITY(1, 1),
[PortfolioID] [bigint] NOT NULL,
[EntityID] [bigint] NOT NULL,
[PercentOwnership] [numeric] (18, 6) NOT NULL,
[AsOfDate] [smalldatetime] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[PortfolioEntity] ADD CONSTRAINT [PK_PortfolioEntity] PRIMARY KEY CLUSTERED  ([PortfolioEntityID]) WITH (FILLFACTOR=80) ON [PRIMARY]
GO

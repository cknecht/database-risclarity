CREATE TABLE [dbo].[Price]
(
[PriceID] [bigint] NOT NULL IDENTITY(1, 1),
[SecurityID] [bigint] NOT NULL,
[AsOfDate] [smalldatetime] NOT NULL,
[Price] [numeric] (18, 6) NOT NULL,
[Price_CurrencyID] [int] NOT NULL CONSTRAINT [DF_Price_Price_CurrencyID] DEFAULT ((234)),
[Estimate] [bit] NOT NULL CONSTRAINT [DF_Price_Estimate] DEFAULT ((0)),
[DateTimeEntered] [smalldatetime] NULL CONSTRAINT [DF_Price_DateTimeEntered] DEFAULT (getdate()),
[DataSource] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EnteredBy] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SourceName] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Yield] [numeric] (18, 6) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE TRIGGER [dbo].[LogPriceChanges_On_Delete]
   ON [dbo].[Price]
   AFTER DELETE
AS 
BEGIN
	SET NOCOUNT ON;
	
	IF EXISTS(SELECT * FROM Deleted)
	BEGIN
		INSERT INTO Price_Change_Log (
			Action,
			ActionSource,
			ActionDateTime,
			ActionBy,
			PriceID,
			SecurityID,
			AsOfDate,
			Price,
			Price_CurrencyID,
			Estimate,
			DateTimeEntered,
			DataSource,
			EnteredBy,
			SourceName
			)
		SELECT 
			'DELETE' as Action,
			'' as ActionSource,
			GETUTCDATE() as ActionDateTime,
			'' as ActionBy,
			Deleted.PriceID,
			Deleted.SecurityID,
			Deleted.AsOfDate,
			Deleted.Price,
			Deleted.Price_CurrencyID,
			Deleted.Estimate,
			Deleted.DateTimeEntered,
			Deleted.DataSource,
			Deleted.EnteredBy,
			Deleted.SourceName
		FROM Deleted
	END

END

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE TRIGGER [dbo].[LogPriceChanges_On_Update]
   ON [dbo].[Price]
   AFTER UPDATE
AS 
BEGIN
	SET NOCOUNT ON;
	
	IF EXISTS(SELECT * FROM Deleted)
	BEGIN
		INSERT INTO Price_Change_Log (
			Action,
			ActionSource,
			ActionDateTime,
			ActionBy,
			PriceID,
			SecurityID,
			AsOfDate,
			Price,
			Price_CurrencyID,
			Estimate,
			DateTimeEntered,
			DataSource,
			EnteredBy,
			SourceName
			)
		SELECT 
			'UPDATE' as Action,
			'' as ActionSource,
			GETUTCDATE() as ActionDateTime,
			'' as ActionBy,
			Deleted.PriceID,
			Deleted.SecurityID,
			Deleted.AsOfDate,
			Deleted.Price,
			Deleted.Price_CurrencyID,
			Deleted.Estimate,
			Deleted.DateTimeEntered,
			Deleted.DataSource,
			Deleted.EnteredBy,
			Deleted.SourceName
		FROM Deleted
	END

END

GO
ALTER TABLE [dbo].[Price] ADD CONSTRAINT [PK_Price] PRIMARY KEY CLUSTERED  ([PriceID]) WITH (FILLFACTOR=80) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [NonClusteredIndex-20130707-165225] ON [dbo].[Price] ([AsOfDate], [SecurityID]) WITH (FILLFACTOR=80, IGNORE_DUP_KEY=ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [NonClusteredIndex-20131127-124046] ON [dbo].[Price] ([SecurityID], [AsOfDate], [Price_CurrencyID]) ON [PRIMARY]
GO

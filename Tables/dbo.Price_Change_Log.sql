CREATE TABLE [dbo].[Price_Change_Log]
(
[ChangeID] [bigint] NOT NULL IDENTITY(1, 1),
[Action] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ActionSource] [varchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ActionDateTime] [datetime] NULL,
[ActionBy] [varchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PriceID] [bigint] NULL,
[SecurityID] [bigint] NULL,
[AsOfDate] [smalldatetime] NULL,
[Price] [numeric] (18, 6) NULL,
[Price_CurrencyID] [int] NULL,
[Estimate] [bit] NULL,
[DateTimeEntered] [smalldatetime] NULL,
[DataSource] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EnteredBy] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SourceName] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[Price_Change_Log] ADD CONSTRAINT [PK_PriceChange] PRIMARY KEY CLUSTERED  ([ChangeID]) WITH (FILLFACTOR=80) ON [PRIMARY]
GO

CREATE TABLE [dbo].[Report_Run_Log]
(
[LogID] [bigint] NOT NULL IDENTITY(1, 1),
[Username] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ReportURL] [varchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DateTime] [datetime] NULL,
[RenderFormatName] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ReportName] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Report_Run_Log] ADD CONSTRAINT [PK_Report_Run_Log] PRIMARY KEY CLUSTERED  ([LogID]) WITH (FILLFACTOR=80) ON [PRIMARY]
GO

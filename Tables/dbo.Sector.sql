CREATE TABLE [dbo].[Sector]
(
[SectorID] [int] NOT NULL IDENTITY(0, 1),
[SectorName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[SortOrder] [smallint] NULL,
[Image32] [varbinary] (max) NULL,
[Image64] [varbinary] (max) NULL,
[Image128] [varbinary] (max) NULL,
[Image256] [varbinary] (max) NULL,
[SectorColor] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EnteredBy] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EnteredDateTime] [smalldatetime] NULL,
[DataSource] [varchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SourceName] [varchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[Sector] ADD CONSTRAINT [PK_Sector] PRIMARY KEY CLUSTERED  ([SectorID]) WITH (FILLFACTOR=80) ON [PRIMARY]
GO

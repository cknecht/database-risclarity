CREATE TABLE [dbo].[Security]
(
[SecurityID] [bigint] NOT NULL IDENTITY(1, 1),
[SecurityName] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Symbol] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CUSIP] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ISIN] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SEDOL] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LocalCurrencyID] [tinyint] NULL,
[PriceFactor] [numeric] (18, 6) NOT NULL CONSTRAINT [DF_Security_PriceFactor] DEFAULT ((1)),
[SecurityTypeID] [int] NULL,
[QuantityofOne] [bit] NULL CONSTRAINT [DF_Security_QuantityofOne] DEFAULT ((1)),
[DataSource] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DataSourceID] [bigint] NULL,
[EnteredBy] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EnteredDateTime] [smalldatetime] NULL,
[SourceName] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ExcludeFromPerformance] [bit] NOT NULL CONSTRAINT [DF_Security_ExcludeFromPerformance] DEFAULT ((0)),
[OtherIdentifier1] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OtherIdentifier2] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IsHedgeFund] [bit] NULL,
[IsPrivateEquity] [bit] NULL,
[IsRealEstate] [bit] NULL,
[CalcTaxLots] [bit] NULL CONSTRAINT [DF_Security_CalcTaxLots] DEFAULT ((1)),
[ContractSize] [numeric] (18, 6) NULL CONSTRAINT [DF_Security_ContractSize] DEFAULT ((1))
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[Security] ADD CONSTRAINT [PK_Security] PRIMARY KEY CLUSTERED  ([SecurityID]) WITH (FILLFACTOR=80) ON [PRIMARY]
GO

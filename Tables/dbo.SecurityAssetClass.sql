CREATE TABLE [dbo].[SecurityAssetClass]
(
[SecurityAssetClassID] [bigint] NOT NULL IDENTITY(1, 1),
[SecurityID] [bigint] NOT NULL,
[AssetClassID] [int] NOT NULL,
[AsOfDate] [smalldatetime] NOT NULL CONSTRAINT [DF_SecurityAssetClass_AsOfDate] DEFAULT ('1/1/1900'),
[PercentAllocated] [int] NOT NULL CONSTRAINT [DF_SecurityAssetClass_PercentAllocated] DEFAULT ((100)),
[DateTimeEntered] [smalldatetime] NULL,
[DataSource] [varchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SourceName] [varchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SecurityAssetClass] ADD CONSTRAINT [PK_SecurityAssetClass] PRIMARY KEY CLUSTERED  ([SecurityAssetClassID]) WITH (FILLFACTOR=80) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SecurityAssetClass] WITH NOCHECK ADD CONSTRAINT [FK_SecurityAssetClass_AssetClass] FOREIGN KEY ([AssetClassID]) REFERENCES [dbo].[AssetClass] ([AssetClassID])
GO

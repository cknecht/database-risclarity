CREATE TABLE [dbo].[SecurityLiquidity]
(
[SecurityLiquidityID] [bigint] NOT NULL IDENTITY(1, 1),
[SecurityID] [bigint] NOT NULL,
[LiquidityDays] [int] NOT NULL CONSTRAINT [DF_SecurityRegion_LiquidityDays] DEFAULT ((0)),
[AsOfDate] [smalldatetime] NOT NULL CONSTRAINT [DF_SecurityLiquidity_AsOfDate] DEFAULT ('1/1/1900'),
[LiquidityHardLockupDate] [smalldatetime] NULL,
[LiquiditySoftLockupDate] [smalldatetime] NULL,
[LiquiditySoftLockupPercent] [numeric] (18, 6) NULL,
[LiquidityNoticePeriodDays] [int] NULL,
[LiquidityGatePercent] [numeric] (18, 6) NULL,
[LiquidityRedemptionPeriodID] [int] NULL,
[DateTimeEntered] [smalldatetime] NULL,
[DataSource] [varchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SourceName] [varchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SecurityLiquidity] ADD CONSTRAINT [PK_SecurityLiquidity] PRIMARY KEY CLUSTERED  ([SecurityLiquidityID]) WITH (FILLFACTOR=80) ON [PRIMARY]
GO

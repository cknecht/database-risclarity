CREATE TABLE [dbo].[SecurityRegion]
(
[SecurityRegionID] [bigint] NOT NULL IDENTITY(1, 1),
[SecurityID] [bigint] NOT NULL,
[RegionID] [int] NOT NULL,
[AsOfDate] [smalldatetime] NOT NULL CONSTRAINT [DF_SecurityRegion_AsOfDate] DEFAULT ('1/1/1900'),
[PercentAllocated] [int] NOT NULL CONSTRAINT [DF_SecurityRegion_PercentAllocated] DEFAULT ((100)),
[DateTimeEntered] [smalldatetime] NULL,
[DataSource] [varchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SourceName] [varchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SecurityRegion] ADD CONSTRAINT [PK_SecurityRegion] PRIMARY KEY CLUSTERED  ([SecurityRegionID]) WITH (FILLFACTOR=80) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SecurityRegion] ADD CONSTRAINT [FK_SecurityRegion_Region] FOREIGN KEY ([RegionID]) REFERENCES [dbo].[Region] ([RegionID])
GO

CREATE TABLE [dbo].[SecuritySector]
(
[SecuritySectorID] [bigint] NOT NULL IDENTITY(1, 1),
[SecurityID] [bigint] NOT NULL,
[SectorID] [int] NOT NULL,
[AsOfDate] [smalldatetime] NOT NULL CONSTRAINT [DF_SecuritySector_AsOfDate] DEFAULT ('1/1/1900'),
[PercentAllocated] [int] NOT NULL CONSTRAINT [DF_SecuritySector_PercentAllocated] DEFAULT ((100)),
[DateTimeEntered] [smalldatetime] NULL,
[DataSource] [varchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SourceName] [varchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SecuritySector] ADD CONSTRAINT [PK_SecuritySector] PRIMARY KEY CLUSTERED  ([SecuritySectorID]) WITH (FILLFACTOR=80) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SecuritySector] ADD CONSTRAINT [FK_SecuritySector_Sector] FOREIGN KEY ([SectorID]) REFERENCES [dbo].[Sector] ([SectorID])
GO

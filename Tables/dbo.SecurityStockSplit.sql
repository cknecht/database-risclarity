CREATE TABLE [dbo].[SecurityStockSplit]
(
[SplitID] [int] NOT NULL IDENTITY(1, 1),
[SecurityID] [int] NULL,
[NewSecurityID] [int] NULL,
[Factor] [numeric] (18, 6) NULL,
[RecordDate] [smalldatetime] NULL,
[ExDate] [smalldatetime] NULL,
[DeclarationDate] [smalldatetime] NULL,
[DataSource] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DateTimeEntered] [smalldatetime] NULL,
[TransCodeID] [tinyint] NULL,
[Notes] [varchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO

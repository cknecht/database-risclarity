CREATE TABLE [dbo].[SecurityType]
(
[SecurityTypeID] [int] NOT NULL IDENTITY(0, 1),
[SecurityTypeName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[SecurityTypeRollupID] [int] NOT NULL CONSTRAINT [DF_SecurityType_SecurityTypeRollupID] DEFAULT ((0)),
[SortOrder] [smallint] NULL CONSTRAINT [DF_SecurityType_SortOrder] DEFAULT ((0)),
[EnteredBy] [varchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EnteredDateTime] [smalldatetime] NULL,
[SourceName] [varchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DataSource] [varchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SecurityType] ADD CONSTRAINT [PK_SecurityType] PRIMARY KEY CLUSTERED  ([SecurityTypeID]) WITH (FILLFACTOR=80) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SecurityType] ADD CONSTRAINT [FK_SecurityType_SecurityTypeRollup] FOREIGN KEY ([SecurityTypeRollupID]) REFERENCES [dbo].[SecurityTypeRollup] ([SecurityTypeRollupID])
GO

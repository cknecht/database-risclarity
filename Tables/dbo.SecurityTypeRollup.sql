CREATE TABLE [dbo].[SecurityTypeRollup]
(
[SecurityTypeRollupID] [int] NOT NULL IDENTITY(0, 1),
[SecurityTypeRollupName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[SortOrder] [smallint] NOT NULL CONSTRAINT [DF_SecurityTypeRollup_SortOrder] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SecurityTypeRollup] ADD CONSTRAINT [PK_SecurityTypeRollup] PRIMARY KEY CLUSTERED  ([SecurityTypeRollupID]) WITH (FILLFACTOR=80) ON [PRIMARY]
GO

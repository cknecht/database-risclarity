CREATE TABLE [dbo].[SourceAccountBalance]
(
[BalanceID] [bigint] NOT NULL IDENTITY(1, 1),
[AccountID] [bigint] NULL,
[SourceName] [varchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SourceAccountID] [bigint] NULL,
[AccountName] [nvarchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AsOfDate] [smalldatetime] NULL,
[MarketValue] [money] NULL,
[CurrencyCode] [varchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EnteredBy] [nvarchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DateTimeEntered] [smalldatetime] NULL,
[DataSource] [nvarchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE TRIGGER [dbo].[LogSourceAccountBalanceChanges_On_Delete]
   ON [dbo].[SourceAccountBalance]
   AFTER DELETE
AS 
BEGIN
	SET NOCOUNT ON;
	
	IF EXISTS(SELECT * FROM Deleted)
	BEGIN
		INSERT INTO SourceAccountBalance_Change_Log (
			Action,
			ActionSource,
			ActionDateTime,
			ActionBy,
			BalanceID,
			AccountID,
			SourceName,
			SourceAccountID,
			AccountName,
			AsOfDate,
			MarketValue,
			CurrencyCode,
			EnteredBy,
			DateTimeEntered,
			DataSource
			)
		SELECT 
			'DELETE' as Action,
			'' as ActionSource,
			GETUTCDATE() as ActionDateTime,
			'' as ActionBy,
			Deleted.BalanceID,
			Deleted.AccountID,
			Deleted.SourceName,
			Deleted.SourceAccountID,
			Deleted.AccountName,
			Deleted.AsOfDate,
			Deleted.MarketValue,
			Deleted.CurrencyCode,
			Deleted.EnteredBy,
			Deleted.DateTimeEntered,
			Deleted.DataSource
		FROM Deleted
	END

END

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE TRIGGER [dbo].[LogSourceAccountBalanceChanges_On_Update]
   ON [dbo].[SourceAccountBalance]
   AFTER UPDATE
AS 
BEGIN
	SET NOCOUNT ON;
	
	IF EXISTS(SELECT * FROM Deleted)
	BEGIN
		INSERT INTO SourceAccountBalance_Change_Log (
			Action,
			ActionSource,
			ActionDateTime,
			ActionBy,
			BalanceID,
			AccountID,
			SourceName,
			SourceAccountID,
			AccountName,
			AsOfDate,
			MarketValue,
			CurrencyCode,
			EnteredBy,
			DateTimeEntered,
			DataSource
			)
		SELECT 
			'UPDATE' as Action,
			'' as ActionSource,
			GETUTCDATE() as ActionDateTime,
			'' as ActionBy,
			Deleted.BalanceID,
			Deleted.AccountID,
			Deleted.SourceName,
			Deleted.SourceAccountID,
			Deleted.AccountName,
			Deleted.AsOfDate,
			Deleted.MarketValue,
			Deleted.CurrencyCode,
			Deleted.EnteredBy,
			Deleted.DateTimeEntered,
			Deleted.DataSource
		FROM Deleted
	END

END

GO
ALTER TABLE [dbo].[SourceAccountBalance] ADD CONSTRAINT [PK_SourceAccountBalance] PRIMARY KEY CLUSTERED  ([BalanceID]) WITH (FILLFACTOR=80) ON [PRIMARY]
GO

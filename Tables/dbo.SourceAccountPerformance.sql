CREATE TABLE [dbo].[SourceAccountPerformance]
(
[PerformanceID] [bigint] NOT NULL IDENTITY(1, 1),
[AccountID] [bigint] NULL,
[SourceName] [varchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SourceAccountID] [bigint] NULL,
[AccountName] [nvarchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BeginDate] [smalldatetime] NULL,
[EndDate] [smalldatetime] NULL,
[PeriodReturn] [money] NULL,
[PeriodText] [nvarchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EnteredBy] [nvarchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DateTimeEntered] [smalldatetime] NULL,
[DataSource] [nvarchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE TRIGGER [dbo].[LogSourceAccountPerformanceChanges_On_Delete]
   ON [dbo].[SourceAccountPerformance]
   AFTER DELETE
AS 
BEGIN
	SET NOCOUNT ON;
	
	IF EXISTS(SELECT * FROM Deleted)
	BEGIN
		INSERT INTO SourceAccountPerformance_Change_Log (
			Action,
			ActionSource,
			ActionDateTime,
			ActionBy,
			PerformanceID,
			AccountID,
			SourceName,
			SourceAccountID,
			AccountName,
			BeginDate,
			EndDate,
			PeriodReturn,
			PeriodText,
			EnteredBy,
			DateTimeEntered,
			DataSource
			)
		SELECT 
			'DELETE' as Action,
			'' as ActionSource,
			GETUTCDATE() as ActionDateTime,
			'' as ActionBy,
			Deleted.PerformanceID,
			Deleted.AccountID,
			Deleted.SourceName,
			Deleted.SourceAccountID,
			Deleted.AccountName,
			Deleted.BeginDate,
			Deleted.EndDate,
			Deleted.PeriodReturn,
			Deleted.PeriodText,
			Deleted.EnteredBy,
			Deleted.DateTimeEntered,
			Deleted.DataSource
		FROM Deleted
	END

END

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE TRIGGER [dbo].[LogSourceAccountPerformanceChanges_On_Update]
   ON [dbo].[SourceAccountPerformance]
   AFTER UPDATE
AS 
BEGIN
	SET NOCOUNT ON;
	
	IF EXISTS(SELECT * FROM Deleted)
	BEGIN
		INSERT INTO SourceAccountPerformance_Change_Log (
			Action,
			ActionSource,
			ActionDateTime,
			ActionBy,
			PerformanceID,
			AccountID,
			SourceName,
			SourceAccountID,
			AccountName,
			BeginDate,
			EndDate,
			PeriodReturn,
			PeriodText,
			EnteredBy,
			DateTimeEntered,
			DataSource
			)
		SELECT 
			'UPDATE' as Action,
			'' as ActionSource,
			GETUTCDATE() as ActionDateTime,
			'' as ActionBy,
			Deleted.PerformanceID,
			Deleted.AccountID,
			Deleted.SourceName,
			Deleted.SourceAccountID,
			Deleted.AccountName,
			Deleted.BeginDate,
			Deleted.EndDate,
			Deleted.PeriodReturn,
			Deleted.PeriodText,
			Deleted.EnteredBy,
			Deleted.DateTimeEntered,
			Deleted.DataSource
		FROM Deleted
	END

END


GO
ALTER TABLE [dbo].[SourceAccountPerformance] ADD CONSTRAINT [PK_SourceAccountPerformance] PRIMARY KEY CLUSTERED  ([PerformanceID]) WITH (FILLFACTOR=80) ON [PRIMARY]
GO

CREATE TABLE [dbo].[SourceAccountPerformance_Change_Log]
(
[ChangeID] [bigint] NOT NULL IDENTITY(1, 1),
[Action] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ActionSource] [varchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ActionDateTime] [datetime] NULL,
[ActionBy] [varchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PerformanceID] [bigint] NULL,
[AccountID] [bigint] NULL,
[SourceName] [varchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SourceAccountID] [bigint] NULL,
[AccountName] [nvarchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BeginDate] [smalldatetime] NULL,
[EndDate] [smalldatetime] NULL,
[PeriodReturn] [money] NULL,
[PeriodText] [nvarchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EnteredBy] [nvarchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DateTimeEntered] [smalldatetime] NULL,
[DataSource] [nvarchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SourceAccountPerformance_Change_Log] ADD CONSTRAINT [PK_SourceAccountPerformance_Change_Log] PRIMARY KEY CLUSTERED  ([ChangeID]) WITH (FILLFACTOR=80) ON [PRIMARY]
GO

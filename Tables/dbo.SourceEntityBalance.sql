CREATE TABLE [dbo].[SourceEntityBalance]
(
[BalanceID] [bigint] NOT NULL IDENTITY(1, 1),
[EntityID] [bigint] NULL,
[SourceName] [varchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SourceEntityID] [bigint] NULL,
[EntityName] [nvarchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AsOfDate] [smalldatetime] NULL,
[MarketValue] [money] NULL,
[CurrencyCode] [varchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EnteredBy] [nvarchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DateTimeEntered] [smalldatetime] NULL,
[DataSource] [nvarchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE TRIGGER [dbo].[LogSourceEntityBalanceChanges_On_Delete]
   ON [dbo].[SourceEntityBalance]
   AFTER DELETE
AS 
BEGIN
	SET NOCOUNT ON;
	
	IF EXISTS(SELECT * FROM Deleted)
	BEGIN
		INSERT INTO SourceEntityBalance_Change_Log (
			Action,
			ActionSource,
			ActionDateTime,
			ActionBy,
			BalanceID,
			EntityID,
			SourceName,
			SourceEntityID,
			EntityName,
			AsOfDate,
			MarketValue,
			CurrencyCode,
			EnteredBy,
			DateTimeEntered,
			DataSource
			)
		SELECT 
			'DELETE' as Action,
			'' as ActionSource,
			GETUTCDATE() as ActionDateTime,
			'' as ActionBy,
			Deleted.BalanceID,
			Deleted.EntityID,
			Deleted.SourceName,
			Deleted.SourceEntityID,
			Deleted.EntityName,
			Deleted.AsOfDate,
			Deleted.MarketValue,
			Deleted.CurrencyCode,
			Deleted.EnteredBy,
			Deleted.DateTimeEntered,
			Deleted.DataSource
		FROM Deleted
	END

END

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE TRIGGER [dbo].[LogSourceEntityBalanceChanges_On_Update]
   ON [dbo].[SourceEntityBalance]
   AFTER UPDATE
AS 
BEGIN
	SET NOCOUNT ON;
	
	IF EXISTS(SELECT * FROM Deleted)
	BEGIN
		INSERT INTO SourceEntityBalance_Change_Log (
			Action,
			ActionSource,
			ActionDateTime,
			ActionBy,
			BalanceID,
			EntityID,
			SourceName,
			SourceEntityID,
			EntityName,
			AsOfDate,
			MarketValue,
			CurrencyCode,
			EnteredBy,
			DateTimeEntered,
			DataSource
			)
		SELECT 
			'UPDATE' as Action,
			'' as ActionSource,
			GETUTCDATE() as ActionDateTime,
			'' as ActionBy,
			Deleted.BalanceID,
			Deleted.EntityID,
			Deleted.SourceName,
			Deleted.SourceEntityID,
			Deleted.EntityName,
			Deleted.AsOfDate,
			Deleted.MarketValue,
			Deleted.CurrencyCode,
			Deleted.EnteredBy,
			Deleted.DateTimeEntered,
			Deleted.DataSource
		FROM Deleted
	END

END

GO
ALTER TABLE [dbo].[SourceEntityBalance] ADD CONSTRAINT [PK_SourceEntityBalance] PRIMARY KEY CLUSTERED  ([BalanceID]) WITH (FILLFACTOR=80) ON [PRIMARY]
GO

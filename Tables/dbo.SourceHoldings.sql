CREATE TABLE [dbo].[SourceHoldings]
(
[SourceHoldingsID] [bigint] NOT NULL IDENTITY(1, 1),
[AccountSecurityID] [bigint] NULL,
[SourceName] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AsOfDate] [smalldatetime] NULL,
[Quantity] [numeric] (18, 6) NULL,
[SourceSecurityName] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SourceSecuritySymbol] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SourceSecurityCUSIP] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SourceSecurityISIN] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SourceSecuritySedol] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SourceSecurityIdentifier] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CurrencyID_Base] [tinyint] NULL,
[Price_Base] [numeric] (18, 6) NULL,
[MarketValue_Base] [numeric] (18, 6) NULL,
[Cost_Base] [numeric] (18, 6) NULL,
[UnitCost_Base] [numeric] (18, 6) NULL,
[GainLoss_Base] [numeric] (18, 6) NULL,
[DateTimeEntered] [smalldatetime] NULL,
[EnteredBy] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DataSource] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO

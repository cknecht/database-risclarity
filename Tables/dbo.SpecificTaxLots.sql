CREATE TABLE [dbo].[SpecificTaxLots]
(
[SpecificID] [int] NOT NULL IDENTITY(1, 1),
[SaleTransID] [bigint] NULL,
[LotTransID] [bigint] NULL,
[Quantity] [numeric] (18, 6) NULL
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[TaxLotReliefMethod]
(
[TaxLotReliefMethodID] [tinyint] NULL,
[TaxLotReliefMethodLongName] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TaxLotReliefMethodShortName] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO

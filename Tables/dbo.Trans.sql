CREATE TABLE [dbo].[Trans]
(
[TransID] [bigint] NOT NULL IDENTITY(1, 1),
[AccountSecurityID] [bigint] NOT NULL,
[TradeDate] [smalldatetime] NOT NULL,
[SettleDate] [smalldatetime] NULL,
[TransCodeID] [int] NOT NULL,
[Quantity] [numeric] (18, 6) NOT NULL,
[UnitPrice_Base] [numeric] (18, 6) NOT NULL,
[UnitPrice_Local] [numeric] (18, 6) NOT NULL,
[Amount_Base] [numeric] (18, 6) NOT NULL,
[Amount_Local] [numeric] (18, 6) NOT NULL,
[CurrencyID_Base] [int] NOT NULL,
[CurrencyID_Local] [int] NOT NULL,
[DateTimeEntered] [smalldatetime] NULL,
[DataSource] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SourceTransactionCodeName] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SourceName] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SourceTransactionID] [bigint] NULL,
[Notes] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TaxCost_Base] [numeric] (18, 6) NULL,
[TaxCost_Local] [numeric] (18, 6) NULL,
[OriginalDate] [smalldatetime] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[LogTransChanges_On_Delete]
   ON [dbo].[Trans]
   AFTER DELETE
AS 
BEGIN
	SET NOCOUNT ON;
	
	IF EXISTS(SELECT * FROM Deleted)
	BEGIN
		INSERT INTO Trans_Change_Log (
			Action,
			ActionSource,
			ActionDateTime,
			ActionBy,
			TransID,
			AccountSecurityID,
			TradeDate,
			SettleDate,
			TransCodeID,
			Quantity,
			UnitPrice_Base,
			UnitPrice_Local,
			Amount_Base,
			Amount_Local,
			CurrencyID_Base,
			CurrencyID_Local,
			DateTimeEntered,
			DataSource,
			SourceTransactionCodeName,
			SourceName,
			SourceTransactionID,
			Notes
			)
		SELECT 
			'DELETE' as Action,
			'' as ActionSource,
			GETUTCDATE() as ActionDateTime,
			'' as ActionBy,
			Deleted.TransID,
			Deleted.AccountSecurityID,
			Deleted.TradeDate,
			Deleted.SettleDate,
			Deleted.TransCodeID,
			Deleted.Quantity,
			Deleted.UnitPrice_Base,
			Deleted.UnitPrice_Local,
			Deleted.Amount_Base,
			Deleted.Amount_Local,
			Deleted.CurrencyID_Base,
			Deleted.CurrencyID_Local,
			Deleted.DateTimeEntered,
			Deleted.DataSource,
			Deleted.SourceTransactionCodeName,
			Deleted.SourceName,
			Deleted.SourceTransactionID,
			Deleted.Notes
		FROM Deleted
	END

END

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[LogTransChanges_On_Update]
   ON [dbo].[Trans]
   AFTER UPDATE
AS 
BEGIN
	SET NOCOUNT ON;
	
	IF EXISTS(SELECT * FROM Deleted)
	BEGIN
		INSERT INTO Trans_Change_Log (
			Action,
			ActionSource,
			ActionDateTime,
			ActionBy,
			TransID,
			AccountSecurityID,
			TradeDate,
			SettleDate,
			TransCodeID,
			Quantity,
			UnitPrice_Base,
			UnitPrice_Local,
			Amount_Base,
			Amount_Local,
			CurrencyID_Base,
			CurrencyID_Local,
			DateTimeEntered,
			DataSource,
			SourceTransactionCodeName,
			SourceName,
			SourceTransactionID,
			Notes
			)
		SELECT 
			'UPDATE' as Action,
			'' as ActionSource,
			GETUTCDATE() as ActionDateTime,
			'' as ActionBy,
			Deleted.TransID,
			Deleted.AccountSecurityID,
			Deleted.TradeDate,
			Deleted.SettleDate,
			Deleted.TransCodeID,
			Deleted.Quantity,
			Deleted.UnitPrice_Base,
			Deleted.UnitPrice_Local,
			Deleted.Amount_Base,
			Deleted.Amount_Local,
			Deleted.CurrencyID_Base,
			Deleted.CurrencyID_Local,
			Deleted.DateTimeEntered,
			Deleted.DataSource,
			Deleted.SourceTransactionCodeName,
			Deleted.SourceName,
			Deleted.SourceTransactionID,
			Deleted.Notes
		FROM Deleted
	END

END

GO
ALTER TABLE [dbo].[Trans] ADD CONSTRAINT [PK_Trans] PRIMARY KEY CLUSTERED  ([TransID]) WITH (FILLFACTOR=80) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [NonClusteredIndex-20131127-120549] ON [dbo].[Trans] ([AccountSecurityID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [NonClusteredIndex-20130707-164743] ON [dbo].[Trans] ([AccountSecurityID], [TradeDate]) WITH (FILLFACTOR=80) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [NonClusteredIndex-20130707-164447] ON [dbo].[Trans] ([TransCodeID], [AccountSecurityID]) WITH (FILLFACTOR=80) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Trans] ADD CONSTRAINT [FK_Trans_AccountSecurity] FOREIGN KEY ([AccountSecurityID]) REFERENCES [dbo].[AccountSecurity] ([AccountSecurityID]) ON DELETE CASCADE ON UPDATE CASCADE
GO

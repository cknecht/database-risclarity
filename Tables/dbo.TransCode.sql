CREATE TABLE [dbo].[TransCode]
(
[TransCodeID] [int] NOT NULL IDENTITY(0, 1),
[TransCodeName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CalcCashFlow] [int] NOT NULL CONSTRAINT [DF_TransCode_CalcCashFlow] DEFAULT ((0)),
[CalcDividend] [int] NOT NULL CONSTRAINT [DF_TransCode_CalcDividend] DEFAULT ((0)),
[CalcInterest] [int] NOT NULL CONSTRAINT [DF_TransCode_CalcInterest] DEFAULT ((0)),
[CalcFee] [int] NOT NULL CONSTRAINT [DF_TransCode_CalcFee] DEFAULT ((0)),
[CalcDisplay] [int] NOT NULL CONSTRAINT [DF_TransCode_CalcDisplay] DEFAULT ((0)),
[CalcIncome] [int] NOT NULL CONSTRAINT [DF_TransCode_CalcIncome] DEFAULT ((0)),
[CalcCashAsset] [int] NOT NULL CONSTRAINT [DF_TransCode_CalcCashAccount] DEFAULT ((0)),
[CalcQuantity] [int] NOT NULL CONSTRAINT [DF_TransCode_CalcQuantity] DEFAULT ((0)),
[CalcQuantityOfOne] [int] NOT NULL CONSTRAINT [DF_TransCode_CalcQuantityOfOne] DEFAULT ((0)),
[CashTransID] [int] NULL,
[CalcCashFlow_By_Group] [int] NULL CONSTRAINT [DF_TransCode_CalcGroupCashFlow] DEFAULT ((0)),
[CalcNetInvested] [int] NULL,
[CalcCostBasisQuantity] [int] NULL CONSTRAINT [DF_TransCode_CalcChangeCostBasis] DEFAULT ((0)),
[CalcCostBasisAmount] [int] NULL CONSTRAINT [DF_TransCode_CalcChangeCostBasis_1] DEFAULT ((0)),
[CalcShort] [int] NULL CONSTRAINT [DF_TransCode_CalcShort] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[TransCode] ADD CONSTRAINT [PK_TransCode] PRIMARY KEY CLUSTERED  ([TransCodeID]) WITH (FILLFACTOR=80) ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Increase or decrease the default cash asset in the account.', 'SCHEMA', N'dbo', 'TABLE', N'TransCode', 'COLUMN', N'CalcCashAsset'
GO

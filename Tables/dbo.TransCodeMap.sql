CREATE TABLE [dbo].[TransCodeMap]
(
[TransMapID] [int] NOT NULL IDENTITY(1, 1),
[TransCodeSource] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TransCodeSourceName] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TransCodeID] [int] NOT NULL,
[TransSign] [int] NOT NULL,
[TakeAbsoluteValue] [bit] NULL,
[QuantityTransSign] [int] NULL,
[AmountTransSign] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[TransCodeMap] ADD CONSTRAINT [PK_TransCodeMap] PRIMARY KEY CLUSTERED  ([TransMapID]) WITH (FILLFACTOR=80) ON [PRIMARY]
GO
ALTER TABLE [dbo].[TransCodeMap] ADD CONSTRAINT [FK_TransCodeMap_TransCode] FOREIGN KEY ([TransCodeID]) REFERENCES [dbo].[TransCode] ([TransCodeID])
GO

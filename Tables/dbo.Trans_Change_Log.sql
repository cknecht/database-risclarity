CREATE TABLE [dbo].[Trans_Change_Log]
(
[ChangeID] [bigint] NOT NULL IDENTITY(1, 1),
[Action] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ActionSource] [varchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ActionDateTime] [datetime] NULL,
[ActionBy] [varchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TransID] [bigint] NULL,
[AccountSecurityID] [bigint] NULL,
[TradeDate] [smalldatetime] NULL,
[SettleDate] [smalldatetime] NULL,
[TransCodeID] [int] NULL,
[Quantity] [numeric] (18, 6) NOT NULL,
[UnitPrice_Base] [numeric] (18, 6) NULL,
[UnitPrice_Local] [numeric] (18, 6) NULL,
[Amount_Base] [numeric] (18, 6) NULL,
[Amount_Local] [numeric] (18, 6) NULL,
[CurrencyID_Base] [int] NULL,
[CurrencyID_Local] [int] NULL,
[DateTimeEntered] [smalldatetime] NULL,
[DataSource] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SourceTransactionCodeName] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SourceName] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SourceTransactionID] [bigint] NULL,
[Notes] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[Trans_Change_Log] ADD CONSTRAINT [PK_TransChange] PRIMARY KEY CLUSTERED  ([ChangeID]) WITH (FILLFACTOR=80) ON [PRIMARY]
GO

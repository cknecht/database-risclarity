CREATE TABLE [dbo].[User_Login_Log]
(
[LogID] [bigint] NOT NULL IDENTITY(1, 1),
[Username] [varchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[LoginDateTime] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[User_Login_Log] ADD CONSTRAINT [PK_User_Login_Log] PRIMARY KEY CLUSTERED  ([LogID]) WITH (FILLFACTOR=80) ON [PRIMARY]
GO

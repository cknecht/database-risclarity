CREATE TABLE [dbo].[Users]
(
[UserID] [bigint] NOT NULL IDENTITY(1, 1),
[Username] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[emailaddress1] [varchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[emailaddress2] [varchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[emailaddress3] [varchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Firstname] [varchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Lastname] [varchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LastLoginDateTime] [datetime] NULL,
[ColorSchemeID] [int] NULL CONSTRAINT [DF_Users_ColorSchemeID] DEFAULT ((1)),
[Admin] [bit] NULL CONSTRAINT [DF_Users_Admin] DEFAULT ((0)),
[LastLevel] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LastLevelID] [bigint] NULL,
[LastAsOfDate] [smalldatetime] NULL,
[LastReportName] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LastAction] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LastActionLevel] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LastActionLevelID] [bigint] NULL,
[LastBeginDate] [smalldatetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Users] ADD CONSTRAINT [PK_Userd] PRIMARY KEY CLUSTERED  ([UserID]) WITH (FILLFACTOR=80) ON [PRIMARY]
GO

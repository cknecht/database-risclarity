CREATE TABLE [dbo].[UsersAdvisor]
(
[ID] [bigint] NOT NULL IDENTITY(1, 1),
[UserID] [bigint] NOT NULL,
[AdvisorID] [bigint] NOT NULL,
[DateTimeEntered] [datetime] NOT NULL CONSTRAINT [DF_UsersAdvisor_DateTimeEntered] DEFAULT (getutcdate()),
[SelectDefault] [bit] NOT NULL CONSTRAINT [DF_UsersAdvisor_SelectDefault] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[UsersAdvisor] ADD CONSTRAINT [PK_UsersAdvisor] PRIMARY KEY CLUSTERED  ([ID]) WITH (FILLFACTOR=80) ON [PRIMARY]
GO

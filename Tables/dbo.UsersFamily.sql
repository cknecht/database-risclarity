CREATE TABLE [dbo].[UsersFamily]
(
[ID] [bigint] NOT NULL IDENTITY(1, 1),
[UserID] [bigint] NOT NULL,
[FamilyID] [bigint] NOT NULL,
[DateTimeEntered] [datetime] NOT NULL CONSTRAINT [DF_UsersFamily_DateTimeEntered] DEFAULT (getutcdate()),
[SelectDefault] [bit] NOT NULL CONSTRAINT [DF_UsersFamily_SelectDefault] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[UsersFamily] ADD CONSTRAINT [PK_UsersFamily] PRIMARY KEY CLUSTERED  ([ID]) WITH (FILLFACTOR=80) ON [PRIMARY]
GO

CREATE TYPE [dbo].[AccountList] AS TABLE
(
[FamilyID] [bigint] NULL,
[EntityID] [bigint] NULL,
[AccountID] [bigint] NOT NULL,
[AccountName] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AccountNumber] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AccountDescription] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PercentOwnershipBegin] [numeric] (18, 6) NULL,
[PercentOwnership] [numeric] (18, 6) NULL,
[ExcludeFromPerformance] [bit] NULL,
[InceptionDate] [smalldatetime] NULL,
[TaxLotReliefMethodID] [tinyint] NULL,
[BreakoutForPerformance] [bit] NULL,
PRIMARY KEY CLUSTERED  ([AccountID])
)
GO

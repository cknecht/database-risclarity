CREATE TYPE [dbo].[PeriodList] AS TABLE
(
[ID] [bigint] NOT NULL IDENTITY(1, 1),
[BeginDate] [smalldatetime] NOT NULL,
[EndDate] [smalldatetime] NOT NULL
)
GO

CREATE TYPE [dbo].[PriceList] AS TABLE
(
[AsOfDate] [smalldatetime] NOT NULL,
[SecurityID] [bigint] NOT NULL,
[PriceAsOfDate] [smalldatetime] NULL,
[Price] [numeric] (18, 6) NULL,
[Price_CurrencyID] [int] NULL,
PRIMARY KEY CLUSTERED  ([AsOfDate], [SecurityID])
)
GO

CREATE TYPE [dbo].[TWR] AS TABLE
(
[TWR_ID] [bigint] NOT NULL IDENTITY(1, 1),
[PeriodBeginDate] [smalldatetime] NULL,
[PeriodEndDate] [smalldatetime] NULL,
[TWRBeginDate] [smalldatetime] NULL,
[TWREndDate] [smalldatetime] NULL,
[BeginMarketValue] [numeric] (18, 6) NULL,
[EndMarketValue] [numeric] (18, 6) NULL,
[CashFlow] [numeric] (18, 6) NULL,
[Additions] [numeric] (18, 6) NULL,
[Withdrawals] [numeric] (18, 6) NULL,
[Income] [numeric] (18, 6) NULL,
[AccruedIncome] [numeric] (18, 6) NULL,
[Fees] [numeric] (18, 6) NULL,
[PeriodTWR] [numeric] (18, 6) NULL,
[PeriodTWR_Net_Of_Fees] [numeric] (18, 6) NULL,
[PeriodTWR_Plus_One] [numeric] (18, 6) NULL,
[PeriodTWR_Net_Of_Fees_Plus_One] [numeric] (18, 6) NULL,
[TWR] [numeric] (18, 6) NULL,
[AnnualizedTWR] [numeric] (18, 6) NULL,
[TWR_Net_Of_Fees] [numeric] (18, 6) NULL,
[AnnualizedTWR_Net_Of_Fees] [numeric] (18, 6) NULL
)
GO

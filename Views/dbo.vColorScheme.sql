SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[vColorScheme]
AS
SELECT     dbo.ColorScheme.SchemeID, dbo.ColorScheme.SchemeName, dbo.ColorScheme.DefaultScheme, 
                      dbo.ColorReportElementJoin.ColorElementID AS ColorElementNumber, 
                      CASE WHEN ColorReportElementJoin.ColorElementID = 0 THEN '#000000' ELSE ColorSchemeElements.ElementHEX END AS ElementHEX, 
                      CASE WHEN ColorReportElementJoin.ColorElementID = 0 THEN '#FFFFFF' ELSE ColorSchemeElements.FontColor END AS FontColor, 
                      dbo.ColorReportElements.ColorElementName, dbo.ColorReportElements.ColorElementID
FROM         dbo.ColorScheme INNER JOIN
                      dbo.ColorReportElementJoin ON dbo.ColorReportElementJoin.SchemeID = dbo.ColorScheme.SchemeID INNER JOIN
                      dbo.ColorReportElements ON dbo.ColorReportElementJoin.ReportElementID = dbo.ColorReportElements.ColorElementID LEFT OUTER JOIN
                      dbo.ColorSchemeElements AS ColorSchemeElements ON dbo.ColorReportElementJoin.ColorElementID = ColorSchemeElements.ElementNumber AND 
                      dbo.ColorScheme.SchemeID = ColorSchemeElements.SchemeID
GO
EXEC sp_addextendedproperty N'MS_DiagramPane1', N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[41] 4[21] 2[18] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1[50] 2[25] 3) )"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 2
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "ColorScheme"
            Begin Extent = 
               Top = 41
               Left = 93
               Bottom = 134
               Right = 245
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "ColorReportElementJoin"
            Begin Extent = 
               Top = 60
               Left = 288
               Bottom = 168
               Right = 494
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "ColorReportElements"
            Begin Extent = 
               Top = 176
               Left = 536
               Bottom = 254
               Right = 794
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "ColorSchemeElements"
            Begin Extent = 
               Top = 2
               Left = 543
               Bottom = 167
               Right = 804
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 12
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      PaneHidden = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOr', 'SCHEMA', N'dbo', 'VIEW', N'vColorScheme', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_DiagramPane2', N'der = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', 'SCHEMA', N'dbo', 'VIEW', N'vColorScheme', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=2
EXEC sp_addextendedproperty N'MS_DiagramPaneCount', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vColorScheme', NULL, NULL
GO

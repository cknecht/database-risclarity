SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[vTrans]
WITH SCHEMABINDING 
AS
SELECT     Trans.TransID, 
                      CASE WHEN Security.SecurityID = Account.DefaultCashSecurityID THEN dbo.Trans.Amount_Base * TransCode.CalcCashAsset ELSE COALESCE (NULLIF (ISNULL(dbo.Trans.Quantity,
                       0), 0), Trans.Amount_Base / ISNULL(NULLIF (Trans.UnitPrice_Base, 0), 1)) * TransCode.CalcQuantity END AS Quantity, dbo.Trans.UnitPrice_Base, 
                      dbo.Trans.UnitPrice_Local, (dbo.Trans.Amount_Base * TransCode.CalcDisplay) AS Amount_Base, dbo.Trans.Amount_Local * TransCode.CalcDisplay AS Amount_Local,
                       COALESCE (dbo.Trans.TaxCost_Base, dbo.Trans.Amount_Base) * TransCode.CalcDisplay AS TaxCost_Base, COALESCE (dbo.Trans.TaxCost_Local, 
                      dbo.Trans.Amount_Local) * TransCode.CalcDisplay AS TaxCost_Local, dbo.Trans.TradeDate, dbo.Trans.SettleDate, dbo.Trans.CurrencyID_Base, 
                      dbo.Trans.CurrencyID_Local, dbo.TransCode.TransCodeName, dbo.TransCode.TransCodeID, dbo.Entity.EntityID, dbo.Entity.EntityName, dbo.Account.AccountID, 
                      Account.AccountName, AccountSecurity.AccountSecurityID, dbo.Security.SecurityID, dbo.Security.SecurityName, dbo.TransCode.CalcCashFlow, 
                      dbo.TransCode.CalcCashFlow_By_Group, dbo.TransCode.CalcDividend, dbo.TransCode.CalcInterest, dbo.TransCode.CalcFee, dbo.TransCode.CalcDisplay, 
                      dbo.TransCode.CalcIncome, dbo.TransCode.CalcNetInvested, TransCode.CalcShort,
                      CASE WHEN Security.SecurityID = Account.DefaultCashSecurityID THEN TransCode.CalcCashAsset ELSE TransCode.CalcQuantity END AS CalcQuantity, 
                      TransCode.CalcQuantityOfOne, TransCode.CalcCostBasisAmount, TransCode.CalcCostBasisQuantity, Security.QuantityOfOne, Trans.Notes, Trans.DateTimeEntered, 
                      Trans.DataSource
FROM         dbo.Trans INNER JOIN
                      dbo.TransCode ON dbo.Trans.TransCodeID = dbo.TransCode.TransCodeID INNER JOIN
                      dbo.AccountSecurity AS AccountSecurity ON dbo.Trans.AccountSecurityID = AccountSecurity.AccountSecurityID INNER JOIN
                      dbo.EntityAccount ON AccountSecurity.AccountID = dbo.EntityAccount.AccountID INNER JOIN
                      dbo.Entity ON dbo.EntityAccount.EntityID = dbo.Entity.EntityID INNER JOIN
                      dbo.Account ON AccountSecurity.AccountID = dbo.Account.AccountID INNER JOIN
                      dbo.Security ON AccountSecurity.SecurityID = dbo.Security.SecurityID
UNION ALL
SELECT     0 AS TransID, (Trans.Amount_Base * CashTransCode.CalcCashAsset) AS Quantity, 1 AS UnitPrice_Base, 1 AS UnitPrice_Local, 
                      Trans.Amount_Base * CashTransCode.CalcCashAsset AS Amount_Base, Trans.Amount_Local * CashTransCode.CalcCashAsset AS Amount_Local, 
                      Trans.Amount_Base * CashTransCode.CalcCashAsset AS TaxCost_Base, Trans.Amount_Local * CashTransCode.CalcCashAsset AS TaxCost_Local, Trans.TradeDate, 
                      Trans.SettleDate, Trans.CurrencyID_Base, Trans.CurrencyID_Local, 
                      /*CASE TransCode.CalcCashAsset WHEN 1 THEN 'Increase' WHEN - 1 THEN 'Decrease' END AS TransCodeName, */ CashTransCode.TransCodeName AS TransCodeName,
                       CashTransCode.TransCodeID AS TransCodeID, Entity.EntityID, Entity.EntityName, Account.AccountID, Account.AccountName, 
                      CashAccountSecurity.AccountSecurityID AS AccountSecurityID, CashSecurity.SecurityID, CashSecurity.SecurityName, TransCode.CalcCashFlow * - 1, 
                      CashTransCode.CalcCashFlow_By_Group, 0 AS CalcDividend, 0 AS CalcInterest, 0 AS CalcFee, TransCode.CalcDisplay * - 1, 0 AS CalcIncome, 0 AS CalcNetInvested, 0 as CalcShort,
                      CashTransCode.CalcCashAsset AS CalcQuantity, 0 AS CalcQuantityOfOne, TransCode.CalcCostBasisAmount, TransCode.CalcCostBasisQuantity, 
                      CashSecurity.QuantityOfOne, Trans.Notes, Trans.DateTimeEntered, Trans.DataSource
FROM         dbo.Trans JOIN
                      dbo.TransCode ON Trans.TransCodeID = TransCode.TransCodeID JOIN
                      dbo.TransCode AS CashTransCode ON Transcode.CashTransID = CashTransCode.TransCodeID JOIN
                      dbo.AccountSecurity ON Trans.AccountSecurityID = AccountSecurity.AccountSecurityID JOIN
                      dbo.Security ON AccountSecurity.SecurityID = Security.SecurityID JOIN
                      dbo.EntityAccount ON AccountSecurity.AccountID = EntityAccount.AccountID JOIN
                      dbo.Entity ON EntityAccount.EntityID = Entity.EntityID JOIN
                      dbo.Account ON AccountSecurity.AccountID = Account.AccountID JOIN
                      dbo.Security AS CashSecurity ON Account.DefaultCashSecurityID = CashSecurity.SecurityID AND Security.SecurityID <> CashSecurity.SecurityID JOIN
                      dbo.AccountSecurity AS CashAccountSecurity ON CashSecurity.SecurityID = CashAccountSecurity.SecurityID AND 
                      Account.AccountID = CashAccountSecurity.AccountID
WHERE     TransCode.CalcCashAsset <> 0 /*AND ISNULL(Account.NoCashAsset, 0) <> 1*/ AND ISNULL(Account.DefaultCashSecurityID, 0) > 0
UNION ALL
SELECT     0 AS TransID, SUM(Trans.Quantity * TransCode.CalcQuantity) * (SecurityStockSplit.Factor - 1) AS Quantity, 0 AS UnitPrice_Base, 0 AS UnitPrice_Local, 
                      0 AS Amount_Base, 0 AS Amount_Local, 0 AS TaxCost_Base, 0 AS TaxCost_Local, SecurityStockSplit.ExDate AS TradeDate, SecurityStockSplit.ExDate AS SettleDate, 
                      Trans.CurrencyID_Base, Trans.CurrencyID_Local, TransCode.TransCodeName AS TransCodeName, SecurityStockSplit.TransCodeID AS TransCodeID, Entity.EntityID, 
                      Entity.EntityName, Account.AccountID, Account.AccountName, SplitAccountSecurity.AccountSecurityID AS AccountSecurityID, SplitSecurity.SecurityID AS SecurityID, 
                      SplitSecurity.SecurityName AS SecurityName, TransCode.CalcCashFlow * - 1, TransCode.CalcCashFlow_By_Group, TransCode.CalcDividend, TransCode.CalcInterest, 
                      TransCode.CalcFee, TransCode.CalcDisplay, TransCode.CalcIncome AS CalcIncome, ISNULL(TransCode.CalcNetInvested, 0), TransCode.CalcShort as CalcShort,
                      TransCode.CalcCashAsset AS CalcQuantity, TransCode.CalcQuantityOfOne, TransCode.CalcCostBasisAmount, TransCode.CalcCostBasisQuantity, 
                      SplitSecurity.QuantityOfOne, SecurityStockSplit.Notes, Trans.DateTimeEntered, Trans.DataSource
FROM         dbo.Trans JOIN
                      dbo.AccountSecurity ON Trans.AccountSecurityID = AccountSecurity.AccountSecurityID JOIN
                      dbo.Security ON AccountSecurity.SecurityID = Security.SecurityID JOIN
                      dbo.EntityAccount ON AccountSecurity.AccountID = EntityAccount.AccountID JOIN
                      dbo.Entity ON EntityAccount.EntityID = Entity.EntityID JOIN
                      dbo.Account ON AccountSecurity.AccountID = Account.AccountID JOIN
                      dbo.SecurityStockSplit ON Security.SecurityID = SecurityStockSplit.SecurityID AND SecurityStockSplit.RecordDate >= Trans.TradeDate JOIN
                      dbo.Security AS SplitSecurity ON COALESCE (SecurityStockSplit.NewSecurityID, SecurityStockSplit.SecurityID) = SplitSecurity.SecurityID JOIN
                      dbo.AccountSecurity AS SplitAccountSecurity ON SplitSecurity.SecurityID = SplitAccountSecurity.SecurityID AND 
                      SplitAccountSecurity.AccountID = Account.AccountID JOIN
                      dbo.TransCode ON SecurityStockSplit.TransCodeID = TransCode.TransCodeID
WHERE     Trans.TradeDate <= SecurityStockSplit.RecordDate
GROUP BY SecurityStockSplit.ExDate, SecurityStockSplit.ExDate, Trans.CurrencyID_Base, Trans.CurrencyID_Local, TransCode.TransCodeName, 
                      SecurityStockSplit.TransCodeID, Entity.EntityID, Entity.EntityName, Account.AccountID, Account.AccountName, /*Trans.AccountSecurityID, */ SplitSecurity.SecurityID, 
                      SplitSecurity.SecurityName, TransCode.CalcCashFlow, TransCode.CalcCashFlow_By_Group, TransCode.CalcDividend, TransCode.CalcInterest, TransCode.CalcFee, 
                      TransCode.CalcDisplay, TransCode.CalcIncome, TransCode.CalcNetInvested, TransCode.CalcCashAsset, TransCode.CalcQuantityOfOne, 
                      TransCode.CalcCostBasisAmount, TransCode.CalcCostBasisQuantity, SplitSecurity.QuantityOfOne, SecurityStockSplit.Notes, SecurityStockSplit.Factor, 
                      SplitAccountSecurity.AccountSecurityID, Trans.DateTimeEntered, Trans.DataSource, TransCode.CalcShort
GO
EXEC sp_addextendedproperty N'MS_DiagramPane1', N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4[30] 2[40] 3) )"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2[66] 3) )"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2) )"
      End
      ActivePaneConfig = 5
   End
   Begin DiagramPane = 
      PaneHidden = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 39
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      PaneHidden = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', 'SCHEMA', N'dbo', 'VIEW', N'vTrans', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=1
EXEC sp_addextendedproperty N'MS_DiagramPaneCount', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vTrans', NULL, NULL
GO
